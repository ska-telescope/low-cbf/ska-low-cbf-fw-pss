### Changelog
## PSS Personality

* 0.1.0
    * AA1 release
    * To be used with Processor v0.15.1
* 0.0.4 (not released)
    * Timing optimisations and Daisy chain set to 2, effective 2 pipelines of 32 beamformers.
    * SPS input support for 100G burst traffic, 4096 byte transfers to HBM.
    * Update PSN handling at start of a scan and don't transmit packets until Beam 0, Freq 0 passes to the packetiser.
    * HBM reset mechanism added to CT1 and CT2 interfaces.
    * Scaling updates.
* 0.0.3
    * Update HBM arrangement for SPS input to 2GB buffer for U55
        * New Mapping - 2Gi 1Gi 2Gi 1Gi 2Gi 1Gi
* 0.0.2
    * Signal path established
* 0.0.1
    * Initial bringup