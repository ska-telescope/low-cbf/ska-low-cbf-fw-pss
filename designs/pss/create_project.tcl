set time_raw [clock seconds];
set date_string [clock format $time_raw -format "%y%m%d_%H%M%S"]

set proj_dir "$env(RADIOHDL)/build/$env(PERSONALITY)/$env(PERSONALITY)_$env(TARGET_ALVEO)_build_$date_string"
set ARGS_PATH "$env(RADIOHDL)/build/ARGS/pss"
set BOARD_PATH "$env(RADIOHDL)/designs/libraries/board"
set DESIGN_PATH "$env(RADIOHDL)/designs/pss"
set RLIBRARIES_PATH "$env(RADIOHDL)/libraries"
set COMMON_PATH "$env(RADIOHDL)/common/libraries"
set BUILD_PATH "$env(RADIOHDL)/build"
set DEVICE "xcu55c-fsvh2892-2L-e"
set BOARD "xilinx.com:au55c:part0:1.0"

puts "RADIOHDL directory:"
puts $env(RADIOHDL)

puts "Timeslave IP in submodule"
# RADIOHDL is ENV_VAR for current project REPO. 
set timeslave_repo "$env(RADIOHDL)/pub-timeslave/hw/cores"

# Create the new build directory
puts "Creating build_directory $proj_dir"
file mkdir $proj_dir

# This script sets the project variables
puts "Creating new project: pss"
cd $proj_dir

set workingDir [pwd]
puts "Working directory:"
puts $workingDir

# WARNING - proj_dir must be relative to workingDir.
# But cannot be empty because args generates tcl with the directory specified as "$proj_dir/"
set proj_dir "../$env(PERSONALITY)_$env(TARGET_ALVEO)_build_$date_string"

create_project $env(PERSONALITY) -part $DEVICE -force
set_property board_part $BOARD [current_project]
set_property target_language VHDL [current_project]
set_property target_simulator XSim [current_project]

############################################################
# Board specific files
############################################################

############################################################
# Timeslave files
############################################################
set_property  ip_repo_paths  $timeslave_repo [current_project]
update_ip_catalog

  # generate Timeslave BD - Instance 1 - U55C TOP PORT.
  # based on Vitis version.
  if { $env(VITIS_VERSION) == "2021.2" } {
    source $COMMON_PATH/ptp/src/genBD_timeslave.tcl
  } else {
    # 2022.2
    source $COMMON_PATH/ptp/src/ts_$env(VITIS_VERSION).tcl
  }

make_wrapper -files [get_files $workingDir/$env(PERSONALITY).srcs/sources_1/bd/ts/ts.bd] -top
add_files -norecurse $workingDir/$env(PERSONALITY).gen/sources_1/bd/ts/hdl/ts_wrapper.vhd

add_files -fileset sources_1 [glob \
 $COMMON_PATH/ptp/src/CMAC_100G_wrap_w_timeslave.vhd \
 $COMMON_PATH/ptp/src/timeslave_scheduler.vhd \
 $COMMON_PATH/ptp/src/timer_pkg.vhd \
 $COMMON_PATH/ptp/src/sub_second_timer.vhd \
]
set_property library Timeslave_CMAC_lib [get_files {\
 */src/CMAC_100G_wrap_w_timeslave.vhd \
 */src/timeslave_scheduler.vhd \
 */src/timer_pkg.vhd \
 */src/sub_second_timer.vhd \
}]

set_property file_type {VHDL 2008} [get_files  $COMMON_PATH/ptp/src/sub_second_timer.vhd]

add_files -fileset sources_1 [glob \
 $ARGS_PATH/CMAC/cmac/CMAC_cmac_reg_pkg.vhd \
 $ARGS_PATH/CMAC/cmac/CMAC_cmac_reg.vhd \
 $ARGS_PATH/Timeslave/timeslave/Timeslave_timeslave_reg_pkg.vhd \
 $ARGS_PATH/Timeslave/timeslave/Timeslave_timeslave_reg.vhd \
]
set_property library Timeslave_CMAC_lib [get_files {\
 *CMAC/cmac/CMAC_cmac_reg_pkg.vhd \
 *CMAC/cmac/CMAC_cmac_reg.vhd \
 */Timeslave/timeslave/Timeslave_timeslave_reg_pkg.vhd \
 */Timeslave/timeslave/Timeslave_timeslave_reg.vhd \ 
}]

############################################################
# ARGS generated files
############################################################

# This script uses the construct $workingDir/$proj_dir
# So $proj_dir must be relative to $workingDir
# 
source $ARGS_PATH/pss_bd.tcl

add_files -fileset sources_1 [glob \
$ARGS_PATH/pss_bus_pkg.vhd \
$ARGS_PATH/pss_bus_top.vhd \
$ARGS_PATH/pss/system/pss_system_reg_pkg.vhd \
$ARGS_PATH/pss/system/pss_system_reg.vhd \
]
set_property library pss_lib [get_files {\
*build/ARGS/pss/pss_bus_pkg.vhd \
*build/ARGS/pss/pss_bus_top.vhd \
*build/ARGS/pss/pss/system/pss_system_reg_pkg.vhd \
*build/ARGS/pss/pss/system/pss_system_reg.vhd \
}]

############################################################
# Design specific files
############################################################

add_files -fileset sources_1 [glob \
$DESIGN_PATH/src/vhdl/pss.vhd \
$DESIGN_PATH/src/vhdl/PSSBeamformCore.vhd \
$DESIGN_PATH/src/vhdl/cdma_wrapper.vhd \
$DESIGN_PATH/src/vhdl/krnl_control_axi.vhd \
$DESIGN_PATH/src/vhdl/version_pkg.vhd \
$COMMON_PATH/hbm_axi_reset_handler/hbm_axi_reset_handler.vhd \
$BUILD_PATH/build_details_pkg.vhd \
]

#add_files -fileset sim_1 [glob \
#$DESIGN_PATH/src/vhdl/tb_pst.vhd \
#$DESIGN_PATH/src/vhdl/lbus_packet_receive.vhd \
#$COMMON_PATH/sim/s_axi_to_txt.vhd \
#$COMMON_PATH/sim/txt_to_s_axi.vhd \
#]

add_files -fileset sim_1 [glob \
$DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd \
]
set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd]

set_property library pss_lib [get_files {\
*/designs/pss/src/vhdl/HBM_axi_tbModel.vhd \
}]


set_property library pss_lib [get_files {\
*/designs/pss/src/vhdl/pss.vhd \
*/designs/pss/src/vhdl/PSSBeamformCore.vhd \
*/designs/pss/src/vhdl/cdma_wrapper.vhd \
*/designs/pss/src/vhdl/krnl_control_axi.vhd \
*hbm_axi_reset_handler/hbm_axi_reset_handler.vhd \
*/build_details_pkg.vhd \
}]

set_property library version_lib [get_files {\
*/designs/pss/src/vhdl/version_pkg.vhd \
}]

#*/designs/pst/src/vhdl/tb_pst.vhd \
#*/designs/pst/src/vhdl/lbus_packet_receive.vhd \
#*/sim/s_axi_to_txt.vhd \
#*/sim/txt_to_s_axi.vhd \

set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/pss.vhd]
set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/PSSBeamformCore.vhd]
#set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/tb_pst.vhd]

# top level testbench
#set_property top tb_pst [get_filesets sim_1]

#add_files -fileset constrs_1 [ glob $DESIGN_PATH/vivado/vcu128_gemini_dsp.xdc ]

# vivado_xci_files: Importing IP to the project
# tcl scripts for ip generation
source $DESIGN_PATH/src/ip/pss.tcl
############################################################
# AXI4

add_files -fileset sources_1 [glob \
$COMMON_PATH/base/axi4/src/vhdl/axi4_lite_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/axi4_full_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/axi4_stream_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/mem_to_axi4_lite.vhd \
]
set_property library axi4_lib [get_files {\
*libraries/base/axi4/src/vhdl/axi4_lite_pkg.vhd \
*libraries/base/axi4/src/vhdl/axi4_full_pkg.vhd \
*libraries/base/axi4/src/vhdl/axi4_stream_pkg.vhd \
*libraries/base/axi4/src/vhdl/mem_to_axi4_lite.vhd \
}]

# Technology select package
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/technology/technology_pkg.vhd \
 $RLIBRARIES_PATH/technology/technology_select_pkg.vhd \
 $RLIBRARIES_PATH/technology/mac_100g/tech_mac_100g_pkg.vhd \
]
set_property library technology_lib [get_files {\
 *libraries/technology/technology_pkg.vhd \
 *libraries/technology/technology_select_pkg.vhd \
 *libraries/technology/mac_100g/tech_mac_100g_pkg.vhd \
}]
#############################################################
# Common

add_files -fileset sources_1 [glob \
 $COMMON_PATH/base/common/src/vhdl/common_reg_r_w.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_str_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_mem_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_field_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_lfsr_sequences_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_interface_layers_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_network_layers_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_network_total_header_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_components_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_spulse.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_switch.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_delay.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_ram_crw_crw.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_pipeline.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_accumulate.vhd \
]
set_property library common_lib [get_files {\
 *libraries/base/common/src/vhdl/common_reg_r_w.vhd \
 *libraries/base/common/src/vhdl/common_pkg.vhd \
 *libraries/base/common/src/vhdl/common_str_pkg.vhd \
 *libraries/base/common/src/vhdl/common_mem_pkg.vhd \
 *libraries/base/common/src/vhdl/common_field_pkg.vhd \
 *libraries/base/common/src/vhdl/common_lfsr_sequences_pkg.vhd \
 *libraries/base/common/src/vhdl/common_interface_layers_pkg.vhd \
 *libraries/base/common/src/vhdl/common_network_layers_pkg.vhd \
 *libraries/base/common/src/vhdl/common_network_total_header_pkg.vhd \
 *libraries/base/common/src/vhdl/common_components_pkg.vhd \
 *libraries/base/common/src/vhdl/common_spulse.vhd \
 *libraries/base/common/src/vhdl/common_switch.vhd \
 *libraries/base/common/src/vhdl/common_delay.vhd \
 *libraries/base/common/src/vhdl/common_ram_crw_crw.vhd \
 *libraries/base/common/src/vhdl/common_pipeline.vhd \
 *libraries/base/common/src/vhdl/common_accumulate.vhd \
}]

#############################################################
# tech memory
# (Used by ARGs)
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/technology/memory/tech_memory_component_pkg.vhd \
 $RLIBRARIES_PATH/technology/memory/tech_memory_ram_cr_cw.vhd \
 $RLIBRARIES_PATH/technology/memory/tech_memory_ram_crw_crw.vhd \
]
set_property library tech_memory_lib [get_files {\
 *libraries/technology/memory/tech_memory_component_pkg.vhd \
 *libraries/technology/memory/tech_memory_ram_cr_cw.vhd \
 *libraries/technology/memory/tech_memory_ram_crw_crw.vhd \
}]

#############################################################
# 100G LFAA decode

source $COMMON_PATH/LFAA_decode_100G/LFAADecode.tcl

add_files -fileset sources_1 [glob \
 $ARGS_PATH/LFAADecode100G/lfaadecode100g/LFAADecode100G_lfaadecode100g_reg_pkg.vhd \
 $ARGS_PATH/LFAADecode100G/lfaadecode100g/LFAADecode100G_lfaadecode100g_reg.vhd \
 $COMMON_PATH/LFAA_decode_100G/src/vhdl/LFAADecodeTop100G.vhd \
 $COMMON_PATH/LFAA_decode_100G/src/vhdl/LFAAProcess100G.vhd \
 $COMMON_PATH/LFAA_decode_100G/src/vhdl/LFAA_decode_axi_bram_wrapper.vhd \
]
set_property library LFAADecode100G_lib [get_files {\
 *LFAADecode100G/lfaadecode100g/LFAADecode100G_lfaadecode100g_reg_pkg.vhd \
 *LFAADecode100G/lfaadecode100g/LFAADecode100G_lfaadecode100g_reg.vhd \
 *LFAA_decode_100G/src/vhdl/LFAADecodeTop100G.vhd \
 *LFAA_decode_100G/src/vhdl/LFAAProcess100G.vhd \
 *LFAA_decode_100G/src/vhdl/LFAA_decode_axi_bram_wrapper.vhd \
}]


#############################################################
# input Corner Turn (ct1)

add_files -fileset sources_1 [glob \
  $ARGS_PATH/pss_ct1/pss_ct1/pss_ct1_reg_pkg.vhd \
  $ARGS_PATH/pss_ct1/pss_ct1/pss_ct1_reg.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_top.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_readout_32bit.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_valid.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_readout.vhd \
  $RLIBRARIES_PATH/ct1/pss_div96.vhd \
  $RLIBRARIES_PATH/ct1/poly_axi_bram_wrapper.vhd \
  $RLIBRARIES_PATH/ct1/poly_eval.vhd \
]

set_property library ct_lib [get_files {\
  *build/ARGS/pss/pss_ct1/pss_ct1/pss_ct1_reg_pkg.vhd \
  *build/ARGS/pss/pss_ct1/pss_ct1/pss_ct1_reg.vhd \
  *libraries/ct1/pss_ct1_top.vhd \
  *libraries/ct1/pss_ct1_readout_32bit.vhd \
  *libraries/ct1/pss_ct1_valid.vhd \
  *libraries/ct1/pss_ct1_readout.vhd \
  *libraries/ct1/pss_div96.vhd \
  *libraries/ct1/poly_axi_bram_wrapper.vhd \
  *libraries/ct1/poly_eval.vhd \
 }]

add_files -fileset sim_1 [glob \
  $RLIBRARIES_PATH/ct1/pss_ct1_tb.vhd \
]

#  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_tb.vhd \ 

source $RLIBRARIES_PATH/ct1/pss_ct1.tcl
source $RLIBRARIES_PATH/ct1/pss_ct1_ct2_common.tcl


#############################################################
# Filterbank

add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/filterbank/vhdl/fb_DSP.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFFTwrapper.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFBmem_4.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFIRTaps.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFBTop_4.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/FB_wrapper.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/ShiftandRound_16bit.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/fineDelay.vhd \
]

set_property library filterbank_lib [get_files {\
 *libraries/filterbank/vhdl/fb_DSP.vhd \
 *libraries/filterbank/vhdl/PSSFFTwrapper.vhd \
 *libraries/filterbank/vhdl/PSSFBmem_4.vhd \
 *libraries/filterbank/vhdl/PSSFIRTaps.vhd \
 *libraries/filterbank/vhdl/PSSFBTop_4.vhd \
 *libraries/filterbank/vhdl/FB_wrapper.vhd \
 *libraries/filterbank/vhdl/ShiftandRound_16bit.vhd \
 *libraries/filterbank/vhdl/fineDelay.vhd \
}] 

source $RLIBRARIES_PATH/filterbank/ip/dspAxB.tcl
source $RLIBRARIES_PATH/filterbank/ip/fineDelay.tcl
source $RLIBRARIES_PATH/filterbank/ip/PSSFB_FFT.tcl

################################################################
# Output corner turn (filterbank output - beamformer input)
add_files -fileset sources_1 [glob \
  $ARGS_PATH/ct2/ct2/ct2_reg_pkg.vhd \
  $ARGS_PATH/ct2/ct2/ct2_reg.vhd \
  $RLIBRARIES_PATH/ct2/ct2_wrapper.vhd \
  $RLIBRARIES_PATH/ct2/ct2_poly_mem.vhd \
  $RLIBRARIES_PATH/ct2/ct2_poly_time.vhd \
  $RLIBRARIES_PATH/ct2/ct2_poly_eval.vhd \
  $RLIBRARIES_PATH/ct2/ct2_out.vhd \
  $RLIBRARIES_PATH/ct2/ct2_buffer_select.vhd \
  $RLIBRARIES_PATH/ct2/ct2_buf_check.vhd \
]

set_property library ct_lib [get_files {\
 *build/ARGS/pss/ct2/ct2/ct2_reg_pkg.vhd \
 *build/ARGS/pss/ct2/ct2/ct2_reg.vhd \
 *libraries/ct2/ct2_wrapper.vhd \
 *libraries/ct2/ct2_poly_mem.vhd \
 *libraries/ct2/ct2_buffer_select.vhd \
 *libraries/ct2/ct2_buf_check.vhd \
 *libraries/ct2/ct2_poly_time.vhd \
 *libraries/ct2/ct2_poly_eval.vhd \
 *libraries/ct2/ct2_out.vhd \
}]

add_files -fileset sim_1 [glob \
  $RLIBRARIES_PATH/ct1/pss_ct1_tb.vhd \
  $RLIBRARIES_PATH/ct1/file2_axifull.vhd \
]
#set_property file_type {VHDL 2008} [get_files  $RLIBRARIES_PATH/signalProcessing/corner_turner/CT_ATOMIC_PST_OUT/axi_4to1.vhd]
#set_property file_type {VHDL 2008} [get_files  $RLIBRARIES_PATH/signalProcessing/corner_turner/CT_ATOMIC_PST_OUT/ct_atomic_pst_out.vhd]

source $RLIBRARIES_PATH/ct2/ct2_ip.tcl

##############################################################
## Beamjones and Output Scaling
add_files -fileset sources_1 [glob \
  $RLIBRARIES_PATH/output_scaling/output_scaling.vhd \
  $RLIBRARIES_PATH/output_scaling/pad_32bit.vhd \
  $RLIBRARIES_PATH/output_scaling/scaling_pkg.vhd \
  $RLIBRARIES_PATH/output_scaling/rms.vhd \
]

set_property library scaling_lib [get_files {\
 *libraries/output_scaling/output_scaling.vhd \
 *libraries/output_scaling/pad_32bit.vhd \
 *libraries/output_scaling/scaling_pkg.vhd \
 *libraries/output_scaling/rms.vhd \ 
}]

source $RLIBRARIES_PATH/output_scaling/scaling.tcl


##############################################################
## Beamformer
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/beamformer/PSSbeamformerTop.vhd \
 $RLIBRARIES_PATH/beamformer/PSSbeamformer.vhd \
 $RLIBRARIES_PATH/beamformer/PSS_applyscale.vhd \
 $RLIBRARIES_PATH/beamformer/PSS_scalefactor.vhd \
 $RLIBRARIES_PATH/beamformer/PSSbeamTrigger.vhd \
 $RLIBRARIES_PATH/stationjones/jonesMatrixMult_16b.vhd \
 $RLIBRARIES_PATH/stationjones/divide16_2cycle.vhd \
 $RLIBRARIES_PATH/stationjones/axi_full_sj.vhd \
 $RLIBRARIES_PATH/stationjones/station_jones.vhd \
 $RLIBRARIES_PATH/beamjones/beam_jones.vhd \
 $RLIBRARIES_PATH/beamjones/jonesMatrixMult_scale_16b.vhd \
 $RLIBRARIES_PATH/beamjones/bjmem.vhd \
]

set_property library bf_lib [get_files {\
 *libraries/beamformer/PSSbeamformerTop.vhd \
 *libraries/beamformer/PSSbeamformer.vhd \
 *libraries/beamformer/PSS_applyscale.vhd \
 *libraries/beamformer/PSS_scalefactor.vhd \
 *libraries/beamformer/PSSbeamTrigger.vhd \
 *libraries/stationjones/jonesMatrixMult_16b.vhd \
 *libraries/stationjones/axi_full_sj.vhd \
 *libraries/stationjones/divide16_2cycle.vhd \
 *libraries/stationjones/station_jones.vhd \
 *libraries/beamjones/beam_jones.vhd \
 *libraries/beamjones/jonesMatrixMult_scale_16b.vhd \
 *libraries/beamjones/bjmem.vhd \
}]

#add_files -fileset sim_1 [glob \
#  $RLIBRARIES_PATH/../designs/pst/src/vhdl/pst_ct2_beamformer_tb.vhd \
#]

source $RLIBRARIES_PATH/beamformer/PSSbeamformer.tcl
source $RLIBRARIES_PATH/stationjones/stationjones.tcl
source $RLIBRARIES_PATH/beamjones/beamjones.tcl

##############################################################
## PSR Packetiser
add_files -fileset sources_1 [glob \
$ARGS_PATH/Packetiser/packetiser/Packetiser_packetiser_reg_pkg.vhd \
$ARGS_PATH/Packetiser/packetiser/Packetiser_packetiser_reg.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/cbfpsrheader_pkg.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/adder_32_int.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/packet_former.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/packetiser100G_Top.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/packet_player.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/test_packet_data_gen.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/stream_config_wrapper.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/cmac_args_axi_wrapper.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/packet_length_check.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/packetiser_wrapper.vhd \
$COMMON_PATH/Packetiser100G/src/vhdl/pss_payloader.vhd \
]
set_property library PSR_Packetiser_lib [get_files {\
*Packetiser/packetiser/Packetiser_packetiser_reg_pkg.vhd \
*Packetiser/packetiser/Packetiser_packetiser_reg.vhd \
*Packetiser100G/src/vhdl/cbfpsrheader_pkg.vhd \
*Packetiser100G/src/vhdl/adder_32_int.vhd \
*Packetiser100G/src/vhdl/packet_former.vhd \
*Packetiser100G/src/vhdl/packetiser100G_Top.vhd \
*Packetiser100G/src/vhdl/packet_player.vhd \
*Packetiser100G/src/vhdl/test_packet_data_gen.vhd \
*Packetiser100G/src/vhdl/stream_config_wrapper.vhd \
*Packetiser100G/src/vhdl/cmac_args_axi_wrapper.vhd \
*Packetiser100G/src/vhdl/packet_length_check.vhd \
*Packetiser100G/src/vhdl/packetiser_wrapper.vhd \
*Packetiser100G/src/vhdl/pss_payloader.vhd \
}]

add_files -fileset sources_1 [glob \
$COMMON_PATH/Packetiser100G/src/vhdl/vc_lower_preload.mem \
$COMMON_PATH/Packetiser100G/src/vhdl/vc_upper_preload.mem \
$COMMON_PATH/Packetiser100G/src/vhdl/pst_default.mem \
$COMMON_PATH/Packetiser100G/src/vhdl/beam_default.mem \
]

set_property file_type {VHDL 2008} [get_files  *Packetiser100G/src/vhdl/cbfpsrheader_pkg.vhd]
set_property file_type {VHDL 2008} [get_files  *Packetiser100G/src/vhdl/test_packet_data_gen.vhd]

## tcl scripts for ip generation
source $COMMON_PATH/Packetiser100G/src/vhdl/packetiser100G.tcl

###############################################################
## setup for SPS SPEAD
add_files -fileset sources_1 [glob \
$COMMON_PATH/spead_sps/src/spead_sps_packet_pkg.vhd \
]
set_property library spead_sps_lib [get_files {\
*libraries/spead_sps/src/spead_sps_packet_pkg.vhd \
}]

##############################################################
## Signal_processing_common
add_files -fileset sources_1 [glob \
$COMMON_PATH/common/src/vhdl/sync.vhd \
$COMMON_PATH/common/src/vhdl/sync_vector.vhd \
$COMMON_PATH/common/src/vhdl/memory_tdp_wrapper.vhd \
$COMMON_PATH/common/src/vhdl/xpm_fifo_wrapper.vhd \
$COMMON_PATH/common/src/vhdl/xpm_sync_fifo_wrapper.vhd \
$COMMON_PATH/ethernet/src/vhdl/ethernet_pkg.vhd \
$COMMON_PATH/ethernet/src/vhdl/ipv4_chksum.vhd \
]

set_property library signal_processing_common [get_files {\
*/common/src/vhdl/sync.vhd \
*/common/src/vhdl/sync_vector.vhd \
*/common/src/vhdl/memory_tdp_wrapper.vhd \
*/common/src/vhdl/xpm_fifo_wrapper.vhd \
*/common/src/vhdl/xpm_sync_fifo_wrapper.vhd \
}]

set_property library ethernet_lib [get_files {\
*ethernet/src/vhdl/ethernet_pkg.vhd \
*ethernet/src/vhdl/ipv4_chksum.vhd \
}]


### tcl scripts for ip generation
##source $ARGS_PATH/Packetiser/packetiser/ip_Packetiser_packetiser_param_ram.tcl

##############################################################
## signal processing Top level

add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/dsp_top/DSP_top_BF.vhd \
 $RLIBRARIES_PATH/dsp_top/DSP_top_pkg.vhd \
]
set_property library DSP_top_lib [get_files  {\
 *libraries/dsp_top/DSP_top_BF.vhd \
 *libraries/dsp_top/DSP_top_pkg.vhd \
}]

set_property file_type {VHDL 2008} [get_files  *libraries/dsp_top/DSP_top_BF.vhd]

###############################################################
## Set top
add_files -fileset constrs_1 -norecurse $DESIGN_PATH/src/constraints/pss_constraints.xdc
set_property PROCESSING_ORDER LATE [get_files pss_constraints.xdc]

#set_property top_lib xil_defaultlib [get_filesets sim_1]

set_property top pss [current_fileset]
update_compile_order -fileset sources_1

###############################################################
## Create sim set for scaling logic

create_fileset -simset sim_scaling

set_property SOURCE_SET sources_1 [get_filesets sim_scaling]

add_files -fileset sim_scaling [glob \
  $RLIBRARIES_PATH/output_scaling/output_scaling.vhd \
  $RLIBRARIES_PATH/output_scaling/pad_32bit.vhd \
  $RLIBRARIES_PATH/output_scaling/tb/tb_scaling.vhd \
  $RLIBRARIES_PATH/output_scaling/tb/tb_scaling_behav.wcfg \
  $COMMON_PATH/base/common/src/vhdl/common_pkg.vhd \
]

set_property library scaling_lib [get_files {\
 *libraries/output_scaling/output_scaling.vhd \
 *libraries/output_scaling/pad_32bit.vhd \
}]

set_property library common_lib [get_files {\
 */base/common/src/vhdl/common_pkg.vhd \
}]

set_property file_type {VHDL 2008} [get_files  */libraries/output_scaling/tb/tb_scaling.vhd]

set_property top tb_scaling [get_filesets sim_scaling]
update_compile_order -fileset sim_scaling

###############################################################
## Create sim set for BF + scaling + packetiser

create_fileset -simset sim_bf_packet

set_property SOURCE_SET sources_1 [get_filesets sim_bf_packet]

add_files -fileset sim_bf_packet [glob \
  $RLIBRARIES_PATH/ct2/pss_ct2_beamformer_tb.vhd \
  $RLIBRARIES_PATH/ct2/file2_axifull.vhd \
  $DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd \
  $COMMON_PATH/base/common/src/vhdl/common_pkg.vhd \
  $RLIBRARIES_PATH/ct2/pss_ct2_beamformer_tb_behav.wcfg \
]

set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd]

set_property library pss_lib [get_files {\
*/designs/pss/src/vhdl/HBM_axi_tbModel.vhd \
}]

set_property library common_lib [get_files {\
 */base/common/src/vhdl/common_pkg.vhd \
}]

set_property file_type {VHDL 2008} [get_files  */libraries/ct2/pss_ct2_beamformer_tb.vhd]

set_property top pss_ct2_beamformer_tb [get_filesets sim_bf_packet]
update_compile_order -fileset sim_bf_packet

##############################################################
# Create sim set for packetiser.

create_fileset -simset sim_tb_packetiser

set_property SOURCE_SET sources_1 [get_filesets sim_tb_packetiser]

add_files -fileset sim_tb_packetiser [glob \
 $COMMON_PATH/Packetiser100G/tb/tb_packetisertop.vhd \
 $COMMON_PATH/Packetiser100G/tb/tb_packetisertop_behav.wcfg \
]

set_property library PSR_Packetiser_lib [get_files {\
 *Packetiser100G/tb/tb_packetisertop.vhd \
}]

set_property top tb_packetisertop [get_filesets sim_tb_packetiser]
update_compile_order -fileset sim_tb_packetiser