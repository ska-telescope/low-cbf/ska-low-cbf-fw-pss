set time_raw [clock seconds];
set date_string [clock format $time_raw -format "%y%m%d_%H%M%S"]

set proj_dir "$env(RADIOHDL)/build/$env(PERSONALITY)/ct1_tb_build_$date_string"
set ARGS_PATH "$env(RADIOHDL)/build/ARGS/pss"
set BOARD_PATH "$env(RADIOHDL)/designs/libraries/board"
set DESIGN_PATH "$env(RADIOHDL)/designs/pss"
set RLIBRARIES_PATH "$env(RADIOHDL)/libraries"
set COMMON_PATH "$env(RADIOHDL)/common/libraries"
set BUILD_PATH "$env(RADIOHDL)/build"
set DEVICE "xcu55c-fsvh2892-2L-e"
set BOARD "xilinx.com:au55c:part0:1.0"

puts "RADIOHDL directory:"
puts $env(RADIOHDL)

#puts "Timeslave IP in submodule"
# RADIOHDL is ENV_VAR for current project REPO. 
#set timeslave_repo "$env(RADIOHDL)/pub-timeslave/hw/cores"

# Create the new build directory
puts "Creating build_directory $proj_dir"
file mkdir $proj_dir

# This script sets the project variables
puts "Creating new project: pst"
cd $proj_dir

set workingDir [pwd]
puts "Working directory:"
puts $workingDir

# WARNING - proj_dir must be relative to workingDir.
# But cannot be empty because args generates tcl with the directory specified as "$proj_dir/"
set proj_dir "../ct1_tb_build_$date_string"

create_project $env(PERSONALITY) -part $DEVICE -force
set_property board_part $BOARD [current_project]
set_property target_language VHDL [current_project]
set_property target_simulator XSim [current_project]

############################################################
# Board specific files
############################################################


############################################################
# Design specific files
############################################################


add_files -fileset sim_1 [glob \
$DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd \
]
#set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd]

set_property file_type {VHDL 2008} [get_files  /home/hum089/projects/perentie/ska-low-cbf-fw-pss/designs/pss/src/vhdl/HBM_axi_tbModel.vhd]
set_property library pss_lib [get_files  /home/hum089/projects/perentie/ska-low-cbf-fw-pss/designs/pss/src/vhdl/HBM_axi_tbModel.vhd]

############################################################
# AXI4

add_files -fileset sources_1 [glob \
$COMMON_PATH/base/axi4/src/vhdl/axi4_lite_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/axi4_full_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/mem_to_axi4_lite.vhd \
]
set_property library axi4_lib [get_files {\
*libraries/base/axi4/src/vhdl/axi4_lite_pkg.vhd \
*libraries/base/axi4/src/vhdl/axi4_full_pkg.vhd \
*libraries/base/axi4/src/vhdl/mem_to_axi4_lite.vhd \
}]

# Technology select package
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/technology/technology_pkg.vhd \
 $RLIBRARIES_PATH/technology/technology_select_pkg.vhd \
]
set_property library technology_lib [get_files {\
 *libraries/technology/technology_pkg.vhd \
 *libraries/technology/technology_select_pkg.vhd \
}]

add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/dsp_top/DSP_top_pkg.vhd \
]
set_property library DSP_top_lib [get_files  {\
 *libraries/dsp_top/DSP_top_pkg.vhd \
}]

#############################################################
# Common

add_files -fileset sources_1 [glob \
 $COMMON_PATH/base/common/src/vhdl/common_reg_r_w.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_ram_crw_crw.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_str_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_mem_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_field_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_lfsr_sequences_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_interface_layers_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_network_layers_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_network_total_header_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_components_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_pipeline.vhd \
]
set_property library common_lib [get_files {\
 *libraries/base/common/src/vhdl/common_reg_r_w.vhd \
 *libraries/base/common/src/vhdl/common_pkg.vhd \
 *libraries/base/common/src/vhdl/common_ram_crw_crw.vhd \
 *libraries/base/common/src/vhdl/common_str_pkg.vhd \
 *libraries/base/common/src/vhdl/common_mem_pkg.vhd \
 *libraries/base/common/src/vhdl/common_field_pkg.vhd \
 *libraries/base/common/src/vhdl/common_lfsr_sequences_pkg.vhd \
 *libraries/base/common/src/vhdl/common_interface_layers_pkg.vhd \
 *libraries/base/common/src/vhdl/common_network_layers_pkg.vhd \
 *libraries/base/common/src/vhdl/common_network_total_header_pkg.vhd \
 *libraries/base/common/src/vhdl/common_components_pkg.vhd \
 *libraries/base/common/src/vhdl/common_pipeline.vhd \
}]

#############################################################
# tech memory
# (Used by ARGs)
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/technology/memory/tech_memory_component_pkg.vhd \
 $RLIBRARIES_PATH/technology/memory/tech_memory_ram_cr_cw.vhd \
 $RLIBRARIES_PATH/technology/memory/tech_memory_ram_crw_crw.vhd \
]
set_property library tech_memory_lib [get_files {\
 *libraries/technology/memory/tech_memory_component_pkg.vhd \
 *libraries/technology/memory/tech_memory_ram_cr_cw.vhd \
 *libraries/technology/memory/tech_memory_ram_crw_crw.vhd \
}]

#############################################################
# input Corner Turn (ct1)


add_files -fileset sources_1 [glob \
  $ARGS_PATH/pss_ct1/pss_ct1/pss_ct1_reg_pkg.vhd \
  $ARGS_PATH/pss_ct1/pss_ct1/pss_ct1_reg.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_top.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_readout_32bit.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_valid.vhd \
  $RLIBRARIES_PATH/ct1/pss_ct1_readout.vhd \
  $RLIBRARIES_PATH/ct1/pss_div96.vhd \
  $RLIBRARIES_PATH/ct1/poly_axi_bram_wrapper.vhd \
  $RLIBRARIES_PATH/ct1/poly_eval.vhd \
]

set_property library ct_lib [get_files {\
  *build/ARGS/pss/pss_ct1/pss_ct1/pss_ct1_reg_pkg.vhd \
  *build/ARGS/pss/pss_ct1/pss_ct1/pss_ct1_reg.vhd \
  *libraries/ct1/pss_ct1_top.vhd \
  *libraries/ct1/pss_ct1_readout_32bit.vhd \
  *libraries/ct1/pss_ct1_valid.vhd \
  *libraries/ct1/pss_ct1_readout.vhd \
  *libraries/ct1/pss_div96.vhd \
  *libraries/ct1/poly_axi_bram_wrapper.vhd \
  *libraries/ct1/poly_eval.vhd \
 }]

add_files -fileset sim_1 [glob \
  $RLIBRARIES_PATH/ct1/pss_ct1_tb.vhd \
]

#  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_tb.vhd \ 

source $RLIBRARIES_PATH/ct1/pss_ct1.tcl
source $RLIBRARIES_PATH/ct1/pss_ct1_ct2_common.tcl


#############################################################
# Filterbank

add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/filterbank/vhdl/fb_DSP.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFFTwrapper.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFBmem_4.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFIRTaps.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/PSSFBTop_4.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/FB_wrapper.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/ShiftandRound_16bit.vhd \
 $RLIBRARIES_PATH/filterbank/vhdl/fineDelay.vhd \
]

set_property library filterbank_lib [get_files {\
 *libraries/filterbank/vhdl/fb_DSP.vhd \
 *libraries/filterbank/vhdl/PSSFFTwrapper.vhd \
 *libraries/filterbank/vhdl/PSSFBmem_4.vhd \
 *libraries/filterbank/vhdl/PSSFIRTaps.vhd \
 *libraries/filterbank/vhdl/PSSFBTop_4.vhd \
 *libraries/filterbank/vhdl/FB_wrapper.vhd \
 *libraries/filterbank/vhdl/ShiftandRound_16bit.vhd \
 *libraries/filterbank/vhdl/fineDelay.vhd \
}] 

source $RLIBRARIES_PATH/filterbank/ip/dspAxB.tcl
source $RLIBRARIES_PATH/filterbank/ip/fineDelay.tcl
source $RLIBRARIES_PATH/filterbank/ip/PSSFB_FFT.tcl


##############################################################
# Set top
set_property top_lib xil_defaultlib [get_filesets sim_1]
# top level testbench
set_property top pss_ct1_tb [get_filesets sim_1]

