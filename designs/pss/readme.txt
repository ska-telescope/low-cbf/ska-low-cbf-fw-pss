# commands to generate a PSS project.

export SVN=/home/hum089/projects/perentie/ska-low-cbf-fw-pss/
source $SVN/tools/bin/setup_radiohdl.sh

python3 $SVN/tools/radiohdl/base/vivado_config.py -l pst -a

export PERSONALITY=pss
export TARGET_ALVEO=u55
export VITIS_VERSION=2022.2
export COMMON_PATH=~/projects/perentie/ska-low-cbf-fw-pss/common

vivado -mode batch -source create_project.tcl

