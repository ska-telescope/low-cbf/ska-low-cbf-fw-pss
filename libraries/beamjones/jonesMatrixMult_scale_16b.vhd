----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 09.11.2020 09:40:15
-- Module Name: jonesMatrixMult - Behavioral
-- Description: 
--  Full parallel 2x2 matrix-vector multiplier. 
--  
-- Scaling 
--  "16384" is treated as unity for the matrix values.
--  i.e. 
--    [(16384 + 0j) 0         ]
--    [ 0           (16384+0j)]
--  results in the output being the same as the input (o_r0real = i_v0real etc).
--
--  There are combinations of inputs that could result in overflow. 
--  If there is an overflow, the "flagged" bit will be set in the output.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity jonesMatrixMult_scale_16b is
    Port(
        i_clk : in std_logic;
        -- The 2x2 matrix
        i_m00real : in std_logic_vector(15 downto 0);
        i_m00imag : in std_logic_vector(15 downto 0);
        i_m01real : in std_logic_vector(15 downto 0);
        i_m01imag : in std_logic_vector(15 downto 0);
        i_m10real : in std_logic_vector(15 downto 0);
        i_m10imag : in std_logic_vector(15 downto 0);
        i_m11real : in std_logic_vector(15 downto 0);
        i_m11imag : in std_logic_vector(15 downto 0);
        -- 2x1 vector
        i_v0real : in std_logic_vector(15 downto 0);
        i_v0imag : in std_logic_vector(15 downto 0);
        i_v1real : in std_logic_vector(15 downto 0);
        i_v1imag : in std_logic_vector(15 downto 0);
        i_scale  : in std_logic_vector(3 downto 0);
        -- 2x1 vector result (9 clock latency)
        o_r0real : out std_logic_vector(15 downto 0);
        o_r0imag : out std_logic_vector(15 downto 0);
        o_r1real : out std_logic_vector(15 downto 0);
        o_r1imag : out std_logic_vector(15 downto 0);
        o_scale  : out std_logic_vector(3 downto 0)
    );
end jonesMatrixMult_scale_16b;

architecture Behavioral of jonesMatrixMult_scale_16b is

    -- convergent_round
    -- drops the low bit
    function convergent_round(din : std_logic_vector(16 downto 0); lowzero : std_logic) return std_logic_vector is
    begin
        if (((din(0) = '1') and (din(1) = '1')) or (din(0) = '1' and lowzero = '0')) and (din(16 downto 1) /= "0111111111111111") then
            return std_logic_vector(unsigned(din(16 downto 1)) + 1);
        else
            return din(16 downto 1);
        end if;
    end convergent_round;

    -- Will there be overflow when we select bits 29:14 ?
    -- If so, how much extra scaling do we need to stop it from happening ?
    function extra_scaling(din : std_logic_vector(5 downto 0)) return std_logic_vector is
    begin
        if din(5 downto 0) = "000000" or din(5 downto 0) = "111111" then
            return "000";
        elsif din(5 downto 1) = "00000" or din(5 downto 1) = "11111" then
            return "001";
        elsif din(5 downto 2) = "0000" or din(5 downto 2) = "1111" then
            return "010";
        elsif din(5 downto 3) = "000" or din(5 downto 3) = "111" then
            return "011";
        elsif din(5 downto 4) = "00" or din(5 downto 4) = "11" then
            return "100";
        else
            return "101"; 
        end if;
    end extra_scaling;
    
    function apply_scale(din : std_logic_vector(21 downto 0); dshift : std_logic_vector(2 downto 0)) return std_logic_vector is
    begin
        if dshift = "000" then
            return din(16 downto 0);
        elsif dshift = "001" then
            return din(17 downto 1);
        elsif dshift = "010" then
            return din(18 downto 2);
        elsif dshift = "011" then
            return din(19 downto 3);
        elsif dshift = "100" then
            return din(20 downto 4);
        else -- if dshift = "101" then
            return din(21 downto 5);
        end if;
    end apply_scale;
    
    function low_zeros(din : std_logic_vector(21 downto 0); dshift : std_logic_vector(2 downto 0)) return std_logic is
    begin
        if dshift = "000" then
            return '1';
        elsif dshift = "001" then
            if din(0) = '0' then
                return '1';
            else
                return '0';
            end if;
        elsif dshift = "010" then
            if din(1 downto 0) = "00" then
                return '1';
            else
                return '0';
            end if;
        elsif dshift = "011" then
            if din(2 downto 0) = "000" then
                return '1';
            else
                return '0';
            end if;
        elsif dshift = "100" then
            if din(3 downto 0) = "0000" then
                return '1';
            else
                return '0';
            end if;
        else -- if dshift = "101" then
            if din(4 downto 0) = "00000" then
                return '1';
            else
                return '0';
            end if;
        end if;
    end low_zeros;
    
    -- create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name complexMult16x8
    -- set_property -dict [list CONFIG.Component_Name {complexMult16x8} CONFIG.BPortWidth {16} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {33} CONFIG.MinimumLatency {4}] [get_ips complexMult16x8]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/complexMult16x8/complexMult16x8.xci]
    
    component complexMult16x16
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(79 DOWNTO 0));  -- real output in bits 32:0, imaginary output in bits 72:40
    end component;
    
    signal m00, m01, m10, m11 : std_logic_vector(31 downto 0);
    signal v0, v1 : std_logic_vector(31 downto 0);
    signal m00v0, m01v1, m10v0, m11v1 : std_logic_vector(79 downto 0);
    signal m00v0real, m00v0imag : std_logic_vector(35 downto 0);
    signal m01v1real, m01v1imag : std_logic_vector(35 downto 0);
    signal m10v0real, m10v0imag : std_logic_vector(35 downto 0);
    signal m11v1real, m11v1imag : std_logic_vector(35 downto 0);
    
    signal r0real, r0imag, r1real, r1imag : std_logic_vector(35 downto 0);
    signal r0real_22bit, r0imag_22bit, r1real_22bit, r1imag_22bit : std_logic_vector(21 downto 0);
    signal r0real_22bit_del1, r0imag_22bit_del1, r1real_22bit_del1, r1imag_22bit_del1 : std_logic_vector(21 downto 0);
    signal r0real_16bit, r0imag_16bit, r1real_16bit, r1imag_16bit : std_logic_vector(15 downto 0);
    signal r0real_overflow, r0imag_overflow, r1real_overflow, r1imag_overflow : std_logic;
    signal rfi_del1, rfi_del2, rfi_del3, rfi_del4, rfi_del5, rfi_del6 : std_logic;
    
    signal extra_scale_ext : std_logic_vector(3 downto 0);
    
    signal scale_del1, scale_Del2, scale_del3, scale_del4, scale_del5, scale_del6, scale_del7, scale_del8 : std_logic_vector(3 downto 0);
    signal extra_scale0, extra_scale1, extra_scale2, extra_scale3 : std_logic_vector(2 downto 0);
    
    signal r0real_12to0_zero_del1, r0real_12to0_zero, r0imag_12to0_zero_del1, r0imag_12to0_zero : std_logic;
    signal r1real_12to0_zero_del1, r1real_12to0_zero, r1imag_12to0_zero_del1, r1imag_12to0_zero : std_logic;
    signal extra_scale : std_logic_vector(2 downto 0);
    
    signal r0real_17bit, r0imag_17bit, r1real_17bit, r1imag_17bit : std_logic_vector(16 downto 0);
    signal r0real_lowzeros, r0imag_lowzeros, r1real_lowzeros, r1imag_lowzeros : std_logic;
    
begin
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            -- 4 clock latency for the complex multiplier
            scale_del1 <= i_scale;
            scale_del2 <= scale_del1;
            scale_del3 <= scale_del2;
            scale_del4 <= scale_del3;
            
            -- 
            r0real <= std_logic_vector(signed(m00v0real) + signed(m01v1real));
            r0imag <= std_logic_vector(signed(m00v0imag) + signed(m01v1imag));
            
            r1real <= std_logic_vector(signed(m10v0real) + signed(m11v1real));
            r1imag <= std_logic_vector(signed(m10v0imag) + signed(m11v1imag));
            scale_del5 <= scale_del4;
            --------------------------------------------------------
            -- Pipeline stage
            scale_del6 <= scale_del5;
            -- get the amount of extra scaling needed to ensure the data fits in 16 bits
            extra_scale0 <= extra_scaling(r0real(34 downto 29));
            extra_scale1 <= extra_scaling(r0imag(34 downto 29));
            extra_scale2 <= extra_scaling(r1real(34 downto 29));
            extra_scale3 <= extra_scaling(r1imag(34 downto 29));
            -- Check if the low 13 bits are zeros for later use in convergent rounding
            if r0real(12 downto 0) = "0000000000000" then
                r0real_12to0_zero <= '1';
            else
                r0real_12to0_zero <= '0';
            end if;
            if r0imag(12 downto 0) = "0000000000000" then
                r0imag_12to0_zero <= '1';
            else
                r0imag_12to0_zero <= '0';
            end if;
            if r1real(12 downto 0) = "0000000000000" then
                r1real_12to0_zero <= '1';
            else
                r1real_12to0_zero <= '0';
            end if;
            if r1imag(12 downto 0) = "0000000000000" then
                r1imag_12to0_zero <= '1';
            else
                r1imag_12to0_zero <= '0';
            end if;
            -- keep 22 bits for use in rounding
            r0real_22bit <= r0real(34 downto 13);
            r0imag_22bit <= r0imag(34 downto 13);
            r1real_22bit <= r1real(34 downto 13);
            r1imag_22bit <= r1imag(34 downto 13);
            
            -------------------------------------------------------
            -- Pipeline stage
            -- Find how much scaling we need to ensure the data fits
            if ((unsigned(extra_scale0) >= unsigned(extra_scale1)) and
                (unsigned(extra_scale0) >= unsigned(extra_scale2)) and 
                (unsigned(extra_scale0) >= unsigned(extra_scale3))) then
                extra_scale <= extra_scale0;
            elsif ((unsigned(extra_scale1) >= unsigned(extra_scale2)) and
                   (unsigned(extra_scale1) >= unsigned(extra_scale3))) then
                extra_scale <= extra_scale1;
            elsif (unsigned(extra_scale2) >= unsigned(extra_scale3)) then
                extra_scale <= extra_scale2;
            else
                extra_scale <= extra_scale3;
            end if;
            scale_del7 <= scale_del6;
            r0real_22bit_del1 <= r0real_22bit;
            r0imag_22bit_del1 <= r0imag_22bit;
            r1real_22bit_del1 <= r1real_22bit;
            r1imag_22bit_del1 <= r1imag_22bit;
            r0real_12to0_zero_del1 <= r0real_12to0_zero;
            r0imag_12to0_zero_del1 <= r0imag_12to0_zero;
            r1real_12to0_zero_del1 <= r1real_12to0_zero;
            r1imag_12to0_zero_del1 <= r1imag_12to0_zero;
            
            -------------------------------------------------------
            -- Pipeline stage
            -- Apply the extra scaling
            r0real_17bit <= apply_scale(r0real_22bit_del1, extra_scale);
            r0imag_17bit <= apply_scale(r0imag_22bit_del1, extra_scale);
            r1real_17bit <= apply_scale(r1real_22bit_del1, extra_scale);
            r1imag_17bit <= apply_scale(r1imag_22bit_del1, extra_scale);
            
            r0real_lowzeros <= r0real_12to0_zero_del1 and low_zeros(r0real_22bit_del1, extra_scale);
            r0imag_lowzeros <= r0imag_12to0_zero_del1 and low_zeros(r0imag_22bit_del1, extra_scale);
            r1real_lowzeros <= r1real_12to0_zero_del1 and low_zeros(r1real_22bit_del1, extra_scale);
            r1imag_lowzeros <= r1imag_12to0_zero_del1 and low_zeros(r1imag_22bit_del1, extra_scale);
            
            scale_del8 <= std_logic_vector(unsigned(scale_del7) + unsigned(extra_scale_ext));
            
            --------------------------------------------------------
            -- Convergent rounding
            o_r0real <= convergent_round(r0real_17bit,r0real_lowzeros);
            o_r0imag <= convergent_round(r0imag_17bit,r0imag_lowzeros);
            o_r1real <= convergent_round(r1real_17bit,r1real_lowzeros);
            o_r1imag <= convergent_round(r1imag_17bit,r1imag_lowzeros);
            o_scale <= scale_del8;
            
        end if;
    end process;
    
    extra_scale_ext <= '0' & extra_scale;
    
    ------------------------------------------------------
    -- complex multipliers

    m00 <= i_m00imag & i_m00real;
    m01 <= i_m01imag & i_m01real;
    m10 <= i_m10imag & i_m10real;
    m11 <= i_m11imag & i_m11real;

    v0 <= i_v0imag & i_v0real;
    v1 <= i_v1imag & i_v1real;
    
    cmult1 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m00, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v0, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m00v0   -- out(79:0)
    );
    m00v0real <= m00v0(32) & m00v0(32) & m00v0(32) & m00v0(32 downto 0);
    m00v0imag <= m00v0(72) & m00v0(72) & m00v0(72) & m00v0(72 downto 40);

    cmult2 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m01, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v1, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m01v1   -- out(79:0)
    );
    m01v1real <= m01v1(32) & m01v1(32) & m01v1(32) & m01v1(32 downto 0);
    m01v1imag <= m01v1(72) & m01v1(72) & m01v1(72) & m01v1(72 downto 40);

    cmult3 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m10, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v0, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m10v0   -- out(79:0)
    );
    m10v0real <= m10v0(32) & m10v0(32) & m10v0(32) & m10v0(32 downto 0);
    m10v0imag <= m10v0(72) & m10v0(72) & m10v0(72) & m10v0(72 downto 40);
    
    cmult4 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m11, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v1, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m11v1   -- out(79:0)
    );
    m11v1real <= m11v1(32) & m11v1(32) & m11v1(32) & m11v1(32 downto 0);
    m11v1imag <= m11v1(72) & m11v1(72) & m11v1(72) & m11v1(72 downto 40);
    
end Behavioral;
