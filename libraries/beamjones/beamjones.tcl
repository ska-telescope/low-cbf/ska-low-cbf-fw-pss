create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_bj
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_sj} CONFIG.MEM_DEPTH {16384} CONFIG.READ_LATENCY {4}] [get_ips axi_bram_ctrl_bj]
create_ip_run [get_ips axi_bram_ctrl_bj]

create_ip -name axi_clock_converter -vendor xilinx.com -library ip -version 2.1 -module_name axi_clock_converter_bj
set_property -dict [list CONFIG.Component_Name {axi_clock_converter_bj} CONFIG.ADDR_WIDTH {16} CONFIG.ACLK_ASYNC {1}] [get_ips axi_clock_converter_bj]
create_ip_run [get_ips axi_clock_converter_bj]
