----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: June 2024
-- Module Name: Beam_jones
-- Description: 
--  Beam jones correction.
--  2x2 matrix multiplication, 
--  with a different Jones matrix for each beam and each coarse (SPS) frequency channel.
--
----------------------------------------------------------------------------------

library IEEE, common_lib, axi4_lib, bf_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;

entity beam_jones is
    port(
        i_clk         : in std_logic;
        i_rst         : in std_logic;
        i_jonesBuffer : in std_logic;
        i_beamsEnabled : in std_logic_vector(9 downto 0); -- Number of beams enabled.
        -- Beam Jones registers
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        i_axi_mosi_bj : in  t_axi4_full_mosi;
        o_axi_miso_bj : out t_axi4_full_miso;
        --
        i_BFdata         : in t_slv_64_arr(3 downto 0);
        i_BFpacketCount  : in std_logic_vector(47 downto 0); -- packetCount is the same for all four parallel inputs
        i_BFBeam         : in t_slv_10_arr(3 downto 0);
        i_BFFreqIndex    : in std_logic_vector(10 downto 0); -- freqIndex is the same for all four parallel inputs; only the low 9 bits are used
        i_BFvalid        : in std_logic_vector(3 downto 0);   
	    i_BFscale	     : in t_slv_4_arr(3 downto 0);
        --
        o_BFdata         : out t_slv_64_arr(3 downto 0);
        o_BFpacketCount  : out std_logic_vector(47 downto 0);
        o_BFBeam         : out t_slv_10_arr(3 downto 0);
        o_BFFreqIndex    : out std_logic_vector(10 downto 0);
        o_BFvalid        : out std_logic_vector(3 downto 0);
	    o_BFscale	     : out t_slv_4_arr(3 downto 0)
    );
end beam_jones;

architecture Behavioral of beam_jones is

    signal clk  : std_logic;
    
    signal BFdata           : std_logic_vector(63 downto 0);
    signal BFpacketCount    : std_logic_vector(47 downto 0);
    signal BFBeam           : t_slv_12_arr(3 downto 0);
    signal BFFreqIndex      : std_logic_vector(9 downto 0);
    signal BFvalid          : std_logic;
    signal BFscale	        : std_logic_vector(3 downto 0);
    type scale_del_t is array(12 downto 0) of t_slv_4_arr(3 downto 0);
    signal BFscale_del      : scale_del_t;
    
    signal BFvalid_del : t_slv_4_arr(21 downto 0); -- std_logic_vector(19 downto 0);
    signal beamsEnabled, freqIndex : std_logic_vector(9 downto 0);
    signal jones_rd_addr : std_logic_vector(11 downto 0);
    
    type t_jones_fsm is (idle, start, mult_wait1, mult_wait2, get_beam0, get_beam1, get_beam2, get_beam3);
    signal jones_fsm, jones_fsm_del1, jones_fsm_del2, jones_fsm_del3, jones_fsm_del4 : t_jones_fsm := idle;
    signal beams_x_coarse : std_logic_vector(19 downto 0);
    signal beams_x_coarse_12bit : std_logic_vector(11 downto 0);
    signal jones_rd_data : std_logic_vector(127 downto 0);
    signal jones_data : t_slv_128_arr(3 downto 0);
    signal BFdata_del1, BFdata_del2, BFdata_del3, BFdata_del4, BFdata_del5, BFdata_del6 : t_slv_64_arr(3 downto 0);
    signal BFdata_del7, BFdata_del8, BFdata_del9, BFdata_del10, BFdata_del11, BFdata_del12 : t_slv_64_arr(3 downto 0);
    signal BFdata_final : t_slv_64_arr(3 downto 0);
    signal r0real, r0imag, r1real, r1imag : t_slv_16_arr(3 downto 0);
    signal jones_scale : t_slv_4_arr(3 downto 0);
    
begin
    
    bjmemi : entity bf_lib.bjmem
    port map (
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk => i_MACE_clk, -- in std_logic;
        i_MACE_rst => i_MACE_rst, -- in std_logic;
        -- Station Jones registers
        i_axi_mosi => i_axi_mosi_bj, --  in  t_axi4_full_mosi;
        o_axi_miso => o_axi_miso_bj, --  out t_axi4_full_miso;
        ----------------------------------------------------------------------
        -- clock for the memory
        i_BF_clk => i_clk, -- in std_logic;
        -- 4k deep x 16 byte wide read interface; 3 clock read latency
        -- i_rd_addr(11) = buffer selected
        -- i_rd_addr(10:0) = matrix selected = (beams_enabled * coarse_channel) + beam
        i_rd_addr => jones_rd_addr, -- in std_logic_vector(11 downto 0); 
        o_rd_data => jones_rd_data  -- out std_logic_vector(127 downto 0) -- one complete matrix per read
    );

    -----------------------------------------------------
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
        
            -- Rising edge of i_BFvalid triggers read of the 4 parallel beam jones matrices.
            -- beam data is delayed to wait for the jones matrices to be valid.
            
            -- 10 bit x 10bit = 20bit result
            -- Assumes same coarse for all four parallel beams
            beams_x_coarse <= std_logic_vector(unsigned(beamsEnabled) * unsigned(BFfreqIndex));
            beams_x_coarse_12bit <= beams_x_coarse(11 downto 0);
            
            if i_BFvalid(0) = '1' and BFvalid_del(1)(0) = '0' then
                jones_fsm <= start;
                BFbeam(0) <= "00" & i_BFbeam(0);
                BFbeam(1) <= "00" & i_BFbeam(1);
                BFbeam(2) <= "00" & i_BFbeam(2);
                BFbeam(3) <= "00" & i_BFbeam(3);
                BFpacketCount <= i_BFpacketCount;
                BFFreqIndex <= i_BFFreqIndex(9 downto 0);
            else
                case jones_fsm is
                    when idle =>
                        jones_fsm <= idle;
                        
                    when start =>
                        beamsEnabled <= i_beamsEnabled;
                        jones_fsm <= mult_wait1;
                    
                    when mult_wait1 =>
                        jones_fsm <= mult_wait2;
                        
                    when mult_wait2 =>
                        jones_fsm <= get_beam0;
                        
                    when get_beam0 =>
                        jones_rd_addr <= std_logic_vector(unsigned(beams_x_coarse_12bit) + unsigned(BFbeam(0)));
                        jones_fsm <= get_beam1;
                    
                    when get_beam1 =>
                        jones_rd_addr <= std_logic_vector(unsigned(beams_x_coarse_12bit) + unsigned(BFbeam(1)));
                        jones_fsm <= get_beam2;
                    
                    when get_beam2 =>
                        jones_rd_addr <= std_logic_vector(unsigned(beams_x_coarse_12bit) + unsigned(BFbeam(2)));
                        jones_fsm <= get_beam3;
                    
                    when get_beam3 =>
                        jones_rd_addr <= std_logic_vector(unsigned(beams_x_coarse_12bit) + unsigned(BFbeam(3)));
                        jones_fsm <= idle;
                    
                    when others =>
                        jones_fsm <= idle;
                    
                end case;
            end if;
            
            jones_fsm_del1 <= jones_fsm;
            jones_fsm_del2 <= jones_fsm_del1;
            jones_fsm_del3 <= jones_fsm_Del2;
            jones_fsm_del4 <= jones_fsm_del3;
            
            if jones_fsm_del4 = get_beam0 then
                jones_data(0) <= jones_rd_data;
            end if;
            if jones_fsm_del4 = get_beam1 then
                jones_data(1) <= jones_rd_data;
            end if;
            if jones_fsm_del4 = get_beam2 then
                jones_data(2) <= jones_rd_data;
            end if;
            if jones_fsm_del4 = get_beam3 then
                jones_data(3) <= jones_rd_data;
            end if;
            
            BFdata_del1 <= i_BFdata;    -- del1 aligns with jones_fsm = start
            BFdata_del2 <= BFdata_del1; -- del2 aligns with jones_fsm = mult_wait1
            BFdata_del3 <= BFdata_del2; -- del3 aligns with jones_fsm = mult_wait2
            BFdata_del4 <= BFdata_del3; -- del4 aligns with jones_fsm = get_beam0
            BFdata_del5 <= BFdata_del4; -- del5 aligns with jones_fsm = get_beam1
            BFdata_del6 <= BFdata_del5; -- del6 aligns with jones_fsm = get_beam2
            BFdata_del7 <= BFdata_del6; -- del7 aligns with jones_fsm = get_beam3
            BFdata_del8 <= BFdata_del7; -- del8 aligns with jones_fsm_del1 = get_beam3
            BFdata_del9 <= BFdata_del8; -- del9 aligns with jones_fsm_del2 = get_beam3
            BFdata_del10 <= BFdata_del9; -- del10 aligns with jones_fsm_del3 = get_beam3
            BFdata_del11 <= BFdata_del10; -- del11 aligns with jones_fsm_del4 = get_beam3
            BFdata_del12 <= BFdata_del11; -- del12 valid when jones_data(3) is valid.
            
            
            BFvalid_del(21 downto 1) <= BFvalid_del(20 downto 0);
            BFscale_del(12 downto 1) <= BFScale_del(11 downto 0);
            
            -- Output
            o_BFdata        <= BFdata_final;  -- t_slv_64_arr(3 downto 0);
            o_BFpacketCount <= BFpacketCount; -- std_logic_vector(47 downto 0);
            o_BFBeam(0)     <= BFBeam(0)(9 downto 0); -- out t_slv_10_arr(3 downto 0);
            o_BFBeam(1)     <= BFBeam(1)(9 downto 0);
            o_BFBeam(2)     <= BFBeam(2)(9 downto 0);
            o_BFBeam(3)     <= BFBeam(3)(9 downto 0);
            o_BFFreqIndex   <= '0' & BFfreqIndex; -- out (10:0);
            o_BFvalid       <= BFvalid_del(21); -- out std_logic;
	        o_BFscale	    <= jones_scale; -- BFscale_del(19);     -- out t_slv_4_arr(3 downto 0)
            
        end if;
    end process;

    BFvalid_del(0) <= i_BFvalid;
    BFscale_del(0) <= i_BFscale;

    jonesGen : for i in 0 to 3 generate
        jonesi : entity bf_lib.jonesMatrixMult_scale_16b
        Port map(
            i_clk => i_clk, -- in std_logic;
            -- The 2x2 matrix
            -- jones_data has 5 clock latency from i_virtualChannel (2 clocks to get virtualChannel_div2_del1, 3 for the memory)
            i_m00real => jones_data(i)(15 downto 0), -- in (15:0);
            i_m00imag => jones_data(i)(31 downto 16), -- in (15:0);
            i_m01real => jones_data(i)(47 downto 32), -- in (15:0);
            i_m01imag => jones_data(i)(63 downto 48), -- in (15:0);
            i_m10real => jones_data(i)(79 downto 64), -- in (15:0);
            i_m10imag => jones_data(i)(95 downto 80), -- in (15:0);
            i_m11real => jones_data(i)(111 downto 96), -- in (15:0);
            i_m11imag => jones_data(i)(127 downto 112), -- in (15:0);
            -- 2x1 vector
            -- data_del1 has 1 clock latency from i_data, +4 clock latency in corner turn 2 (i_Data is 4 clocks behind i_virtualChannel)
            --  = total 5 clock latency to match jones_data.
            i_v0real => BFdata_del12(i)(15 downto 0),   -- in (15:0);
            i_v0imag => BFdata_del12(i)(31 downto 16),  -- in (15:0);
            i_v1real => BFdata_del12(i)(47 downto 32),  -- in (15:0);
            i_v1imag => BFdata_del12(i)(63 downto 48),  -- in (15:0);
            i_scale  => BFscale_del(12)(i), -- in (3:0);
            -- 2x1 vector result (9 clock latency)
            o_r0real => r0real(i), -- out (15:0);
            o_r0imag => r0imag(i), -- out (15:0);
            o_r1real => r1real(i), -- out (15:0);
            o_r1imag => r1imag(i), -- out (15:0);
            o_scale  => jones_scale(i) -- out (3:0);
        );
        
        BFdata_final(i)(15 downto 0) <= r0real(i);
        BFdata_final(i)(31 downto 16) <= r0imag(i);
        BFdata_final(i)(47 downto 32) <= r1real(i);
        BFdata_final(i)(63 downto 48) <= r1imag(i);
        
    end generate;

end Behavioral;
