----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: June 2024
-- Module Name: rms
-- Description: 
-- 
-- N_samples = (54 channels)*(16 times)*(2 pol)*(2 (re+im)) = 3456
-- 54 x 16 = 864 = number of cycles of a burst from the BF.
-- Adder latency = 11
-- +1 for register loop
-- accum loop of latency 12
-- 864/12 = 72.
----------------------------------------------------------------------------------

library IEEE, common_lib, scaling_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE scaling_lib.scaling_pkg.ALL;

entity rms is
    generic(
        g_SIM           : boolean := FALSE;
        g_DEBUG_ILA     : boolean := FALSE
    );
    port(
        i_clk               : in std_logic;
        i_rst               : in std_logic;

        i_rms_data          : t_slv_32_arr(3 downto 0);
        i_valid             : in std_logic;
	
        o_scale_ready       : out std_logic;
        o_scale_value       : out std_logic_vector(31 downto 0)
    );
end rms;

architecture Behavioral of rms is

constant LATENCY    : integer := 5;     -- pipeline stages.

component fixed32_to_sp_float is
    Port ( 
        aclk                    : in STD_LOGIC;
        s_axis_a_tvalid         : in STD_LOGIC;
        s_axis_a_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        m_axis_result_tvalid    : out STD_LOGIC;
        m_axis_result_tdata     : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

component fp32_mult is
    Port ( 
        aclk                    : in STD_LOGIC;
        s_axis_a_tvalid         : in STD_LOGIC;
        s_axis_a_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s_axis_b_tvalid         : in STD_LOGIC;
        s_axis_b_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        m_axis_result_tvalid    : out STD_LOGIC;
        m_axis_result_tdata     : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

component fp32_add is
    Port ( 
        aclk                    : in STD_LOGIC;
        s_axis_a_tvalid         : in STD_LOGIC;
        s_axis_a_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s_axis_b_tvalid         : in STD_LOGIC;
        s_axis_b_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        m_axis_result_tvalid    : out STD_LOGIC;
        m_axis_result_tdata     : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

component fp32_div is
    Port ( 
        aclk                    : in STD_LOGIC;
        s_axis_a_tvalid         : in STD_LOGIC;
        s_axis_a_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s_axis_b_tvalid         : in STD_LOGIC;
        s_axis_b_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        m_axis_result_tvalid    : out STD_LOGIC;
        m_axis_result_tdata     : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

component fp_sqrt is
    Port ( 
      aclk                      : in STD_LOGIC;
      s_axis_a_tvalid           : in STD_LOGIC;
      s_axis_a_tdata            : in STD_LOGIC_VECTOR ( 31 downto 0 );
      m_axis_result_tvalid      : out STD_LOGIC;
      m_axis_result_tdata       : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;
------------------------------------------------------
-- Number of samples is 3456 = (54 channels)*(16 times)*(2 pol)*(2 (re+im))
-- 1/3456 ~ 0.000289352
-- in IEEE 574 this is 0x3997b42b
constant inverse_of_no_of_samples   : std_logic_vector(31 downto 0) := x"3997b42b";

-- Scale factor is between 8 and 13, choosing 10.5
-- 10.5 in IEEE 574 is 0x41280000
constant scaling_calc_constant      : std_logic_vector(31 downto 0) := x"41280000";

------------------------------------------------------
-- Sim signals
signal sim_ftof_a           : real := 0.0;
signal sim_ftof_b           : real := 0.0;
signal sim_ftof_c           : real := 0.0;
signal sim_ftof_d           : real := 0.0;

signal sim_adder_a1         : real := 0.0;
signal sim_adder_b1         : real := 0.0;
signal sim_adder_2          : real := 0.0;
signal sim_adder_accum      : real := 0.0;
signal sim_adder_drain      : real := 0.0;
signal sim_rms_sum          : real := 0.0;
signal sim_multi_inverse    : real := 0.0;
signal sim_sqrt             : real := 0.0;
signal sim_scale_value      : real := 0.0;

------------------------------------------------------
-- General declarations

signal clk          : std_logic;
signal rst          : std_logic;

signal ftof_data    : t_slv_32_arr(3 downto 0);
signal ftof_valid   : std_logic_vector(3 downto 0);

signal sqrd_data    : t_slv_32_arr(3 downto 0);
signal sqrd_valid   : std_logic_vector(3 downto 0);

signal adder_1a_q_data      : std_logic_vector(31 downto 0);
signal adder_1a_q_valid     : std_logic;

signal adder_1b_q_data      : std_logic_vector(31 downto 0);
signal adder_1b_q_valid     : std_logic;

signal adder_2_q_data       : std_logic_vector(31 downto 0);
signal adder_2_q_valid      : std_logic;

signal adder_accum_q_data   : std_logic_vector(31 downto 0);
signal adder_accum_q_valid  : std_logic;

signal adder_accum_reg      : std_logic_vector(31 downto 0);
signal adder_drain_reg      : std_logic_vector(31 downto 0);

signal adder_accum_delay    : t_slv_32_arr(11 downto 0);

signal draining             : std_logic;
signal end_of_frame         : std_logic := '0';
signal end_of_frame_edge    : std_logic;
signal drain_shift          : std_logic;

signal adder_drain_a        : std_logic_vector(31 downto 0);
signal adder_drain_b        : std_logic_vector(31 downto 0);
signal adder_drain_validin  : std_logic;
signal adder_drain_q        : std_logic_vector(31 downto 0);
signal adder_drain_q_valid  : std_logic;

signal adder_drain_b_input_sel  : std_logic;

signal draining_loop_cnt    : unsigned(7 downto 0)  := x"00";
signal draining_cnt         : unsigned(3 downto 0)  := x"0";

type rms_draining_statemachine is (IDLE, START, SHIFT, ADDING, FINISH);
signal draining_sm : rms_draining_statemachine;

signal rms_sum                  : std_logic_vector(31 downto 0);

signal multi_invers_validin     : std_logic;
signal multi_invers_q           : std_logic_vector(31 downto 0);
signal multi_invers_q_valid     : std_logic;

signal sqrt_q                   : std_logic_vector(31 downto 0);
signal sqrt_q_valid             : std_logic;

signal compute_scale_q          : std_logic_vector(31 downto 0);
signal compute_scale_q_valid    : std_logic;


begin
-----------------------------------------------------

clk     <= i_clk;
rst     <= i_rst;


-----------------------------------------------------

ftof_multi_gen : for i in 0 to 3 GENERATE

    i_ftof : fixed32_to_sp_float port map (
            aclk                    => clk,

            s_axis_a_tdata          => i_rms_data(i),
            s_axis_a_tvalid         => i_valid,

            m_axis_result_tdata     => ftof_data(i),
            m_axis_result_tvalid    => ftof_valid(i)
        );

    i_sqr : fp32_mult port map ( 
            aclk                    => clk,

            s_axis_a_tvalid         => ftof_valid(i),
            s_axis_a_tdata          => ftof_data(i),
            s_axis_b_tvalid         => ftof_valid(i),
            s_axis_b_tdata          => ftof_data(i),

            m_axis_result_tvalid    => sqrd_valid(i),
            m_axis_result_tdata     => sqrd_data(i)
        );

END GENERATE;

-- FP accumulator
-- FP adder tree with registers and additional adders at the end to drain the pipeline
--
-- FP adder tree
-- 3 adders, reducing the 4 streams to 1 from above.

i_adder_1a : fp32_add port map ( 
      aclk                  => clk,

      s_axis_a_tvalid       => sqrd_valid(0),
      s_axis_a_tdata        => sqrd_data(0),
      s_axis_b_tvalid       => sqrd_valid(1),
      s_axis_b_tdata        => sqrd_data(1),

      m_axis_result_tvalid  => adder_1a_q_valid,
      m_axis_result_tdata   => adder_1a_q_data
);

i_adder_1b : fp32_add port map ( 
      aclk                  => clk,

      s_axis_a_tvalid       => sqrd_valid(2),
      s_axis_a_tdata        => sqrd_data(2),
      s_axis_b_tvalid       => sqrd_valid(3),
      s_axis_b_tdata        => sqrd_data(3),

      m_axis_result_tvalid  => adder_1b_q_valid,
      m_axis_result_tdata   => adder_1b_q_data
);

i_adder_2 : fp32_add port map ( 
      aclk                  => clk,

      s_axis_a_tvalid       => adder_1a_q_valid,
      s_axis_a_tdata        => adder_1a_q_data,
      s_axis_b_tvalid       => adder_1b_q_valid,
      s_axis_b_tdata        => adder_1b_q_data,

      m_axis_result_tvalid  => adder_2_q_valid,
      m_axis_result_tdata   => adder_2_q_data
);

-- accumulator adder
i_adder_accum : fp32_add port map ( 
      aclk                  => clk,

      s_axis_a_tvalid       => adder_2_q_valid,
      s_axis_a_tdata        => adder_2_q_data,
      s_axis_b_tvalid       => '1',
      s_axis_b_tdata        => adder_accum_reg,

      m_axis_result_tvalid  => adder_accum_q_valid,
      m_axis_result_tdata   => adder_accum_q_data
);

gen_accum_delay_line : for i in 0 to 10 GENERATE
    delay_line_proc : process(clk)
    begin
        if rising_edge(clk) then
            if (adder_accum_q_valid = '1' AND draining = '0') OR (drain_shift = '1') then
                adder_accum_delay(i+1) <= adder_accum_delay(i);
            end if;
        end if;
    end process;
END GENERATE;

-- zero out the delay pipe ready for next accumulation cycle, when stream finishes.
reset_delay_proc : process(clk)
begin
    if rising_edge(clk) then
        if adder_accum_q_valid = '0' then
            adder_accum_reg     <= (others => '0');
        else
            adder_accum_reg     <= adder_accum_q_data;
            adder_drain_reg     <= adder_accum_q_data;
        end if;

        if adder_accum_q_valid = '1' AND draining = '0' then
            adder_accum_delay(0) <= adder_accum_q_data;
        elsif (drain_shift = '1') then
            adder_accum_delay(0) <= (others => '0');
        end if;

    end if;
end process;

-----------------------------------------------------
-- drain logic
draining_proc : process(clk)
begin
    if rising_edge(clk) then
        end_of_frame_edge   <= adder_accum_q_valid;

        if end_of_frame_edge = '1' and adder_accum_q_valid = '0' then
            end_of_frame    <= '1';
        else
            end_of_frame    <= '0';
        end if;

        -- register inputs to ADDER
        adder_drain_a       <= adder_accum_delay(11);

        if adder_drain_b_input_sel = '0' then
            adder_drain_b   <= adder_drain_reg;
        else
            adder_drain_b   <= adder_drain_q;
        end if;

        if rst = '1' then
            adder_drain_validin     <= '0';
            draining                <= '0';
            draining_sm             <= IDLE;
            draining_loop_cnt       <= x"00";
            draining_cnt            <= x"0";
            adder_drain_b_input_sel <= '0';
            drain_shift             <= '0';
            multi_invers_validin    <= '0';
        else
            case draining_sm is
                when IDLE =>
                    adder_drain_b_input_sel <= '0';
                    multi_invers_validin    <= '0';
                    draining_loop_cnt       <= x"00";
                    draining_cnt            <= x"0";
                    if end_of_frame = '1' then
                        draining_sm     <= START;
                        draining        <= '1';
                    end if;

                when START =>
                    -- kick off adder
                    adder_drain_validin <= '1';
                    draining_sm         <= SHIFT;

                when SHIFT =>
                    adder_drain_validin     <= '0';
                    adder_drain_b_input_sel <= '1';     -- Position 0 for first input only.
                    -- shift the delay pipeline one position
                    drain_shift         <= '1';
                    draining_sm         <= ADDING;

                when ADDING =>
                    drain_shift         <= '0';

                    draining_cnt        <= draining_cnt + 1;

                    if draining_cnt = 9 then
                        draining_cnt        <= x"0";
                        draining_loop_cnt   <= draining_loop_cnt + 1;
                        if draining_loop_cnt = 10 then      -- started with the first 2 values in the pipe, so only loop 11 of 12.
                            draining_sm         <= FINISH;
                        else
                            draining_sm         <= START;
                        end if;
                    end if;

                when FINISH =>
                    draining_sm             <= IDLE;
                    draining                <= '0';
                    multi_invers_validin    <= '1';


                when OTHERS => draining_sm <= IDLE;
            end case;

        end if;
    end if;
end process;

i_adder_drain : fp32_add port map ( 
      aclk                  => clk,

      s_axis_a_tvalid       => adder_drain_validin,
      s_axis_a_tdata        => adder_drain_a,
      s_axis_b_tvalid       => adder_drain_validin,
      s_axis_b_tdata        => adder_drain_b,

      m_axis_result_tvalid  => adder_drain_q_valid,
      m_axis_result_tdata   => adder_drain_q
);

-----------------------------------------------------
sum_proc : process(clk)
begin
    if rising_edge(clk) then

        if adder_drain_q_valid = '1' then
            rms_sum <= adder_drain_q;
        end if;
    end if;
end process;

-- RMS calc
-- Prior to this all samples have been squared and then summed
-- Now div by number of samples = multiply by 1/3456
i_multi_inverse_no_of_samples : fp32_mult port map ( 
    aclk                    => clk,

    s_axis_a_tvalid         => multi_invers_validin,
    s_axis_a_tdata          => rms_sum,
    s_axis_b_tvalid         => multi_invers_validin,
    s_axis_b_tdata          => inverse_of_no_of_samples,

    m_axis_result_tvalid    => multi_invers_q_valid,
    m_axis_result_tdata     => multi_invers_q
);

-- Take the sqrt of above to get R
i_sqrt : fp_sqrt port map ( 
      aclk                  => clk,
      s_axis_a_tvalid       => multi_invers_q_valid,
      s_axis_a_tdata        => multi_invers_q,
      m_axis_result_tvalid  => sqrt_q_valid,
      m_axis_result_tdata   => sqrt_q
);


-- Scale factor is 10.5 (scaling_calc_constant) / R (sqrt above)
i_compute_scale : fp32_div port map ( 
    aclk                    => clk,

    s_axis_a_tvalid         => sqrt_q_valid,
    s_axis_a_tdata          => scaling_calc_constant,
    s_axis_b_tvalid         => sqrt_q_valid,
    s_axis_b_tdata          => sqrt_q,

    m_axis_result_tvalid    => compute_scale_q_valid,
    m_axis_result_tdata     => compute_scale_q
);

scaled_output_proc : process(clk)
begin
    if rising_edge(clk) then
        if compute_scale_q_valid = '1' then
            o_scale_ready       <= '1';
            o_scale_value       <= compute_scale_q;
        else 
            o_scale_ready       <= '0';
        end if;
    end if;
end process;

-----------------------------------------------------
-- Sim signals
gen_sim : if g_SIM GENERATE
    sim_ftof_a          <= float2real(ftof_data(0));
    sim_ftof_b          <= float2real(ftof_data(1));
    sim_ftof_c          <= float2real(ftof_data(2));
    sim_ftof_d          <= float2real(ftof_data(3));
    
    sim_adder_a1        <= float2real(adder_1a_q_data);
    sim_adder_b1        <= float2real(adder_1b_q_data);
    sim_adder_2         <= float2real(adder_2_q_data);
    sim_adder_accum     <= float2real(adder_accum_q_data);
    sim_adder_drain     <= float2real(adder_drain_q);
    sim_rms_sum         <= float2real(rms_sum);
    sim_multi_inverse   <= float2real(multi_invers_q);
    sim_sqrt            <= float2real(sqrt_q);
    sim_scale_value     <= float2real(compute_scale_q);
END GENERATE;

end Behavioral;