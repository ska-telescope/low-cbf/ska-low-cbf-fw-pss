create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fixed32_to_sp_float
set_property -dict [list \
  CONFIG.A_Precision_Type {Int32} \
  CONFIG.C_A_Exponent_Width {32} \
  CONFIG.C_A_Fraction_Width {0} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {3} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {8} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {fixed32_to_sp_float} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Maximum_Latency {false} \
  CONFIG.Operation_Type {Fixed_to_float} \
  CONFIG.Result_Precision_Type {Single} \
] [get_ips fixed32_to_sp_float]
create_ip_run [get_ips fixed32_to_sp_float]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp32_add
set_property -dict [list \
  CONFIG.Add_Sub_Value {Add} \
  CONFIG.C_Latency {11} \
  CONFIG.Component_Name {fp32_add} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
] [get_ips fp32_add]
create_ip_run [get_ips fp32_add]

# fp32_multi made in another tcl file.

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp32_div
set_property -dict [list \
  CONFIG.A_Precision_Type {Single} \
  CONFIG.C_A_Exponent_Width {8} \
  CONFIG.C_A_Fraction_Width {24} \
  CONFIG.C_Latency {28} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {8} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {fp32_div} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Divide} \
  CONFIG.Result_Precision_Type {Single} \
] [get_ips fp32_div]
create_ip_run [get_ips fp32_div]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp32_int8
set_property -dict [list \
  CONFIG.A_Precision_Type {Single} \
  CONFIG.C_A_Exponent_Width {8} \
  CONFIG.C_A_Fraction_Width {24} \
  CONFIG.C_Latency {6} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {32} \
  CONFIG.C_Result_Fraction_Width {0} \
  CONFIG.Result_Precision_Type {Int32} \
  CONFIG.Component_Name {fp32_int8} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Float_to_fixed} \
] [get_ips fp32_int8]
create_ip_run [get_ips fp32_int8]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp_sqrt
set_property -dict [list \
  CONFIG.A_Precision_Type {Single} \
  CONFIG.C_A_Exponent_Width {8} \
  CONFIG.C_A_Fraction_Width {24} \
  CONFIG.C_Latency {28} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {8} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {fp_sqrt} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Square_root} \
  CONFIG.Result_Precision_Type {Single} \
] [get_ips fp_sqrt]
create_ip_run [get_ips fp_sqrt]