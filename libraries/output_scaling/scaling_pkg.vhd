----------------------------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
----------------------------------------------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

PACKAGE scaling_pkg IS

    FUNCTION float2real(fp_sp_in: std_logic_vector(31 downto 0)) return real;

END scaling_pkg;


PACKAGE BODY scaling_pkg IS

    function float2real(fp_sp_in: std_logic_vector(31 downto 0)) return real is
        constant xdiv               : real      := 2.0**23;
        variable exp,mant           : integer   := 0;
        variable multexp,mantdec    : real      := 0.0;
        variable result             : real      := 0.0;
        variable mant_full          : std_logic_vector(23 downto 0);
        variable mant_full_u        : unsigned(23 downto 0);
        begin
            exp         := to_integer(unsigned(fp_sp_in(30 downto 23)) - 127); 
            multexp     := 2.0**exp;
            mant_full   := '1' & fp_sp_in(22 downto 0);
            mant_full_u := unsigned(mant_full);
            mant        := to_integer(mant_full_u);
            mantdec     := real(mant)/xdiv;
            result         := mantdec*multexp;
        
            --check sign
            if(fp_sp_in(31)='1')then
                result := -result;
            end if;
        
            return(result);
    end float2real;                  

end scaling_pkg;