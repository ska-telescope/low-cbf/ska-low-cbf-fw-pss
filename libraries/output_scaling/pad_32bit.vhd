----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: June 2024
-- Module Name: pad_32bit
-- Description: 
-- Take in 16 bit vector and scale to 32 bit based on scale value provided.
--
-- Expected to run at 400 MHz
--
-- Pad 16-bit data to 32 bits : P32 = P16 * 2^S
--
-- assume 8 cycles between packets from BF, and that data is streaming.
----------------------------------------------------------------------------------

library IEEE, common_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;

entity pad_32bit is
    generic(
        g_DEBUG_ILA     : boolean := FALSE
    );
    port(
        i_clk               : in std_logic;
        i_rst               : in std_logic;

        i_data              : in std_logic_vector(15 downto 0);
        i_valid             : in std_logic;
	    i_scale	            : in std_logic_vector(3 downto 0);

        o_data              : out std_logic_vector(31 downto 0);
        o_valid             : out std_logic
    );

    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of pad_32bit : entity is "yes";

end pad_32bit;

architecture Behavioral of pad_32bit is

constant LATENCY    : integer := 5;     -- pipeline stages.

signal data         : t_slv_32_arr((latency - 1) downto 0); --std_logic_vector(31 downto 0);
signal scale        : t_slv_4_arr((latency - 1) downto 0); --std_logic_vector(3 downto 0);
signal valid        : std_logic_vector((latency - 1) downto 0) := (OTHERS => '0');

signal sign_extend  : std_logic_vector(16 downto 0);

begin

sign_extend <= (others => i_data(15));

reg_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        -- stage 1 - input and register
        data(0)     <= sign_extend & i_data(14 downto 0); --i_data(15) & x"0000" & i_data(14 downto 0);
        scale(0)    <= i_scale;
        valid(0)    <= i_valid;

        -- stage 2 - begin scaling
        if scale(0)(3) = '1' then
            data(1)(31)             <= data(0)(31);
            data(1)(30 downto 0)    <= data(0)(22 downto 0) & x"00";
        else
            data(1)                 <= data(0);
        end if;
        valid(1)    <= valid(0);
        scale(1)    <= scale(0);

        -- stage 3
        if scale(1)(2) = '1' then
            data(2)(31)             <= data(1)(31);
            data(2)(30 downto 0)    <= data(1)(26 downto 0) & x"0";
        else
            data(2)                 <= data(1);
        end if;
        valid(2)    <= valid(1);
        scale(2)    <= scale(1);

        -- stage 4
        if scale(2)(1) = '1' then
            data(3)(31)             <= data(2)(31);
            data(3)(30 downto 0)    <= data(2)(28 downto 0) & "00";
        else
            data(3)                 <= data(2);
        end if;
        valid(3)    <= valid(2);
        scale(3)    <= scale(2);

        -- stage 5
        if scale(3)(0) = '1' then
            data(4)(31)             <= data(3)(31);
            data(4)(30 downto 0)    <= data(3)(29 downto 0) & '0';
        else
            data(4)                 <= data(3);
        end if;
        valid(4)    <= valid(3);

        -- output
        o_data      <= data(4);
        o_valid     <= valid(4);

    end if;
end process;

end Behavioral;
