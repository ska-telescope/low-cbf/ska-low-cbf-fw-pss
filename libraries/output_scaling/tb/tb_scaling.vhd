----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: Nov 2020
-- Design Name: tb_packetisertop
-- 
-- Target Devices: +US
-- 
-- test bench written to be used in Vivado
-- project and waveview view also provided, tb_packetiser.xpr for Vivado 2020.1
--
library IEEE,technology_lib, PSR_Packetiser_lib, signal_processing_common, axi4_lib, ethernet_lib, scaling_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use axi4_lib.axi4_stream_pkg.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
USE technology_lib.tech_mac_100g_pkg.ALL;

entity tb_scaling is
--  Port ( );
end tb_scaling;

architecture Behavioral of tb_scaling is


signal clock_300 : std_logic := '0';    -- 3.33ns
signal clock_400 : std_logic := '0';    -- 2.50ns
signal clock_322 : std_logic := '0';

signal testCount            : integer   := 0;
signal testCount_300        : integer   := 0;
signal testCount_322        : integer   := 0;

signal clock_400_rst        : std_logic := '1';
signal clock_300_rst        : std_logic := '1';
signal clock_322_rst        : std_logic := '1';

signal i_clk400_rst         : std_logic := '1';

signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;

signal i_clk400                 : std_logic;

signal test_count               : integer := 0;

signal sim_data                 : std_logic;

signal o_data_to_transmit      : t_lbus_sosi;
signal i_data_to_transmit_ctl  : t_lbus_siso;

-- AXI to CMAC interface to be implemented
signal o_tx_axis_tdata          : STD_LOGIC_VECTOR(511 downto 0);
signal o_tx_axis_tkeep          : STD_LOGIC_VECTOR(63 downto 0);
signal o_tx_axis_tvalid         : STD_LOGIC;
signal o_tx_axis_tlast          : STD_LOGIC;
signal o_tx_axis_tuser          : STD_LOGIC;
signal i_tx_axis_tready         : STD_LOGIC;

signal beamformer_to_packetiser_data    :  packetiser_stream_in; 
signal beamformer_to_packetiser_data_2  :  packetiser_stream_in;
signal beamformer_to_packetiser_data_3  :  packetiser_stream_in;
 
signal packet_stream_stats              :  t_packetiser_stats(2 downto 0);

signal packetiser_stream_1_host_bus_in      : packetiser_config_in;
signal packetiser_stream_2_host_bus_in      : packetiser_config_in;
signal packetiser_stream_3_host_bus_in      : packetiser_config_in;
  
signal packetiser_host_bus_out          : packetiser_config_out;  
signal packetiser_host_bus_out_2        : packetiser_config_out;
signal packetiser_host_bus_out_3        : packetiser_config_out;  

signal packetiser_host_bus_ctrl         :  packetiser_stream_ctrl;
signal packetiser_host_bus_ctrl_2       :  packetiser_stream_ctrl;
signal packetiser_host_bus_ctrl_3       :  packetiser_stream_ctrl;

signal packetiser_host_bus_ctrl_hold    :  packetiser_stream_ctrl;
signal packetiser_stream_1_host_bus_in_hold : packetiser_config_in;

type test_mode_statemachine is (IDLE, RUN);
signal test_mode_sm : test_mode_statemachine;

signal BFdata           : std_logic_vector(63 downto 0);
signal BFpacketCount    : std_logic_vector(47 downto 0);
signal BFBeam           : std_logic_vector(9 downto 0)      := 10D"1";
signal BFFreqIndex      : std_logic_vector(10 downto 0)     := (others => '0');
signal BFvalid          : std_logic;
signal BFjones_status   : std_logic_vector(1 downto 0);
signal BFpoly_ok        : std_logic_vector(1 downto 0);
signal BFscale	        : std_logic_vector(3 downto 0);

signal bf_packet_count  : unsigned(7 downto 0);

signal i_beam_output_stream_1      : t_bf_output_stream;
signal i_beam_output_stream_2      : t_bf_output_stream;

signal stim_counter     : unsigned(7 downto 0);

signal i_PSR_packetiser_Lite_axi_mosi : t_axi4_lite_mosi_arr((2-1) downto 0);
signal o_PSR_packetiser_Lite_axi_miso : t_axi4_lite_miso_arr((2-1) downto 0);
        
signal i_PSR_packetiser_Full_axi_mosi : t_axi4_full_mosi_arr((2-1) downto 0);
signal o_PSR_packetiser_Full_axi_miso : t_axi4_full_miso_arr((2-1) downto 0);

signal sim_count        : integer := 0;

signal sim_runs         : integer := 0;

begin

clock_300 <= not clock_300 after 3.33 ns;
clock_322 <= not clock_322 after 3.11 ns;

clock_400 <= not clock_400 after 2.50 ns;


i_clk400        <= clock_400;


-------------------------------------------------------------------------------------------------------------
-- powerup resets for SIM
test_runner_proc_clk300: process(clock_300)
begin
    if rising_edge(clock_300) then
        -- power up reset logic
        if power_up_rst_clock_300(31) = '1' then
            power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
            clock_300_rst   <= '1';
            testCount_300   <= 0;
        else
            clock_300_rst   <= '0';
            testCount_300   <= testCount_300 + 1;
            

        end if;

    end if;
end process;

test_runner_proc_clk322: process(clock_322)
begin
    if rising_edge(clock_322) then
        -- power up reset logic
        if power_up_rst_clock_322(31) = '1' then
            power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
            clock_322_rst   <= '1';
            testCount_322   <= 0;
        else
            clock_322_rst   <= '0';
            testCount_322   <= testCount_322 + 1;
            

        end if;

    end if;
end process;

test_runner_proc_clk400: process(clock_400)
begin
    if rising_edge(clock_400) then
        -- power up reset logic
        if power_up_rst_clock_400(31) = '1' then
            power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
            testCount           <= 0;
            clock_400_rst       <= '1';
        else
            testCount       <= testCount + 1;
            clock_400_rst   <= '0';
         
        end if;
    end if;
end process;

-------------------------------------------------------------------------------------------------------------

dut_stim_proc : process(clock_400)
begin
    if rising_edge(clock_400) then
        if clock_400_rst = '1' then
            sim_data        <= '0';
            BFdata          <= (others => '0');
            BFscale         <= "0000";
            BFvalid         <= '0';

            BFpacketCount   <= (others => '0');
            BFBeam          <= (others => '0');
            BFFreqIndex     <= (others => '0');
            BFjones_status  <= (others => '0');
            BFpoly_ok       <= (others => '0');
            stim_counter    <= x"01";

            bf_packet_count <= 8D"100";
            
            sim_count       <= 0;
            sim_runs        <= 0;
        else
            if testCount >= 1000 then
                stim_counter <= stim_counter + 1;
            end if;

            if sim_runs < 1 then
                BFdata(15 downto 0)     <= x"00" & std_logic_vector(stim_counter);
                BFdata(31 downto 16)    <= x"00" & std_logic_vector(stim_counter);
                BFdata(47 downto 32)    <= x"00" & std_logic_vector(stim_counter);
                BFdata(63 downto 48)    <= x"00" & std_logic_vector(stim_counter);
            else
                BFdata(15 downto 0)     <= x"DECC"; --x"00" & std_logic_vector(stim_counter);
                BFdata(31 downto 16)    <= x"2134"; --x"00" & std_logic_vector(stim_counter);
                BFdata(47 downto 32)    <= x"DECC"; --x"00" & std_logic_vector(stim_counter);
                BFdata(63 downto 48)    <= x"2134"; --x"00" & std_logic_vector(stim_counter);
            end if;
            
            
            if sim_runs < 2 then
                if sim_count = 1000 then
                    BFvalid             <= '1';
                    sim_count           <= sim_count + 1;
                elsif sim_count = 1864 then
                    BFvalid             <= '0';
                    sim_runs            <= sim_runs + 1;
                    sim_count           <= 900;
                    BFscale             <= std_logic_vector(unsigned(BFscale) + x"1"); 
                else    
                    
                    sim_count           <= sim_count + 1;            
                end if;
            end if;            
            
              
--            if testCount = 1000 then
--                BFvalid     <= '1';
--            elsif testCount = 1864 then
--                BFvalid <= '0';                
--            end if;

--            -- test a gap of 36 cycles between BF
--            if testCount = 1900 then
--                BFvalid <= '1';
--            elsif testCount = 2764 then
--                BFvalid <= '0';
--            end if;

--            if testCount = 1865 then
--                BFBeam          <= std_logic_vector(unsigned(BFBeam) + 1);
--                BFFreqIndex     <= std_logic_vector(unsigned(BFFreqIndex) + 1);
--            end if;
            -- create one burst from the BF.
            -- if (testCount = 1000) then
            --     BFdata(15 downto 0)     <= x"0001";
            --     BFdata(31 downto 16)    <= x"0002"; 
            --     BFdata(47 downto 32)    <= x"0004";
            --     BFdata(63 downto 48)    <= x"0005";
                
            --     BFscale <= "0000";
            -- elsif (testCount = 1001) then
            --     BFdata(15 downto 0)     <= x"0011";
            --     BFdata(31 downto 16)    <= x"0012"; 
            --     BFdata(47 downto 32)    <= x"0014";
            --     BFdata(63 downto 48)    <= x"0015";
            -- elsif (testCount = 1002) then
            --     BFdata(15 downto 0)     <= x"0021";
            --     BFdata(31 downto 16)    <= x"0022"; 
            --     BFdata(47 downto 32)    <= x"0024";
            --     BFdata(63 downto 48)    <= x"0025";
            -- elsif (testCount = 1003) then
            --     BFdata(15 downto 0)     <= x"0031";
            --     BFdata(31 downto 16)    <= x"0032"; 
            --     BFdata(47 downto 32)    <= x"0034";
            --     BFdata(63 downto 48)    <= x"0035";
            -- elsif (testCount = 1004) then
            --     BFdata(15 downto 0)     <= x"0041";
            --     BFdata(31 downto 16)    <= x"0042"; 
            --     BFdata(47 downto 32)    <= x"0044";
            --     BFdata(63 downto 48)    <= x"0045";
            -- elsif (testCount = 1005) then
            --     BFdata(15 downto 0)     <= x"0051";
            --     BFdata(31 downto 16)    <= x"0052"; 
            --     BFdata(47 downto 32)    <= x"0054";
            --     BFdata(63 downto 48)    <= x"0055";
            -- elsif (testCount = 1006) then
            --     BFdata(15 downto 0)     <= x"0061";
            --     BFdata(31 downto 16)    <= x"0062"; 
            --     BFdata(47 downto 32)    <= x"0064";
            --     BFdata(63 downto 48)    <= x"0065";
            -- elsif (testCount = 1007) then
            --     BFdata(15 downto 0)     <= x"0071";
            --     BFdata(31 downto 16)    <= x"0072"; 
            --     BFdata(47 downto 32)    <= x"0074";
            --     BFdata(63 downto 48)    <= x"0075";
            -- elsif (testCount > 1007) AND (testCount <= 1851) then -- 24 cycles for pipe alignment.
            --     BFdata(15 downto 0)     <= x"0006";
            --     BFdata(31 downto 16)    <= x"0008"; 
            --     BFdata(47 downto 32)    <= x"0017";
            --     BFdata(63 downto 48)    <= x"0019";
                
            --     BFscale <= "0000";
            -- elsif (testCount >= 1852) AND (testCount <= 1855) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"000A";
            --     BFdata(31 downto 16)    <= x"000B"; 
            --     BFdata(47 downto 32)    <= x"000C";
            --     BFdata(63 downto 48)    <= x"000D";
            -- elsif (testCount = 1856) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"001A";
            --     BFdata(31 downto 16)    <= x"001B"; 
            --     BFdata(47 downto 32)    <= x"001C";
            --     BFdata(63 downto 48)    <= x"001D";
            -- elsif (testCount = 1857) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"002A";
            --     BFdata(31 downto 16)    <= x"002B"; 
            --     BFdata(47 downto 32)    <= x"002C";
            --     BFdata(63 downto 48)    <= x"002D";
            -- elsif (testCount = 1858) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"003A";
            --     BFdata(31 downto 16)    <= x"003B"; 
            --     BFdata(47 downto 32)    <= x"003C";
            --     BFdata(63 downto 48)    <= x"003D";                                
            -- elsif (testCount = 1859) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"004A";
            --     BFdata(31 downto 16)    <= x"004B"; 
            --     BFdata(47 downto 32)    <= x"004C";
            --     BFdata(63 downto 48)    <= x"004D";
            -- elsif (testCount = 1860) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"005A";
            --     BFdata(31 downto 16)    <= x"005B"; 
            --     BFdata(47 downto 32)    <= x"005C";
            --     BFdata(63 downto 48)    <= x"005D";
            -- elsif (testCount = 1861) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"006A";
            --     BFdata(31 downto 16)    <= x"006B"; 
            --     BFdata(47 downto 32)    <= x"006C";
            --     BFdata(63 downto 48)    <= x"006D";                                
            -- elsif (testCount = 1862) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"007A";
            --     BFdata(31 downto 16)    <= x"007B"; 
            --     BFdata(47 downto 32)    <= x"007C";
            --     BFdata(63 downto 48)    <= x"007D";
            -- elsif (testCount = 1863) then                         -- 864 cycles in a BF burst
            --     BFdata(15 downto 0)     <= x"001A";
            --     BFdata(31 downto 16)    <= x"001B"; 
            --     BFdata(47 downto 32)    <= x"001C";
            --     BFdata(63 downto 48)    <= x"001D";
            -- else
            --     BFdata  <= (others => '0');
            -- end if;

            if bf_packet_count = 0 then
                BFBeam  <= std_logic_vector(unsigned(BFBeam)+1);
                bf_packet_count <= 8D"100";
            else
                bf_packet_count <= bf_packet_count - 1;
            end if;
        end if;
    end if;
end process;


-------------------------------------------------------------------------------------------------------------
 
dut : entity scaling_lib.output_scaling 
    generic map (
        g_SIM           => TRUE,
        g_DEBUG_ILA     => FALSE
    )
    port map (
        i_clk               => clock_400,
        i_rst               => clock_400_rst,

        i_BFdata            => BFdata,
        i_BFpacketCount     => BFpacketCount,
        i_BFBeam            => BFBeam,
        i_BFFreqIndex       => BFFreqIndex,
        i_BFvalid           => BFvalid,
        i_BFjones_status    => BFjones_status,
        i_BFpoly_ok         => BFpoly_ok,
	    i_BFscale	        => BFscale,

        i_scale_data        => x"00000000", --x"3f800000",

        o_BFdata            => i_beam_output_stream_1(0).beamData,
        o_BFpacketCount     => i_beam_output_stream_1(0).beamPacketCount,
        o_BFBeam            => i_beam_output_stream_1(0).beamBeam,
        o_BFFreqIndex       => i_beam_output_stream_1(0).beamFreqIndex,
        o_BFvalid           => i_beam_output_stream_1(0).beamValid,
        o_BFjones_status    => i_beam_output_stream_1(0).beamJonesStatus,
        o_BFpoly_ok         => i_beam_output_stream_1(0).beamPoly_ok

    );

-- i_beam_output_stream_1(0).beamData          <= 
-- i_beam_output_stream_1(0).beamPacketCount   <= 
-- i_beam_output_stream_1(0).beamBeam          <= 
-- i_beam_output_stream_1(0).beamFreqIndex     <= 
-- i_beam_output_stream_1(0).beamValid         <= 
-- i_beam_output_stream_1(0).beamJonesStatus   <= 
-- i_beam_output_stream_1(0).beamPoly_ok       <= 

i_beam_output_stream_1(0).weights_data      <= (others => '0');
i_beam_output_stream_1(0).weights_data_wr   <= '0';

i_beam_output_stream_1(1).beamData          <= i_beam_output_stream_1(0).beamData;
i_beam_output_stream_1(1).beamPacketCount   <= i_beam_output_stream_1(0).beamPacketCount;
i_beam_output_stream_1(1).beamBeam          <= i_beam_output_stream_1(0).beamBeam;
i_beam_output_stream_1(1).beamFreqIndex     <= i_beam_output_stream_1(0).beamFreqIndex;
i_beam_output_stream_1(1).beamValid         <= i_beam_output_stream_1(0).beamValid;
i_beam_output_stream_1(1).beamJonesStatus   <= i_beam_output_stream_1(0).beamJonesStatus;
i_beam_output_stream_1(1).beamPoly_ok       <= i_beam_output_stream_1(0).beamPoly_ok;
i_beam_output_stream_1(1).weights_data      <= (others => '0');
i_beam_output_stream_1(1).weights_data_wr   <= '0';

i_beam_output_stream_1(2).beamData          <= i_beam_output_stream_1(0).beamData;
i_beam_output_stream_1(2).beamPacketCount   <= i_beam_output_stream_1(0).beamPacketCount;
i_beam_output_stream_1(2).beamBeam          <= i_beam_output_stream_1(0).beamBeam;
i_beam_output_stream_1(2).beamFreqIndex     <= i_beam_output_stream_1(0).beamFreqIndex;
i_beam_output_stream_1(2).beamValid         <= i_beam_output_stream_1(0).beamValid;
i_beam_output_stream_1(2).beamJonesStatus   <= i_beam_output_stream_1(0).beamJonesStatus;
i_beam_output_stream_1(2).beamPoly_ok       <= i_beam_output_stream_1(0).beamPoly_ok;
i_beam_output_stream_1(2).weights_data      <= (others => '0');
i_beam_output_stream_1(2).weights_data_wr   <= '0';

i_beam_output_stream_1(3).beamData          <= i_beam_output_stream_1(0).beamData;
i_beam_output_stream_1(3).beamPacketCount   <= i_beam_output_stream_1(0).beamPacketCount;
i_beam_output_stream_1(3).beamBeam          <= i_beam_output_stream_1(0).beamBeam;
i_beam_output_stream_1(3).beamFreqIndex     <= i_beam_output_stream_1(0).beamFreqIndex;
i_beam_output_stream_1(3).beamValid         <= i_beam_output_stream_1(0).beamValid;
i_beam_output_stream_1(3).beamJonesStatus   <= i_beam_output_stream_1(0).beamJonesStatus;
i_beam_output_stream_1(3).beamPoly_ok       <= i_beam_output_stream_1(0).beamPoly_ok;
i_beam_output_stream_1(3).weights_data      <= (others => '0');
i_beam_output_stream_1(3).weights_data_wr   <= '0';
-------------------------------------------------------------------------------------------------------------
    args_stim_proc : process(clock_300)
    begin
        if rising_edge(clock_300) then
            -- 0 - enable packetiser
            -- 1 - enable test gen
            -- 2 - limited runs
            -- 3 - use defaults
            --  0x03 = Run Test gen
            --  0x09 = Run packetiser with defaults, assume BF sourced data.
            if (clock_300_rst = '1') then
                packetiser_host_bus_ctrl.instruct    <= x"00";
                packetiser_host_bus_ctrl_2.instruct  <= x"00";
                packetiser_host_bus_ctrl_3.instruct  <= x"00";
            else 
                if (testCount_300 = 1000) then
                    packetiser_host_bus_ctrl.instruct    <= x"01";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                end if;
                if (testCount_300 = 20000) then
                    packetiser_host_bus_ctrl.instruct    <= x"01";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                end if;
                if (testCount_300 = 200000) then
                    packetiser_host_bus_ctrl.instruct    <= x"01";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                end if;
            end if;
        end if;
    end process;
     
    cmac_emulator : process(clock_322)
    begin
        if rising_edge(clock_322) then
            if (testCount_322 > 15) then
                if testCount_322 = 200 or testCount_322 = 250 then
                    i_data_to_transmit_ctl.ready    <= '0';
                    i_tx_axis_tready                <= '0';
                else    
                    i_data_to_transmit_ctl.ready    <= '1';
                    i_tx_axis_tready                <= '1';
                end if;
            else
                i_data_to_transmit_ctl.ready        <= '0';   
                i_data_to_transmit_ctl.overflow     <= '0';
                i_data_to_transmit_ctl.underflow    <= '0';
                i_tx_axis_tready                    <= '0';     
            end if;
        end if;
    end process;
    
    packetiser_stream_1_host_bus_in.config_data_clk <= i_clk400;
    packetiser_stream_2_host_bus_in.config_data_clk <= i_clk400;
    packetiser_stream_3_host_bus_in.config_data_clk <= i_clk400;
    
    --- DUT/UUT
    DUT_PSR : entity PSR_Packetiser_lib.psr_packetiser100G_Top 
    Generic Map (
        g_DEBUG_ILA                 => TRUE,
        g_Number_of_streams         => 2,
        g_PST_config                => FALSE
    )
    Port Map ( 
        -- ~322 MHz
        i_cmac_clk                  => clock_322,
        i_cmac_rst                  => clock_322_rst,
        
        i_packetiser_clk            => i_clk400,
        i_packetiser_rst            => clock_400_rst,
        
        -- Lbus to MAC
        o_data_to_transmit          => o_data_to_transmit,
        i_data_to_transmit_ctl      => i_data_to_transmit_ctl,
        
        -- AXI to CMAC interface to be implemented
        o_tx_axis_tdata             => o_tx_axis_tdata,
        o_tx_axis_tkeep             => o_tx_axis_tkeep,
        o_tx_axis_tvalid            => o_tx_axis_tvalid,
        o_tx_axis_tlast             => o_tx_axis_tlast,
        o_tx_axis_tuser             => o_tx_axis_tuser,
        i_tx_axis_tready            => i_tx_axis_tready,
        
        -- beamformer output streams
        i_beam_output_stream_1      => i_beam_output_stream_1,
        i_beam_output_stream_2      => i_beam_output_stream_2,

        -- signals from signal processing/HBM/the moon/etc
        i_packet_stream_ctrl(0)     => packetiser_host_bus_ctrl,
        i_packet_stream_ctrl(1)     => packetiser_host_bus_ctrl_2,
        i_packet_stream_ctrl(2)     => packetiser_host_bus_ctrl_3,
        
        o_packet_stream_stats       => packet_stream_stats,
                
        i_packet_stream(0)          => beamformer_to_packetiser_data,
        i_packet_stream(1)          => beamformer_to_packetiser_data,
        i_packet_stream(2)          => beamformer_to_packetiser_data,
        o_packet_stream_out         => open,
        
        -- AXI BRAM to packetiser
        i_packet_config_in_stream_1 => packetiser_stream_1_host_bus_in,
        i_packet_config_in_stream_2 => packetiser_stream_2_host_bus_in,
        i_packet_config_in_stream_3 => packetiser_stream_3_host_bus_in,
        
        -- AXI BRAM return path from packetiser 
        o_packet_config_stream_1    => packetiser_host_bus_out.config_data_out,
        o_packet_config_stream_2    => packetiser_host_bus_out_2.config_data_out,
        o_packet_config_stream_3    => packetiser_host_bus_out_3.config_data_out
        
    );
    
    --------------------------------------------------------------------------------------------------
    beamformer_to_packetiser_data.data_clk                  <= i_clk400;
    beamformer_to_packetiser_data.data_in_wr                <= '0'; -- AND (NOT beamformer_to_packetiser_data.PST_beam(0));
    beamformer_to_packetiser_data.data                      <= (others => '0'); 
    beamformer_to_packetiser_data.bytes_to_transmit         <= (others => '0');
    
    beamformer_to_packetiser_data.PST_beam_freq_index       <= (others => '0');
    beamformer_to_packetiser_data.PST_beam                  <= (others => '0');
    beamformer_to_packetiser_data.PST_time_ref              <= (others => '0');
    beamformer_to_packetiser_data.PST_jones                 <= (others => '0');
    beamformer_to_packetiser_data.PST_delay_poly            <= (others => '0');


    --------------------------------------------------------------------------------------------------
packetiser_host : entity PSR_Packetiser_lib.cmac_args_axi_wrapper 
    Port Map ( 
    
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          => clock_300,
        i_MACE_rst                          => clock_300_rst,
        
        i_packetiser_clk                    => i_clk400,
        
        i_PSR_packetiser_Lite_axi_mosi      => i_PSR_packetiser_Lite_axi_mosi(0),
        o_PSR_packetiser_Lite_axi_miso      => o_PSR_packetiser_Lite_axi_miso(0),
        
        i_axi_mosi                          => i_PSR_packetiser_Full_axi_mosi(0),
        o_axi_miso                          => o_PSR_packetiser_Full_axi_miso(0),
        
        o_packet_stream_ctrl                => packetiser_host_bus_ctrl_hold,
                
        i_packet_stream_stats               => packet_stream_stats(0),
                
        o_packet_config                     => packetiser_stream_1_host_bus_in_hold,
        i_packet_config_out                 => packetiser_host_bus_out

    );


end Behavioral;
