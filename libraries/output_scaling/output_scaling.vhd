----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: June 2024
-- Module Name: output_scaling
-- Description: 
--  Scaling to 8 bit
--
-- Data in per clock = Beam Jones output data order = dual pol 16+16 bit complex samples.
-- A_I = 15:0, A_Q = 31:16, B_I = 47:32, B_Q = 63:48
-- Input data also includes a 4-bit scale factor = S 
-- Processing :
-- Pad 16-bit data to 32 bits : P32 = P16 * 2^S
-- Compute RMS across the full packet R = sqrt(sum(P32^2)/N_samples)
-- N_samples = (54 channels)*(16 times)*(2 pol)*(2 (re+im)) = 3456
-- Compute scale factor Scale = 11/R   (target rms for samples in the output packet is 8 to 13)
-- Apply scale to the data, i.e. scaled_data = (11/R)*P32
-- Convergent round and saturate scaled_data to 8 bit values.
-- Output the packet in PSR packet order 
--
-- relative weights are true for once beam 1 is coming out and is true for all.
-- to be appended to start of data stream.
-- burst of 54 clocks with one 16-bit value per clock, happens at the start of each burst of packets from the beamformers
-- might overlap with the beamformer packet for the first beam
--
-- assume 8 cycles between packets from BF, and that data is streaming.
----------------------------------------------------------------------------------

library IEEE, common_lib, scaling_lib, signal_processing_common;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE scaling_lib.scaling_pkg.ALL;

entity output_scaling is
    generic(
        g_SIM           : boolean := FALSE;
        g_DEBUG_ILA     : boolean := FALSE
    );
    port(
        i_clk               : in std_logic;
        i_rst               : in std_logic;

        i_BFdata            : in std_logic_vector(63 downto 0);
        i_BFpacketCount     : in std_logic_vector(47 downto 0);
        i_BFBeam            : in std_logic_vector(9 downto 0);
        i_BFFreqIndex       : in std_logic_vector(10 downto 0);
        i_BFjones_status    : in std_logic_vector(3 downto 0);
        i_BFpoly_ok         : in std_logic_vector(1 downto 0);
	    i_BFscale	        : in std_logic_vector(3 downto 0);
        i_BFvalid           : in std_logic;

        i_scale_data        : in std_logic_vector(31 downto 0);     -- over ride value for calculated RMS, 32 bit, FP-SP
                                                                    -- only use when not zero.

        o_BFdata            : out std_logic_vector(63 downto 0);
        o_BFpacketCount     : out std_logic_vector(47 downto 0);
        o_BFBeam            : out std_logic_vector(9 downto 0);
        o_BFFreqIndex       : out std_logic_vector(10 downto 0);
        o_BFjones_status    : out std_logic_vector(3 downto 0);
        o_BFpoly_ok         : out std_logic_vector(1 downto 0);
        o_BFvalid           : out std_logic

    );

    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of output_scaling : entity is "yes";
    
end output_scaling;

architecture Behavioral of output_scaling is

component fp32_int8 is
    Port ( 
      aclk                  : in STD_LOGIC;
      s_axis_a_tvalid       : in STD_LOGIC;
      s_axis_a_tdata        : in STD_LOGIC_VECTOR ( 31 downto 0 );
      m_axis_result_tvalid  : out STD_LOGIC;
      m_axis_result_tdata   : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

component fixed32_to_sp_float is
    Port ( 
        aclk                    : in STD_LOGIC;
        s_axis_a_tvalid         : in STD_LOGIC;
        s_axis_a_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        m_axis_result_tvalid    : out STD_LOGIC;
        m_axis_result_tdata     : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

component fp32_mult is
    Port ( 
        aclk                    : in STD_LOGIC;
        s_axis_a_tvalid         : in STD_LOGIC;
        s_axis_a_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s_axis_b_tvalid         : in STD_LOGIC;
        s_axis_b_tdata          : in STD_LOGIC_VECTOR ( 31 downto 0 );
        m_axis_result_tvalid    : out STD_LOGIC;
        m_axis_result_tdata     : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
end component;

type meta_rec is record
    packetCount     : STD_LOGIC_VECTOR(47 DOWNTO 0);    -- 48
    Beam            : STD_LOGIC_VECTOR(9 DOWNTO 0);     -- 10
    FreqIndex       : STD_LOGIC_VECTOR(10 DOWNTO 0);    -- 11
    jones_status    : STD_LOGIC_VECTOR(3 DOWNTO 0);     -- 4
    poly_ok         : STD_LOGIC_VECTOR(1 DOWNTO 0);     -- 2
end record;

------------------------------------------------------
-- Sim signals
signal sim_ftof_a_i         : real := 0.0;
signal sim_scaler           : real := 0.0;
signal sim_scaler_rms       : real := 0.0;
signal sim_a_i_scaled       : real := 0.0;
signal sim_ftof_d           : real := 0.0;

------------------------------------------------------
constant meta_array         : integer := 3;
TYPE t_meta_rec_arr IS ARRAY(0 TO (meta_array-1)) OF meta_rec;
signal meta_rec_del : t_meta_rec_arr;

signal rst  : std_logic;

signal BFdata           : std_logic_vector(63 downto 0);
signal BFpacketCount    : std_logic_vector(39 downto 0);
signal BFBeam           : std_logic_vector(7 downto 0);
signal BFFreqIndex      : std_logic_vector(10 downto 0);
signal BFvalid          : std_logic;
signal BFjones_status   : std_logic_vector(1 downto 0);
signal BFpoly_ok        : std_logic_vector(1 downto 0);
signal BFscale	        : std_logic_vector(3 downto 0);

signal input_data               : t_slv_16_arr(3 downto 0);
signal scaled_pre_rms_data      : t_slv_32_arr(3 downto 0);
signal scaled_pre_rms_valid     : std_logic_vector(3 downto 0);

signal scaled_value_rms : std_logic_vector(31 downto 0);
signal scaled_value     : std_logic_vector(31 downto 0);
signal scaled_ready     : std_logic;

signal scale_data_int       : std_logic_vector(31 downto 0);
signal scale_data_test      : std_logic_vector(31 downto 0);
signal use_scale_software   : std_logic;


signal BFvalid_del          : std_logic;
signal buffer_fifo_rd_del   : std_logic;
----------
-- data path
constant URAM_CACHE_WIDTH   : integer := 68;
signal uram_in_data     : std_logic_vector((URAM_CACHE_WIDTH-1) downto 0);
signal uram_in_addr     : unsigned(9 downto 0);
signal uram_in_wr       : std_logic;

signal uram_out_data    : std_logic_vector((URAM_CACHE_WIDTH-1) downto 0);
signal uram_out_addr    : unsigned(9 downto 0);

signal uram_wr_page     : unsigned(1 downto 0);
signal uram_rd_page     : unsigned(1 downto 0);

type processing_statemachine is (IDLE, RUN, FINISH);
signal processing_sm : processing_statemachine;

signal data_delay_vld   : std_logic_vector(3 downto 0);

signal scale_for_dp     : std_logic_vector(3 downto 0);

signal dp_data          : t_slv_16_arr(3 downto 0);
signal dp_data_valid    : std_logic;

signal dp_inc_scaled_data   : t_slv_32_arr(3 downto 0);
signal dp_inc_scaled_valid  : std_logic_vector(3 downto 0);

signal dp_ftf_data      : t_slv_32_arr(3 downto 0);
signal dp_ftf_valid     : std_logic_vector(3 downto 0);

signal dp_scaled_data   : t_slv_32_arr(3 downto 0);
signal dp_scaled_valid  : std_logic_vector(3 downto 0);

signal dp_int8_data    : t_slv_32_arr(3 downto 0);
signal dp_int8_clipped : t_slv_8_arr(3 downto 0);
signal dp_int8_valid   : std_logic_vector(3 downto 0);

----------------------------------------------------------
-- Output CT - FIFOs
constant BUFFER_FIFO_DEPTH      : integer := 512;
constant BUFFER_FIFO_WIDTH      : integer := 64;
signal buffer_fifo_rd_count     : std_logic_vector(ceil_log2(BUFFER_FIFO_DEPTH) downto 0);
signal buffer_fifo_data         : std_logic_vector((BUFFER_FIFO_WIDTH - 1) downto 0);
signal buffer_fifo_q            : std_logic_vector((BUFFER_FIFO_WIDTH - 1) downto 0);
signal buffer_fifo_rd           : std_logic;
signal buffer_fifo_wr           : std_logic;
signal buffer_data_sel          : std_logic;
signal buffer_fifo_empty        : std_logic;

constant POLA_FIFO_DEPTH        : integer := 1024;
constant POLA_FIFO_WIDTH        : integer := 64;
signal polA_fifo_rd_count       : std_logic_vector(ceil_log2(POLA_FIFO_DEPTH) downto 0);
signal polA_fifo_data           : std_logic_vector((POLA_FIFO_WIDTH - 1) downto 0);
signal polA_fifo_q              : std_logic_vector((POLA_FIFO_WIDTH - 1) downto 0);
signal polA_fifo_rd             : std_logic;
signal polA_fifo_wr, polA_fifo_wr_adv : std_logic;
signal polA_fifo_empty          : std_logic;

constant POLB_FIFO_DEPTH        : integer := 1024;
constant POLB_FIFO_WIDTH        : integer := 64;
signal polB_fifo_rd_count       : std_logic_vector(ceil_log2(POLB_FIFO_DEPTH) downto 0);
signal polB_fifo_data           : std_logic_vector((POLB_FIFO_WIDTH - 1) downto 0);
signal polB_fifo_q              : std_logic_vector((POLB_FIFO_WIDTH - 1) downto 0);
signal polB_fifo_rd             : std_logic;
signal polB_fifo_wr, polB_fifo_wr_adv : std_logic;
signal polB_fifo_empty          : std_logic;

signal pol_stack_up_cnt         : unsigned(1 downto 0);

type output_ct_statemachine is (IDLE, WR_SCALE, PRE_DRAIN, DRAIN, CLEANUP, FINISH);
signal output_ct_sm : output_ct_statemachine := IDLE;

signal buffer_fifo_data_cache   : std_logic_vector(63 downto 0) := (others => '0');
signal buffer_wr_count          : unsigned(11 downto 0) := (others => '0');
signal buffer_loop_cnt          : unsigned(2 downto 0);
signal buffer_fine_chan_cnt     : unsigned(5 downto 0);
signal enable_buffer_wr         : std_logic;
signal enable_buffer_wr_cnt     : std_logic;

constant META_FIFO_DEPTH        : integer := 16;
constant META_FIFO_WIDTH        : integer := 75;
signal meta_fifo_rd_count       : std_logic_vector(ceil_log2(META_FIFO_DEPTH) downto 0);
signal meta_fifo_data           : std_logic_vector((META_FIFO_WIDTH - 1) downto 0);
signal meta_fifo_q              : std_logic_vector((META_FIFO_WIDTH - 1) downto 0);
signal meta_fifo_rd             : std_logic;
signal meta_fifo_wr             : std_logic;
signal meta_fifo_empty          : std_logic;

signal scaled_value_exp, scaled_value_exp_plus9 : std_logic_vector(7 downto 0);
signal scaled_value_x512 : std_logic_vector(31 downto 0);
signal scale_is_inf : std_logic := '0';

begin
-----------------------------------------------------

rst     <= i_rst;

reg_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        BFdata          <= i_BFdata;

        input_data(0)   <= i_BFdata(15 downto 0);
        input_data(1)   <= i_BFdata(31 downto 16);
        input_data(2)   <= i_BFdata(47 downto 32);
        input_data(3)   <= i_BFdata(63 downto 48);

        BFdata          <= i_BFdata;
        BFvalid         <= i_BFvalid;
        BFscale	        <= i_BFscale;

        BFvalid_del         <= BFvalid;
        buffer_fifo_rd_del  <= buffer_fifo_rd;

        meta_fifo_data   <= i_BFpacketCount &
                            i_BFBeam &
                            i_BFFreqIndex &
                            i_BFjones_status &
                            i_BFpoly_ok;

        o_BFpacketCount     <= meta_fifo_q(74 downto 27);
        o_BFBeam            <= meta_fifo_q(26 downto 17);
        o_BFFreqIndex       <= meta_fifo_q(16 downto 6);
        o_BFjones_status    <= meta_fifo_q(5 downto 2);
        o_BFpoly_ok         <= meta_fifo_q(1 downto 0);
        
        o_BFdata            <= buffer_fifo_q;
        o_BFvalid           <= buffer_fifo_rd;

        if BFvalid = '1' and BFvalid_del = '0' then
            meta_fifo_wr <= '1';
        else
            meta_fifo_wr <= '0';
        end if;

        if buffer_fifo_rd_del = '1' and buffer_fifo_rd = '0' then
            meta_fifo_rd <= '1';
        else
            meta_fifo_rd <= '0';
        end if;

    end if;
end process;



meta_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH          => META_FIFO_DEPTH,
        DATA_WIDTH          => META_FIFO_WIDTH,
        FIFO_MEMORY_TYPE    => "distributed"
    )
    Port Map ( 
        fifo_reset      => rst,
        -- RD    
        fifo_rd_clk     => i_clk,
        fifo_rd         => meta_fifo_rd,
        fifo_q          => meta_fifo_q,
        fifo_q_valid    => open,
        fifo_empty      => meta_fifo_empty,
        fifo_rd_count   => meta_fifo_rd_count,
        -- WR        
        fifo_wr_clk     => i_clk,
        fifo_wr         => meta_fifo_wr,
        fifo_data       => meta_fifo_data,
        fifo_full       => open,
        fifo_wr_count   => open
    );

-----------------------------------------------------
--
-- sum all the values as they come in.
-- signed 16 bit to a signed 32 bit.
--



-----------------------------------------------------
-- RMS to workout scaling value path.
-- pad data to 32 bit

pad_instance : for i in 0 to 3 GENERATE
    pad_inst : entity scaling_lib.pad_32bit port map (
            i_clk       => i_clk,
            i_rst       => rst,

            i_data      => input_data(i),
            i_valid     => BFvalid,
            i_scale	    => BFscale,

            o_data      => scaled_pre_rms_data(i),
            o_valid     => scaled_pre_rms_valid(i)
        );
END GENERATE;

i_rms : entity scaling_lib.rms 
    generic map (
        g_SIM               => g_SIM,
        g_DEBUG_ILA         => g_DEBUG_ILA
    )
    port map (
        i_clk               => i_clk,
        i_rst               => rst,

        i_rms_data          => scaled_pre_rms_data,
        i_valid             => scaled_pre_rms_valid(0),
	
        o_scale_ready       => scaled_ready,
        o_scale_value       => scaled_value_rms
    );

-----------------------------------------------------
-- Data path
data_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        if rst = '1' then
            uram_wr_page    <= "00";
            uram_in_addr    <= (others => '0');
        else
            uram_in_data    <= BFScale & BFdata;
            uram_in_wr      <= BFvalid;

            if uram_in_wr = '1' then
                uram_in_addr <= uram_in_addr + 1;
            else
                uram_in_addr <= (others => '0');
            end if;
            if uram_in_wr = '1' AND BFvalid = '0' then
                uram_wr_page <= uram_wr_page + 1;
            end if;

        end if;
    end if;
end process;

i_uram_inc_buffer : entity signal_processing_common.memory_tdp_wrapper
    GENERIC MAP (
        MEMORY_PRIMITIVE    => "ultra", -- "auto", "distributed", "block" or "ultra" ;
        CLOCKING_MODE       => "common_clock",
        g_NO_OF_ADDR_BITS   => 12,
        g_D_Q_WIDTH         => URAM_CACHE_WIDTH,
        g_READ_LATENCY_B    => 3
    )
    PORT MAP( 
        clk_a           => i_clk,
        clk_b           => i_clk,
    
        data_a                  => uram_in_data,
        addr_a(11 downto 10)    => std_logic_vector(uram_wr_page),
        addr_a(9 downto 0)      => std_logic_vector(uram_in_addr),
        data_a_wr               => uram_in_wr,
        data_a_q                => open,

        data_b                  => ( others => '0' ),
        addr_b(11 downto 10)    => std_logic_vector(uram_rd_page),
        addr_b(9 downto 0)      => std_logic_vector(uram_out_addr),
        data_b_wr               => '0',
        data_b_q                => uram_out_data
    
    );

process_data_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        if rst = '1' then
            processing_sm   <= IDLE;
            uram_rd_page    <= "00";
            uram_out_addr   <= (others => '0');
            data_delay_vld  <= (others => '0');
        else
            
            case processing_sm is
                when IDLE =>
                    if scaled_ready = '1' then
                        processing_sm       <= RUN;
                        
                    end if;

                when RUN =>
                    if uram_out_addr = 863 then
                        uram_out_addr       <= (others => '0');
                        processing_sm       <= FINISH;
                        data_delay_vld(0)   <= '0';
                    else
                        uram_out_addr       <= uram_out_addr + 1;
                        data_delay_vld(0)   <= '1';
                    end if;
                    
                when FINISH =>
                    uram_rd_page        <= uram_rd_page + 1;
                    processing_sm       <= IDLE;

                when OTHERS => processing_sm <= IDLE;
            end case;

            data_delay_vld(3 downto 1)  <= data_delay_vld(2 downto 0);
            dp_data_valid   <= data_delay_vld(2) OR data_delay_vld(3);
            dp_data(0)      <= uram_out_data(15 downto 0);
            dp_data(1)      <= uram_out_data(31 downto 16);
            dp_data(2)      <= uram_out_data(47 downto 32);
            dp_data(3)      <= uram_out_data(63 downto 48);
            scale_for_dp    <= uram_out_data(67 downto 64);

        end if;
    end if;
end process;

----------------------------------------------------------------------
-- select scale value, if input port is 0.0 use calculated otherwise use input which is set by software.
scale_sel_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        scale_data_int      <= i_scale_data;
        scale_data_test     <= i_scale_data;

        if (unsigned(scale_data_test) = 0) then
            use_scale_software  <= '0';
        else
            use_scale_software  <= '1';
        end if;

        if (use_scale_software = '0') then
            scaled_value <= scaled_value_rms;
        else
            scaled_value <= scale_data_int;
        end if;
    end if;
end process;

-- scale and F to F and Multi
scaler_gen : for i in 0 to 3 GENERATE
    dp_scaling : entity scaling_lib.pad_32bit port map (
            i_clk       => i_clk,
            i_rst       => rst,

            i_data      => dp_data(i),
            i_valid     => dp_data_valid,
            i_scale	    => scale_for_dp,

            o_data      => dp_inc_scaled_data(i),
            o_valid     => dp_inc_scaled_valid(i)
        );

    i_ftof : fixed32_to_sp_float port map (
            aclk                    => i_clk,

            s_axis_a_tdata          => dp_inc_scaled_data(i),
            s_axis_a_tvalid         => dp_inc_scaled_valid(i),

            m_axis_result_tdata     => dp_ftf_data(i),
            m_axis_result_tvalid    => dp_ftf_valid(i)
        );

    i_multi_scale_factor : fp32_mult port map ( 
            aclk                    => i_clk,

            s_axis_a_tvalid         => dp_ftf_valid(i),
            s_axis_a_tdata          => dp_ftf_data(i),
            s_axis_b_tvalid         => dp_ftf_valid(i),
            s_axis_b_tdata          => scaled_value,

            m_axis_result_tvalid    => dp_scaled_valid(i),
            m_axis_result_tdata     => dp_scaled_data(i)
        );

    i_f_to_int : fp32_int8 port map ( 
            aclk                    => i_clk,
            s_axis_a_tvalid         => dp_scaled_valid(i),
            s_axis_a_tdata          => dp_scaled_data(i),
            m_axis_result_tvalid    => dp_int8_valid(i),
            m_axis_result_tdata     => dp_int8_data(i)
        );
        
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            if signed(dp_int8_data(i)) > 127 then
                dp_int8_clipped(i) <= "01111111";
            elsif signed(dp_int8_data(i)) < -127 then
                dp_int8_clipped(i) <= "10000000";
            else
                dp_int8_clipped(i) <= dp_int8_data(i)(7 downto 0);
            end if;
        end if;
    end process;    
        
END GENERATE;

------------------------------------------------------------------------
-- A_I = 15:0, A_Q = 31:16, B_I = 47:32, B_Q = 63:48

-- stack up the data to 8 bytes.
stack_pol_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        
        
        polA_fifo_data(63 downto 16)    <= polA_fifo_data(47 downto 0);        
        -- Real part is in (0), imaginary is in (1), real comes first in the output packet.
        polA_fifo_data(15 downto 0) <= dp_int8_clipped(0) & dp_int8_clipped(1);

        polA_fifo_wr_adv <= pol_stack_up_cnt(1) AND pol_stack_up_cnt(0); --dp_int8_valid(0);
        polA_fifo_wr <= polA_fifo_wr_adv;

        polB_fifo_data(63 downto 16) <= polB_fifo_data(47 downto 0);
        polB_fifo_data(15 downto 0)  <= dp_int8_clipped(2) & dp_int8_clipped(3);

        polB_fifo_wr_adv <= pol_stack_up_cnt(1) AND pol_stack_up_cnt(0); --dp_int8_valid(0);
        polB_fifo_wr <= polB_fifo_wr_adv;

        if dp_int8_valid(0) = '1' then
            pol_stack_up_cnt <= pol_stack_up_cnt + 1;
        else
            pol_stack_up_cnt <= "00";
        end if;

    end if;
end process;


Pol_A_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH      => POLA_FIFO_DEPTH,
        DATA_WIDTH      => POLA_FIFO_WIDTH
    )
    Port Map ( 
        fifo_reset      => rst,
        -- RD    
        fifo_rd_clk     => i_clk,
        fifo_rd         => polA_fifo_rd,
        fifo_q          => polA_fifo_q,
        fifo_q_valid    => open,
        fifo_empty      => polA_fifo_empty,
        fifo_rd_count   => polA_fifo_rd_count,
        -- WR        
        fifo_wr_clk     => i_clk,
        fifo_wr         => polA_fifo_wr,
        fifo_data       => polA_fifo_data,
        fifo_full       => open,
        fifo_wr_count   => open
    );

Pol_B_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH      => POLB_FIFO_DEPTH,
        DATA_WIDTH      => POLB_FIFO_WIDTH
    )
    Port Map ( 
        fifo_reset      => rst,
        -- RD    
        fifo_rd_clk     => i_clk,
        fifo_rd         => polB_fifo_rd,
        fifo_q          => polB_fifo_q,
        fifo_q_valid    => open,
        fifo_empty      => polB_fifo_empty,
        fifo_rd_count   => polB_fifo_rd_count,
        -- WR        
        fifo_wr_clk     => i_clk,
        fifo_wr         => polB_fifo_wr,
        fifo_data       => polB_fifo_data,
        fifo_full       => open,
        fifo_wr_count   => open
    );

buffer_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH      => BUFFER_FIFO_DEPTH,
        DATA_WIDTH      => BUFFER_FIFO_WIDTH
    )
    Port Map ( 
        fifo_reset      => rst,
        -- RD    
        fifo_rd_clk     => i_clk,
        fifo_rd         => buffer_fifo_rd,
        fifo_q          => buffer_fifo_q,
        fifo_q_valid    => open,
        fifo_empty      => buffer_fifo_empty,
        fifo_rd_count   => buffer_fifo_rd_count,
        -- WR        
        fifo_wr_clk     => i_clk,
        fifo_wr         => buffer_fifo_wr,
        fifo_data       => buffer_fifo_data,
        fifo_full       => open,
        fifo_wr_count   => open
    );

buffer_fifo_rd  <= NOT buffer_fifo_empty when output_ct_sm = FINISH else '0';

polA_fifo_rd        <= (NOT buffer_loop_cnt(2)) when output_ct_sm = DRAIN else '0';
polB_fifo_rd        <= (buffer_loop_cnt(2)) when output_ct_sm = DRAIN else '0';

-- buffer_fifo_data(63 downto 16)  <=  buffer_fifo_data_cache(63 downto 16);
-- buffer_fifo_data(15 downto 0)   <=  polA_fifo_q when (buffer_loop_cnt(4) = '0') else
--                                     polB_fifo_q;

buffer_fifo_data    <= buffer_fifo_data_cache;

-- Adjust the exponent in the floating point scale factor by +9
-- This compensates for the scaling through the signal chain, so that 
-- the rms output level in a fine channel is the same as the rms input level, 
-- except for scaling due to the number of stations accumulated, and Jones matrix settings.
-- For 32-bit float, the exponent is bits 30:23
scaled_value_exp <= scaled_value(30 downto 23);
scaled_value_exp_plus9 <= std_logic_vector(unsigned(scaled_value_exp) + 9);

scaled_value_x512(31) <= scaled_value(31);
scaled_value_x512(30 downto 23) <= "11111111" when scaled_value_exp = "11111111" else scaled_value_exp_plus9;
scaled_value_x512(22 downto 0) <= scaled_value(22 downto 0);

buffer_wr_proc : process(i_clk)
begin
    if rising_edge(i_clk) then
        if buffer_loop_cnt(2) = '0' AND polA_fifo_rd = '1' then
            if scale_is_inf = '1' then
                buffer_fifo_data_cache <= (others => '0');
            else
                buffer_fifo_data_cache <= polA_fifo_q;
            end if;
        elsif (buffer_loop_cnt(2) = '1' AND polB_fifo_rd = '1') OR (output_ct_sm = CLEANUP) or (output_ct_sm = FINISH) then
            if scale_is_inf = '1' then
                buffer_fifo_data_cache <= (others => '0');
            else
                buffer_fifo_data_cache <= polB_fifo_q;
            end if;
        elsif (output_ct_sm = IDLE) then
            buffer_fifo_data_cache <= x"DEADBEEF" & scaled_value_x512;
            if scaled_value_exp = "11111111" then
                scale_is_inf <= '1';
            else
                scale_is_inf <= '0';
            end if;
        end if;
        

        if rst = '1' then
            buffer_fifo_wr  <= '0';
            buffer_data_sel <= '0';
            output_ct_sm    <= IDLE;

            buffer_wr_count         <= (others => '0');

            buffer_loop_cnt         <= (others => '0');
            buffer_fine_chan_cnt    <= (others => '0');
            enable_buffer_wr        <= '0';
            enable_buffer_wr_cnt    <= '0';
        else

            if (output_ct_sm = DRAIN) OR (output_ct_sm = CLEANUP) then --or (output_ct_sm = FINISH) then
                buffer_loop_cnt         <= buffer_loop_cnt + 1;
                if buffer_loop_cnt = 7 then
                    buffer_fine_chan_cnt <= buffer_fine_chan_cnt + 1;
                end if;
            end if;

            -- offset the writing pattern to pre-align it for the packetiser.
            -- 108 bytes of weights precedes this. Value of 3 will align data to 64 bits, 1 = first word is 00 half data.
            if (output_ct_sm = DRAIN) OR (output_ct_sm = CLEANUP) or (output_ct_sm = WR_SCALE) then
                buffer_fifo_wr <= '1';
            else
                buffer_fifo_wr <= '0';
            end if;

            case output_ct_sm is
                when IDLE =>
                    -- 128 a starting value, can probably be trimmed down...
                    if unsigned(polA_fifo_rd_count) > 128 then
                        output_ct_sm    <= WR_SCALE;
                    end if;
                    buffer_data_sel         <= '0';
                    buffer_loop_cnt         <= (others => '0');
                    buffer_fine_chan_cnt    <= (others => '0');
                    buffer_wr_count         <= (others => '0');
                    enable_buffer_wr        <= '0';

                when WR_SCALE => 
                    output_ct_sm    <= PRE_DRAIN;
                    --buffer_fifo_wr  <= '1';
                
                when PRE_DRAIN =>
                    output_ct_sm    <= DRAIN;
                    --buffer_fifo_wr  <= '0';

                when DRAIN =>
                    -- buffer_loop_cnt         <= buffer_loop_cnt + 1;
                     if buffer_loop_cnt = 7 then
                         if buffer_fine_chan_cnt = 53 then
                             output_ct_sm    <= CLEANUP;
                    --     else
                    --         buffer_fine_chan_cnt <= buffer_fine_chan_cnt + 1;
                         end if;
                     end if;

                    -- -- offset the writing pattern to pre-align it for the packetiser.
                    -- -- 108 bytes of weights precedes this. Value of 3 will align data to 64 bits, 1 = first word is 00 half data.
                    -- if buffer_loop_cnt(1 downto 0) = "01" then
                    --     buffer_fifo_wr <= '1';
                    -- else
                    --     buffer_fifo_wr <= '0';
                    -- end if;

                when CLEANUP =>
                    buffer_fifo_wr  <= '0';
                    output_ct_sm    <= FINISH;


                when FINISH =>
                    if buffer_fifo_empty = '1' then
                        output_ct_sm    <= IDLE;
                    end if;

                when OTHERS => output_ct_sm <= IDLE;
            end case;

        end if;
    end if;
end process;


------------------------------------------------------
-- Sim signals
gen_sim : if g_SIM GENERATE
    sim_ftof_a_i        <= float2real(dp_ftf_data(0));
    sim_scaler          <= float2real(scaled_value);
    sim_scaler_rms      <= float2real(scaled_value_rms);
    sim_a_i_scaled      <= float2real(dp_scaled_data(0));
END GENERATE;
------------------------------------------------------

END Behavioral;
