----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 09.11.2020 09:40:15
-- Module Name: jonesMatrixMult - Behavioral
-- Description: 
--  Full parallel 2x2 matrix-vector multiplier. 
--  
-- Scaling 
--  "16384" is treated as unity for the matrix values.
--  i.e. 
--    [(16384 + 0j) 0         ]
--    [ 0           (16384+0j)]
--  results in the output being the same as the input (o_r0real = i_v0real etc).
--
--  There are combinations of inputs that could result in overflow. 
--  If there is an overflow, the "flagged" bit will be set in the output.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity jonesMatrixMult_16b is
    Port(
        i_clk : in std_logic;
        -- The 2x2 matrix
        i_m00real : in std_logic_vector(15 downto 0);
        i_m00imag : in std_logic_vector(15 downto 0);
        i_m01real : in std_logic_vector(15 downto 0);
        i_m01imag : in std_logic_vector(15 downto 0);
        i_m10real : in std_logic_vector(15 downto 0);
        i_m10imag : in std_logic_vector(15 downto 0);
        i_m11real : in std_logic_vector(15 downto 0);
        i_m11imag : in std_logic_vector(15 downto 0);
        -- 2x1 vector
        i_v0real : in std_logic_vector(15 downto 0);
        i_v0imag : in std_logic_vector(15 downto 0);
        i_v1real : in std_logic_vector(15 downto 0);
        i_v1imag : in std_logic_vector(15 downto 0);
        i_rfi    : in std_logic;
        -- 2x1 vector result (7 clock latency)
        o_r0real : out std_logic_vector(15 downto 0);
        o_r0imag : out std_logic_vector(15 downto 0);
        o_r1real : out std_logic_vector(15 downto 0);
        o_r1imag : out std_logic_vector(15 downto 0);
        -- Flag is propagated through from i_rfi and will also be set if there is an arithmetic overflow in the matrix multiplication
        o_flag   : out std_logic
    );
end jonesMatrixMult_16b;

architecture Behavioral of jonesMatrixMult_16b is

    -- convergent_round
    -- Selects bits (29:14) from the input, and applies convergent rounding based on bits 13:0
    function convergent_round(din : std_logic_vector(35 downto 0)) return std_logic_vector is
    begin
        if (din(13 downto 0) = "10000000000000" and din(14) = '1') or (din(13) = '1' and din(12 downto 0) /= "0000000000000") then
            return std_logic_vector(unsigned(din(29 downto 14)) + 1);
        else
            return din(29 downto 14);
        end if;
    end convergent_round;

    -- Will there be overflow when we select bits 29:14 ?
    function overflow_detect(din : std_logic_vector(35 downto 0)) return std_logic is
    begin
        if din(29 downto 13) = "01111111111111111" then
            return '1'; -- after the +1 in the rounding, it will overflow
        elsif (din(35 downto 29) = "0000000" or din(35 downto 29) = "1111111") then
            return '0'; -- value is in range when selecting bits 29:14
        else
            return '1';
        end if;
    end overflow_detect;

    -- create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name complexMult16x8
    -- set_property -dict [list CONFIG.Component_Name {complexMult16x8} CONFIG.BPortWidth {16} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {33} CONFIG.MinimumLatency {4}] [get_ips complexMult16x8]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/complexMult16x8/complexMult16x8.xci]
    
    component complexMult16x16
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(79 DOWNTO 0));  -- real output in bits 32:0, imaginary output in bits 72:40
    end component;
    
    signal m00, m01, m10, m11 : std_logic_vector(31 downto 0);
    signal v0, v1 : std_logic_vector(31 downto 0);
    signal m00v0, m01v1, m10v0, m11v1 : std_logic_vector(79 downto 0);
    signal m00v0real, m00v0imag : std_logic_vector(35 downto 0);
    signal m01v1real, m01v1imag : std_logic_vector(35 downto 0);
    signal m10v0real, m10v0imag : std_logic_vector(35 downto 0);
    signal m11v1real, m11v1imag : std_logic_vector(35 downto 0);
    
    signal r0real, r0imag, r1real, r1imag : std_logic_vector(35 downto 0);
    signal r0real_16bit, r0imag_16bit, r1real_16bit, r1imag_16bit : std_logic_vector(15 downto 0);
    signal r0real_overflow, r0imag_overflow, r1real_overflow, r1imag_overflow : std_logic;
    signal rfi_del1, rfi_del2, rfi_del3, rfi_del4, rfi_del5, rfi_del6 : std_logic;
    
begin
    
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            r0real <= std_logic_vector(signed(m00v0real) + signed(m01v1real));
            r0imag <= std_logic_vector(signed(m00v0imag) + signed(m01v1imag));
            
            r1real <= std_logic_vector(signed(m10v0real) + signed(m11v1real));
            r1imag <= std_logic_vector(signed(m10v0imag) + signed(m11v1imag));
            
            r0real_16bit <= convergent_round(r0real);
            r0imag_16bit <= convergent_round(r0imag);
            r1real_16bit <= convergent_round(r1real);
            r1imag_16bit <= convergent_round(r1imag);

            r0real_overflow <= overflow_detect(r0real);
            r0imag_overflow <= overflow_detect(r0imag);
            r1real_overflow <= overflow_detect(r1real);
            r1imag_overflow <= overflow_detect(r1imag);
            
            rfi_del1 <= i_rfi; -- i_rfi aligns with v0
            rfi_del2 <= rfi_del1;
            rfi_del3 <= rfi_del2;
            rfi_del4 <= rfi_del3; -- del4 matches 4 clock latency through the complex multiplier.
            rfi_del5 <= rfi_del4; -- del5 aligns with r0real
            rfi_del6 <= rfi_del5; -- del6 aligns with r0real_16bit
            
            o_r0real <= r0real_16bit;
            o_r0imag <= r0imag_16bit;
            o_r1real <= r1real_16bit;
            o_r1imag <= r1imag_16bit;
            o_flag <= r0real_overflow or r0imag_overflow or r1real_overflow or r1imag_overflow or rfi_del6;
            
        end if;
    end process;

    m00 <= i_m00imag & i_m00real;
    m01 <= i_m01imag & i_m01real;
    m10 <= i_m10imag & i_m10real;
    m11 <= i_m11imag & i_m11real;

    v0 <= i_v0imag & i_v0real;
    v1 <= i_v1imag & i_v1real;
    
    cmult1 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m00, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v0, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m00v0   -- out(79:0)
    );
    m00v0real <= m00v0(32) & m00v0(32) & m00v0(32) & m00v0(32 downto 0);
    m00v0imag <= m00v0(72) & m00v0(72) & m00v0(72) & m00v0(72 downto 40);

    cmult2 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m01, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v1, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m01v1   -- out(79:0)
    );
    m01v1real <= m01v1(32) & m01v1(32) & m01v1(32) & m01v1(32 downto 0);
    m01v1imag <= m01v1(72) & m01v1(72) & m01v1(72) & m01v1(72 downto 40);

    cmult3 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m10, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v0, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m10v0   -- out(79:0)
    );
    m10v0real <= m10v0(32) & m10v0(32) & m10v0(32) & m10v0(32 downto 0);
    m10v0imag <= m10v0(72) & m10v0(72) & m10v0(72) & m10v0(72 downto 40);
    
    cmult4 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m11, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v1, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m11v1   -- out(79:0)
    );
    m11v1real <= m11v1(32) & m11v1(32) & m11v1(32) & m11v1(32 downto 0);
    m11v1imag <= m11v1(72) & m11v1(72) & m11v1(72) & m11v1(72 downto 40);
    
end Behavioral;
