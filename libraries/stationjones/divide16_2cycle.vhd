----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: August 2024
-- Module Name: divide16_2cycle
-- Description: 
--  Divider. 
--  - Assumes dividend is <= divider
--  - Generates 16 fractional bits
--  - At most one new input every 2 clocks.
----------------------------------------------------------------------------------

library IEEE, common_lib, bf_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;


entity divide16_2cycle is
    port(
        i_clk      : in std_logic;
        -- input
        i_dividend : in std_logic_vector(31 downto 0); 
        i_divisor  : in std_logic_vector(31 downto 0);
        i_valid    : in std_logic; -- Valid can go high at most once every 2nd clock.
        i_first    : in std_logic;
        -- output
        o_quotient : out std_logic_vector(15 downto 0); -- quotient = dividend/divisor with fixed latency from i_valid
        o_valid    : out std_logic;
        o_first    : out std_logic
    );
end divide16_2cycle;

architecture Behavioral of divide16_2cycle is
    
    signal quotient1 : std_logic_vector(1 downto 0);
    signal quotient2 : std_logic_vector(3 downto 0);
    signal quotient3 : std_logic_Vector(5 downto 0);
    signal quotient4 : std_logic_vector(7 downto 0);
    signal quotient5 : std_logic_vector(9 downto 0);
    signal quotient6 : std_logic_vector(11 downto 0);
    signal quotient7 : std_logic_vector(13 downto 0);
    signal quotient8 : std_logic_vector(15 downto 0);
    signal divisor_div2, divisor_div4, divisor_div8, divisor_div16 : std_logic_vector(31 downto 0);
    signal divisor_div32, divisor_div64, divisor_div128, divisor_div256 : std_logic_vector(31 downto 0);
    signal divisor_div512, divisor_div1024, divisor_div2048, divisor_div4096 : std_logic_vector(31 downto 0);
    signal divisor_div8192, divisor_div16384, divisor_div32768 : std_logic_vector(31 downto 0);
    signal dividend, divisor : std_logic_vector(31 downto 0);
    signal partial_remainder1, partial_remainder2, partial_remainder3, partial_remainder4 : std_logic_vector(31 downto 0);
    signal partial_remainder5, partial_remainder6, partial_remainder7, partial_remainder8 : std_logic_vector(31 downto 0);
    signal valid_del : std_logic_vector(16 downto 0);
    signal first_del : std_logic_vector(16 downto 0);
    
begin
    
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            valid_del(0) <= i_valid;
            valid_del(16 downto 1) <= valid_del(15 downto 0);
            first_del(0) <= i_first;
            first_del(16 downto 1) <= first_del(15 downto 0);
    
            if i_valid = '1' then
                dividend <= i_dividend;
                divisor <= i_divisor;
            end if;
            
            if valid_del(0) = '1' then
                if unsigned(dividend) >= unsigned(divisor) then
                    partial_remainder1 <= std_logic_vector(unsigned(dividend) - unsigned(divisor));
                    quotient1 <= "10";
                else
                    partial_remainder1 <= dividend;
                    quotient1 <= "00";
                end if;
            elsif valid_del(1) = '1' then
                if unsigned(partial_remainder1) >= unsigned(divisor_div2) then
                    partial_remainder1 <= std_logic_vector(unsigned(partial_remainder1) - unsigned(divisor_div2));
                    quotient1(0) <= '1';
                end if;
                divisor_div4 <= "00" & divisor(31 downto 2);
            end if;
            --
            if valid_del(2) = '1' then
                if unsigned(partial_remainder1) >= unsigned(divisor_div4) then
                    partial_remainder2 <= std_logic_vector(unsigned(partial_remainder1) - unsigned(divisor_div4));
                    quotient2 <= quotient1 & "10";
                else
                    partial_remainder2 <= partial_remainder1;
                    quotient2 <= quotient1 & "00";
                end if;
            elsif valid_del(3) = '1' then
                if unsigned(partial_remainder2) >= unsigned(divisor_div8) then
                    partial_remainder2 <= std_logic_vector(unsigned(partial_remainder2) - unsigned(divisor_div8));
                    quotient2(0) <= '1';
                end if;
                divisor_div16 <= "00" & divisor_div4(31 downto 2);
            end if;
            --
            if valid_del(4) = '1' then
                if unsigned(partial_remainder2) >= unsigned(divisor_div16) then
                    partial_remainder3 <= std_logic_vector(unsigned(partial_remainder2) - unsigned(divisor_div16));
                    quotient3 <= quotient2 & "10";
                else
                    partial_remainder3 <= partial_remainder2;
                    quotient3 <= quotient2 & "00";
                end if;
            elsif valid_del(5) = '1' then
                if unsigned(partial_remainder3) >= unsigned(divisor_div32) then
                    partial_remainder3 <= std_logic_vector(unsigned(partial_remainder3) - unsigned(divisor_div32));
                    quotient3(0) <= '1';
                end if;
                divisor_div64 <= "00" & divisor_div16(31 downto 2);
            end if;
            --
            if valid_del(6) = '1' then
                if unsigned(partial_remainder3) >= unsigned(divisor_div64) then
                    partial_remainder4 <= std_logic_vector(unsigned(partial_remainder3) - unsigned(divisor_div64));
                    quotient4 <= quotient3 & "10";
                else
                    partial_remainder4 <= partial_remainder3;
                    quotient4 <= quotient3 & "00";
                end if;
            elsif valid_del(7) = '1' then
                if unsigned(partial_remainder4) >= unsigned(divisor_div128) then
                    partial_remainder4 <= std_logic_vector(unsigned(partial_remainder4) - unsigned(divisor_div128));
                    quotient4(0) <= '1';
                end if;
                divisor_div256 <= "00" & divisor_div64(31 downto 2);
            end if;
            --
            if valid_del(8) = '1' then
                if unsigned(partial_remainder4) >= unsigned(divisor_div256) then
                    partial_remainder5 <= std_logic_vector(unsigned(partial_remainder4) - unsigned(divisor_div256));
                    quotient5 <= quotient4 & "10";
                else
                    partial_remainder5 <= partial_remainder4;
                    quotient5 <= quotient4 & "00";
                end if;
            elsif valid_del(9) = '1' then
                if unsigned(partial_remainder5) >= unsigned(divisor_div512) then
                    partial_remainder5 <= std_logic_vector(unsigned(partial_remainder5) - unsigned(divisor_div512));
                    quotient5(0) <= '1';
                end if;
                divisor_div1024 <= "00" & divisor_div256(31 downto 2);
            end if;
            --
            if valid_del(10) = '1' then
                if unsigned(partial_remainder5) >= unsigned(divisor_div1024) then
                    partial_remainder6 <= std_logic_vector(unsigned(partial_remainder5) - unsigned(divisor_div1024));
                    quotient6 <= quotient5 & "10";
                else
                    partial_remainder6 <= partial_remainder5;
                    quotient6 <= quotient5 & "00";
                end if;
            elsif valid_del(11) = '1' then
                if unsigned(partial_remainder6) >= unsigned(divisor_div2048) then
                    partial_remainder6 <= std_logic_vector(unsigned(partial_remainder6) - unsigned(divisor_div2048));
                    quotient6(0) <= '1';
                end if;
                divisor_div4096 <= "00" & divisor_div1024(31 downto 2);
            end if;
            --
            if valid_del(12) = '1' then
                if unsigned(partial_remainder6) >= unsigned(divisor_div4096) then
                    partial_remainder7 <= std_logic_vector(unsigned(partial_remainder6) - unsigned(divisor_div4096));
                    quotient7 <= quotient6 & "10";
                else
                    partial_remainder7 <= partial_remainder6;
                    quotient7 <= quotient6 & "00";
                end if;
            elsif valid_del(13) = '1' then
                if unsigned(partial_remainder7) >= unsigned(divisor_div8192) then
                    partial_remainder7 <= std_logic_vector(unsigned(partial_remainder7) - unsigned(divisor_div8192));
                    quotient7(0) <= '1';
                end if;
                divisor_div16384 <= "00" & divisor_div4096(31 downto 2);
            end if;
            --
            if valid_del(14) = '1' then
                if unsigned(partial_remainder7) >= unsigned(divisor_div16384) then
                    partial_remainder8 <= std_logic_vector(unsigned(partial_remainder7) - unsigned(divisor_div16384));
                    quotient8 <= quotient7 & "10";
                else
                    partial_remainder8 <= partial_remainder7;
                    quotient8 <= quotient7 & "00";
                end if;
            elsif valid_del(15) = '1' then
                if unsigned(partial_remainder8) >= unsigned(divisor_div32768) then
                    partial_remainder8 <= std_logic_vector(unsigned(partial_remainder8) - unsigned(divisor_div32768));
                    quotient8(0) <= '1';
                end if;
            end if;
            o_quotient <= quotient8;
            o_valid <= valid_del(16);
            o_first <= first_del(16);
            
        end if;
    end process;
    
    divisor_div2 <= '0' & divisor(31 downto 1);
    divisor_div8 <= '0' & divisor_div4(31 downto 1);
    divisor_div32 <= '0' & divisor_div16(31 downto 1);
    divisor_div128 <= '0' & divisor_div64(31 downto 1);
    divisor_div512 <= '0' & divisor_div256(31 downto 1);
    divisor_div2048 <= '0' & divisor_div1024(31 downto 1);
    divisor_div8192 <= '0' & divisor_div4096(31 downto 1);
    divisor_div32768 <= '0' & divisor_div16384(31 downto 1);
    
end Behavioral;
