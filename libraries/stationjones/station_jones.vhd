----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: August 2024
-- Module Name: station_jones
-- Description: 
--  Applies Jones corrections to the station data.
--  No buffering in this module, just a pipeline delay.
--  Two functions:
--   1. Jones 
--   Data for 2 dual-pol stations is delivered every clock. 
--   This module has 2 fully parallel matrix multipliers to implement the Jones correction
--
--   2. Weights
--   For each fine frequency channel:
--     - S_total = Sum of all the weights 
--     - S_unflagged = sum of all the weights for samples that are not flagged
--     - Weight = s_unflagged / s_total
--       weights are 16-bit unsigned values with 0x8000 = all samples unflagged.
----------------------------------------------------------------------------------

library IEEE, common_lib, axi4_lib, bf_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity station_jones is
    port(
        i_clk         : in std_logic;
        i_jonesBuffer : in std_logic;
        ----------------------------------------------------------------------
        -- Beamformer data from the corner turn
        -- i_data, i_flagged have a 4-cycle latency relative to the control signals (i_fine, i_coarse etc).
        -- !! NOTE : 4 cycle latency coming in, 8 cycle latency going out for data relative to control. !! 
        i_data    : in std_logic_vector(127 downto 0); -- 2 stations delivered every clock,
        i_flagged : in std_logic_vector(1 downto 0);   -- aligns with i_data, indicates if either of the stations in i_data are flagged as RFI.
        --
        i_fine    : in std_logic_vector(7 downto 0);   -- fine channel, 0 to 53
        i_coarse  : in std_logic_vector(9 downto 0);   -- index of the coarse channel.
        i_stations01 : in std_logic;   -- Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
        i_firstStation : in std_logic; -- First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation  : in std_logic;
        i_timeStep     : in std_logic_vector(3 downto 0); -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        i_station      : in std_logic_vector(9 downto 0); -- Station
        i_virtualChannel : in std_logic_vector(9 downto 0); -- note this takes steps of 4, associated with the 4 stations delivered in a burst of 2 clocks.
        -- The PSS output packet count for this packet, based on the original packet count from LFAA.
        -- Each PSS output packet is 16 time samples = 16 * 69.12us = 1.105920 ms of data. There are 48 PSS output packets per corner turn frame.
        i_packetCount  : in std_logic_vector(47 downto 0);
        i_outputPktOdd : in std_logic;
        i_valid   : in std_logic;
        -----------------------------------------------------------------------
        -- Data output
        o_data    : out std_logic_vector(127 downto 0); -- 2 stations delivered every clock; o_data has 8 clock latency relative to the other outputs.
        o_fine    : out std_logic_vector(7 downto 0);   -- fine channel, 0 to 53
        o_coarse  : out std_logic_vector(9 downto 0);   -- index of the coarse channel.
        o_stations01 : out std_logic;   -- Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
        o_firstStation : out std_logic; -- First station (used to trigger a new accumulator cycle in the beamformers).
        o_lastStation  : out std_logic;
        o_timeStep     : out std_logic_vector(3 downto 0); -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        o_station      : out std_logic_vector(9 downto 0); -- Station
        -- The PSS output packet count for this packet, based on the original packet count from LFAA.
        -- Each PSS output packet is 16 time samples = 16 * 69.12us = 1.105920 ms of data. There are 48 PSS output packets per corner turn frame.
        o_packetCount  : out std_logic_vector(47 downto 0);
        o_outputPktOdd : out std_logic;
        o_valid   : out std_logic;
        -------------------------------------------------------------------------
        -- Weights output
        -- burst of 54 clocks (one weight for each fine channel in a packet)
        o_weights       : out std_logic_vector(15 downto 0);
        o_weights_valid : out std_logic;
        o_weights_sop   : out std_logic;
        --------------------------------------------------------------------------
        -- Register interface
        -- Station Jones registers
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        i_axi_mosi_sj : in  t_axi4_full_mosi;
        o_axi_miso_sj : out t_axi4_full_miso
    );
end station_jones;

architecture Behavioral of station_jones is

    signal rst  : std_logic;
    
    signal BFdata           : std_logic_vector(63 downto 0);
    signal BFpacketCount    : std_logic_vector(47 downto 0);
    signal BFBeam           : std_logic_vector(9 downto 0);
    signal BFFreqIndex      : std_logic_vector(10 downto 0);
    signal BFvalid          : std_logic;
    signal BFjones_status   : std_logic_vector(1 downto 0);
    signal BFpoly_ok        : std_logic_vector(1 downto 0);
    signal BFscale	        : std_logic_vector(3 downto 0);
    
    signal virtualChannel_div2, virtualChannel_div2_del1 : std_logic_vector(9 downto 0);
    signal jones_data : std_logic_vector(255 downto 0);
    signal data_del1 : std_logic_vector(127 downto 0); -- 2 stations delivered every clock,
    signal flagged_del1 : std_logic_vector(1 downto 0);
    signal r0real, r0imag, r1real, r1imag : t_slv_16_arr(1 downto 0);
    signal flag : std_logic_vector(1 downto 0);
    signal weights_data : std_logic_vector(31 downto 0);
    signal weights_data_del : t_slv_32_arr(6 downto 0);
    
    signal fine_del    : t_slv_8_arr(15 downto 0);  -- fine channel, 0 to 53
    signal coarse_del  : t_slv_10_arr(3 downto 0);   -- index of the coarse channel.
    signal stations01_del : std_logic_vector(15 downto 0);   -- Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
    signal firstStation_del : std_logic_vector(15 downto 0); -- First station (used to trigger a new accumulator cycle in the beamformers).
    signal lastStation_del  : std_logic_vector(15 downto 0);
    signal timeStep_del     : t_slv_4_arr(15 downto 0); -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
    signal station_del      : t_slv_10_arr(3 downto 0); -- Station
    signal packetCount_del  : t_slv_48_arr(3 downto 0);
    signal outputPktOdd_del : std_logic_vector(15 downto 0);
    signal valid_del    : std_logic_vector(15 downto 0);
    signal wsum0, wsum1, wsum0_flagged, wsum1_flagged, weights0_del6, weights1_del6 : std_logic_vector(31 downto 0);
    signal weights_dout, weights_din : std_logic_vector(63 downto 0);
    signal weights_rdAddr, weights_wrAddr : std_logic_vector(5 downto 0);
    signal weights_we : std_logic_vector(0 downto 0);
    signal divide_start : std_logic;
    signal fine_del12_eq0, first_divide : std_logic;
    
begin
    
    sjmem : entity bf_lib.axi_full_sj
    port map(
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk => i_MACE_clk, -- in std_logic;
        i_MACE_rst => i_MACE_rst, -- in std_logic;
        -- Station Jones registers
        i_axi_mosi => i_axi_mosi_sj, -- in  t_axi4_full_mosi;
        o_axi_miso => o_axi_miso_sj, -- out t_axi4_full_miso;
        ----------------------------------------------------------------------
        -- Read interface for the memory
        -- 1k deep x 32 byte wide read interface
        -- Reads data for 2 virtual channels at a time
        -- 3 cycle read latency
        -- Address 0-511 : First buffer
        -- Address 512-1023 : Second buffer
        i_BF_clk => i_clk, --  in std_logic;
        i_rd_addr => virtualChannel_div2_del1,  -- in (9:0);
        o_rd_jones => jones_data,     -- out (255:0); 2 matrices at a time = 16 x 16-bit values.
        o_rd_weights => weights_data  -- out (31:0); 2x(16bit weights)
    );
    
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            
            virtualChannel_div2 <= i_jonesBuffer & i_virtualChannel(9 downto 2) & i_stations01;
            virtualChannel_div2_del1 <= virtualChannel_div2;
            
            data_del1 <= i_data;
            flagged_del1 <= i_flagged;
            
            --
            if flag(0) = '1' then
                o_data(63 downto 0) <= (others => '0');
            else
                o_data(15 downto 0) <= r0real(0);
                o_data(31 downto 16) <= r0imag(0);
                o_data(47 downto 32) <= r1real(0);
                o_data(63 downto 48) <= r1imag(0);
            end if;
            
            if flag(1) = '1' then
                o_data(127 downto 64) <= (others => '0');
            else
                o_data(64+15 downto 64+0) <= r0real(1);
                o_data(64+31 downto 64+16) <= r0imag(1);
                o_data(64+47 downto 64+32) <= r1real(1);
                o_data(64+63 downto 64+48) <= r1imag(1);
            end if;
            
            -- 
            --    For timeGroup = 0:47                          -- 48 * 16 = 768 PSS time samples in a 53 ms corner turn
            --       For Coarse = 0:(i_coarse-1)                -- step through each SPS coarse channel
            --          For Time = 0:15                         -- 16 times needed for an output packet
            --             For Station_block = 0:(i_stations/4)
            --                For fine_offset = 0:53            -- Total of 54 fine channels per SPS channel
            --                   For station = 0:3
            --                       - 2 clocks, data for 2 stations per clock
            -- Handle weights
            -- Delay "weights_data" so that it aligns with "flag"
            -- 7 clock latency to match latency of jonesMatrixMult_16b
            weights_data_del(0) <= weights_data;
            weights_data_del(6 downto 1) <= weights_data_del(5 downto 0);
            
            -- Delay control signals to align with "flag"
            -- i_stations01
            -- del(0)    virtualChannel_div2
            -- del(1)    virtualChannel_div2_del1
            -- del(2)
            -- del(3)                               i_data (4 cycle latency relative to control signals i_stations01)
            -- del(4)    weights_data   jones_data  data_del1     (inputs to the matrix multiplier)  o_fine, o_timestep etc. (8 cycles ahead of o_data)
            -- del(5)    weights_data_del(0)
            -- del(6)    weights_data_del(1)
            -- del(7)    weights_data_del(2)
            -- del(8)    weights_data_del(3)
            -- del(9)    weights_data_del(4)
            -- del(10)   weights_data_del(5)    weights_rdAddr
            -- del(11)   weights_data_del(6)    flag           r0real
            -- del(12)   wsum0, wsum0_flagged   weights_dout   o_data
            -- del(13)   wsum1, wsum1_flagged
            -- del(14)   
            --
            
            stations01_del(0) <= i_stations01;
            fine_del(0) <= i_fine;
            timeStep_del(0) <= i_timeStep;
            firstStation_del(0) <= i_firstStation;
            lastStation_del(0) <= i_lastStation;
            valid_del(0) <= i_valid;
            coarse_del(0) <= i_coarse;
            station_del(0) <= i_station;
            packetCount_del(0) <= i_packetCount;
            outputPktOdd_del(0) <= i_outputPktOdd;
            
            stations01_del(15 downto 1) <= stations01_del(14 downto 0);
            fine_del(15 downto 1) <= fine_del(14 downto 0);
            timeStep_del(15 downto 1) <= timeStep_del(14 downto 0);
            firstStation_del(15 downto 1) <= firstStation_del(14 downto 0);
            valid_del(15 downto 1) <= valid_del(14 downto 0);
            coarse_del(3 downto 1) <= coarse_del(2 downto 0);
            lastStation_del(15 downto 1) <= lastStation_del(14 downto 0);
            station_del(3 downto 1) <= station_del(2 downto 0);
            packetCount_del(3 downto 1) <= packetCount_del(2 downto 0);
            outputPktOdd_del(3 downto 1) <= outputPktOdd_del(2 downto 0);
            
            o_fine         <= fine_del(3);
            o_coarse       <= coarse_del(3);
            o_stations01   <= stations01_del(3);
            o_firstStation <= firstStation_del(3);
            o_lastStation  <= lastStation_del(3);
            o_timeStep     <= timeStep_del(3);
            o_station      <= station_del(3);
            o_packetCount  <= packetCount_del(3);
            o_outputPktOdd <= outputPktOdd_del(3);
            o_valid        <= valid_del(3);
            
            -- Add up total weights and weights that are being used.
            wsum0 <= std_logic_vector(unsigned(weights0_del6) + unsigned(weights1_del6));
            if flag = "00" then
                wsum0_flagged <= std_logic_vector(unsigned(weights0_del6) + unsigned(weights1_del6));
            elsif flag = "01" then
                wsum0_flagged <= weights0_del6;
            elsif flag = "10" then
                wsum0_flagged <= weights1_del6;
            else
                wsum0_flagged <= (others => '0');
            end if;
            
            if valid_del(12) = '1' then
                if (stations01_del(12) = '1') then
                    if timeStep_del(12) = "0000" and firstStation_del(12) = '1' then
                        wsum1 <= wsum0;
                        wsum1_flagged <= wsum0_flagged;
                    else
                        wsum1 <= std_logic_vector(unsigned(wsum0) + unsigned(weights_dout(31 downto 0)));
                        wsum1_flagged <= std_logic_vector(unsigned(wsum0_flagged) + unsigned(weights_dout(63 downto 32)));
                    end if;
                else
                    wsum1 <= std_logic_vector(unsigned(wsum1) + unsigned(wsum0));
                    wsum1_flagged <= std_logic_vector(unsigned(wsum1_flagged) + unsigned(wsum0_flagged));
                end if;
            end if;
            
            weights_rdAddr <= fine_del(9)(5 downto 0);
            
            weights_wrAddr <= fine_del(12)(5 downto 0);
            if (valid_del(12) = '1' and stations01_del(12) = '0') then
                weights_we(0) <= '1';
            else
                weights_we(0) <= '0';
            end if;
            
            if unsigned(fine_del(11)) = 0 then
                fine_del12_eq0 <= '1';
            else
                fine_del12_eq0 <= '0';
            end if;
            
            if (valid_del(12) = '1' and stations01_del(12) = '0' and lastStation_del(12) = '1' and timeStep_del(12) = "1111") then
                divide_start <= '1';
                if fine_del12_eq0 = '1' then
                    first_divide <= '1';
                else
                    first_divide <= '0';
                end if;
            else
                divide_start <= '0';
                first_divide <= '0';
            end if;
            
        end if;
    end process;

    weights0_del6 <= x"0000" & weights_data_del(6)(15 downto 0);
    weights1_del6 <= x"0000" & weights_data_del(6)(31 downto 16);

    weights_din <= wsum1_flagged & wsum1;

    jonesGen : for i in 0 to 1 generate
        jonesi : entity bf_lib.jonesMatrixMult_16b
        Port map(
            i_clk => i_clk, -- in std_logic;
            -- The 2x2 matrix
            -- jones_data has 5 clock latency from i_virtualChannel (2 clocks to get virtualChannel_div2_del1, 3 for the memory)
            i_m00real => jones_data(i*128+15 downto i*128), -- in (15:0);
            i_m00imag => jones_data(i*128+31 downto i*128+16), -- in (15:0);
            i_m01real => jones_data(i*128+47 downto i*128+32), -- in (15:0);
            i_m01imag => jones_data(i*128+63 downto i*128+48), -- in (15:0);
            i_m10real => jones_data(i*128+79 downto i*128+64), -- in (15:0);
            i_m10imag => jones_data(i*128+95 downto i*128+80), -- in (15:0);
            i_m11real => jones_data(i*128+111 downto i*128+96), -- in (15:0);
            i_m11imag => jones_data(i*128+127 downto i*128+112), -- in (15:0);
            -- 2x1 vector
            -- data_del1 has 1 clock latency from i_data, +4 clock latency in corner turn 2 (i_Data is 4 clocks behind i_virtualChannel)
            --  = total 5 clock latency to match jones_data.
            i_v0real => data_del1(i*64 + 15 downto i*64),      -- in (15:0);
            i_v0imag => data_del1(i*64 + 31 downto i*64 + 16), -- in (15:0);
            i_v1real => data_del1(i*64 + 47 downto i*64 + 32), -- in (15:0);
            i_v1imag => data_del1(i*64 + 63 downto i*64 + 48), -- in (15:0);
            i_rfi    => flagged_del1(i), -- in std_logic;
            -- 2x1 vector result (7 clock latency)
            o_r0real => r0real(i), -- out (15:0);
            o_r0imag => r0imag(i), -- out (15:0);
            o_r1real => r1real(i), -- out (15:0);
            o_r1imag => r1imag(i), -- out (15:0)
            o_flag   => flag(i)
        );
    end generate;
    
    
    -- Memory to store the partial sums for the weights for each of the 54 fine channels.
    -- Weights are 16 bit values, and we accumulate up to 512 of them, so we use 28-bit accumulators.
    -- both weights and weights used are accumulated so the memory is 32+32=64 bits wide.
    -- xpm_memory_sdpram: Simple Dual Port RAM
    -- Xilinx Parameterized Macro, version 2022.2
    weight_accum_i : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 6,               -- DECIMAL
        ADDR_WIDTH_B => 6,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 4096,             -- DECIMAL; 64*64 = 4096
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL 
    ) port map (
        dbiterrb => open,        -- 1-bit output
        doutb => weights_dout,   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,        -- 1-bit output
        addra => weights_wrAddr, -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => weights_rdAddr, -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_clk,           -- 1-bit input: Clock signal for port A and B (when common clock)
        clkb => i_clk,           -- 1-bit input: Clock signal for port B; Unused when parameter CLOCKING_MODE is "common_clock".
        dina => weights_din,     -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',              -- 1-bit input: Memory enable signal for port A
        enb => '1',              -- 1-bit input: Memory enable signal for port B
        injectdbiterra => '0',   -- 1-bit input
        injectsbiterra => '0',   -- 1-bit input
        regceb => '1',           -- 1-bit input: Clock Enable for the last register stage
        rstb => '0',             -- 1-bit input
        sleep => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => weights_we        -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input
    );
    
    
    
    divideri : entity bf_lib.divide16_2cycle
    port map (
        i_clk      => i_clk, --  in std_logic;
        -- input
        i_dividend => wsum1_flagged, -- in (31:0); 
        i_divisor  => wsum1, -- in (31:0);
        i_valid    => divide_start, -- in std_logic; -- Valid can go high at most once every 2nd clock.
        i_first    => first_divide, -- in std_logic
        -- output
        o_quotient => o_weights,       -- out (15:0); quotient = dividend/divisor with fixed latency from i_valid
        o_valid    => o_weights_valid, -- out std_logic
        o_first    => o_weights_sop    -- out std_logic
    );
    
end Behavioral;
