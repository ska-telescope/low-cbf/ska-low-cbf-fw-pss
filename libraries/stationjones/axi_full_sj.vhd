----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 4 July 2024
-- Module Name: axi_full_64k - Behavioral
-- Description: 
--   axi full interface to the station jones memory
--
--  Memory space to specify parameters for the station Jones and station Weights
--  64 kBytes of address space.
--    Within the 64kBytes:
--   Address 0 to 16kByte         = Jones Buffer 0 
--   Address 16 kByte to 32 kByte = Jones Buffer 1 
--   Address 32 kByte to 34 kByte = Weights Buffer 0 
--   Address 34 kByte to 36 kByte = Weights Buffer 1 
--  Which of the two buffers is selected is determined by the jones_buffer0_valid_ registers in Corner turn 2.
--  The corner turn 2 registers select the buffer to use based on the corner turn frame that is being processed.
--  Within each Jones Buffer:
--   1024 Jones matrices, with each matrix occupying 16 bytes.
--   The Jones matrices are indexed via the virtual channel that they apply to.
--   Jones coefficients are 16 bit values, with
--                    input pol 0 | input pol 1   
--     output pol 0  [ a + bi,      c + di      ]
--     output pol 1  [ e + fi,      g + hi      ]
--
--  Within each block of 16 bytes, the first two bytes are the value a, then b etc.
--  Likewise, within each weights buffer, there are 1024 2-byte weights, indexed by the virtual channel
--  that they apply to. Note that weights are only used in the calculation of the weights field in the
--  output packets. The weight must also be incorporated into the Jones matrices.
--
----------------------------------------------------------------------------------

library IEEE, axi4_lib, common_lib, bf_lib, xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;

entity axi_full_sj is
    port(
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        -- Station Jones registers
        i_axi_mosi : in  t_axi4_full_mosi;
        o_axi_miso : out t_axi4_full_miso;
        ----------------------------------------------------------------------
        -- Read clock for the memory
        i_BF_clk : in std_logic;
        -- 1k deep x 32 byte wide read interface
        -- Reads data for 2 virtual channels at a time
        -- 3 cycle read latency
        -- Address 0-511 : First buffer
        -- Address 512-1023 : Second buffer
        i_rd_addr : in std_logic_vector(9 downto 0);
        o_rd_jones : out std_logic_vector(255 downto 0); -- 2 x 128 bit Jones matrices; Each 128 bits = 8 x 16bit signed values = 1 Jones matrix
        o_rd_weights : out std_logic_vector(31 downto 0) -- 2 x 16-bit station weights
    );
end axi_full_sj;

architecture Behavioral of axi_full_sj is

    -- create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_sj
    -- set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_sj} CONFIG.MEM_DEPTH {16384} CONFIG.READ_LATENCY {4}] [get_ips axi_bram_ctrl_sj]
    component axi_bram_ctrl_sj
    port (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;
    
    signal BFmemRst   : std_logic;
    signal BFmemClk   : std_logic;
    signal BFmemEn    : std_logic;
    signal BFmemWrEn  : std_logic_vector(3 downto 0);
    signal BFmemAddr  : std_logic_vector(15 downto 0);
    signal BFmemDin, BFmemDout   : std_logic_vector(31 downto 0);
    
    signal MACE_rstn : std_logic;
    
    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;
    
    signal axi_mosi_arlock : std_logic_vector(0 downto 0);
    signal axi_mosi_awlock : std_logic_vector(0 downto 0);
    signal highbits : std_logic_vector(1 downto 0) := "00";
    
    signal MACE_rstn_BFclk : std_logic;
    signal rst_BFclk : std_logic;
    
    signal regWrEn : std_logic;
    
    signal jones_douta, jones_doutb, jones_dina, jones_dinb : std_logic_vector(255 downto 0);
    signal weights_dinb : std_logic_vector(31 downto 0);
    signal weights_web : std_logic_vector(0 downto 0);
    signal jones_addra : std_logic_vector(9 downto 0);
    signal weights_rdSel : std_logic;
    signal jones_we : std_logic_vector(31 downto 0);
    signal jones_rdSel_del2, jones_rdSel_del1, jones_addra_4downto2 : std_logic_vector(2 downto 0);
    signal weights_rdSel_del2, weights_rdSel_del1 : std_logic;
    signal weights_douta : std_logic_vector(31 downto 0);
    signal weights_we : std_logic_vector(0 downto 0);
    signal weights_addra : std_logic_vector(9 downto 0);
    signal weights_dina : std_logic_vector(31 downto 0);
    
    
begin

    MACE_rstn <= not i_MACE_rst;
    
    MACE_rstn <= not i_MACE_rst;
    -- Convert register interface from AXI full to address + data
    BF_ctrli : axi_bram_ctrl_sj
    port map (
        s_axi_aclk      => i_MACE_clk,
        s_axi_aresetn   => MACE_rstn, -- in std_logic;
        s_axi_awaddr    => i_axi_mosi.awaddr(15 downto 0),
        s_axi_awlen     => i_axi_mosi.awlen,
        s_axi_awsize    => i_axi_mosi.awsize,
        s_axi_awburst   => i_axi_mosi.awburst,
        s_axi_awlock    => i_axi_mosi.awlock ,
        s_axi_awcache   => i_axi_mosi.awcache,
        s_axi_awprot    => i_axi_mosi.awprot,
        s_axi_awvalid   => i_axi_mosi.awvalid,
        s_axi_awready   => o_axi_miso.awready,
        s_axi_wdata     => i_axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => i_axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => i_axi_mosi.wlast,
        s_axi_wvalid    => i_axi_mosi.wvalid,
        s_axi_wready    => o_axi_miso.wready,
        s_axi_bresp     => o_axi_miso.bresp,
        s_axi_bvalid    => o_axi_miso.bvalid,
        s_axi_bready    => i_axi_mosi.bready ,
        s_axi_araddr    => i_axi_mosi.araddr(15 downto 0),
        s_axi_arlen     => i_axi_mosi.arlen,
        s_axi_arsize    => i_axi_mosi.arsize,
        s_axi_arburst   => i_axi_mosi.arburst,
        s_axi_arlock    => i_axi_mosi.arlock ,
        s_axi_arcache   => i_axi_mosi.arcache,
        s_axi_arprot    => i_axi_mosi.arprot,
        s_axi_arvalid   => i_axi_mosi.arvalid,
        s_axi_arready   => o_axi_miso.arready,
        s_axi_rdata     => o_axi_miso.rdata(31 downto 0),
        s_axi_rresp     => o_axi_miso.rresp,
        s_axi_rlast     => o_axi_miso.rlast,
        s_axi_rvalid    => o_axi_miso.rvalid,
        s_axi_rready    => i_axi_mosi.rready,
        bram_rst_a      => BFmemRst,   -- out std_logic;
        bram_clk_a      => BFmemClk,   -- out std_logic;
        bram_en_a       => BFmemEn,    -- out std_logic;
        bram_we_a       => BFmemWrEn,  -- out (3:0)
        bram_addr_a     => BFmemAddr,  -- out (15:0)
        bram_wrdata_a   => BFmemDin,   -- out (31:0)
        bram_rddata_a   => BFmemDout   -- in (31:0)
    );
    
    regWrEn <= BFmemWrEn(0) and BFmemEn;  -- BFmemWrEn is 4 bits wide, one bit per byte; This assumes that all writes are 4 bytes wide. 
    
    process(i_MACE_clk)
    begin
        if rising_edge(i_MACE_clk) then
            
            ------------------------------------------------------------------------------------------
            -- jones memory is 32 bytes wide, writes from the axi memory controller are 4 bytes wide.
            -- Address is a byte address, 4-byte aligned
            jones_addra <= BFmemAddr(14 downto 5);
            jones_addra_4downto2 <= BFmemAddr(4 downto 2);
            weights_rdSel <= BFmemAddr(15);
            if BFmemAddr(15) = '1' or regWrEn = '0' then
                jones_we <= x"00000000";
            else
                case BFmemAddr(4 downto 2) is
                    when "000"  => jones_we <= x"0000000f";
                    when "001"  => jones_we <= x"000000f0";
                    when "010"  => jones_we <= x"00000f00";
                    when "011"  => jones_we <= x"0000f000";
                    when "100"  => jones_we <= x"000f0000";
                    when "101"  => jones_we <= x"00f00000";
                    when "110"  => jones_we <= x"0f000000";
                    when others => jones_we <= x"f0000000";
                end case;
            end if;
            jones_dina <= BFmemDin & BFmemDin & BFmemDin & BFmemDin & BFmemDin & BFmemDin & BFmemDin & BFmemDin;
            
            -- Read is 2 clock latency for the memory
            jones_rdSel_del1 <= jones_addra_4downto2; -- BFmemAddr(4 downto 2);
            weights_rdSel_del1 <= weights_rdSel;
            
            jones_rdSel_del2 <= jones_rdSel_del1;
            weights_rdSel_del2 <= weights_rdSel_del1;
            
            if weights_rdSel_del2 = '1' then
                BFmemDout <= weights_douta;
            else
                case jones_rdSel_Del2 is
                    when "000"  => BFmemDout <= jones_douta(31 downto 0);
                    when "001"  => BFmemDout <= jones_douta(63 downto 32);
                    when "010"  => BFmemDout <= jones_douta(95 downto 64);
                    when "011"  => BFmemDout <= jones_douta(127 downto 96);
                    when "100"  => BFmemDout <= jones_douta(159 downto 128);
                    when "101"  => BFmemDout <= jones_douta(191 downto 160);
                    when "110"  => BFmemDout <= jones_douta(223 downto 192);
                    when others => BFmemDout <= jones_douta(255 downto 224);
                end case;
            end if;
            
            -------------------------------------------------------------------------------------------
            -- Weights memory
            if regWrEn = '1' and BFmemAddr(15 downto 12) = "1000" then
                weights_we(0) <= '1';
            else
                weights_we(0) <= '0';
            end if;
            weights_addra <= BFmemAddr(11 downto 2);
            weights_dina <= BFmemDin;
            
        end if;
    end process;
    
    -- Memory for the Jones Matrices
    -- 1024 deep x 32 bytes wide
    -- 
    -- xpm_memory_tdpram: True Dual Port RAM
    -- Xilinx Parameterized Macro, version 2022.2
    xpm_jonesmem_i : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 10,              -- DECIMAL
        ADDR_WIDTH_B => 10,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL; 8 to enable byte write enables
        BYTE_WRITE_WIDTH_B => 8,         -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "independent_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 262144,           -- DECIMAL  1024 deep x 256 bits wide
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 256,        -- DECIMAL 256 bits = 32 bytes
        READ_DATA_WIDTH_B => 256,        -- DECIMAL
        READ_LATENCY_A => 2,             -- DECIMAL
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 256,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 256,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        dbiterra => open,      -- 1-bit output: Status signal to indicate double bit error occurrence
        dbiterrb => open,      -- 1-bit output: Status signal to indicate double bit error occurrence.
        douta => jones_douta,  -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => o_rd_jones,   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,      -- 1-bit output: Status signal to indicate single bit error occurrence
        sbiterrb => open,      -- 1-bit output: Status signal to indicate single bit error occurrence
        addra => jones_addra,  -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => i_rd_Addr,     -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_MACE_clk,    -- 1-bit input: Clock signal for port A
        clkb => i_BF_clk,      -- 1-bit input: Clock signal for port B 
        dina => jones_dina,    -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => jones_dinb,    -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',            -- 1-bit input: Memory enable signal for port A
        enb => '1',            -- 1-bit input: Memory enable signal for port B
        injectdbiterra => '0', -- 1-bit input: Controls double bit error injection on input data
        injectdbiterrb => '0', -- 1-bit input: Controls double bit error injection on input data
        injectsbiterra => '0', -- 1-bit input: Controls single bit error injection on input data
        injectsbiterrb => '0', -- 1-bit input: Controls single bit error injection on input data
        regcea => '1',         -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',         -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',           -- 1-bit input: Reset signal for the final port A output register stage
        rstb => '0',           -- 1-bit input: Reset signal for the final port B output register stage
        sleep => '0',          -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => jones_we,       -- 32 bit wide write enable a (32 byte wide interface)
        web => x"00000000"     -- 32 bit wide write enable b
    );
    
    jones_dinb <= (others => '0');
    
    -- Memory for the weights
    -- 1024 deep x 32 bits wide
    --
    -- xpm_memory_tdpram: True Dual Port RAM
    -- Xilinx Parameterized Macro, version 2022.2
    xpm_weightsmemi : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 10,              -- DECIMAL
        ADDR_WIDTH_B => 10,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 32,        -- DECIMAL; 32 bit wide, single bit write enable
        BYTE_WRITE_WIDTH_B => 32,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "independent_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 32768,            -- DECIMAL  1024 deep x 32 bits wide
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 32,         -- DECIMAL
        READ_DATA_WIDTH_B => 32,         -- DECIMAL
        READ_LATENCY_A => 2,             -- DECIMAL 2 clock latency here, + address register, + dout register = 4 clock latency in the axi memory interface component. 
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 32,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 32,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        dbiterra => open,       -- 1-bit output: Status signal to indicate double bit error occurrence
        dbiterrb => open,       -- 1-bit output: Status signal to indicate double bit error occurrence.
        douta => weights_douta, -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => o_rd_weights, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,       -- 1-bit output: Status signal to indicate single bit error occurrence
        sbiterrb => open,       -- 1-bit output: Status signal to indicate single bit error occurrence
        addra => weights_addra, -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => i_rd_Addr,      -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_MACE_clk,     -- 1-bit input: Clock signal for port A
        clkb => i_BF_clk,       -- 1-bit input: Clock signal for port B 
        dina => weights_dina,   -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => weights_dinb,   -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',             -- 1-bit input: Memory enable signal for port A
        enb => '1',             -- 1-bit input: Memory enable signal for port B
        injectdbiterra => '0',  -- 1-bit input: Controls double bit error injection on input data
        injectdbiterrb => '0',  -- 1-bit input: Controls double bit error injection on input data
        injectsbiterra => '0',  -- 1-bit input: Controls single bit error injection on input data
        injectsbiterrb => '0',  -- 1-bit input: Controls single bit error injection on input data
        regcea => '1',          -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',          -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',            -- 1-bit input: Reset signal for the final port A output register stage
        rstb => '0',            -- 1-bit input: Reset signal for the final port B output register stage
        sleep => '0',           -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => weights_we,      -- 1 bit wide write enable a
        web => weights_web      -- 1 bit wide write enable b
    );    
    weights_dinb <= (others => '0');
    weights_web(0) <= '0';
            
end Behavioral;


