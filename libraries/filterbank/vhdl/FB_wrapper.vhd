----------------------------------------------------------------------------------
-- Company: CSIRO CASS 
-- Engineer: David Humphrey
-- 
-- Create Date: 14.11.2018 11:27:29
-- Module Name: FB_wrapper - Behavioral
-- Description: 
--  Wrapper for the PSS filterbank.
--  Includes :
--    - Read from HBM
--    - Filterbank
--    - Fine Delay
--    - Write to HBM
--  Designed for ~100Gb/s throughput.
--  
-- 
--  Resource Use
--  ------------
--   
--  -----------------------------------------------------------------------------------------------
--  Description
--  -----------
--
----------------------------------------------------------------------------------
library IEEE, common_lib, filterbank_lib, DSP_top_lib;
use IEEE.STD_LOGIC_1164.ALL;
use common_lib.common_pkg.all;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
--use filterbank_lib.filterbank_pkg.all;
use DSP_top_lib.DSP_top_pkg.all;

entity FB_wrapper is
    port(
        -- clock, minimum 250 MHz to process 1024 virtual channels.
        i_data_clk : in std_logic;
        i_data_rst : in std_logic;
        i_RFIScale : in std_logic_vector(4 downto 0);
        -- Data input, common valid signal, expects packets of 64 samples. 
        -- Requires at least 2 clocks idle time between packets.
        i_SOF    : in std_logic;
        -- Data is 4 stations, 8+8 complex, dual pol, each 32 bits is (7:0) = re pol0, (15:8) = im pol0, 23:16 = re pol1, 31:24 = im pol1
        i_data   : in std_logic_vector(127 downto 0);
        i_meta01 : in t_atomic_CT_pst_META_out;
        i_meta23 : in t_atomic_CT_pst_META_out;
        i_meta45 : in t_atomic_CT_pst_META_out;
        i_meta67 : in t_atomic_CT_pst_META_out;
        i_dataValid : in std_logic;
        -- Data out; bursts of 54 clocks for each channel.
        -- PST filterbank data output
        o_frameCount     : out std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
        o_virtualChannel : out t_slv_16_arr(3 downto 0);      -- 4 virtual channels, one for each of the PST data streams.
        o_HeaderValid    : out std_logic_vector(3 downto 0);
        o_Data           : out t_slv_64_arr(3 downto 0);
        o_DataValid      : out std_logic
    );
end FB_wrapper;

architecture Behavioral of FB_wrapper is

    signal FB_out_data : t_slv_32_arr(15 downto 0);
    
    signal FB_vc    : std_logic_vector(15 downto 0);
    signal dataIn  : std_logic_vector(127 downto 0);
    signal validIn : std_logic;
    
    signal PSSDout_arr, PSSDout_arr_final : t_FB_output_payload_a(3 downto 0);
    signal PSSFBHeaderValid : std_logic;
    signal PSSFBHeader : t_atomic_CT_pst_META_out_arr(3 downto 0);
    signal FB_out_valid_del, FB_out_valid : std_logic := '0';
    
    signal meta01_del1, meta01_del2, meta01_del3, meta01_del4 : t_atomic_CT_pst_META_out;
    signal meta23_del1, meta23_del2, meta23_del3, meta23_del4 : t_atomic_CT_pst_META_out;
    signal meta45_del1, meta45_del2, meta45_del3, meta45_del4 : t_atomic_CT_pst_META_out;
    signal meta67_del1, meta67_del2, meta67_del3, meta67_del4 : t_atomic_CT_pst_META_out;
    signal meta_del1_delcount, meta_del2_delcount, meta_del3_delcount, meta_del4_delcount : std_logic_vector(5 downto 0) := "000000";
    signal FDPSSDataValid : std_logic_vector(3 downto 0);
    signal pkt_count : std_logic_vector(15 downto 0) := (others => '0');
    signal past_startup_del1, past_startup_del2, past_startup_del3, past_startup_del4 : std_logic := '0';
    
    signal FDHeader : t_atomic_CT_pst_META_out_arr(3 downto 0);
    signal fb_out_valid_final, pkt_active : std_logic := '0';

begin
    
    --------------------------------------------------------------
    -- Filterbank
    
    process(i_data_clk)
    begin
        if rising_edge(i_data_clk) then
        
            -- !!!! Just replace RFI with zeros; We also need to do something to flag the output of the filterbank if the input data was flagged. 
            for i in 0 to 15 loop
                if i_data(i*8 + 7 downto i*8) = x"80" then
                    dataIn(i*8+7 downto i*8) <= x"00";
                else
                    dataIn(i*8+7 downto i*8) <= i_data(i*8 + 7 downto i*8);
                end if;
            end loop;
            validIn <= i_dataValid;
            
            if i_sof = '1' then
                pkt_count <= (others => '0');
            elsif i_dataValid = '1' and validIn = '0' then
                pkt_count <= std_logic_vector(unsigned(pkt_count) + 1);
            end if;
            
            -- Delay line for the meta data
            -- This just accounts for the latency of the filterbank.
            -- New data at most once every 65 clocks, so move on the delay line once per 64 clocks to avoid overwrites
            if i_dataValid = '1' and validIn = '0' then
                meta01_del1 <= i_meta01;
                meta23_del1 <= i_meta23;
                meta45_del1 <= i_meta45;
                meta67_del1 <= i_meta67;
                meta_del1_delcount <= "111111";
                if unsigned(pkt_count) > 10 then
                    past_startup_del1 <= '1';
                else
                    past_startup_del1 <= '0';
                end if;
            else
                meta_del1_delcount <= std_logic_vector(unsigned(meta_del1_delcount) - 1);
            end if;
            
            if meta_del1_delcount = "000001" then
                meta01_del2 <= meta01_del1;
                meta23_del2 <= meta23_del1;
                meta45_del2 <= meta45_del1;
                meta67_del2 <= meta67_del1;
                meta_del2_delcount <= "111111";
                past_startup_del2 <= past_startup_del1;
            else
                meta_del2_delcount <= std_logic_vector(unsigned(meta_del2_delcount) - 1);
            end if;
            
            if meta_del2_delcount = "000001" then
                meta01_del3 <= meta01_del2;
                meta23_del3 <= meta23_del2;
                meta45_del3 <= meta45_del2;
                meta67_del3 <= meta67_del2;
                meta_del3_delcount <= "111111";
                past_startup_del3 <= past_startup_del2;
            else
                meta_del3_delcount <= std_logic_vector(unsigned(meta_del3_delcount) - 1);
            end if;

            if meta_del3_delcount = "000001" then
                meta01_del4 <= meta01_del3;
                meta23_del4 <= meta23_del3;
                meta45_del4 <= meta45_del3;
                meta67_del4 <= meta67_del3;
                meta_del4_delcount <= "111111";
                past_startup_del4 <= past_startup_del3;
            else
                meta_del4_delcount <= std_logic_vector(unsigned(meta_del4_delcount) - 1);
            end if;
            
        end if;
    end process;    
    
    
    -- Every input packet to the filterbank generates an output packet.
    -- 
    FBi : entity filterbank_lib.PSSFBTop_4
    port map(
        -- clock, ~400 MHz to process data at about 100Gb/sec
        clk     => i_data_clk,   -- in std_logic;
        rst     => i_data_rst,   -- in std_logic;
        -- Data input, common valid signal, expects packets of 64 samples. Requires at least 2 clocks idle time between packets.
        i_data  => dataIn,       -- in (127:0);
        i_vc    => FB_vc,        -- in std_logic_vector(15 downto 0); First virtual channel
        i_valid => validIn,      -- in std_logic;
        -- Data out; bursts of 54 clocks for each channel.
        o_data  => PSSDout_arr,  -- out t_FB_output_payload_a(3 downto 0); 4 virtual channels output
        o_valid => FB_out_valid  -- out std_logic
    );
    
    --------------------------------------------------------------
    -- Fine Delay
    
    process(i_data_clk)
    begin
        if rising_edge(i_data_clk) then
            FB_out_valid_del <= FB_out_valid;
            if ((FB_out_valid = '1' and FB_out_valid_del = '0' and past_startup_del4 = '1') or
                (FB_out_valid = '1' and pkt_active = '1')) then
                FB_out_valid_final <= '1';
                pkt_active <= '1';
            else
                FB_out_valid_final <= '0';
                pkt_active <= '0';
            end if;
            PSSDout_arr_final <= PSSDout_arr;
            PSSFBHeader(0) <= meta01_del4;
            PSSFBHeader(1) <= meta23_del4;
            PSSFBHeader(2) <= meta45_del4;
            PSSFBHeader(3) <= meta67_del4;
            PSSFBHeaderValid <= FB_out_valid and (not FB_out_valid_del);
        end if;
    end process;
    
    FDGen : for i in 0 to 3 generate 
        FineDelay : entity filterbank_lib.fineDelay
        generic map (
            FBSELECTION => 0  -- 0 = PSS data
        )
        port map (
            i_clk  => i_data_clk,
            -- data and header in
            i_data        => PSSDout_arr_final(i), -- in t_FB_output_payload;  16 bit data : .Hpol.re, Hpol.im, .Vpol.re, .Vpol.im 
            i_dataValid   => FB_out_valid_final,   -- in std_logic;
            i_header      => PSSFBHeader(i),       -- in t_atomic_CT_pst_META_out; -- .HDeltaP(15:0), .VDeltaP(15:0), .frameCount(36:0), virtualChannel(15:0), .valid
            i_headerValid => PSSFBHeaderValid,     -- in std_logic;
            -- Data and Header out            
            o_data        => o_data(i),         -- out std_logic_vector(63 downto 0);   -- 16+16 complex data, dual pol.
            o_dataValid   => FDPSSDataValid(i), -- out std_logic;
            o_header      => FDHeader(i),       -- out t_atomic_CT_pst_META_out; -- .HDeltaP(15:0), .VDeltaP(15:0), .frameCount(36:0), virtualChannel(15:0), .valid
            o_headerValid => o_headerValid(i),  -- out std_logic;
    
            -------------------------------------------
            -- control and monitoring
            -- Disable the fine delay. Instead of multiplying by the output of the sin/cos lookup, just scale by unity.
            i_disable     => '0', -- in std_logic;
            -- Scale factor does nothing for the 16 bit version (i.e. this version).
            i_RFIScale    => i_RFIScale, -- in (4:0); 
            -- For monitoring of the output level.
            -- Higher level should keep track of : 
            --   * The total number of frames processed.
            --   * The sum of each of the outputs below. (but note one is superfluous since it can be calculated from the total frames processed and the sum of all the others).
            --      - These sums should be 32 bit values, which ensures wrapping will occur at most once per hour.
            -- For the correlator:
            --   - Each frame corresponds to 3456 fine channels x 2 (H & V polarisations) * 2 (re+im).
            --   - Every fine channel must be one of the categories below, so they will sum to 3456*2*2 = 13824.
            o_overflow    => open, -- hist_overflow(i),     -- out(15:0); -- Number of fine channels which were clipped.
            o_64_127      => open, -- hist_64_127(i),  -- out(15:0); -- Number of fine channels in the range 64 to 128.
            o_32_63       => open, -- hist_32_63(i),   -- out(15:0); -- Number of fine channels in the range 32 to 64.
            o_16_31       => open, -- hist_16_31(i),   -- out(15:0); -- Number of fine channels in the range 16 to 32.
            o_0_15        => open, -- hist_0_15(i),    -- out(15:0); -- Number of fine channels in the range 0 to 15.
            o_virtualChannel => open, -- hist_virtualChannel(i), -- out(8:0);
            o_histogramValid => open  -- hist_valid(i) -- out std_logic -- indicates histogram data is valid.
        );
    end generate;
    
    o_dataValid <= FDPSSDataValid(0);  -- FDPSSDataValid(3:0) will all be the same.
    o_virtualChannel(0) <= FDHeader(0).virtualChannel;
    o_virtualChannel(1) <= FDHeader(1).virtualChannel;
    o_virtualChannel(2) <= FDHeader(2).virtualChannel;
    o_virtualChannel(3) <= FDHeader(3).virtualChannel;
    o_frameCount <= FDHeader(0).frameCount;
    
end Behavioral;
