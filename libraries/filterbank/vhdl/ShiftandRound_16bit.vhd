----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 15.01.2020 12:00:27
-- Module Name: ShiftandRound - Behavioral
-- Description: 
--  Takes a 35 bit value, shifts right by 15 bits, saturates and applies convergent rounding to get a 16 bit result.  
--  Three cycle latency.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ShiftandRound_16bit is
    Port(
        i_clk   : in std_logic;
        i_data  : in std_logic_vector(34 downto 0);
        o_data16 : out std_logic_vector(15 downto 0)  -- 4 cycle latency
    );
end ShiftandRound_16bit;

architecture Behavioral of ShiftandRound_16bit is

    signal saturated : std_logic;
    signal lowZero : std_logic;
    signal dataDel1 : std_logic_vector(16 downto 0);
    signal scaled : std_logic_vector(15 downto 0);
    signal roundup : std_logic;
    signal rounded : std_logic_vector(15 downto 0);
    signal data8 : std_logic_vector(7 downto 0);
    signal signbitDel1, signbitDel2, saturatedDel2 : std_logic;

begin

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            -- Keeping 16 bits, bits 30:15 
            dataDel1 <= i_data(30 downto 14);  -- keep bit 14 for rounding.
            if i_data(34 downto 30) = "00000" or i_data(34 downto 30) = "11111" then
                saturated <= '0';
            else
                saturated <= '1';
            end if;
            if i_data(13 downto 0) = "00000000000000" then
                lowZero <= '1';
            else
                lowZero <= '0';
            end if;
            signbitDel1 <= i_data(34);
            
            ----------------------------------------------
            -- Calculate the convergent rounding.
            scaled <= dataDel1(16 downto 1);
            if (dataDel1(0) = '1' and (lowZero = '0' or (lowZero = '1' and dataDel1(1) = '1'))) then 
                roundUp <= '1';
            else
                roundUp <= '0';
            end if;
            signbitDel2 <= signbitDel1;
            saturatedDel2 <= saturated;
            
            ----------------------------------------------------
            -- Apply convergent rounding
            if saturatedDel2 = '1' or scaled = "0111111111111111" then
                if signbitDel2 = '1' then
                    rounded <= "1000000000000000";
                else
                    rounded <= "0111111111111111";
                end if;
            else
                if roundUp = '1' then
                    rounded <= std_logic_vector(unsigned(scaled) + 1);
                else
                    rounded <= scaled;
                end if;
            end if;
            
            ------------------------------------------------------
            -- 
            o_data16 <= rounded;
            
        end if;
    end process;

end Behavioral;
