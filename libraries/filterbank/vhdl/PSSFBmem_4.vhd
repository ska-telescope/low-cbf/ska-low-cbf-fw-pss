----------------------------------------------------------------------------------
-- Company: CSIRO - CASS 
-- Engineer: David Humphrey
-- 
-- Create Date: 15.11.2018 09:30:43
-- Module Name: fb_mem - Behavioral
-- Description: 
--  Memories for the PSS Filterbank.
-- Notes:
--  * The number of taps is semi-configurable; some modification is required if "TAPS" is not set to 12.
--  * Read data is staggered by one clock for each of the 12 samples, so that the FIR filter can use the adders in the DSPs.
--
----------------------------------------------------------------------------------
library IEEE, common_lib, filterbank_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use common_lib.common_pkg.all;
Library xpm;
use xpm.vcomponents.all;

entity PSSFBMem_4 is
    generic (
        TAPS : integer := 12      -- Note only partially parameterized; modification needed to support anything other than 12.
    );
    port(
        clk          : in std_logic;
        -- Write data for the start of the chain
        i_wrData     : in std_logic_vector(127 downto 0);
        i_wrEn       : in std_logic; -- should be a burst of 64 clocks.
        -- Read data, comes out 2 clocks after the first write.
        o_rd_data    : out t_slv_128_arr(TAPS-1 downto 0); -- 128 (=4*2*16) bits wide, 12 taps simultaneously; First sample is i_wr_data delayed by 1 clock. 
        o_coef       : out t_slv_18_arr(TAPS-1 downto 0)   -- 18 bits per filter tap.
    );
end PSSFBMem_4;

architecture Behavioral of PSSFBMem_4 is
    
    signal FIRTapAddrDel1, FIRTapAddrDel2, FIRTapAddrDel3 : std_logic_vector(3 downto 0) := "0000";
    
    signal rdAddr : std_logic_vector(5 downto 0);
    signal rdAddrDel : t_slv_9_arr((TAPS) downto 0) := (others => (others => '0'));
    signal wrDataDel1 : std_logic_vector(127 downto 0);
    signal wrEnDel1 : std_logic := '0';
    
    signal wr_en_slv : std_logic_vector(0 downto 0);
    
    signal romAddrDel : t_slv_6_arr((TAPS-1) downto 0):= (others => (others => '0'));
    signal wrEnDel : t_slv_1_arr(TAPS downto 0); 
    signal rdDataDel : t_slv_128_arr((TAPS-1) downto 0);
    signal rdDataDel2 : t_slv_128_arr((TAPS-1) downto 0);
    
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
        
            wrDataDel1 <= i_wrData;       -- Extra Delay on the the input data so that the read data from the first coefficient ROM matches the first data output.
            rdDataDel2(0) <= wrDataDel1;  -- Two cycle latency for the first data; Each memory has a 2 cycle latency so second data has 3 cycle latency.
        
            wrEnDel1 <= i_wrEn;
            wrEnDel(1)(0) <= wrEnDel1; 
            if i_wrEn = '0' then
                rdAddr <= (others => '0');
            else
                rdAddr <= std_logic_vector(unsigned(rdAddr) + 1);
            end if;
            
            rdAddrDel(TAPS downto 1) <= rdAddrDel(TAPS-1 downto 0);
            
            romAddrDel(0) <= rdAddr;
            romAddrDel((TAPS-1) downto 1) <= romAddrDel((TAPS-2) downto 0);
            
            wrEnDel(TAPS downto 2) <= wrEnDel(TAPS-1 downto 1);
        end if;
    end process;
    
    rdAddrDel(0) <= "000" & rdAddr;
    
    dataMem : for i in 1 to (TAPS-1) generate
        
        xpm_memory_sdpram_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,               -- DECIMAL
            ADDR_WIDTH_B => 9,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 128,       -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "auto",      -- String
            MEMORY_SIZE => 65536,            -- DECIMAL - 512 x 128 = 65536
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 128,        -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            USE_MEM_INIT_MMI => 0,           -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 128,       -- DECIMAL
            WRITE_MODE_B => "no_change",     -- String
            WRITE_PROTECT => 0               -- DECIMAL  -- if '1', adds a lut to gate the wr enable based on ena/enb (I think)
        ) port map (
            dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
            doutb => rdDataDel(i),   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
            addra => rdAddrDel(i+1), -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => rdAddrDel(i-1), -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => clk,           -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
            clkb => clk,           -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is "independent_clock". 
            dina => rdDataDel2(i-1), -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',            -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
            enb => '1',            -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
            injectdbiterra => '0', -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
            injectsbiterra => '0', -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
            regceb => '1',         -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',           -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',          -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => wrEnDel(i)      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used. 
        );
        
        -- Extra pipeline stage at the output of the memory; avoids read/write collisions. 
        process(clk)
        begin
            if rising_edge(clk) then
                rdDataDel2(i) <= rdDataDel(i);  
            end if;
        end process;
        
        o_rd_data(i) <= rdDataDel2(i);
        
    end generate;
    
    o_rd_data(0) <= rdDataDel2(0);
    
    --------------------------------------------------------------------------------------
    -- Filter Coefficients
    -- 12 memories, each 18 bits wide, 64 deep.
    
    FIRTapsi : entity filterbank_lib.PSSFIRTaps
    port map (
        clk      => clk, -- in std_logic;
        i_addr   => romAddrDel, -- in std_logic_vector(5 downto 0);   -- 64 entries
        -- read data, 1 clock latency 
        o_coef   => o_coef --  out t_slv_18_arr(11 downto 0)   -- 18 bits per filter tap.
    );
    
end Behavioral;

