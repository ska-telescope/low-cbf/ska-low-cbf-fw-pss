----------------------------------------------------------------------------------
-- Company: CSIRO - CASS 
-- Engineer: David Humphrey
-- 
-- Create Date: 04.12.2018 16:17:07
-- Module Name: PSSFBtesttop - Behavioral
-- Description: 
--  Top level to test place and route of the PSS filterbank. 
----------------------------------------------------------------------------------
library IEEE, filterbanks_lib, common_lib;
use common_lib.common_pkg.all;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PSSFBtesttop is
    port(
        clkin : in std_logic;
        din  : in std_logic;
        dout : out std_logic_vector(31 downto 0)
     );
end PSSFBtesttop;

architecture Behavioral of PSSFBtesttop is

    component clk_wiz_0
    port (
        clk400out : out std_logic;
        clk100in  : in  std_logic);
    end component;

    signal clk400 : std_logic;
    signal rst400 : std_logic;
    signal rstCount : std_logic_vector(7 downto 0) := "11111111";
    signal validIn, validOut : std_logic;
    
    signal din1  : t_slv_16_arr(15 downto 0);
    signal count16 : std_logic_vector(15 downto 0);
    signal dout1 : t_slv_32_arr(15 downto 0);
    signal vcout : std_logic_vector(15 downto 0);

begin

    c1 : clk_wiz_0
    port map (
        clk100in => clkin,
        clk400out => clk400
    );
    
    process(clk400)
    begin
        if rising_edge(clk400) then
            if rstCount /= "00000000" then
                rst400 <= '1';
                rstCount <= std_logic_vector(unsigned(rstCount) - 1);
                count16 <= (others => '0');
            else
                rst400 <= '0';
                count16 <= std_logic_vector(unsigned(count16) + 1);
            end if;
        
            for i in 0 to 15 loop
                if rst400 = '1' then
                    din1(i) <= std_logic_vector(to_unsigned(i,16));
                else
                    din1(i) <= std_logic_vector(unsigned(din1(i)) + 1);
                end if;
            end loop;
            
            validIn <= rstCount(2); 
            
            dout <= dout1(to_integer(unsigned(count16(3 downto 0))));
            
        end if;
    end process;
    
    
    fb : entity filterbanks_lib.PSSFBTop
    port map(
        -- clock, target is 380 MHz
        clk => clk400,
        rst => rst400,
        
        -- Data input, common valid signal, expects packets of 64 samples. Requires at least 2 clocks idle time between packets.
        i_data  => din1,    -- in t_slv_16_arr(15 downto 0);  -- 16 Inputs, each complex data, 8 bit real (7:0), 8 bit imaginary (15:8).
        i_vc    => count16, -- in std_logic_vector(15 downto 0); -- First virtual channel
        i_valid => validIn, -- in std_logic;
        
        -- Data input, common valid signal, expects packets of 4096 samples. Requires at least 2 clocks idle time between packets.
        o_data  => dout1, -- : out t_slv_32_arr(15 downto 0); -- 16 outputs, real and imaginary parts in (15:0) and (31:16) respectively;
        o_vc    => vcout, -- : out std_logic_vector(15 downto 0);
        o_valid => validOut -- : out std_logic
    );
     
end Behavioral;
