Matlab code to generate the filter taps
Run "get_rom_coefficients.m" to :
 - Generate the FIR taps, using "generate_MaxFlt_centered.m" 
 - Generate plots showing the filter taps and the frequency response
 - Write to a text file, which is used by the python code for verifying the filterbank
 - Create "PSSFIRTaps.vhd" which is the lookup table/ROM used by the firmware

The other matlab scripts in this directory are legacy functions which have in the past been used to verify the filterbank. However this is now done by the python code (look in the ct1 directory for the testbench which instantiates ct1 and filterbank, and python code that checks the output).
