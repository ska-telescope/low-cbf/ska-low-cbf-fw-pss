% Get the ROM contents for the FIR filter coefficients
filtertaps = round(2^17 * generate_MaxFlt_centered(64,12));   % PSS, 64 point FFT, 12 taps.

% simple text file
save('pss_filtertaps.txt','filtertaps','-ascii')

% Write as a lookup table in VHDL

fid = fopen('PSSFIRTaps.vhd','w');

fprintf(fid,'-- Created with matlab script get_rom_coefficients.m \n');
fprintf(fid,'-- Using matlab function "generate_MaxFlt_centered(64,12)" to generate the filter taps\n');
dt = datestr(datetime);
fprintf(fid,['-- Creation date : ' dt ' \n\n']);
fprintf(fid,'library IEEE, common_lib;\n');
fprintf(fid,'use IEEE.STD_LOGIC_1164.ALL;\n');
fprintf(fid,'use IEEE.NUMERIC_STD.ALL;\n');
fprintf(fid,'use common_lib.common_pkg.all;\n');
fprintf(fid,' \n');
fprintf(fid,'entity PSSFIRTaps is\n');
fprintf(fid,'    port(\n');
fprintf(fid,'        clk      : in std_logic;\n');
fprintf(fid,'        i_addr   : in t_slv_6_arr(11 downto 0);   -- 12 ROMs, 64 entries each\n');
fprintf(fid,'        -- read data, 1 clock latency \n');
fprintf(fid,'        o_coef   : out t_slv_18_arr(11 downto 0)   -- 18 bits per filter tap.\n');
fprintf(fid,'    );\n');
fprintf(fid,'end PSSFIRTaps;\n');
fprintf(fid,'\n');
fprintf(fid,'architecture Behavioral of PSSFIRTaps is\n');
fprintf(fid,'\n');
fprintf(fid,'    signal coef_int : t_slv_18_arr(11 downto 0);\n');
fprintf(fid,'begin\n');
fprintf(fid,'   \n');
fprintf(fid,'    process(clk)\n');
fprintf(fid,'    begin\n');
fprintf(fid,'        if rising_edge(clk) then\n');
            
for rom = 1:12
    fprintf(fid,['            o_coef(' num2str(rom-1) ') <= coef_int(' num2str(rom-1) ');\n']); 
end

fprintf(fid,'        end if;\n');
fprintf(fid,'    end process;\n');

for rom = 1:12
    fprintf(fid,['    with i_addr(' num2str(rom-1) ') select \n']);
    fprintf(fid,['    coef_int(' num2str(rom-1) ') <= \n']);
    for rline = 1:64
        % dstr = dec2binX(filtertaps((rom-1)*64 + (rline-1) + 1),18);
        dstr = dec2binX(filtertaps((12-rom)*64 + (rline-1) + 1),18);
        if rline == 64
            fprintf(fid,['        "' dstr '" when others;\n']);
        else
            fprintf(fid,['        "' dstr '" when "' dec2binX(rline-1,6) '",\n']);
        end
    end
    
end

fprintf(fid,'end;\n');
fclose(fid);
