create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_ct1_poly
set_property -dict [list \
  CONFIG.SUPPORTS_NARROW_BURST {0} \
  CONFIG.SINGLE_PORT_BRAM {1} \
  CONFIG.Component_Name {axi_bram_ctrl_ct1_poly} \
  CONFIG.READ_LATENCY {3} \
  CONFIG.MEM_DEPTH {65536}] [get_ips axi_bram_ctrl_ct1_poly]
create_ip_run [get_ips axi_bram_ctrl_ct1_poly]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp64_add
set_property -dict [list \
  CONFIG.A_Precision_Type {Double} \
  CONFIG.Add_Sub_Value {Add} \
  CONFIG.C_A_Exponent_Width {11} \
  CONFIG.C_A_Fraction_Width {53} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {14} \
  CONFIG.C_Mult_Usage {Full_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {11} \
  CONFIG.C_Result_Fraction_Width {53} \
  CONFIG.Component_Name {fp64_add} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Result_Precision_Type {Double} \
] [get_ips fp64_add]
create_ip_run [get_ips fp64_add]


create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp64_to_int
set_property -dict [list \
  CONFIG.A_Precision_Type {Double} \
  CONFIG.C_A_Exponent_Width {11} \
  CONFIG.C_A_Fraction_Width {53} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {6} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {32} \
  CONFIG.C_Result_Fraction_Width {32} \
  CONFIG.Component_Name {fp64_to_int} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Float_to_fixed} \
  CONFIG.Result_Precision_Type {Custom} \
] [get_ips fp64_to_int]
create_ip_run [get_ips fp64_to_int]
