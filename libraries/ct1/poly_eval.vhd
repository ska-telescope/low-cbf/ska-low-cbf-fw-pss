----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 04 june 2023 01:11:00 AM
-- Module Name: poly_eval - Behavioral
-- Description: 
--  Evaluate Delay polynomials.
--  Each polynomial is defined by 6 double precision values that are read from the
--  memory (o_rd_addr, i_rd_data).
--  A seventh value defines the sky frequency in cycles per ns = GHz = (freq in Hz) * 1e-9.
--
-- For PSS, we have to evaluate the polynomials 
--  - 4 different polynomials 
--      - (because the corner turn sends on 4 virtual channels at a time)
--  - New value required once every ~64 clocks.
--     - New output packet every 65 clocks, but can go a bit slower depending on the number 
--       of virtual channels supported.
--     - For 1024 virtual channels and 300 MHz clock, there is about 80 clocks available per packet.
--       24 SPS packet per frame, 2048 samples per packet, 1080ns samples, 1024 virtual channels,
--       768 + 11 (preload) x (64-sample packets), 4 filterbanks in parallel.
--        = (24*2048*1080e-9/3.333e-9) / (1024*(768+11)/4) = 79.9 clocks)
--  - 64 values are generated per corner turn frame
--     - 64 * (4096 samples) * (1080ns/sample) = 283.11552 ms
--     - Time step between packets = 4096*1080ns = 4423680 ns = 0.004423680 seconds
--
--
--  Polynomial data is stored in a memory block that this module accesses through 
--  the "o_rd_addr", "i_rd_data" ports.
--  Within that memory :
--    words 0 to 9 : Config for virtual channel 0, buffer 0 (see below for specification of contents)
--    words 10 to 19 : Config for virtual channel 1, buffer 0
--    ...
--    words 10230 to 10239 : Config for virtual channel 1023, first buffer
--    words 10240 to 20479 : Config for all 1024 virtual channels, second buffer
--
--  Polynomial data is stored in the memory as a block of 9 x 64bit words for each virtual channel:   
--      word 0 = c0,
--       ...  
--      word 5 = c5,
--               c0 to c5 are double precision floating point values for the delay polynomial :
--               c0 + c1*t + c2 * t^2 + c3 * t^3 + c4 * t^4 + c5 * t^5
--               Units for c0,.. c5 are ns/s^k for k=0,1,..5
--      word 6 = Sky frequency in GHz
--               Used to convert the delay (in ns) to a phase rotation.
--               (delay in ns) * (sky frequency in GHz) = # of rotations
--               From the Matlab code:
--                % Phase Rotation
--                %  The sampling point is (DelayOffset_all * Ts)*1e-9 ns
--                %  then multiply by the center frequency (CF) to get the number of rotations.
--                %
--                %  The number of cycles of the center frequency per output sample is not an integer due to the oversampling of the LFAA data.
--                %  For example, for coarse channel 65, the center frequency is 65 * 781250 Hz = 50781250 Hz.
--                %  50781250 Hz = a period of 1/50781250 = 19.692 ns. The sampling period for the LFAA data is 1080 ns, so 
--                %  a signal at the center of channel 65 goes through 1080/19.692 = 54.8438 cycles. 
--                %  So a delay which is an integer number of LFAA samples still requires a phase shift to be correct.
--                resampled = resampled .* exp(1i * 2*pi*DelayOffset_all * Ts * 1e-9 * CF);
--                # Note : DelayOffset_all = delay in number of samples (of period Ts)
--                #        Ts = sample period in ns (i.e. 1080 for SPS data)
--                #        CF = channel center frequency in Hz, e.g. 65 * 781250 = 50781250 for the first SPS channel
--                #        - The value [Ts * 1e-9 * CF] is the value stored here.
--
--      word 7 = buf_offset_seconds : seconds from the polynomial epoch to the start of the integration period, as a double precision value 
--              
--      word 8 = double precision offset in ns for the second polarisation (relative to the first polarisation).   
--
--      word 9 = Validity time
--               - bits 31:0 = buf_frame : Corner turn frame at which the polynomial becomes valid.
--                                         In units of 53.084160 ms, i.e. units of (24 SPS packets), relative to the epoch.
--               - bit 32 = Entry is valid.
--
-- -------------------------------------------------------------------------------
-- Processing Steps :
--  - Work out which polynomial is valid for each virtual channel:
--      - Read word 7 (validity time) for the two buffers
--        Pick the correct buffer
--  - Determine the time to use for "t" in the polynomial.
--      - Note "t" can be different for each virtual channel, since each virtual 
--        channel can have a different validity time
--      - To calculate t :
--          - Find PSS samples from start of validity 
--              ct_frame_offset = i_ct_frame - buf_frame
--              - Time offset in units of output samples relative to the epoch
--                There are 768 output time samples per corner turn frame  
--                = (24*2048 SPS samples) / (64 SPS samples / PSS sample) = 768 PSS samples/corner turn frame
--              sample_offset = ct_frame_offset * 768
--          - Calculate for g_TIMES_PER_BLOCK (== 8) consecutive samples
--              sample_offset = sample_offset + [0:8]
--              sample_offset_seconds = (double)(sample_offset * 69120e-9 = 64*1080ns)
--              sample_offset_epoch = sample_offset_seconds + buf_offset_seconds
--          - For each new time sample within a corner turn frame, add g_TIMES_PER_BLOCK (== 8) samples to each time 
--              add (8*64*1080e-9) = 552960 ns = 0.000552960 seconds
--  - Evaluate polynomial to get a delay in samples and phase offset:
--      - get c0
--      - get c1
--      - * : c1 * t
--      - + : c0 + c1*t
--      - * : t*t
--      - * : c2*t*t
--      - + : c0 + c1*t + c2*t*t
--      - * : t*t*t
--      - * : c3*t*t*t
--      - + : c0 + c1*t + c2*t*t + c3*t*t*t
--      - * : t*t*t*t
--      - * : c4*t*t*t*t
--      - + : c0 + c1*t + c2*t*t + c3*t*t*t + c4*t*t*t*t
--      - * : t*t*t*t*t
--      - * : c5*t*t*t*t*t
--      - + : delay_ns_hpol = c0 + c1*t + c2*t*t + c3*t*t*t + c4*t*t*t*t + c5*t*t*t*t*t
--      - + : delay_ns_vpol = delay_ns_hpol + offset (offset comes from config memory word 9)
--      - * : delay_samples_hpol = delay_ns_hpol * (1/1080ns)
--          : delay_samples_vpol = delay_ns_vpol * (1/1080ns)
--          - Convert to 32.32 bit fixed point value
--          - Integer part of delay_samples_hpol is o_sample_offset
--          - fractional part is o_hpol_deltaP, as a 14 bit value
--               so .111111111 => o_hpol_deltaP = 16383
--          - subtract off the integer part of delay_samples_hpol from delay_samples_vpol
--               -> gives o_vpol_deltaP
--      - * : Hpol rotations = (double) delay_ns_hpol * (sky_frequency_GHz) 
--      - * : Vpol rotations = (double) delay_ns_hpol * (sky_frequency_GHz)
--      - to_int : o_Hpol_phase = (int64) Hpol rotations
--                 o_Vpol_phase = (int64) Vpol rotations
--                 - o_Hpol/Vpol_phase = fractional bits of hpol/vpol_phase  
--
--
-- fsm states : 
--   Per virtual channel -(1) (a) Read validity time buffer 0
--                            (b) Read validity time buffer 1
--                            - Trigger pipeline to calculate :
--                                - 
--                        (2) 
--                        (2) (a) Read c0
--                            (b) Read c1
--
----------------------------------------------------------------------------------
library IEEE, ct_lib;
use IEEE.STD_LOGIC_1164.ALL;
library vd_datagen_lib;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
USE common_lib.common_pkg.ALL;

entity poly_eval is
    generic(
        -- Number of virtual channels to generate in at a time, code supports up to 16
        -- Code assumes at least 4, otherwise it would need extra delays to wait for data to return from the memory. 
        g_VIRTUAL_CHANNELS : integer range 4 to 16 := 4;
        g_TIMES_PER_BLOCK : integer := 8; -- Time steps to calculate for each virtual channel in a block.
        g_VC_LOG2 : integer := 2;
        -- Total number of time samples run is g_TIMESTEPS * g_TIMES_PER_BLOCK (96 * 8 = 768 = PSS samples per corner turn frame)
        g_TIMESTEPS : integer := 96;
        -- Time per corner turn frame as a double precision value. 27 * 2048* 1080ns = 0.05971968
        -- g_fp64_ct_frame : std_logic_vector(63 downto 0) := x"3FAE9393F10E1FA8";
        -- for 256 timesteps, 24 * 2048 * 1080ns = 0.053084160 seconds
        g_fp64_ct_frame : std_logic_vector(63 downto 0) := x"3FAB2DD8D6457179";
        -- Time step in seconds for a single packet; for PST, 256 point FFT, 4/3 oversampled = 192 * 1080ns = 0.000207360
        --g_fp64_one_packet : std_logic_vector(63 downto 0) := x"3F2B2DD8D6457179"
        -- Time step in seconds for a single packet; for PST, 64 point FFT, critically sampled = 64 * 1080ns = 0.00006912
        g_fp64_one_packet : std_logic_vector(63 downto 0) := x"3F121E908ED8F651"
    );
    port(
        clk : in std_logic;
        -- First output after a reset will reset the data generation
        i_rst : in std_logic;
        -- Control
        i_start            : in std_logic; -- start on a batch of 4 polynomials
        i_virtual_channels : in t_slv_16_arr((g_VIRTUAL_CHANNELS-1) downto 0); -- List of virtual channels to evaluate; this maps to the address in the lookup table.
        i_ct_frame         : in std_logic_vector(39 downto 0);  -- Corner turn frame; counts from the epoch.
        i_polyValidTime    : in std_logic_vector(23 downto 0);  -- Number of corner turn frames polynomials are valid for.
        o_idle             : out std_logic;
        o_bad_poly         : out std_logic; -- No valid polynomial detected.
        o_no_valid_buffer_count : out std_logic_vector(15 downto 0); -- count of invalid polynomials detected.
        -- read the config memory (to get polynomial coefficients)
        -- Block ram interface for access by the rest of the module
        -- Memory is 20480 x 8 byte words = (2 buffers) x (10240 words) = (1024 virtual channels) x (10 words)
        -- read latency 3 clocks
        o_rd_addr  : out std_logic_vector(14 downto 0);
        i_rd_data  : in std_logic_vector(63 downto 0);  -- 3 clock latency.
        
        -----------------------------------------------------------------------
        -- Output delay parameters 
        -- For each pulse on i_start, this module generates 
        --  g_TIMESTEPS*g_TIMES_PER_BLOCK*4 = 4 * 768 = 3072 outputs,
        -- in bursts of g_VIRTUAL_CHANNELS (=4) outputs. (4 virtual channels, 768 time samples each)
        --
        -- For each virtual channel :
        --  - o_vc = Virtual channel. 16 bits. Copy of one of the i_poly entries
        --  - o_packet = Packet count, 16 bits. Counts from 0 to 767 for the 768 PSS time samples per corner turn frame 
        --  - Coarse delay : 11 bits. Number of 1080ns samples to delay by
        --  - bufHpolDeltaP : 32 bits. Delay as a phase step across the coarse channel
        --  - bufHpolPhase  : 32 bits. Phase offset for H pol
        --  - bufVpolDeltaP : 32 bits. Delay as a phase step across the coarse channel
        --  - bufVpolPhase  : 32 bits. Phase offset for V pol
        o_vc : out std_logic_vector(15 downto 0);
        o_vc_count : out std_logic_vector(7 downto 0);
        o_packet : out std_logic_vector(15 downto 0);
        --
        o_sample_offset : out std_logic_vector(11 downto 0); -- Number of whole 1080ns samples to delay by.
        -- Units for deltaP are rotations; 1 sign bit, 15 fractional bits. + 16 more fractional bits
        -- So pi radians at the band edge = 16384 * 65536.
        -- As a fraction of a coarse sample, 1 coarse sample = pi radian at the band edge = 16384 * 65536
        --                                   0.5 coarse samples = pi/2 radians at the band edge = 8192 * 65536
        o_Hpol_deltaP : out std_logic_vector(31 downto 0);
        -- Phase uses 32768*65536 to represent pi radians. Note this differs by a factor of 2 compared with Hpol_deltaP.
        o_Hpol_phase : out std_logic_vector(31 downto 0);
        o_Vpol_deltaP : out std_logic_vector(31 downto 0);
        o_Vpol_phase : out std_logic_vector(31 downto 0);
        o_valid : out std_logic
    );
end poly_eval;

architecture Behavioral of poly_eval is

    -- double precision 1e-9
    constant c_fp64_1eMinus9 : std_logic_vector(63 downto 0) := x"3E112E0BE826D695";
    -- Need to multiply time in ns by the period in ns to get number of samples,
    -- divide by 1080 = multiply by 1/1080
    constant c_fp64_rate : std_logic_vector(63 downto 0) := x"3F4E573AC901E574"; -- = 1/1080
    -- one PSS sample, seconds, fp64
    constant c_PSS_sample : std_logic_vector(63 downto 0) := x"3F121E908ED8F651"; -- = 69120e-9 = 64 * 1080 ns
    -- 8 PSS samples, seconds, fp64
    constant c_8PSS_samples : std_logic_vector(63 downto 0) := x"3F421E908ED8F651"; -- = 8 * 64 * 1080e-9
    -- double precision 1
    constant c_fp64_unity : std_logic_vector(63 downto 0) := x"3FF0000000000000"; -- == 1
    
    
    COMPONENT fp64_add
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    END COMPONENT;
    
    COMPONENT fp64_mult
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    END COMPONENT;

    COMPONENT fp64_to_int
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    END COMPONENT;

    component uint64_to_double
    port (
        aclk : in std_logic;
        s_axis_a_tvalid : in std_logic;
        s_axis_a_tdata : in std_logic_vector(63 downto 0);
        m_axis_result_tvalid : out std_logic;
        m_axis_result_tdata : out std_logic_vector(63 downto 0));
    end component;
    
    signal poly_term : std_logic_vector(63 downto 0);
    signal time_ns : std_logic_vector(63 downto 0);
    signal time_s : std_logic_vector(63 downto 0);
    signal running : std_logic := '0';
    signal start_del1, start_del2 : std_logic := '0';
    signal phase_del : t_slv_4_arr(32 downto 0);
    signal phase_count_del : t_slv_11_arr(32 downto 0);
    signal short_phase : std_logic;
    signal fp64_add_valid_in, fp64_mult_valid_in, fp64_to_int_valid_in : std_logic := '0';
    signal fp64_add_din0, fp64_add_din1, fp64_add_dout : std_logic_vector(63 downto 0);
    signal fp64_mult_din0, fp64_mult_din1, fp64_mult_dout : std_logic_vector(63 downto 0);
    signal fp64_add_valid_out, fp64_mult_valid_out, fp64_to_int_valid_out : std_logic := '0';
    signal fp64_to_int_din, fp64_to_int_dout : std_logic_vector(63 downto 0);
    
    signal int_to_fp64_valid_in, int_to_fp64_valid_out : std_logic;
    signal int_to_fp64_din, int_to_fp64_dout : std_logic_vector(63 downto 0);    
    
    signal cur_val_rdAddr : std_logic_vector(8 downto 0); 
    signal cur_val_rdData : std_logic_vector(63 downto 0);
    signal cur_val_wrAddr : std_logic_vector(8 downto 0);
    signal cur_val_wrEn : std_logic := '0';
    signal cur_val_wrData : std_logic_vector(63 downto 0);
    signal offset_del1, offset_del2 : std_logic_vector(19 downto 0);
    signal frame_count : std_logic_vector(31 downto 0) := x"00000000";
    signal startup : std_logic := '0';
    signal long_phase : std_logic := '0';

    ------------------------------------------------------
    type poly_fsm_type is (start, wait_x10, get_validity_buf0, get_validity_buf1, run_t_calculation, get_buf_offset, wait_t_calculation, add_packet_time, wait_add_packet_time, 
get_c0, get_c1, get_c2, get_c3, get_c4, get_c5, wait_poly_done, add_vpol_offset, mult_hpol_1_on_1080ns, mult_hpol_sky_frequency, wait_vpol, vpol_idle, mult_vpol_1_on_1080ns,
mult_vpol_sky_frequency, wait_done, send_values, done, wait_new_vc);
    
    signal poly_fsm : poly_fsm_type := wait_new_vc;
    type t_poly_fsm_del is array(47 downto 0) of poly_fsm_type;
    signal poly_fsm_del : t_poly_fsm_del;
    signal virtual_channels : t_slv_16_arr((g_VIRTUAL_CHANNELS-1) downto 0);
    signal virtual_channels_x10, virtual_channels_x8, virtual_channels_x2, vc_base_addr : t_slv_20_arr((g_VIRTUAL_CHANNELS-1) downto 0);
    --signal integration : std_logic_vector(31 downto 0);
    signal ct_frame : std_logic_vector(39 downto 0);
    signal vc_count : std_logic_vector((g_VC_LOG2-1) downto 0);
    signal vc_count_del : t_slv_4_arr(47 downto 0);
    signal state_count_del : t_slv_8_arr(47 downto 0);
    signal no_valid_buffer_count : std_logic_vector(15 downto 0) := x"0000";
    signal state_count : std_logic_vector(7 downto 0);
    
    signal cur_time, cur_poly_state, cur_time_n : t_slv_64_arr((g_VIRTUAL_CHANNELS*g_TIMES_PER_BLOCK-1) downto 0);
    signal sample_offset : t_slv_12_arr((g_VIRTUAL_CHANNELS-1) downto 0);
    signal sample_diff : t_slv_12_arr((g_VIRTUAL_CHANNELS*g_TIMES_PER_BLOCK-1) downto 0);
    
    signal ct_frame_buf0_del5, ct_frame_buf1_del6, ct_frame_buf0_del6, ct_frame_buf0_del7, ct_frame_buf1_del7, ct_frame_offset_del8  : std_logic_vector(39 downto 0);
    signal valid_buf0, valid_buf1_del6, buf0_ok_del6, buf1_more_recent_del7, buf1_ok_del7, buf0_ok_del7 : std_logic := '0';
    signal buffer_select : std_logic_vector(g_VIRTUAL_CHANNELS-1 downto 0);
    signal packets_sent : std_logic_vector(11 downto 0) := x"000";
    signal packets_sent_eq_zero_del : std_logic_vector(31 downto 0);
    
    signal Hpol_deltaP, Hpol_phase : t_slv_32_arr((g_VIRTUAL_CHANNELS*g_TIMES_PER_BLOCK - 1) downto 0);
    signal Vpol_deltaP, Vpol_phase : t_slv_32_arr((g_VIRTUAL_CHANNELS*g_TIMES_PER_BLOCK - 1) downto 0);
    
    signal poly_rd_addr : std_logic_vector(19 downto 0);
    signal polyValidTime_ext : std_logic_vector(39 downto 0);
    signal ct_frame_buf0_del7_end_of_validity,  ct_frame_buf1_del7_end_of_validity : std_logic_vector(39 downto 0);
    signal buf0_out_of_range, buf1_out_of_range : std_logic;
    signal buf0_ok_del8, buf1_ok_del8 : std_logic;
    
    signal fp64_t_mult_valid_in : std_logic;
    signal fp64_t_mult_din0, fp64_t_mult_din1 : std_logic_vector(63 downto 0);
    signal cur_time_n_wrEn : std_logic := '0';
    signal cur_time_n_wrData, fp64_t_mult_dout : std_logic_vector(63 downto 0);
    signal cur_time_n_wrAddr : std_logic_vector(4 downto 0);
    signal ct_sample_offset_del9 : std_logic_vector(63 downto 0);
    signal cur_time_n_rdData, cur_time_rdData, cur_time_wrData : std_logic_vector(63 downto 0);
    signal buf_offset_seconds : std_logic_vector(63 downto 0);
    signal cur_time_rdAddr, cur_time_wrAddr : std_logic_vector(4 downto 0);
    signal cur_time_wrEn : std_logic;
    
    signal cur_state_wrEn : std_logic;
    signal cur_state_wrData, cur_state_rdData : std_logic_vector(63 downto 0);
    signal cur_state_wrAddr, cur_state_rdAddr : std_logic_vector(4 downto 0);
    signal fp64_t_mult_valid_out : std_logic;
    signal output_select_del24, output_select_del0 : std_logic_vector(4 downto 0);
    
begin
    
    o_rd_addr <= poly_rd_addr(14 downto 0);
    o_no_valid_buffer_count <= no_valid_buffer_count;
    
    vc_gen : for i in 0 to (g_VIRTUAL_CHANNELS-1) generate
        -- Logic for each of the virtual channels being processed
        
        virtual_channels_x8(i) <= '0' & virtual_channels(i) & "000";
        virtual_channels_x2(i) <= "000" & virtual_channels(i) & '0';
        
        process(clk)
        begin
            if rising_edge(clk) then
                virtual_channels_x10(i) <= std_logic_vector(unsigned(virtual_channels_x8(i)) + unsigned(virtual_channels_x2(i)));
                
                if buffer_select(i) = '0' then
                    vc_base_addr(i) <= virtual_channels_x10(i);
                else
                    vc_base_addr(i) <= std_logic_vector(unsigned(virtual_channels_x10(i)) + 10240);
                end if;
                
                if (poly_fsm_del(24) = mult_hpol_1_on_1080ns) then
                    -- fp64_to_int_din is valid on del(18),
                    -- 6 clock latency, so output is valid on del(24)
                    if (to_integer(unsigned(vc_count_del(24)(1 downto 0))) = i) then
                        if packets_sent_eq_zero_del(24) = '1' then
                            -- Sample offset is only set once (at the start of each frame) 
                            sample_offset(i) <= fp64_to_int_dout(43 downto 32);
                        end if;
                    end if;
                end if;
            end if;
        end process;
    
    end generate;
    
    time_and_state_gen : for i in 0 to (g_TIMES_PER_BLOCK - 1) generate
        vc_gen : for vc in 0 to (g_VIRTUAL_CHANNELS - 1) generate
            -- Logic for each of the parallel polynomials being processed.
            -- Expected : 4 virtual channels * 8 time samples each = 32 parallel polynomial evaluations
            -- vc * g_TIMES_PER_BLOCK + i
            process(clk)
            begin
                if rising_edge(clk) then 
                    if (poly_fsm_del(24) = mult_hpol_1_on_1080ns) then
                        -- fp64_to_int_din is valid on del(18),
                        -- 6 clock latency, so output is valid on del(24)
                        if (to_integer(unsigned(output_select_del24)) = (vc * g_TIMES_PER_BLOCK + i)) then
    
                            if packets_sent_eq_zero_del(24) = '1' then
                                --  .111111111 => o_hpol_deltaP = 16383
                                Hpol_deltaP(vc * g_TIMES_PER_BLOCK + i) <= "00" & fp64_to_int_dout(31 downto 2);
                            else
                                -- For later frames, sample offset can't be changed, so 
                                -- deltaP can be more than 1 sample.
                                -- 3 possible cases:
                                --   sample_diff = 0  --> Hpol_deltaP <= "00" & fp64_to_int_dout(31 downto 18);
                                --   sample_diff = 1  --> Hpol_deltaP <= "01" & fp64_to_int_dout(31 downto 18);  i.e. fine delay is positive, greater than 1 sample
                                --   sample_diff = -1 --> Hpol_deltaP <= "11" & fp64_to_int_dout(31 downto 18);  i.e. fine delay is negative.
                                Hpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(29 downto 0) <= fp64_to_int_dout(31 downto 2);
                                if (signed(fp64_to_int_dout(43 downto 32)) > signed(sample_offset(vc))) then
                                    Hpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(31 downto 30) <= "01";
                                elsif (signed(fp64_to_int_dout(43 downto 32)) = signed(sample_offset(vc))) then
                                    Hpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(31 downto 30) <= "00";
                                else
                                    Hpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(31 downto 30) <= "11";
                                end if;
                            end if;
                        end if;
                    end if;
                    if (poly_fsm_del(24) = mult_vpol_1_on_1080ns) then
                        if (to_integer(unsigned(output_select_del24)) = (vc * g_TIMES_PER_BLOCK + i)) then
                            -- fine delay for Vpol is same calculation as for Hpol, except that
                            -- the integer delay is set by Hpol sample delay (V and H have same integer delay).
                            Vpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(29 downto 0) <= fp64_to_int_dout(31 downto 2);
                            if (signed(fp64_to_int_dout(43 downto 32)) > signed(sample_offset(vc))) then
                                Vpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(31 downto 30) <= "01";
                            elsif (signed(fp64_to_int_dout(43 downto 32)) = signed(sample_offset(vc))) then
                                Vpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(31 downto 30) <= "00";
                            else
                                Vpol_deltaP(vc * g_TIMES_PER_BLOCK + i)(31 downto 30) <= "11";
                            end if;
                        end if;
                    end if;
                    if (poly_fsm_del(24) = mult_hpol_sky_frequency) then
                        if (to_integer(unsigned(output_select_del24)) = (vc * g_TIMES_PER_BLOCK + i)) then
                            Hpol_phase(vc * g_TIMES_PER_BLOCK + i) <= fp64_to_int_dout(31 downto 0);
                        end if;
                    end if;
                    if (poly_fsm_del(24) = mult_vpol_sky_frequency) then
                        if (to_integer(unsigned(output_select_del24)) = (vc * g_TIMES_PER_BLOCK + i)) then
                            Vpol_phase(vc * g_TIMES_PER_BLOCK + i) <= fp64_to_int_dout(31 downto 0);
                        end if;
                    end if;
                    
                end if;
            end process;
        end generate;
    end generate;
    
    output_select_del0 <= vc_count(1 downto 0) & state_count(2 downto 0); -- Assumes g_VIRTUAL_CHANNELS = 4 and g_TIMESTEPS = 8;
    output_select_del24 <= vc_count_del(24)(1 downto 0) & state_count_del(24)(2 downto 0); -- Assumes g_VIRTUAL_CHANNELS = 4 and g_TIMESTEPS = 8;
    
    vc_count_del(0)(g_VC_LOG2-1 downto 0) <= vc_count;
    state_count_del(0) <= state_count;
    poly_fsm_del(0) <= poly_fsm;
    
    process(clk)
        variable ct_frame_offset_del8_x512 : std_logic_vector(63 downto 0);
        variable ct_frame_offset_del8_x256 : std_logic_vector(63 downto 0);
        variable ct_sample_add             : std_logic_vector(63 downto 0);
    begin
        if rising_edge(clk) then
            
            polyValidTime_ext <= x"0000" & i_polyValidTime;
            
            if i_start = '1' then
                poly_fsm <= start;
                -- Number of packets sent for this set of virtual channels in this corner turn frame.
                -- Counts from 0 to 
                packets_sent <= (others => '0');
            else
                case poly_fsm is
                    when start =>
                        poly_fsm <= wait_x10;
                        virtual_channels <= i_virtual_channels;
                        ct_frame <= i_ct_frame;
                    
                    when wait_x10 =>
                        -- wait until virtual_channels_x10 is valid.
                        poly_fsm <= get_validity_buf0;
                        vc_count <= (others => '0');
                    
                    when get_validity_buf0 =>
                        -- read the validity time from the polynomial memory.
                        -- Offset 7 in each 10-word block = validity time, bits 39:0 = ct frame at which 
                        --  the polynomial becomes valid, bit 40 = entry is valid.
                        -- Loop through g_VIRTUAL_CHANNELS x 2 times, once for each buffer.
                        poly_fsm <= get_validity_buf1;
                        state_count <= (others => '0');
                    
                    when get_validity_buf1 =>
                        -- read validity info for the second buffer for each virtual channel
                        -- validity = offset 7 within set of 10 words, second buffer = offset 10240
                        poly_fsm <= run_t_calculation;
                        state_count <= (others => '0');
                       
                        ------------------------------------------------------------
                        -- Pipeline for calculating the time in the polynomial
                        --
                        --  Calculates :
                        --     - determine which buffer to use (valid and most recent)
                        --     - Get the integration offset as an integer:
                        --
                        --          - Find PSS samples from start of validity 
                        --              ct_frame_offset = i_ct_frame - buf_frame
                        --              - Time offset in units of output samples relative to the epoch
                        --                There are 768 output time samples per corner turn frame  
                        --                = (24*2048 SPS samples) / (64 SPS samples / PSS sample) = 768 PSS samples/corner turn frame
                        --              sample_offset = ct_frame_offset * 768
                        --          - Calculate for g_TIMES_PER_BLOCK (== 8) consecutive samples
                        --              int2fp64 : 
                        --                sample_offset_samples = double(sample_offset + [0:8])   # Loop through 8 times to get 8 sample offsets for first 8 PSS samples
                        --              fp64 mult-add :
                        --                cur_time(.) = sample_offset_samples * (69120e-9 = 64*1080ns) + buf_offset_seconds                       
                        --
                        --
                        -- cycle state_count    poly_fsm          Valid at this cycle + comments
                        --   0               get_validity_buf0----     
                        --   1               get_validity_buf1-- |-o_rd_addr = buffer 0 validity
                        --   2     0      /--run_t_calculation |---o_rd_addr = buffer 1 validity
                        --   3     1      |                        
                        --   4     2      |                        i_rd_data = buffer 0 validity    poly_fsm_del(4) = get_validity_buf0
                        --   5     3      |                        i_rd_data = buffer 1 validity    valid_buf0, buf0_ok_del5
                        --   6     4      |                        buf0_ok_del6, buf1_ok_del6
                        --   7     5      |                        buf0_ok_del7, buf1_ok_del7
                        --   8     6      |                        ct_frame_offset_del8    
                        --   9     7      |                        ct_sample_offset_del9            poly_fsm_del(7) = run_t_calculation
                        --  10     0      |  get_buf_offset-----   int_to_fp64_din                 (input to fp64 conversion)                     
                        --  11     0      |  get_validity_buf0 |                            
                        --  12     0      |  get_validity_buf1 |   
                        --  13     0      |  run_t_calculation |   
                        --  14     1      |                    |   
                        --  15     2      |                    |   
                        --  16     3      |                    |   int_to_fp64_dout          [6 cycle latency to convert sample offset to fp64]
                        --  17     4      |                    |   fp64_mult_din0            [Multiply sample offset by (69120e-9 = 64*1080ns)]
                        --  18     5      |                    |     
                        --  19     6      |                    |     
                        --  20     7      |                    |     
                        --  21     0      |  get_buf_offset    |     
                        --  22     0      |  get_validity_buf0 |     
                        --  23     0      |  get_validity_buf1 |     
                        --  24     0      |  run_t_calculation |                                  [Assignment of o_rd_addr here on poly_fsm_del(14) = get_buf_offset]
                        --  25     1      |                    |---o_rd_addr (= t_offset address) [for first of 4 virtual channels]                                       
                        --  26     2      |                           |
                        --  27     3      |                           |
                        --  28     4      \del(26)=run_t_calculation; i_rd_data (= buf_offset_seconds)
                        --  29     5                                  fp64_mult_dout, buf_offset_seconds              [12 cycle latency for the multiplier]
                        --  30     6                                  fp64_add_din0   fp64_add_din1                   [Add time in seconds to buf_offset_seconds]
                        --  31     7                                  
                        --  32     0         get_buf_offset             
                        --  33     0         get_validity_buf0          
                        --  34     0         get_validity_buf1        
                        --  35     0         run_t_calculation          
                        --  36     1                                    
                        --  37     2                                        
                        --  38     3                                        
                        --  39     4                                  
                        --  40     5                                        
                        --  41     6                                  
                        --  42     7                                    
                        --  43     0         get_buf_offset           
                        --  44     0       del(42)=run_t_calculation; fp64_add_dout             [This value is the time used in the polynomial, store to cur_time()]
                        --  45               

                    when run_t_calculation =>
                        -- calculate t, pipeline through int2fp64 and fp64 mult-add :
                        --   int2fp64 : sample_offset_seconds = double(sample_offset + [0:8])   # Loop through 8 times to get 8 sample offsets for first 8 PSS samples
                        --   fp64 mult-add : cur_time(.) = sample_offset * (69120e-9 = 64*1080ns) + buf_offset_seconds
                        --
                        -- Total 8 clocks required to calculate 8 sample offsets; 
                        if (unsigned(state_count) = 7) then
                            state_count <= (others => '0');
                            poly_fsm <= get_buf_offset;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                        
                    when get_buf_offset =>
                        -- Get the offset in seconds for this virtual channel, to be added to the time
                        if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                            poly_fsm <= wait_t_calculation;
                        else
                            vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            poly_fsm <= get_validity_buf0;
                        end if;
                        state_count <= (others => '0');
                    
                    when wait_t_calculation =>
                        -- Wait until we finish the calculation of the time at the start of the 
                        -- corner turn. This is needed to avoid clashes on o_rd_addr (read from the configuration memory)
                        -- last setting of o_rd_addr is 15 clocks after poly_fsm = get_buf_offset;
                        -- (as described in comments above)
                        if (unsigned(state_count) = 16) then
                            poly_fsm <= get_c0;
                            state_count <= (others => '0');
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                        vc_count <= (others => '0');
                        
                    when add_packet_time =>
                        -- Get the new time sample within a corner turn frame;
                        -- Add (8*64*1080e-9) = 552960 ns = 0.00055296 seconds
                        if (unsigned(state_count) = (g_VIRTUAL_CHANNELS*g_TIMES_PER_BLOCK - 1)) then
                            poly_fsm <= wait_add_packet_time;
                            state_count <= (others => '0');
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                        
                    when wait_add_packet_time =>
                        -- Time for the first virtual channel is already valid by the time we get to this state.
                        -- so no need to wait here for multiple clocks (or maybe any clocks). 
                        state_count <= (others => '0');
                        vc_count <= (others => '0');
                        poly_fsm <= get_c0;
                        
                    -------------------------------------------------------------------------------------------------
                    -- Pipeline for get_c0, get_c1, ... get_c5 states
                    --
                    -- del(0) = get_cX   
                    -- del(1)          o_rd_addr                                     [read address for memory with c0, c1, etc.]
                    --     2
                    --     3                       cur_time_rdAddr
                    --     4           i_rd_data   cur_time_rdData cur_time_n_rdData
                    --     5           fp64_mult_din, fp64_t_mult_din                [input to multipliers to get next power of t, and updated poly state]
                    --     6            
                    --     7            
                    --     8            
                    --     9            
                    --    10            
                    --    11            
                    --    12            
                    --    13            
                    --    14            
                    --    15            
                    --    16            
                    --    17           fp64_mult_dout, fp64_t_mult_dout              [12 clock latency for the fp64 multiplier]
                    --    18           fp64_add_din            
                    --    19           
                    --    20            
                    --    21            
                    --    22            
                    --    23            
                    --    24            
                    --    25            
                    --    26            
                    --    27           
                    --    28           
                    --    29           
                    --    30           
                    --    31           
                    --    32           fp64_add_dout                        [14 clock latency for the fp64 adder]
                    --    33
                    --    34           
                    --
                    --
                        
                        
                    when get_c0 =>
                        -- Read c0 coefficient, address offset = 0, store in the cur_poly_state(i) register
                        -- Step through g_TIMES_PER_BLOCK cycles for each g_VIRTUAL_CHANNELS, so that c0 can 
                        -- be loaded into all entries in cur_poly_state (g_VIRTUAL_CHANNELS * g_TIMES_PER_BLOCK of them, nominally = 32)
                        -- Also multiply t by 1 to put the result in cur_time_n
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= get_c1;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when get_c1 =>
                        -- read c1 coefficient, address offset = 1, feed into pipeline calculation of c1 * t + c0
                        -- This state also does the multiplication t^2 = t*t 
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= get_c2;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when get_c2 =>
                        -- read c2 coefficient, address offset = 2, feed into pipeline calculation of c2 * t^2 + (cur_poly_state = c1 * t + c0)
                        -- This state also does the multiplication t^3 = t*(t^2)
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= get_c3;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;                       
                        
                    when get_c3 =>
                        -- read c3 coefficient, address offset = 3, feed into pipeline calculation of c3 * t^3 + (cur_poly_state = c2 * t^2 + c1 * t + c0)
                        -- This state also does the multiplication t^4 = t*(t^3)
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= get_c4;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when get_c4 =>
                        -- read c4 coefficient, address offset = 4, feed into pipeline calculation of c4 * t^4 +  (cur_poly_state = c3 * t^3 + c2 * t^2 + c1 * t + c0)
                        -- This state also does the multiplication t^5 = t*(t^4)
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= get_c5;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                        
                    when get_c5 =>
                        -- read c5 coefficient, address offset = 5, feed into pipeline calculation of c5*t^5 + (cur_poly_state = c4*t^4 + c3 * t^3 + c2 * t^2 + c1 * t + c0)
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= wait_poly_done;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when wait_poly_done =>
                        -- Latency on using the fp64 adder is higher for the "get_c5" state than for 
                        -- the "add_vpol_offset" state, so wait a while here to avoid clashes
                        if (unsigned(state_count) > 15) then
                            state_count <= (others => '0');
                            poly_fsm <= add_vpol_offset;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                        
                    when add_vpol_offset =>
                        -- add the offset for the other polarisation (config word 8)
                        poly_fsm <= mult_hpol_1_on_1080ns;
                    
                    when mult_hpol_1_on_1080ns => 
                        -- multiply : delay (from polynomial) * (1/1080ns) 
                        -- to get the coarse sample offset.
                        poly_fsm <= mult_hpol_sky_frequency;
                    
                    when mult_hpol_sky_frequency =>
                        -- Multiply by the sky frequency
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= wait_vpol;
                                vc_count <= (others => '0');
                            else
                                poly_fsm <= add_vpol_offset;
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            poly_fsm <= add_vpol_offset;
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when wait_vpol =>
                        -- Previous version waited here for the adder to complete, but with 32 states
                        -- this isn't needed (add_vpol_offset will be complete already for the first few virtual channels).
                        poly_fsm <= vpol_idle;
                        state_count <= (others => '0');
                        vc_count <= (others => '0');
                        
                    when vpol_idle =>
                        -- do nothing for one clock.
                        -- This is here so that there is 3 clocks per virtual channel for 
                        -- calculating the x1/1080 and x sky_frequency steps for vpol just as there is for hpol
                        -- Otherwise it would be possible to get ahead of the vpol adder for later virtual channels.
                        poly_fsm <= mult_vpol_1_on_1080ns;
                    
                    when mult_vpol_1_on_1080ns => 
                        -- multiply : delay (from polynomial) * (1/1080ns) 
                        -- to get the coarse sample offset.
                        poly_fsm <= mult_vpol_sky_frequency;
                    
                    when mult_vpol_sky_frequency =>
                        -- multiply by the sky frequency
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= wait_done;
                                vc_count <= (others => '0');
                            else
                                poly_fsm <= vpol_idle;
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            poly_fsm <= vpol_idle;
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when wait_done =>
                        poly_fsm <= send_values;
                        vc_count <= (others => '0');
                        state_count <= (others => '0');
                    
                    when send_values =>
                        -- Put values on the output bus 
                        -- (o_vc, o_packet, o_sample_offset, o_Hpol_deltaP, o_Hpol_phase, o_Vpol_deltaP, o_Vpol_phase)
                        if (unsigned(state_count) = (g_TIMES_PER_BLOCK-1)) then
                            state_count <= (others => '0');
                            if (unsigned(vc_count) = (g_VIRTUAL_CHANNELS-1)) then
                                poly_fsm <= done;
                                vc_count <= (others => '0');
                            else
                                vc_count <= std_logic_vector(unsigned(vc_count) + 1);
                            end if;
                        else
                            state_count <= std_logic_vector(unsigned(state_count) + 1);
                        end if;
                    
                    when done =>
                        -- Either move to the next timestep, or wait for the next set of virtual channels to process.
                        if (unsigned(packets_sent) < (g_TIMESTEPS-1)) then
                            poly_fsm <= add_packet_time;
                            packets_sent <= std_logic_vector(unsigned(packets_sent) + 1);
                        else
                            poly_fsm <= wait_new_vc;
                        end if;
                        vc_count <= (others => '0');
                        
                    when wait_new_vc =>
                        poly_fsm <= wait_new_vc;
                        
                    when others =>
                        poly_fsm <= wait_new_vc;
                    
                end case;
            end if;
            
            vc_count_del(47 downto 1) <= vc_count_del(46 downto 0);
            state_count_del(47 downto 1) <= state_count_del(46 downto 0);
            poly_fsm_del(47 downto 1) <= poly_fsm_del(46 downto 0);
            
            if (unsigned(packets_sent) = 0) and (unsigned(state_count) = 0) then
                -- This is the first time sample in the corner turn frame
                packets_sent_eq_zero_del(0) <= '1';
            else
                packets_sent_eq_zero_del(0) <= '0';
            end if;
            packets_sent_eq_zero_del(31 downto 1) <= packets_sent_eq_zero_del(30 downto 0);
            
            if poly_fsm = send_values then
                o_vc <= virtual_channels(to_integer(unsigned(vc_count)));
                o_vc_count(7 downto g_VC_LOG2) <= (others => '0');
                o_vc_count((g_VC_LOG2-1) downto 0) <= vc_count((g_VC_LOG2-1) downto 0);
                o_packet <= '0' & packets_sent & state_count(2 downto 0); -- out (15:0);
                o_sample_offset  <= sample_offset(to_integer(unsigned(vc_count)))(11 downto 0); -- Number of whole 1080ns samples to delay by.
                o_Hpol_deltaP <= Hpol_deltaP(to_integer(unsigned(output_select_del0))); -- out (15:0);
                o_Hpol_phase  <= Hpol_phase(to_integer(unsigned(output_select_del0)));
                o_Vpol_deltaP <= Vpol_deltaP(to_integer(unsigned(output_select_del0)));
                o_Vpol_phase <= Vpol_phase(to_integer(unsigned(output_select_del0)));
                o_valid <= '1';
            else
                o_vc <= (others => '0');
                o_packet <= (others => '0'); -- out std_logic_vector(15 downto 0);
                o_sample_offset <= (others => '0'); -- Number of whole 1080ns samples to delay by.
                o_Hpol_deltaP <= (others => '0');
                o_Hpol_phase  <= (others => '0');
                o_Vpol_deltaP <= (others => '0');
                o_Vpol_phase  <= (others => '0');
                o_valid <= '0';
            end if;
            
            
            if poly_fsm = get_validity_buf0 then
                poly_rd_addr <= std_logic_vector(unsigned(virtual_channels_x10(to_integer(unsigned(vc_count)))) + 9);
            elsif poly_fsm = get_validity_buf1 then
                -- read validity info for the second buffer for each virtual channel
                -- validity = offset 7 within set of 10 words, second buffer = offset 10240
                poly_rd_addr <= std_logic_vector(unsigned(virtual_channels_x10(to_integer(unsigned(vc_count)))) + 10240 + 9);
            elsif (poly_fsm_del(14) = get_buf_offset) then
                -- Fetch from configuration memory the offset in seconds from the corner turn frame to the 
                -- start of validity for the polynomial (word 7).
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(15)(g_VC_LOG2-1 downto 0))))) + 7);
            elsif (poly_fsm_del(0) = get_c0) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 0);
            elsif (poly_fsm_del(0) = get_c1) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 1);
            elsif (poly_fsm_del(0) = get_c2) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 2);
            elsif (poly_fsm_del(0) = get_c3) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 3);
            elsif (poly_fsm_del(0) = get_c4) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 4);
            elsif (poly_fsm_del(0) = get_c5) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 5);
            elsif (poly_fsm_del(0) = add_vpol_offset) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 8);
            else -- if ((poly_fsm_del(0) = mult_hpol_sky_frequency) or (poly_fsm_del(0) = mult_vpol_sky_frequency)) then
                poly_rd_addr <= std_logic_vector(unsigned(vc_base_addr(to_integer(unsigned(vc_count_del(0)(g_VC_LOG2-1 downto 0))))) + 6);
            end if;
            
            if poly_fsm = wait_new_vc then
                o_idle <= '1';
            else
                o_idle <= '0';
            end if;
            -------------------------------------------------------------------
            -- Determine which buffer to use for each of the virtual channels
            --
            -- del1 : o_rd_addr valid, del4 : i_rd_data valid
            if poly_fsm_del(4) = get_validity_buf0 then
                ct_frame_buf0_del5 <= i_rd_data(39 downto 0);
                valid_buf0 <= i_rd_data(40);
            end if;
            
            -- pipeline : buf1 data read from the config memory is valid.
            if poly_fsm_del(5) = get_validity_buf0 then
                ct_frame_buf1_del6 <= i_rd_data(39 downto 0);
                valid_buf1_del6 <= i_rd_data(40);
                if ((valid_buf0 = '1') and (unsigned(ct_frame_buf0_del5) <= unsigned(ct_frame))) then
                    buf0_ok_del6 <= '1';  -- del5 is relative to the fsm
                else
                    buf0_ok_del6 <= '0';
                end if;
            end if;
            ct_frame_buf0_del6 <= ct_frame_buf0_del5;
            
            -- pipeline : calculate which buffer is more recent.
            if poly_fsm_del(6) = get_validity_buf0 then
                if (unsigned(ct_frame_buf1_del6) > unsigned(ct_frame_buf0_del6)) then
                    buf1_more_recent_del7 <= '1';
                else
                    buf1_more_recent_del7 <= '0';
                end if;
                if (valid_buf1_del6 = '1' and (unsigned(ct_frame_buf1_del6) <= unsigned(ct_frame))) then
                    buf1_ok_del7 <= '1';
                else
                    buf1_ok_del7 <= '0';
                end if;
            end if;
            buf0_ok_del7 <= buf0_ok_del6;
            ct_frame_buf0_del7 <= ct_frame_buf0_del6;
            ct_frame_buf1_del7 <= ct_frame_buf1_del6;
            
            ct_frame_buf0_del7_end_of_validity <= std_logic_vector(unsigned(ct_frame_buf0_del6) + unsigned(polyValidTime_ext));
            ct_frame_buf1_del7_end_of_validity <= std_logic_vector(unsigned(ct_frame_buf1_del6) + unsigned(polyValidTime_ext));
            
            -- pipeline : Assign which buffer we will use for each virtual channel
            -- and which integration the times are referenced to.
            if poly_fsm_del(7) = get_validity_buf0 then
                if ((buf0_ok_del7 = '1' and buf1_ok_del7 = '1' and buf1_more_recent_del7 = '0') or
                    (buf0_ok_del7 = '1' and buf1_ok_del7 = '0') or
                    (buf0_ok_del7 = '0' and buf1_ok_del7 = '0')) then
                    -- choose buf0 for vc = vc_count_del6
                    -- This is also the default choice in the case where neither buffer is valid.
                    buffer_select(to_integer(unsigned(vc_count_del(7)(g_VC_LOG2-1 downto 0)))) <= '0';
                    ct_frame_offset_del8 <= std_logic_vector(unsigned(ct_frame) - unsigned(ct_frame_buf0_del7));
                elsif buf1_ok_del7 = '1' then
                    -- choose buf1
                    buffer_select(to_integer(unsigned(vc_count_del(7)(g_VC_LOG2-1 downto 0)))) <= '1';
                    ct_frame_offset_del8 <= std_logic_vector(unsigned(ct_frame) - unsigned(ct_frame_buf1_del7));
                end if;
            end if;
            
            if (unsigned(ct_frame) > unsigned(ct_frame_buf0_del7_end_of_validity)) then
                buf0_out_of_range <= '1';
            else
                buf0_out_of_range <= '0';
            end if;
            
            if (unsigned(ct_frame) > unsigned(ct_frame_buf1_del7_end_of_validity)) then
                buf1_out_of_range <= '1';
            else
                buf1_out_of_range <= '0';
            end if;
            buf0_ok_del8 <= buf0_ok_del7;
            buf1_ok_del8 <= buf1_ok_del7;
            
            --
            if i_rst = '1' then
                no_valid_buffer_count <= (others => '0');
                o_bad_poly <= '0';
            elsif ((poly_fsm_del(8) = get_validity_buf1) and 
                   (buf0_ok_del8 = '0' or (buf0_ok_del8 = '1' and buf0_out_of_range = '1')) and 
                   (buf1_ok_del8 = '0' or (buf1_ok_del8 = '1' and buf1_out_of_range = '1'))) then
                no_valid_buffer_count <= std_logic_vector(unsigned(no_valid_buffer_count) + 1);
                o_bad_poly <= '1';
            else
                o_bad_poly <= '0';
            end if;
            
            -- Pipeline : multiply frame offset by 768 to get the sample offset
            ct_frame_offset_del8_x512 := x"00" & "0000000" & ct_frame_offset_del8 & "000000000";  -- 64 bit value
            ct_frame_offset_del8_x256 := x"00" & "00000000" & ct_frame_offset_del8 & "00000000";
            ct_sample_offset_del9 <= std_logic_vector(unsigned(ct_frame_offset_del8_x512) + unsigned(ct_frame_offset_del8_x256));
            
            ---------------------------------------------------------------------------------------
            -- Code for distributed memories for cur_time, cur_time_n, cur_poly_state
            ---------------------------------------------------------------------------------------

            -- Write to the distributed memory for cur_time used in the polynomials.
            -- (i.e. seconds since polynomial epoch for this virtual channel) 
            -- Comes from the output of the adder, either
            --     - At the start of processing a set of virtual channels, after adding time in the frame to buffer offset
            --  or - After adding 8x(PSS sample time) to move to the next 8 time samples
            if (poly_fsm_del(42) = run_t_calculation) then
                cur_time_wrEn <= '1';
                cur_time_wrAddr <= vc_count_del(42)(1 downto 0) & state_count_del(42)(2 downto 0);
            elsif (poly_fsm_del(17) = add_packet_time) then
                -- add din was valid on poly_fsm_del(3), 14 clock latency, output is valid on del(17)
                cur_time_wrEn <= '1';
                cur_time_wrAddr <= state_count_del(17)(4 downto 0);
            else
                cur_time_wrEn <= '0';
            end if;
            cur_time_wrData <= fp64_add_dout;
            
            if cur_time_wrEn = '1' then
                cur_time(to_integer(unsigned(cur_time_wrAddr))) <= cur_time_wrData;
            end if;
            
            if poly_fsm = add_packet_time then
                cur_time_rdAddr <= state_count(4 downto 0);
            else -- if poly_fsm_del(2) = get_c0, get_c1, ... get_c4 then
                cur_time_rdAddr <= vc_count_del(2)(1 downto 0) & state_count_del(2)(2 downto 0);  -- Assumes g_VIRTUAL_CHANNELS = 4 and g_TIMESTEPS = 8;
            end if;
            
            cur_time_rdData <= cur_time(to_integer(unsigned(cur_time_rdAddr)));
            
            --
            -- Write to distributed memory for cur_time_n, from the output of the multiplier.
            --  Multiplier input is valid on poly_fsm_del(5), 12 clock latency, so output is valid on poly_fsm_del(17)
            if (poly_fsm_del(17) = get_c0) or (poly_fsm_del(17) = get_c1) or (poly_fsm_del(17) = get_c2) or (poly_fsm_del(17) = get_c3) or (poly_fsm_del(17) = get_c4) then
                cur_time_n_wrEn <= '1';
            else
                cur_time_n_wrEn <= '0';
            end if;
            cur_time_n_wrData <= fp64_t_mult_dout;
            cur_time_n_wrAddr <= vc_count_del(17)(1 downto 0) & state_count_del(17)(2 downto 0);
            
            if cur_time_n_wrEn = '1' then
                cur_time_n(to_integer(unsigned(cur_time_n_wrAddr))) <= cur_time_n_wrData;
            end if;
            
            cur_time_n_rdData <= cur_time_n(to_integer(unsigned(cur_time_rdAddr))); -- same read address for cur_time_n as for cur_time
            
            --
            -- Current polynomial state, starts at c0 and endds with the polynomial fully evaluated
            --
            if poly_fsm_del(4) = get_c0 then
                -- When del(1) = get_c0, o_rd_addr is valid; 3 clock latency to del(4) for i_rd_data valid.
                cur_state_wrEn <= '1';
                cur_state_wrData <= i_rd_data;
                cur_state_wrAddr <= vc_count_del(4)(1 downto 0) & state_count_del(4)(2 downto 0); -- Assumes g_VIRTUAL_CHANNELS = 4 and g_TIMESTEPS = 8;
            elsif ((poly_fsm_del(32) = get_c1) or (poly_fsm_del(32) = get_c2) or (poly_fsm_del(32) = get_c3) or 
                   (poly_fsm_del(32) = get_c4) or (poly_fsm_del(32) = get_c5)) then
                --
                cur_state_wrEn <= '1';
                cur_state_wrData <= fp64_add_dout;
                cur_state_wrAddr <= vc_count_del(32)(1 downto 0) & state_count_del(32)(2 downto 0); -- Assumes g_VIRTUAL_CHANNELS = 4 and g_TIMESTEPS = 8;
            else
                cur_state_wrEn <= '0';
                cur_state_wrData <= fp64_add_dout;
                cur_state_wrAddr <= vc_count_del(32)(1 downto 0) & state_count_del(32)(2 downto 0);  -- Not used since cur_state_wrEn = 0 in this state.
            end if;
            
            if ((poly_fsm_del(15) = get_c1) or (poly_fsm_del(15) = get_c2) or (poly_fsm_del(15) = get_c3) or 
                (poly_fsm_del(15) = get_c4) or (poly_fsm_del(15) = get_c5)) then
                cur_state_rdAddr <= vc_count_del(15)(1 downto 0) & state_count_del(15)(2 downto 0);
            else -- if (poly_fsm_del(2) = add_vpol_offset) 
                cur_state_rdAddr <= vc_count_del(2)(1 downto 0) & state_count_del(2)(2 downto 0);
            end if;
            
            
            if cur_state_wrEn = '1' then
                cur_poly_state(to_integer(unsigned(cur_state_wrAddr))) <= cur_state_wrData;
            end if;
            cur_state_rdData <= cur_poly_state(to_integer(unsigned(cur_state_rdAddr)));
            
            -------------------------------------------------------------------------------
            -- End of distributed memory code
            -------------------------------------------------------------------------------
            
            -------------------------------------------------------------------------------
            -- Inputs to the floating point arithmetic blocks
            -------------------------------------------------------------------------------
            
            -----------------------------------------------------------------------------
            -- Input to the int to float conversion
            -- Used to get the time used in the polynomial
            ct_sample_add := x"00000000000000" & state_count_del(7);
            int_to_fp64_din <= std_logic_vector(unsigned(ct_sample_offset_del9) + unsigned(ct_sample_add));
            if poly_fsm_del(7) = run_t_calculation then
                int_to_fp64_valid_in <= '1';
            else
                int_to_fp64_valid_in <= '0';
            end if;
            
            -----------------------------------------------------------------------------
            -- Input to the time calculation double precision multiplier
            -- Used for calculating t^n = t^(n-1) * t
            fp64_t_mult_din0 <= cur_time_rdData;
            if poly_fsm_del(4) = get_c0 then
                -- Just multiply cur_time by 1 to populate cur_time_n  
                fp64_t_mult_valid_in <= '1';
                fp64_t_mult_din1 <= c_fp64_unity;
            elsif (poly_fsm_del(4) = get_c1) or (poly_fsm_del(4) = get_c2) or (poly_fsm_del(4) = get_c3) or (poly_fsm_del(4) = get_c4) then
                -- Calculate time^(n+1) = time * time^n
                fp64_t_mult_valid_in <= '1';
                fp64_t_mult_din1 <= cur_time_n_rdData;
            else
                fp64_t_mult_valid_in <= '0';
                fp64_t_mult_din1 <= (others => '0');
            end if;            
            
            -------------------------------------------------------------------------------
            -- Input to the second double precision multiplier
            --  - used for polynomial calculation
            --  - and also for converting the evaluated polynomial into phase offsets.
            if (poly_fsm_del(14) = run_t_calculation) then
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= int_to_fp64_dout;
                fp64_mult_din1 <= c_PSS_SAMPLE;
            elsif (poly_fsm_del(4) = get_c1) or (poly_fsm_del(4) = get_c2) or (poly_fsm_del(4) = get_c3) or (poly_fsm_del(4) = get_c4) or (poly_fsm_del(4) = get_c5) then
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= cur_time_n_rdData;
                fp64_mult_din1 <= i_rd_data;
            elsif (poly_fsm_del(4) = mult_hpol_1_on_1080ns) or (poly_fsm_del(4) = mult_vpol_1_on_1080ns) then
                -- To avoid clashes in the use of the multiplier, this has to have the same latency 
                -- relative to the state machine as for the mult_hpol_sky_frequency state
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= cur_state_rdData;
                fp64_mult_din1 <= c_fp64_rate;
            elsif ((poly_fsm_del(4) = mult_hpol_sky_frequency) or (poly_fsm_del(4) = mult_vpol_sky_frequency)) then
                -- o_rd_addr is valid on del(1)
                -- i_rd_data is valid on del(4)
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= cur_state_rdData;
                fp64_mult_din1 <= i_rd_data;
            else
                fp64_mult_valid_in <= '0';
                fp64_mult_din0 <= (others => '0');
                fp64_mult_din1 <= (others => '0');
            end if;
            
            -----------------------------------------------------------------------------
            -- Input to the double precision adder
            if poly_fsm_del(18) = get_buf_offset then
                buf_offset_seconds <= i_rd_data;
            end if;
            
            if poly_fsm_del(27) = run_t_calculation then
                -- offset_epoch = sample_offset_seconds + buf_offset_seconds
                -- for reasoning behind del(26) see sequencing diagram in comments in the fsm. 
                fp64_add_valid_in <= '1';
                fp64_add_din0 <= fp64_mult_dout;  -- output of the multiplier = (samples since polynomial epoch) * (length of a PSS sample)
                fp64_add_din1 <= buf_offset_seconds; -- Offset for t in the polynomials relative to the corner turn frame.
            elsif poly_fsm_del(2) = add_packet_time then
                fp64_add_valid_in <= '1';
                fp64_add_din0 <= c_8PSS_samples;
                fp64_add_din1 <= cur_time_rdData;
            elsif ((poly_fsm_del(17) = get_c1) or (poly_fsm_del(17) = get_c2) or (poly_fsm_del(17) = get_c3) or 
                   (poly_fsm_del(17) = get_c4) or (poly_fsm_del(17) = get_c5)) then
                -- (cX*t^X) + cur_poly_state
                -- input to the fp64 multiplier is valid on del(5), 12 cycle latency, fp64_mult_dout is valid on del(17)
                fp64_add_valid_in <= '1';
                fp64_add_din0 <= fp64_mult_dout;
                fp64_add_din1 <= cur_state_rdData;
            elsif (poly_fsm_del(4) = add_vpol_offset) then
                -- o_rd_addr aligns with del(1)
                -- 3 cycle latency to read, i_rd_data aligns with del(4)
                fp64_add_valid_in <= '1';
                fp64_add_din0 <= i_rd_data;
                fp64_add_din1 <= cur_state_rdData;
            else
                fp64_add_valid_in <= '0';
                fp64_add_din0 <= (others => '0');
                fp64_add_din1 <= (others => '0');
            end if;
            
            -----------------------------------------------------------------------------
            -- Input to the double -> int conversion
            if ((poly_fsm_del(17) = mult_hpol_1_on_1080ns) or (poly_fsm_del(17) = mult_vpol_1_on_1080ns) or 
                (poly_fsm_del(17) = mult_hpol_sky_frequency) or (poly_fsm_del(17) = mult_vpol_sky_frequency)) then
                -- fp64_mult_din was valid on del(5), fp64_mult_dout is valid on del(17)
                fp64_to_int_valid_in <= '1';
                fp64_to_int_din <= fp64_mult_dout;
            else
                fp64_to_int_valid_in <= '0';
                fp64_to_int_din <= (others => '0');
            end if;

            
            -----------------------------------------------------------------------------
            
        end if;
    end process;
    
    -- Double precision floating point adder, 14 clock latency
    fp64_addi : fp64_add
    PORT map (
        aclk => clk, -- 
        s_axis_a_tvalid => fp64_add_valid_in, -- IN STD_LOGIC;
        s_axis_a_tdata  => fp64_add_din0, -- IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid => fp64_add_valid_in, -- IN STD_LOGIC;
        s_axis_b_tdata  => fp64_add_din1, -- IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid => fp64_add_valid_out, --  OUT STD_LOGIC;
        m_axis_result_tdata  => fp64_add_dout  -- OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    );
    
    -- double precision floating point multiplier, 12 clock latency
    fp64_multi : fp64_mult
    PORT MAP (
        aclk => clk,
        s_axis_a_tvalid => fp64_mult_valid_in,
        s_axis_a_tdata => fp64_mult_din0,
        s_axis_b_tvalid => fp64_mult_valid_in,
        s_axis_b_tdata => fp64_mult_din1,
        m_axis_result_tvalid => fp64_mult_valid_out,
        m_axis_result_tdata => fp64_mult_dout
    );
    
    -- second double precision multipplier, used for the calculating powers of t
    fp64_mult2i : fp64_mult
    PORT MAP (
        aclk => clk,
        s_axis_a_tvalid => fp64_t_mult_valid_in,
        s_axis_a_tdata => fp64_t_mult_din0,
        s_axis_b_tvalid => fp64_t_mult_valid_in,
        s_axis_b_tdata => fp64_t_mult_din1,
        m_axis_result_tvalid => fp64_t_mult_valid_out,
        m_axis_result_tdata => fp64_t_mult_dout
    );
    
    -- Double precision to 32.32 int, 6 clock latency
    fp64_to_inti : fp64_to_int
    PORT MAP (
        aclk => clk,
        s_axis_a_tvalid => fp64_to_int_valid_in,
        s_axis_a_tdata => fp64_to_int_din,
        m_axis_result_tvalid => fp64_to_int_valid_out,
        m_axis_result_tdata => fp64_to_int_dout
    );
    
    -- Int to double precision float, 6 clock latency
    int_to_fp64i : uint64_to_double
    port map (
        aclk => clk,
        s_axis_a_tvalid => int_to_fp64_valid_in,
        s_axis_a_tdata => int_to_fp64_din,
        m_axis_result_tvalid => int_to_fp64_valid_out,
        m_axis_result_tdata => int_to_fp64_dout
    );
    
end Behavioral;
