schema_name   : args
schema_version: 1.0
schema_type   : peripheral

hdl_library_name       : pss_ct1
hdl_library_description: "PSS Corner Turn 1 configuration"

peripherals:

  - peripheral_name        : pss_ct1
    peripheral_description : "PSS corner turn 1 configuration"

    slave_ports:
      - slave_name        : polynomial_ram
        slave_type        : RAM
        number_of_slaves  : 1
        slave_description : "Polynomial coefficients and control"
        fields            :
          - - field_name        : poly_config_ram
              width             : 32
              user_width        : 32
              access_mode       : RW
              number_of_fields  : 65536
              interface         : simple
              reset_value       : 0
              field_description : "65536 fields x 4 bytes = 262144 Bytes of space, of which 163840 bytes is used.
                                    Bytes 0 to 79 : Config for virtual channel 0, buffer 0 (see below for specification of contents)
                                    Bytes 80 to 159 : Config for virtual channel 1, buffer 0
                                    ...
                                    bytes 81840 to 81919 : Config for virtual channel 1023, first buffer
                                    bytes 81920 to 163839 : Config for all 1024 virtual channels, second buffer
                                   
                                   Each group of 80 bytes is 10 x 8byte words, where each 8-byte word contains : 
                                    word 0 = c0
                                     ...  
                                    word 5 = c5
                                     - c0 to c5 are double precision floating point values for the delay polynomial :
                                       c0 + c1*t + c2 * t^2 + c3 * t^3 + c4 * t^4 + c5 * t^5
                                     - Units for c0,.. c5 are ns/s^k for k=0,1,..5
                                    word 6 = Sky frequency in GHz
                                     - Used to convert the delay (in ns) to a phase rotation.
                                    word 7 = buf_offset_seconds : seconds from the polynomial epoch to the start of buf_ct_frame, as a double precision value 
                                    word 8 = double precision offset in ns for the second polarisation (relative to the first polarisation).
                                    word 9 = Validity time
                                     - bits 39:0 = buf_ct_frame : corner turn frame at which the polynomial becomes valid.
                                                   In units of 53.084160 ms, i.e. units of (24 SPS packets), relative to the epoch. 
                                     - bit 40 = Entry is valid.
                                  i.e. if the polynomial is valid at time t (relative to the epoch) then buf_ct_frame is the first corner turn
                                  frame after t, and buf_offset_seconds = buf_ct_frame - t
                                  "
      
      - slave_name        : config
        slave_type        : reg
        number_of_slaves  : 1
        slave_description : "PSS corner turn 1 control"
        fields:
          - - field_name        : full_reset
              width             : 1
              access_mode       : RW
              reset_value       : 0x0
              field_description : "This is the reset line for CT-2, needs to be driven high and then low to enable the CT-2 logic. (To be replaced before AA0.5)"
        ###########################################################################
          - - field_name        : status
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "status. "
        ###########################################################################      
          - - field_name        : output_cycles
              width             : 16
              access_mode       : RW
              reset_value       : 0x42
              field_description : "Number of clock cycles from the start of one output packet to the start of the next packet.\
                                   Actual packet length is 64 clocks. \
                                   Default and minimum allowed is 66."
              
          - - field_name        : framecount_start
              width             : 7
              access_mode       : RW
              reset_value       : 0x0A
              field_description : "Reading of a buffer will start when a packet count with this offset within a buffer or greater \
                                   is received for the next buffer." 
          
          - - field_name        : input_packets
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Number of input packet notifications received from LFAA ingest module"
          
          - - field_name        : frame_count_low
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Low 32 bits of the 53ms corner turn frame that we are currently writing data for.\
                                   This value counts from the start of the SPS epoch."
          
          - - field_name        : frame_count_high
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "High 32 bits of the 53ms corner turn frame that we are currently writing data for.\
                                   This value counts from the start of the SPS epoch."
          
          - - field_name        : early_or_late_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Count of SPS packets dropped due to being outside the time window for expected packets."

          - - field_name        : pre_latch_on_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Number of packets dropped while waiting to latch on to the incoming data stream"

          - - field_name        : duplicates_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Count of duplicate input packets (i.e. input packets with the same timestamp and channel as a previous packet)."

          - - field_name        : missing_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Count of SPS packets on the filterbank read out with no data."
              
          - - field_name        : buffers_sent_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "total number of 59.9ms buffers read out and sent to the filterbanks."

          - error:
            - field_name        : input_overflow
              width             : 1
              access_mode       : RO
              reset_value       : 0x0
              field_description : "input buffer has overflowed."
          
            - field_name        : read_overflow
              width             : 1
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Readout was triggered, but the previous readout was still running"
              
          - - field_name        : read_overflow_buffer_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "buffers_sent_count at most recent occurrence of read_overflow"
              
          - - field_name        : max_frame_jump
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "maximum value by which the SPS frame count has jumped from one packet to the next"
          
          - - field_name        : recent_clocks_between_readouts
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Number of clocks between most recent two readout triggers."

          - - field_name        : min_clocks_between_readouts
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "minimum number of clocks between readout triggers"
          
          - - field_name        : buffer_count_at_min_readout_gap
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "buffers_sent_count at the time when min_clocks_between_readouts was most recently set"

          - - field_name        : recent_packets_per_readout
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Number of SPS packets received between most recent two readout triggers"
          
          - - field_name        : pss_output_count
              width             : 32
              number_of_fields  : 1024
              access_mode       : RO
              field_description : "Number of valid input blocks read for each channel."

          - - field_name        : poly_valid_time
              width             : 24
              access_mode       : RW
              reset_value       : 0x2C27
              field_description : "Time for which polynomials remain valid after their start of validity time. Units are corner turn frames = 53.08416ms. Default is equivalent to approximately 600 seconds. "

          - - field_name        : invalid_poly_count
              width             : 16
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Count of polynomial evaluations that used invalid polynomial data."

          - - field_name        : RFI_scale
              width             : 8
              access_mode       : RW
              reset_value       : 0x0F
              field_description : "Ignored. Was a scale factor applied after the fine delay; removed after the change to 16 bit data. Still here in case it needs to be reinstated."
