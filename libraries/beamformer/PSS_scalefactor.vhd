----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: August 2024
-- Module Name: PSS_scalefactor - Behavioral
-- Description: 
--
--  Compute a 4 bit scale factor to ensure that 32-bit values will fit into 16-bits
--  Input values in the range -32768 to 32767 return 0
--                            -65536:-32768 or 32768:65535 return 1
--                            etc.
--  i.e. scale factor is the right shift to be applied to the 32 bit data to make it fit in 16-bit data.
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSS_scalefactor is
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        ---------------------------------------------------------------------------
        -- Data in to the beamformer
        i_data  : in std_logic_vector(31 downto 0);
        o_scale : out std_logic_vector(4 downto 0)  -- 2 cycle latency. 
    );
end PSS_scalefactor;

architecture Behavioral of PSS_scalefactor is

    signal scale0, scale1, scale2, scale3 : std_logic_vector(4 downto 0);
    signal use_scale0, use_scale1, use_scale2 : std_logic;
begin
    
    -- Pipelines that go through to the next beamformer
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
           
            if i_data(31 downto 15) = "00000000000000000" or i_data(31 downto 15) = "11111111111111111" then
                -- Already fits in 16 bits
                scale0 <= "00000";
                use_scale0 <= '1';
            elsif i_data(31 downto 16) = "0000000000000000" or i_data(31 downto 16) = "1111111111111111" then
                scale0 <= "00001";
                use_scale0 <= '1';
            elsif i_data(31 downto 17) = "000000000000000" or i_data(31 downto 17) = "111111111111111" then
                scale0 <= "00010";
                use_scale0 <= '1';
            elsif i_data(31 downto 18) = "00000000000000" or i_data(31 downto 18) = "11111111111111" then
                scale0 <= "00011";
                use_scale0 <= '1';
            elsif i_data(31 downto 19) = "0000000000000" or i_data(31 downto 19) = "1111111111111" then
                scale0 <= "00100";
                use_scale0 <= '1';
            else
                use_scale0 <= '0';
            end if;
            
            if i_data(31 downto 20) = "000000000000" or i_data(31 downto 20) = "111111111111" then
                scale1 <= "00101";
                use_scale1 <= '1';
            elsif i_data(31 downto 21) = "00000000000" or i_data(31 downto 21) = "11111111111" then
                scale1 <= "00110";
                use_scale1 <= '1';
            elsif i_data(31 downto 22) = "0000000000" or i_data(31 downto 22) = "1111111111" then
                scale1 <= "00111";
                use_scale1 <= '1';
            elsif i_data(31 downto 23) = "000000000" or i_data(31 downto 23) = "111111111" then
                scale1 <= "01000";
                use_scale1 <= '1';
            else
                use_scale1 <= '0';
            end if;
            
            if i_data(31 downto 24) = "00000000" or i_data(31 downto 24) = "11111111" then
                scale2 <= "01001";
                use_scale2 <= '1';
            elsif i_data(31 downto 25) = "0000000" or i_data(31 downto 25) = "1111111" then
                scale2 <= "01010";
                use_scale2 <= '1';
            elsif i_data(31 downto 26) = "000000" or i_data(31 downto 26) = "111111" then
                scale2 <= "01011";
                use_scale2 <= '1';
            elsif i_data(31 downto 27) = "00000" or i_data(31 downto 27) = "11111" then
                scale2 <= "01100";
                use_scale2 <= '1';
            else
                use_scale2 <= '0';
            end if;
            
            if i_data(31 downto 28) = "0000" or i_data(31 downto 28) = "1111" then
                scale3 <= "01101";
            elsif i_data(31 downto 29) = "000" or i_data(31 downto 29) = "111" then
                scale3 <= "01110";
            elsif i_data(31 downto 30) = "00" or i_data(31 downto 30) = "11" then
                scale3 <= "01111";
            else
                scale3 <= "10000";
            end if;
            
            if use_scale0 = '1' then
                o_scale <= scale0;
            elsif use_scale1 = '1' then
                o_scale <= scale1;
            elsif use_scale2 = '1' then
                o_scale <= scale2;
            else
                o_scale <= scale3;
            end if;
            
        end if;
    end process;
    
end Behavioral;
