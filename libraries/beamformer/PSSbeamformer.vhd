----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: August 2024
-- Module Name: PSSbeamformer - Behavioral
-- Description: 
--
--  PSS beamformer Data flow:
--  ------------------------
--  Data input :
--
--    For timeGroup = 0:47                          -- 53 ms corner turn, so this is 0:(256/32-1) = 0:7
--       For Coarse = 0:(i_coarse-1)                -- step through each SPS coarse channel
--          For Time = 0:15                         -- 16 times needed for an output packet
--             For Station_block = 0:(i_stations/4)
--                For fine_offset = 0:53            -- Total of 54 fine channels per SPS channel
--                   For station = 0:3
--                       - 2 clocks, data for 2 stations per clock
--
--  Number of clocks to input data for an output packet :
--    (16 times) * (54 fine channels) * (2 clocks) * (ceil(stations/4))
--    = 1728 * ceil(stations/4)
--
--  ------------------------------------------------------------------------------
--  Data output
--   - Output packets are 
--       (16 time samples) x (54 fine channels) x (2 pol) x (1+1 bytes(complex)) = 3456 bytes
--     Note the output of the beamformer is 16+16 bit complex, which is later scaled to 8+8 bit complex
--
--   - The output bus carries 1 complex dual-pol sample per clock
--     So it takes 16*54 = 864 clocks to output a packet
--
--  ------------------------------------------------------------------------------
--  Functionality
--   - The phase correction is supplied as a base value and a step per fine channel, for each of 4 stations
--   - Phase is tracked across the 54 fine channels by adding the phase step for each fine channel
--   - Phase is converted via a sin/cos lookup table to a complex correction
--   - (input data) * (phase correction)
--   - Accumulate across all stations, using 32+32 bit complex values
--   - Scale back to 16 bit values, keeping track of the scaling, 
--     using the largest value across all the fine channels for each time sample.
--   - Store for the full packet (54 fine channels, 16 time samples)
--   - Output packets.
--    
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSSbeamformer is
    generic(
        -- pipe runs from 0 to 3, beam is the element in the daisy chain that this instance is.
        -- Actual beam number is 
        --  (DAISY_CHAIN*16) + [0, 4, 8, 12] + (PIPE)
        -- i.e.
        --                         Beams                           Beams
        -- pipe=0 : daisy_chain=0: 0, 4, 8,  12 --> daisy_chain=1: 16, 20, 24, 28 --> etc.
        -- pipe=1 : daisy_chain=0: 1, 5, 9,  13 --> daisy_chain=1: 17, 21, 25, 29 --> etc.
        -- pipe=2 : daisy_chain=0: 2, 6, 10, 14 --> daisy_chain=1: 18, 22, 26, 30 --> etc.
        -- pipe=3 : daisy_chain=0: 3, 7, 11, 15 --> daisy_chain=1: 19, 23, 27, 31 --> etc.
        g_PIPE : integer; 
        g_DAISY_CHAIN : integer;
        -- The number of clocks to output a packet is 864 (=(54 fine channels)*(16 times))
        -- g_PACKET_GAP is the gap from one packet output to the next, so 960 allows for 960-864 = 96 clocks idle time between packets
        g_PACKET_GAP : natural range 0 to 1048575
    );
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        ---------------------------------------------------------------------------
        -- Data in to the beamformer
        i_data    : in std_logic_vector(127 downto 0); -- 2 stations delivered every clock. Must have a 8 cycle latency relative to the other inputs ("i_valid", "i_fine", "i_station", etc)
        i_flagged : in std_logic_vector(1 downto 0);
        i_fine    : in std_logic_vector(5 downto 0);  -- Fine channel, 0 to 53
        i_coarse  : in std_logic_vector(9 downto 0);  -- Coarse channel index.
        i_stations01 : in std_logic;
        i_firstStation : in std_logic;  -- Indicates that this is the first group of 4 stations.
        i_lastStation  : in std_logic;  -- Indicates that this is the last group of 4 stations. Used to move the result of the accumulation to the output buffer.
        i_timeStep     : in std_logic_vector(3 downto 0);  -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        i_station      : in std_logic_vector(9 downto 0);  -- Station index for the first of the two counts in steps of 4
        -- i_packetCount should be a count of the output frame relative to the SKA epoch 
        -- Each PSS output packet is 16 time samples = 16 * 69.12us = 1.105920 ms of data. There are 48 PSS output packets per corner turn frame.
        -- 48 bit value = 2^48 * 1.10592 ms = 9870 years (40 bit value would be 38 years)
        i_packetCount : in std_logic_vector(47 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        i_pktOdd  : in std_logic;
        i_valid   : in std_logic;
        i_beamTestEnable : in std_logic;
        --
        -- Data out to the next beamformer (pipelined version of the data in) (2 pipeline stages in this module)
        o_data    : out std_logic_vector(127 downto 0);  -- pipelined i_data 
        o_flagged : out std_logic_vector(1 downto 0);
        o_fine    : out std_logic_vector(5 downto 0);   -- pipelined i_fine
        o_coarse  : out std_logic_vector(9 downto 0);   -- pipelined i_coarse
        o_stations01   : out std_logic;
        o_firstStation : out std_logic;                 -- pipelined i_firstStation
        o_lastStation  : out std_logic;                 -- pipelined i_lastStation
        o_timeStep     : out std_logic_vector(3 downto 0);   -- pipelined i_timeStep
        o_station      : out std_logic_vector(9 downto 0);   --
        o_packetCount : out std_logic_vector(47 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        o_pktOdd  : out std_logic;
        o_valid   : out std_logic;
        o_beamTestEnable : out std_logic;
        --------------------------------------------------------------------------
        -- Phase data
        -- This is captured in a pair of registers, which form a 2-deep FIFO, so data for the next station or time can be
        -- loaded while the current station/time is being used.
        i_phase_virtualChannel : in std_logic_vector(9 downto 0);
        i_phase_timeStep : in std_logic_vector(9 downto 0);
        -- i_phase_beam is the index within this pipeline.
        -- This pipeline has absolute beam numbers g_PIPE + [0, 4, 8, 12, 16, etc] 
        -- So absolute beam number = g_PIPE + i_phase_beam*4
        i_phase_beam : in std_logic_vector(7 downto 0);  
        i_phase : in std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        i_phase_step : in std_logic_vector(23 downto 0); -- Phase step per fine channel.
        i_phase_valid : in std_logic;
        i_phase_clear : in std_logic; -- start of a corner turn frame, ensure we don't use old phase values.
        -- Pass on phase data to the next beamformer.
        o_phase_virtualChannel : out std_logic_vector(9 downto 0);
        o_phase_timeStep : out std_logic_vector(9 downto 0);
        o_phase_beam : out std_logic_vector(7 downto 0);
        o_phase : out std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        o_phase_step : out std_logic_vector(23 downto 0); -- Phase step per fine channel.
        o_phase_valid : out std_logic;
        o_phase_clear : out std_logic;
        ----------------------------------------------------------------------------
        -- Configuration
        i_beamsEnabled : in std_logic_vector(9 downto 0); -- Total number of beams enabled; 
        -- pipelined outputs for buffer settings
        o_beamsEnabled : out std_logic_vector(9 downto 0);
        ---------------------------------------------------------------------
        -- Packets of data from the previous beamformer, to be passed on to the next beamformer.
        i_BFdata        : in std_logic_vector(63 downto 0);
        i_BFscale       : in std_logic_vector(3 downto 0);
        i_BFpacketCount : in std_logic_vector(47 downto 0);
        i_BFBeam        : in std_logic_vector(9 downto 0);
        i_BFFreqIndex   : in std_logic_vector(10 downto 0);
        i_BFvalid       : in std_logic;
        -- Packets of data out to the 100G interface
        o_BFdata        : out std_logic_vector(63 downto 0);  -- PSR packetiser expects the first 16 bit value in bits 63:48, next in bits 47:32, etc.
        o_BFscale       : out std_logic_vector(3 downto 0);   -- scale factor to convert 16-bit values to 32-bit values in o_BFdata
        o_BFpacketCount : out std_logic_vector(47 downto 0);  -- Copy of i_packetCount, held for the duration of the output packets with the same timestamp. 
        o_BFFreqIndex   : out std_logic_vector(10 downto 0);  -- Index of the coarse (SPS) channel
        o_BFBeam        : out std_logic_vector(9 downto 0);  
        o_BFvalid       : out std_logic;
        i_badPacket     : in std_logic;  -- just used to trigger the debug core.
        o_badPacket     : out std_logic
    );
    
    -- prevent optimisation between adjacent instances of the PSSbeamformer.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of PSSbeamformer : entity is "yes";
    
end PSSbeamformer;

architecture Behavioral of PSSbeamformer is

    -- create_ip -name dds_compiler -vendor xilinx.com -library ip -version 6.0 -module_name sincosLookup
    -- set_property -dict [list CONFIG.Component_Name {sincosLookup} CONFIG.PartsPresent {SIN_COS_LUT_only} CONFIG.Noise_Shaping {None} CONFIG.Phase_Width {12} CONFIG.Output_Width {18} CONFIG.Amplitude_Mode {Unit_Circle} CONFIG.Parameter_Entry {Hardware_Parameters} CONFIG.Has_Phase_Out {false} CONFIG.DATA_Has_TLAST {Not_Required} CONFIG.S_PHASE_Has_TUSER {Not_Required} CONFIG.M_DATA_Has_TUSER {Not_Required} CONFIG.Latency {6} CONFIG.Output_Frequency1 {0} CONFIG.PINC1 {0}] [get_ips sincosLookup]
    --generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/sincosLookup/sincosLookup.xci]
    -- 12 bit phase gives 10 effective bits of precision in the sin/cos output. Since the input data to the beamformer is only 8 bits, 
    -- errors in the sin/cos will be negligible.
    -- 12 bit phase uses 1x18k BRAM.
    -- 6 clock latency
    component sincosLookup
    port (
        aclk                : in std_logic;
        s_axis_phase_tvalid : in std_logic;
        s_axis_phase_tdata  : in std_logic_vector(15 downto 0);  -- Phase in bits 11:0
        m_axis_data_tvalid  : out std_logic;
        m_axis_data_tdata   : out std_logic_vector(47 downto 0)); -- cosine in bits 17:0, sine in bits 41:24, 6 clock latency
    end component;

    -- create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name BF_cMult27x18
    -- set_property -dict [list CONFIG.Component_Name {BF_cMult27x18} CONFIG.APortWidth {27} CONFIG.BPortWidth {18} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {46} CONFIG.MinimumLatency {4}] [get_ips BF_cMult27x18]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/BF_cMult27x18/BF_cMult27x18.xci]
    component BF_cMult27x18
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(95 DOWNTO 0)); -- 4 cycle latency
    end component;
    
    -- convergent_round 35:0 -> 26:0, drop msb
    function convergent_round(din : std_logic_vector(35 downto 0)) return std_logic_vector is
    begin
        if (din(7 downto 0) = "10000000" and din(8) = '1') or (din(7) = '1' and din(6 downto 0) /= "0000000") then
            return std_logic_vector(unsigned(din(34 downto 8)) + 1);
        else
            return din(34 downto 8);
        end if;
    end convergent_round;
    
    component ila_2
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(63 downto 0)); 
    end component;
    
    component ila_beamData
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(119 downto 0)); 
    end component;
    
    attribute keep : string;
    attribute max_fanout : integer;
    
    signal data    : std_logic_vector(127 downto 0); -- 2 stations delivered every clock. Must have a six cycle latency relative to the other inputs ("i_fine", "i_station", etc)
    signal flagged : std_logic_vector(1 downto 0);
    signal fine    : std_logic_vector(5 downto 0);  -- Fine channel, 0 to 53
    signal coarse  : std_logic_vector(9 downto 0);  -- Coarse channel index.
    signal firstStation : std_logic;                -- Indicates that this is the first station in a group of stations. Used to reset the beam accumulator.
    signal lastStation  : std_logic;                -- Indicates that this is the last station in a group of stations. Used to move the result of the accumulation to the output buffer.
    signal timeStep     : std_logic_vector(3 downto 0); -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
    signal station      : std_logic_vector(9 downto 0); -- Station count for first of 4 stations delivered in a 2 clock burst.
    signal packetCount : std_logic_vector(47 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
    signal pktOdd  : std_logic;
    signal valid   : std_logic;
    signal phase_virtualChannel : std_logic_vector(9 downto 0);
    signal phase_timeStep : std_logic_vector(9 downto 0);
    signal phase_beam  : std_logic_vector(7 downto 0);
    signal phase_phase : std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
    signal phase_step  : std_logic_vector(23 downto 0); -- Phase step per fine channel.
    signal phase_valid : std_logic;
    signal phase_clear : std_logic;
    
    signal beamsEnabled : std_logic_vector(9 downto 0);
    signal badPacket : std_logic;
    signal beamEnabled, beamEnabled_del : std_logic_vector(3 downto 0); -- one enable bit for each of the 4 beams this module can generate
    
    type t_phase_data is array(3 downto 0) of t_slv_24_arr(3 downto 0);
    signal phase_phase1, current_phase : t_phase_data;
    signal phase_step1, current_phase_step : t_phase_data;
    signal phase_valid1 : t_slv_4_arr(3 downto 0);
    
    signal MultDinPol0, MultDinPol1 : t_slv_64_arr(1 downto 0);
    
    signal Pol0Real_Sum, Pol0Imag_Sum, Pol1Real_Sum, Pol1Imag_Sum : t_slv_32_arr(3 downto 0);
    type t_16 is array(3 downto 0) of t_slv_16_arr(1 downto 0);
    signal phase_16bit : t_16;
    type t_48 is array(3 downto 0) of t_slv_48_arr(1 downto 0);
    signal sinCos : t_48;
    type t_96 is array(3 downto 0) of t_slv_96_arr(1 downto 0);
    signal weightedPol0, weightedPol1 : t_96;
    type t_32 is array(3 downto 0) of t_slv_32_arr(1 downto 0);
    signal weightedPol0Real, weightedPol0Imag, weightedPol1Real, weightedPol1Imag : t_32;
    
    signal pol0Real_accumulator, pol1Real_accumulator, pol0Imag_accumulator, pol1Imag_accumulator : t_slv_32_arr(3 downto 0);
    signal pol0Real_accumulator_16bit, pol0Imag_accumulator_16bit, pol1Real_accumulator_16bit, pol1Imag_accumulator_16bit : t_slv_16_arr(3 downto 0);
    signal pol0Real_accumulator_16bit_del1, pol0Imag_accumulator_16bit_del1, pol1Real_accumulator_16bit_del1, pol1Imag_accumulator_16bit_del1 : t_slv_16_arr(3 downto 0);
    signal pol0Real_accumulator_del1, pol1Real_accumulator_del1, pol0Imag_accumulator_del1, pol1Imag_accumulator_del1 : t_slv_32_arr(3 downto 0);
    signal pol0Real_accumulator_del2, pol1Real_accumulator_del2, pol0Imag_accumulator_del2, pol1Imag_accumulator_del2 : t_slv_32_arr(3 downto 0);
    signal pol0Real_accumulator_del3, pol1Real_accumulator_del3, pol0Imag_accumulator_del3, pol1Imag_accumulator_del3 : t_slv_32_arr(3 downto 0);
    signal pol0Real_accumulator_del4, pol1Real_accumulator_del4, pol0Imag_accumulator_del4, pol1Imag_accumulator_del4 : t_slv_32_arr(3 downto 0);
    signal pol0Real_accDout, pol1Real_accDout, pol0Imag_accDout, pol1Imag_accDout : t_slv_32_arr(3 downto 0);
    signal AccDin, AccDout : t_slv_128_arr(3 downto 0);
    signal AccWrEn : std_logic_vector(0 downto 0);
    signal AccWriteAddr     : std_logic_vector(5 downto 0);

    signal AccReadAddr      : std_logic_vector(5 downto 0);

    signal AccReadAddr_arr  : t_slv_6_arr(3 downto 0);
    
    
    attribute max_fanout of AccReadAddr : signal is 100;
        
    signal pol0Real_scale, pol0Imag_scale, pol1Real_scale, pol1Imag_scale : t_slv_5_arr(3 downto 0);
    signal pol0_maxScale, pol1_maxScale, maxScale, maxScale_del1, maxScale_del2 : t_slv_5_arr(3 downto 0);
    
    signal pStoreDin, pStoreDout : std_logic_vector(143 downto 0);
    signal pStoreWrEn : std_logic_vector(0 downto 0);
    signal pStoreWriteAddr, pStoreReadAddr : std_logic_vector(11 downto 0);
    signal beamsEnabledHere : std_logic_vector(2 downto 0);
    signal read_trigger, frameSend : std_logic;
    signal frameCount : std_logic_vector(1 downto 0);
    signal sendingData_del1, sendingData, sendingData_del2, sendingData_del3, sendingData_del4 : std_logic;
    signal pStoreReadSel : std_logic;
    signal frameCount_del1, frameCount_hold, frameCount_del2, frameCount_del3 : std_logic_vector(1 downto 0);
    signal read_packetCount, pPacketCount : std_logic_vector(47 downto 0);
    signal read_coarse, pCoarse : std_logic_vector(9 downto 0);
    signal read_pktOdd : std_logic := '0';
    signal stations01 : std_logic := '0';

    signal fine_del : t_slv_6_arr(19 downto 0);

    attribute keep of fine_del : signal is "true";
    attribute max_fanout of fine_del : signal is 100;        -- FANOUT of 500+ at LL 1

    signal timeStep_del : t_slv_4_arr(19 downto 0);
    signal packetCount_del : t_slv_48_arr(19 downto 0);
    signal coarse_del, station_del : t_slv_10_arr(19 downto 0);
    signal valid_del, stations01_del, firstStation_del, lastStation_del, pktOdd_del : std_logic_vector(19 downto 0);

    attribute max_fanout of valid_del : signal is 100;        -- FANOUT of 500+ at LL 5
    attribute max_fanout of stations01_del : signal is 100;
    attribute max_fanout of firstStation_del : signal is 100;
    attribute max_fanout of lastStation_del : signal is 100;
    attribute max_fanout of pktOdd_del : signal is 100;

    signal fine_del13_eq0 : std_logic;
    signal pstoreWrEn_second, pStoreWrEn_first : std_logic := '0';
    signal last_time, last_fine, beamTestEnable : std_logic := '0';
    signal this_BFbeam : std_logic_vector(9 downto 0);
    signal beam_match_del12 : t_slv_2_arr(3 downto 0);
    signal stations23_del11 : std_logic;
    

begin
    
    -- Pipelines that go through to the next beamformer
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- Buffer settings
            beamsEnabled <= i_beamsEnabled;
            o_beamsEnabled <= beamsEnabled;
            
            badPacket <= i_badPacket;
            o_badPacket <= badPacket;
            
            for i in 0 to 3 loop
                -- 1 bit enable for each of 4 beams this module can generate.
                -- See comments above at the definition of the generics to understand which beams are enabled.
                if (unsigned(beamsEnabled) > (g_DAISY_CHAIN*16 + g_PIPE + 4*i)) then
                    beamEnabled(i) <= '1';
                else
                    beamEnabled(i) <= '0';
                end if;
            end loop; 
            -- Also find the number of beams enabled in this module
            if beamEnabled = "0000" then
                beamsEnabledHere <= "000";
            elsif beamEnabled = "0001" then
                beamsEnabledHere <= "001";
            elsif beamEnabled = "0011" then
                beamsEnabledHere <= "010";
            elsif beamEnabled = "0111" then
                beamsEnabledHere <= "011";
            else
                beamsEnabledHere <= "100";
            end if;
            beamEnabled_del <= beamEnabled;
            
            -- Pipeline the input data
            data <= i_data;               -- (127:0); 2 consecutive stations delivered every clock.
            flagged <= i_flagged;         -- (2:0);
            fine <= i_fine;               -- (5:0); fine channel 0 to 53
            coarse <= i_coarse;           -- (9:0);
            stations01 <= i_stations01;   -- 4 stations delivered in a burst for each fine channel, this is the first two stations
            firstStation <= i_firstStation; -- first Station in the burst.
            lastStation <= i_lastStation;   -- last station in the burst.
            timeStep <= i_timeStep;         -- (4:0); Time step within the output packet.
            station <= i_station;           -- (9:0)
            packetCount <= i_packetCount;   -- (47:0); The packet count for this packet, based on the original packet count from LFAA.
            pktOdd <= i_pktOdd;
            valid <= i_valid;
            beamTestEnable <= i_beamTestEnable;
            -- 
            phase_virtualChannel <= i_phase_virtualChannel; -- (9:0);
            phase_timeStep <= i_phase_timeStep; -- (9:0);
            phase_beam <= i_phase_beam;         -- (3:0);
            phase_phase <= i_phase;             -- (23:0);  -- Phase at the start of the coarse channel.
            phase_step  <= i_phase_step;        -- (23:0);  -- Phase step per fine channel.
            phase_valid <= i_phase_valid;       --
            phase_clear <= i_phase_clear;
            --
            o_phase_virtualChannel <= phase_virtualChannel; -- (9:0);
            o_phase_timeStep  <= phase_timeStep;            -- (9:0);
            o_phase_beam  <= phase_beam;      -- (3:0);
            o_phase       <= phase_phase;     -- (23:0);  -- Phase at the start of the coarse channel.
            o_phase_step  <= phase_step;      -- (23:0);  -- Phase step per fine channel.
            o_phase_valid <= phase_valid;     -- 
            o_phase_clear <= phase_clear;
            --
            o_data <= data;
            o_flagged <= flagged;
            o_fine <= fine;  
            o_coarse <= coarse;
            o_stations01 <= stations01;
            o_firstStation <= firstStation;
            o_lastStation <= lastStation;
            o_timeStep <= timeStep;
            o_station <= station;
            o_packetCount <= packetCount;
            o_pktOdd <= pktOdd;
            o_valid <= valid;
            o_beamTestEnable <= beamTestEnable;
            
            -- Capture the phase data to use for the next set of 4 stations being sent
            -- This module supports 4 beams, with 4 stations being processed in parallel.
            -- So we have 16 different phase + phase_step values. 
            for beam in 0 to 3 loop
                for station in 0 to 3 loop
                    if phase_clear = '1' then
                        phase_valid1(beam)(station) <= '0';
                    elsif ((phase_valid = '1') and 
                           (unsigned(phase_beam) = g_DAISY_CHAIN*4 + beam) and
                           (unsigned(phase_virtualChannel(1 downto 0)) = station)) then
                        phase_phase1(beam)(to_integer(unsigned(phase_virtualChannel(1 downto 0)))) <= phase_phase;
                        phase_step1(beam)(to_integer(unsigned(phase_virtualChannel(1 downto 0)))) <= phase_step;   -- Phase step per frequency channel
                        phase_valid1(beam)(to_integer(unsigned(phase_virtualChannel(1 downto 0)))) <= '1';
                    end if;
                end loop;
            end loop;
            
            -- the beam number for this beam
            case g_PIPE is
                when 0 => this_BFbeam(1 downto 0) <= "00";
                when 1 => this_BFbeam(1 downto 0) <= "01";
                when 2 => this_BFbeam(1 downto 0) <= "10";
                when others => this_BFbeam(1 downto 0) <= "11";
            end case;
            this_BFbeam(3 downto 2) <= "00";  -- these two bits select the four different beams in this module.
            this_BFbeam(9 downto 4) <= std_logic_vector(to_unsigned(g_DAISY_CHAIN, 6));
            
        end if;
    end process; 
    

    --------------------------------------------------------------------------------------------
    --
    -- Processing pipeline
    --  phase correction -> sum -> output packet memory.
    -- 
    
    -- fine_del(0) <= fine;
    -- valid_del(0) <= valid;
    -- station_del(0) <= station;
    -- stations01_del(0) <= stations01;
    -- firstStation_del(0) <= firstStation;
    -- lastStation_del(0) <= lastStation;
    -- pktOdd_del(0) <= pktOdd;
    -- timeStep_del(0) <= timeStep;
    -- packetCount_del(0) <= packetCount;
    -- coarse_del(0) <= coarse;
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            
            -- Input data order : 
            --
            --    For timeGroup = 0:47                          -- 53 ms corner turn, so this is 0:(256/32-1) = 0:7
            --       For Coarse = 0:(i_coarse-1)                -- step through each SPS coarse channel
            --          For Time = 0:15                         -- 16 times needed for an output packet
            --             For Station_block = 0:(i_stations/4)
            --                For fine_offset = 0:53            -- Total of 54 fine channels per SPS channel
            --                   For station = 0:3
            --                       - 2 clocks, data for 2 stations per clock  
            --
            
            fine_del(1)                     <= fine;
            valid_del(1)                    <= valid;
            station_del(1)                  <= station;
            stations01_del(1)               <= stations01;
            firstStation_del(1)             <= firstStation;
            lastStation_del(1)              <= lastStation;
            pktOdd_del(1)                   <= pktOdd;
            timeStep_del(1)                 <= timeStep;
            packetCount_del(1)              <= packetCount;
            coarse_del(1)                   <= coarse;

            fine_del(19 downto 2)           <= fine_del(18 downto 1);
            valid_del(19 downto 2)          <= valid_del(18 downto 1);
            station_del(19 downto 2)        <= station_del(18 downto 1);
            stations01_del(19 downto 2)     <= stations01_del(18 downto 1);
            firstStation_del(19 downto 2)   <= firstStation_del(18 downto 1);
            lastStation_del(19 downto 2)    <= lastStation_del(18 downto 1);
            pktOdd_del(19 downto 2)         <= pktOdd_del(18 downto 1);
            timeStep_del(19 downto 2)       <= timeStep_del(18 downto 1);
            packetCount_del(19 downto 2)    <= packetCount_del(18 downto 1);
            coarse_del(19 downto 2)         <= coarse_del(18 downto 1);
            
            
            -- Processing Pipeline :
            --   i_valid
            --   valid          stations01          fine = fine_del(0)
            --   valid_del(1)   current_phase  stations01_del(1)     fine_del(1)
            --   valid_del(2)   phase_16bit
            --   valid_del(3)
            --   valid_del(4)
            --   valid_del(5)
            --   valid_del(6)
            --   valid_del(7)                                i_data
            --   valid_del(8)   sincos (6 cycle latency from phase_16bit)   multDinPolX = data
            --   valid_del(9)
            --   valid_del(10)            
            --   valid_del(11)                          fine_del(11)
            --   valid_del(12)  weightedPol0Real        AccReadAddr
            --   valid_del(13)  pol0Real_sum            pol0Real_accDout
            --   valid_del(14)  pol0Real_accumulator (= AccDin)  AccWrAddr  AccWrEn
            --   valid_del(15)  pol0Real_accumulator_del1
            --   valid_del(16)  pol0Real_accumulator_del2  pol0Real_scale
            --   valid_del(17)  pol0Real_accumulator_del3  pol0_maxScale
            --   valid_del(18)  pol0Real_accumulator_del4  maxScale
            --   valid_del(19)  pol0Real_accumulator_16bit maxScale_del1
            --   valid_del(20)  pStoreDin  pStoreWrAddr  pStoreWrEn
            --
            for beam in 0 to 3 loop
                for station in 0 to 3 loop
                    if valid = '1' and stations01 = '1' then
                        if unsigned(fine) = 0 then
                            -- First clock cycle for data with fine = 0
                            -- Get the phase information for a new set of 4 stations
                            current_phase(beam)(station) <= phase_phase1(beam)(station);
                            current_phase_step(beam)(station) <= phase_step1(beam)(station);
                        elsif unsigned(fine) /= 0 then
                            -- first clock cycle for data with fine > 0
                            -- Increment the phase 
                            current_phase(beam)(station) <= std_logic_vector(unsigned(current_phase(beam)(station)) + unsigned(current_phase_step(beam)(station)));
                        end if;
                    end if;
                end loop;
            end loop;
            
            if (unsigned(fine_del(12)) = 0) then
                fine_del13_eq0 <= '1';
            else
                fine_del13_eq0 <= '0';
            end if;
            
            stations23_del11 <= not stations01_del(10);
            
            if station_del(11)(9 downto 4) = this_BFBeam(9 downto 4) and (station_del(11)(3 downto 2) = "00") and (this_BFBeam(1) = stations23_del11) then
                -- beam 0 of 3 in this module matches one of the stations being calculated
                beam_match_del12(0) <= '1' & this_BFBeam(0);
            else
                beam_match_del12(0) <= "00";
            end if;
            if station_del(11)(9 downto 4) = this_BFBeam(9 downto 4) and (station_del(11)(3 downto 2) = "01") and (this_BFBeam(1) = stations23_del11) then
                -- beam 0 of 3 in this module matches one of the stations being calculated
                beam_match_del12(1) <= '1' & this_BFBeam(0);
            else
                beam_match_del12(1) <= "00";
            end if;
            if station_del(11)(9 downto 4) = this_BFBeam(9 downto 4) and (station_del(11)(3 downto 2) = "10") and (this_BFBeam(1) = stations23_del11) then
                -- beam 0 of 3 in this module matches one of the stations being calculated
                beam_match_del12(2) <= '1' & this_BFBeam(0);
            else
                beam_match_del12(2) <= "00";
            end if;
            if station_del(11)(9 downto 4) = this_BFBeam(9 downto 4) and (station_del(11)(3 downto 2) = "11") and (this_BFBeam(1) = stations23_del11) then
                -- beam 0 of 3 in this module matches one of the stations being calculated
                beam_match_del12(3) <= '1' & this_BFBeam(0);
            else
                beam_match_del12(3) <= "00";
            end if;
            
            for beam in 0 to 3 loop
                if (stations01_del(1) = '1') then
                    -- Data for 4 stations delivered over 2 clocks, this is the first of those two clocks.
                    phase_16bit(beam)(0) <= "0000" & current_phase(beam)(0)(23 downto 12);
                    phase_16bit(beam)(1) <= "0000" & current_phase(beam)(1)(23 downto 12);
                else
                    phase_16bit(beam)(0) <= "0000" & current_phase(beam)(2)(23 downto 12);
                    phase_16bit(beam)(1) <= "0000" & current_phase(beam)(3)(23 downto 12);
                end if;

                -- sum data from two stations processed in this clock
                if beamTestEnable = '1' then
                    -- In test mode we only add in the station with the same index as the beam
                    -- The beam is this_BFBeam, with this_BFBeam(3:2) = beam
                    -- The two stations being processed are 
                    --  stations01_del(12) = '1' : station_del(12), station_del(12) + 1 
                    --  stations01_del(12) = '0' : station_del(12) + 2, station_del(12) + 3
                    if beam_match_del12(beam) = "10" then
                        pol0Real_sum(beam) <= weightedPol0Real(beam)(0);
                        pol0Imag_sum(beam) <= weightedPol0Imag(beam)(0);
                        pol1Real_sum(beam) <= weightedPol1Real(beam)(0);
                        pol1Imag_sum(beam) <= weightedPol1Imag(beam)(0);
                    elsif beam_match_del12(beam) = "11" then
                        pol0Real_sum(beam) <= weightedPol0Real(beam)(1);
                        pol0Imag_sum(beam) <= weightedPol0Imag(beam)(1);
                        pol1Real_sum(beam) <= weightedPol1Real(beam)(1);
                        pol1Imag_sum(beam) <= weightedPol1Imag(beam)(1);
                    else
                        pol0Real_sum(beam) <= (others => '0');
                        pol0Imag_sum(beam) <= (others => '0');
                        pol1Real_sum(beam) <= (others => '0');
                        pol1Imag_sum(beam) <= (others => '0');
                    end if;
                else
                    pol0Real_sum(beam) <= std_logic_vector(unsigned(weightedPol0Real(beam)(0)) + unsigned(weightedPol0Real(beam)(1)));
                    pol0Imag_sum(beam) <= std_logic_vector(unsigned(weightedPol0Imag(beam)(0)) + unsigned(weightedPol0Imag(beam)(1)));
                    pol1Real_sum(beam) <= std_logic_vector(unsigned(weightedPol1Real(beam)(0)) + unsigned(weightedPol1Real(beam)(1)));
                    pol1Imag_sum(beam) <= std_logic_vector(unsigned(weightedPol1Imag(beam)(0)) + unsigned(weightedPol1Imag(beam)(1)));
                end if;
                
                -- Accumulate across all stations
                --if beamTestEnable = '1' then !!!
                if (valid_del(13) = '1' and firstStation_del(13) = '1' and stations01_del(13) = '1') then
                    -- First station, so there is no existing accumulation value.
                    pol0Real_accumulator(beam) <= pol0Real_sum(beam);
                    pol0Imag_accumulator(beam) <= pol0Imag_sum(beam);
                    pol1Real_accumulator(beam) <= pol1Real_sum(beam);
                    pol1Imag_accumulator(beam) <= pol1Imag_sum(beam);
                elsif (valid_del(13) = '1' and stations01_del(13) = '1' and firstStation_del(13) = '0') then
                    -- not the first station, first of the 2 clocks for the innermost 2-clock long loop.
                    -- Add to the current value of the accumulation from the distributed memory
                    pol0Real_accumulator(beam) <= std_logic_vector(unsigned(pol0Real_accDout(beam)) + unsigned(pol0Real_sum(beam)));
                    pol0Imag_accumulator(beam) <= std_logic_vector(unsigned(pol0Imag_accDout(beam)) + unsigned(pol0Imag_sum(beam)));
                    pol1Real_accumulator(beam) <= std_logic_vector(unsigned(pol1Real_accDout(beam)) + unsigned(pol1Real_sum(beam)));
                    pol1Imag_accumulator(beam) <= std_logic_vector(unsigned(pol1Imag_accDout(beam)) + unsigned(pol1Imag_sum(beam)));
                elsif (valid_del(13) = '1' and stations01_del(13) = '0') then
                    -- Second of the two clocks in the innermost loop, add to the accumulator register
                    pol0Real_accumulator(beam) <= std_logic_vector(unsigned(pol0Real_accumulator(beam)) + unsigned(pol0Real_sum(beam)));
                    pol0Imag_accumulator(beam) <= std_logic_vector(unsigned(pol0Imag_accumulator(beam)) + unsigned(pol0Imag_sum(beam)));
                    pol1Real_accumulator(beam) <= std_logic_vector(unsigned(pol1Real_accumulator(beam)) + unsigned(pol1Real_sum(beam)));
                    pol1Imag_accumulator(beam) <= std_logic_vector(unsigned(pol1Imag_accumulator(beam)) + unsigned(pol1Imag_sum(beam)));
                end if;
                
                -- Delay by 2 to align with the output of scale factor calculation
                pol0Real_accumulator_del1(beam) <= pol0Real_accumulator(beam);
                pol0Imag_accumulator_del1(beam) <= pol0Imag_accumulator(beam);
                pol1Real_accumulator_del1(beam) <= pol1Real_accumulator(beam);
                pol1Imag_accumulator_del1(beam) <= pol1Imag_accumulator(beam);
                
                pol0Real_accumulator_del2(beam) <= pol0Real_accumulator_del1(beam);
                pol0Imag_accumulator_del2(beam) <= pol0Imag_accumulator_del1(beam);
                pol1Real_accumulator_del2(beam) <= pol1Real_accumulator_del1(beam);
                pol1Imag_accumulator_del2(beam) <= pol1Imag_accumulator_del1(beam);
                
                pol0Real_accumulator_del3(beam) <= pol0Real_accumulator_del2(beam);
                pol0Imag_accumulator_del3(beam) <= pol0Imag_accumulator_del2(beam);
                pol1Real_accumulator_del3(beam) <= pol1Real_accumulator_del2(beam);
                pol1Imag_accumulator_del3(beam) <= pol1Imag_accumulator_del2(beam);
                
                pol0Real_accumulator_del4(beam) <= pol0Real_accumulator_del3(beam);
                pol0Imag_accumulator_del4(beam) <= pol0Imag_accumulator_del3(beam);
                pol1Real_accumulator_del4(beam) <= pol1Real_accumulator_del3(beam);
                pol1Imag_accumulator_del4(beam) <= pol1Imag_accumulator_del3(beam);
                
            end loop;
            
            if (valid_del(13) = '1' and stations01_del(13) = '0') then
                AccWrEn(0) <= '1';
            else
                AccWrEn(0) <= '0';
            end if;
            AccWriteAddr <= fine_del(13);

            -- timing update
            AccReadAddr         <= fine_del(9);
            AccReadAddr_arr(0)  <= AccReadAddr;
            AccReadAddr_arr(1)  <= AccReadAddr;
            AccReadAddr_arr(2)  <= AccReadAddr;
            AccReadAddr_arr(3)  <= AccReadAddr;

        end if;
    end process;
    
    MultDinPol0(0)(31 downto 11) <= data(15) & data(15) & data(15) & data(15) & data(15) & data(15 downto 0);
    MultDinPol0(0)(10 downto 0) <= (others => '0');
    MultDinPol0(0)(63 downto 43) <= data(31) & data(31) & data(31) & data(31) & data(31) & data(31 downto 16);
    MultDinPol0(0)(42 downto 32) <= (others => '0');
    
    MultDinPol1(0)(31 downto 11) <= data(47) & data(47) & data(47) & data(47) & data(47) & data(47 downto 32);
    MultDinPol1(0)(10 downto 0) <= (others => '0');
    MultDinPol1(0)(63 downto 43) <= data(63) & data(63) & data(63) & data(63) & data(63) & data(63 downto 48);
    MultDinPol1(0)(42 downto 32) <= (others => '0');
    
    MultDinPol0(1)(31 downto 11) <= data(79) & data(79) & data(79) & data(79) & data(79) & data(79 downto 64);
    MultDinPol0(1)(10 downto 0) <= (others => '0');
    MultDinPol0(1)(63 downto 43) <= data(95) & data(95) & data(95) & data(95) & data(95) & data(95 downto 80);
    MultDinPol0(1)(42 downto 32) <= (others => '0');
    
    MultDinPol1(1)(31 downto 11) <= data(111) & data(111) & data(111) & data(111) & data(111) & data(111 downto 96);
    MultDinPol1(1)(10 downto 0) <= (others => '0');
    MultDinPol1(1)(63 downto 43) <= data(127) & data(127) & data(127) & data(127) & data(127) & data(127 downto 112);
    MultDinPol1(1)(42 downto 32) <= (others => '0');

    beamGen : for beam in 0 to 3 generate
        multGen : for i in 0 to 1 generate
            
            sclookupPol0 : sincosLookup
            port map (
                aclk                => i_BF_clk,  -- in std_logic;
                s_axis_phase_tvalid => '1',       -- in std_logic;
                s_axis_phase_tdata  => phase_16bit(beam)(i), -- in (15:0);  -- Phase in bits 11:0
                m_axis_data_tvalid  => open,      -- out std_logic;
                m_axis_data_tdata   => sinCos(beam)(i)  -- out (47:0)); -- cosine in bits 17:0, sine in bits 41:24, 6 clock latency
            );
            
            -- complex multiplier for the phase, first polarisation 
            bmultPol0 : BF_cMult27x18
            port map (
                aclk            => i_BF_clk,
                s_axis_a_tvalid => '1',
                s_axis_a_tdata  => MultDinPol0(i), -- in (63:0); real in 26:0, imaginary in 58:32
                s_axis_b_tvalid => '1',
                s_axis_b_tdata  => sinCos(beam)(i),         -- in 47:0; real in 17:0, imaginary in 41:24; unity is 2^17.
                m_axis_dout_tvalid => open,           -- out std_logic;
                m_axis_dout_tdata  => weightedPol0(beam)(i) -- out 95:0, real in 45:0, imaginary in 93:48
            );
            
            -- keep the top 23 bits, sign extend by 8 bits for use in the accumulator
            weightedPol0Real(beam)(i) <= weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45) & weightedPol0(beam)(i)(45 downto 22);
            weightedPol0Imag(beam)(i) <= weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93) & weightedPol0(beam)(i)(93 downto 70);
            
            -- second polarisation
            bmultPol1 : BF_cMult27x18
            port map (
                aclk            => i_BF_clk,
                s_axis_a_tvalid => '1',
                s_axis_a_tdata  => MultDinPol1(i), -- in (63:0); real in 26:0, imaginary in 58:32
                s_axis_b_tvalid => '1',
                s_axis_b_tdata  => sinCos(beam)(i),         -- in 47:0; real in 17:0, imaginary in 41:24
                m_axis_dout_tvalid => open,           -- out STD_LOGIC;
                m_axis_dout_tdata  => weightedPol1(beam)(i) -- out 95:0, real in 45:0, imaginary in 93:48
            );
            
            weightedPol1Real(beam)(i) <= weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45) & weightedPol1(beam)(i)(45 downto 22);
            weightedPol1Imag(beam)(i) <= weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93) & weightedPol1(beam)(i)(93 downto 70);
            
        end generate;
        
        -- Distributed memory for the partial sum for 54 fine channels
        
        AccDin(beam)(31 downto 0) <= pol0Real_accumulator(beam);
        AccDin(beam)(63 downto 32) <= pol0Imag_accumulator(beam);
        AccDin(beam)(95 downto 64) <= pol1Real_accumulator(beam);
        AccDin(beam)(127 downto 96) <= pol1Imag_accumulator(beam);
        
        pol0Real_accDout(beam) <= AccDout(beam)(31 downto 0);
        pol0Imag_accDout(beam) <= AccDout(beam)(63 downto 32);
        pol1Real_accDout(beam) <= AccDout(beam)(95 downto 64);
        pol1Imag_accDout(beam) <= AccDout(beam)(127 downto 96);
        
        xpm_memory_sdpram_Acc_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 6,               -- DECIMAL
            ADDR_WIDTH_B => 6,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 128,       -- DECIMAL; 32 bit real+imaginary for 2 polarisations
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "block", -- String
            MEMORY_SIZE => 8192,             -- 128 bits wide x 64 deep = 8192 bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 128,        -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 128,       -- DECIMAL
            WRITE_MODE_B => "read_first"      -- String
        ) port map (
            dbiterrb => open,  
            doutb => AccDout(beam),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => AccWriteAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => AccReadAddr_arr(beam),    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => AccDin(beam), -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',     -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',       -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',      -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => AccWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
        
        pol0re_sci : entity bf_lib.PSS_scalefactor
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol0Real_accumulator(beam), -- in (31:0);
            o_scale  => pol0Real_scale(beam) -- out (4:0); 2 cycle latency. 
        );
        
        pol0im_sci : entity bf_lib.PSS_scalefactor
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol0Imag_accumulator(beam), -- in (31:0);
            o_scale  => pol0Imag_scale(beam) -- out (4:0); 2 cycle latency. 
        );
        
        pol1re_sci : entity bf_lib.PSS_scalefactor
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol1Real_accumulator(beam), -- in (31:0);
            o_scale  => pol1Real_scale(beam) -- out (4:0); 2 cycle latency. 
        );
        
        pol1im_sci : entity bf_lib.PSS_scalefactor
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol1Imag_accumulator(beam), -- in (31:0);
            o_scale  => pol1Imag_scale(beam) -- out (4:0); 2 cycle latency. 
        );
        
        -- Apply the scale factors; del4 : 2 clocks for PSS_scaleFactor and 2 to find the maximum scale factor.
        pol0re_asci : entity bf_lib.PSS_ApplyScale
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol0Real_accumulator_del4(beam), -- in (31:0);
            i_scale  => maxScale(beam), -- in (4:0);
            o_data   => pol0Real_accumulator_16bit(beam) -- out (15:0); 1 clock latency
        );
        
        pol0im_asci : entity bf_lib.PSS_ApplyScale
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol0Imag_accumulator_del4(beam), -- in (31:0);
            i_scale  => maxScale(beam), -- in (4:0);
            o_data   => pol0Imag_accumulator_16bit(beam) -- out (15:0); 1 clock latency
        );
        
        pol1re_asci : entity bf_lib.PSS_ApplyScale
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol1Real_accumulator_del4(beam), -- in (31:0);
            i_scale  => maxScale(beam), -- in (4:0);
            o_data   => pol1Real_accumulator_16bit(beam) -- out (15:0); 1 clock latency
        );
        
        pol1im_asci : entity bf_lib.PSS_ApplyScale
        Port map(
            i_BF_clk => i_BF_clk,
            i_data   => pol1Imag_accumulator_del4(beam), -- in (31:0);
            i_scale  => maxScale(beam), -- in (4:0);
            o_data   => pol1Imag_accumulator_16bit(beam)  -- out (15:0); 1 clock latency
        );
        
    end generate;
    
    ----------------------------------------------------------------------------------
    -- Data is written to the output ultraRAM buffer from 
    --  pol0Real_accumulator_del2, pol0Imag_accumulator_del2, pol1Real_accumulator_del2, pol1Imag_accumulator_del2
    -- Delayed by 2 clocks to align with pol0Real_scale etc. 
    -- 
    -- This is written to the ultraRAM buffer together with the scaling factor used for each value.
    --
     
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- find the largest scaling factor across pol0 real, pol0 imag, pol1 real, pol1 imag
            -- The largest scaling factor is applied to all 4 values.
            for beam in 0 to 3 loop
                if unsigned(pol0Real_scale(beam)) > unsigned(pol0Imag_scale(beam)) then
                    pol0_maxScale(beam) <= pol0Real_scale(beam);
                else
                    pol0_maxScale(beam) <= pol0Imag_scale(beam);
                end if;
                if unsigned(pol1Real_scale(beam)) > unsigned(pol1Imag_scale(beam)) then
                    pol1_maxScale(beam) <= pol1Real_scale(beam);
                else
                    pol1_maxScale(beam) <= pol1Imag_scale(beam);
                end if;
                if unsigned(pol0_maxScale(beam)) > unsigned(pol1_maxScale(beam)) then
                    maxScale(beam) <= pol0_maxScale(beam);
                else
                    maxScale(beam) <= pol1_maxScale(beam);
                end if;
                
                maxScale_del1(beam) <= maxScale(beam);
                maxScale_del2(beam) <= maxScale_del1(beam);
            end loop;
            
            -- top 8 bits hold the scale factor, remaining 64 bits hold dual-pol 16+16 bit complex data 
            -- Two beams are written each clock cycle
            --   first beam  : pStoreDin(71:0) <= <scale factor> <pol 1 im> <pol 1 re> <pol 0 im> <pol 0 re>
            --   second beam : pStoreDin(143:72) <= <scale factor> <pol 1 im> <pol 1 re> <pol 0 im> <pol 0 re>
            pol0Real_accumulator_16bit_del1 <= pol0Real_accumulator_16bit;
            pol0Imag_accumulator_16bit_del1 <= pol0Imag_accumulator_16bit;
            pol1Real_accumulator_16bit_del1 <= pol1Real_accumulator_16bit;
            pol1Imag_accumulator_16bit_del1 <= pol1Imag_accumulator_16bit;
            
            if (valid_del(19) = '1' and stations01_del(19) = '0') then
                pStoreDin(71 downto 64) <= "000" & maxScale_del1(0);
                pStoreDin(15 downto 0) <= pol0Real_accumulator_16bit(0);
                pStoreDin(31 downto 16) <= pol0Imag_accumulator_16bit(0);
                pStoreDin(47 downto 32) <= pol1Real_accumulator_16bit(0);
                pStoreDin(63 downto 48) <= pol1Imag_accumulator_16bit(0);
                
                pStoreDin(143 downto 136) <= "000" & maxScale_del1(1);
                pStoreDin(87 downto 72) <= pol0Real_accumulator_16bit(1);
                pStoreDin(103 downto 88) <= pol0Imag_accumulator_16bit(1);
                pStoreDin(119 downto 104) <= pol1Real_accumulator_16bit(1);
                pStoreDin(135 downto 120) <= pol1Imag_accumulator_16bit(1);
                
                pStoreWriteAddr(11) <= pktOdd_del(19);
                pStoreWriteAddr(9 downto 0) <= fine_del(19)(5 downto 0) & timestep_del(19)(3 downto 0);
                pStoreWriteAddr(10) <= '0';
            else
                -- Second clock cycle 
                -- stations01_del(19) can't be low two clocks in a row, so we can use the second clock
                -- cycle to write the other 2 beams into the ultraRAM.
                pStoreDin(71 downto 64) <= "000" & maxScale_del2(2);
                pStoreDin(15 downto 0) <= pol0Real_accumulator_16bit_del1(2);
                pStoreDin(31 downto 16) <= pol0Imag_accumulator_16bit_del1(2);
                pStoreDin(47 downto 32) <= pol1Real_accumulator_16bit_del1(2);
                pStoreDin(63 downto 48) <= pol1Imag_accumulator_16bit_del1(2);
                
                pStoreDin(143 downto 136) <= "000" & maxScale_del2(3);
                pStoreDin(87 downto 72) <= pol0Real_accumulator_16bit_del1(3);
                pStoreDin(103 downto 88) <= pol0Imag_accumulator_16bit_del1(3);
                pStoreDin(119 downto 104) <= pol1Real_accumulator_16bit_del1(3);
                pStoreDin(135 downto 120) <= pol1Imag_accumulator_16bit_del1(3);
                
                pStoreWriteAddr(10) <= '1';
            end if;
            
            if (valid_del(18) = '1' and lastStation_del(18) = '1' and stations01_del(18) = '0') then
                pStoreWrEn_first <= '1';
            else
                pStoreWrEn_first <= '0';
            end if;
            pstoreWrEn_second <= pStoreWrEn_first;
            pStoreWrEn(0) <= pStoreWrEn_first or pStoreWrEn_second;
            
            -- Trigger readout when the last data is written to the ultraRAM
            if (unsigned(fine_del(18)(5 downto 0)) = 53) then
                last_fine <= '1';
            else
                last_fine <= '0';
            end if;
            if timeStep_del(18) = "1111" then
                last_time <= '1';
            else
                last_time <= '0';
            end if;
            if (valid_del(19) = '1') and (lastStation_Del(19) = '1') and (last_fine = '1') and (last_time = '1') then
                read_trigger <= '1';
                read_pktOdd <= pktOdd_del(19);
                read_packetCount <= packetCount_del(19);
                read_coarse <= coarse_del(19);
            else
                read_trigger <= '0';
            end if;
            
        end if;
    end process;
    
    xpm_memory_pstore_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 12,              -- DECIMAL
        ADDR_WIDTH_B => 12,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 144,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => 589824,           -- 144 bits wide x 4096 deep = 589824 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 144,         -- DECIMAL
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 144,       -- DECIMAL
        WRITE_MODE_B => "read_first"     -- String
    ) port map (
        dbiterrb => open,  
        doutb => pStoreDout,     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => pStoreWriteAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => pStoreReadAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_BF_clk,                 
        clkb => i_BF_clk,             
        dina => pStoreDin, -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',     -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',       -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',      -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => pStoreWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    
    -------------------------------------------------------------------------------------
    -- Read out the packet store ultraRAM when we process the last station + time sample
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            if read_trigger = '1' then
                pStoreReadAddr(11) <= read_pktOdd; -- which buffer we are reading out from the ultraRAM
                pPacketCount <= read_packetCount;
                pCoarse <= read_coarse;
            end if;
            
            if frameSend = '1' then
                pStoreReadAddr(10) <= frameCount(1);
                pStoreReadAddr(9 downto 0) <= "0000000000";
                pStoreReadSel <= frameCount(0);   -- high or low 72 bits read from the ultraRAM to select between beams (0 and 1) or (2 and 3)
                sendingData <= '1';
                frameCount_hold <= frameCount;
            elsif sendingData = '1' then
                pStoreReadAddr <= std_logic_vector(unsigned(pStoreReadAddr) + 1);
                if unsigned(pStoreReadAddr(9 downto 0)) = 863 then
                    sendingData <= '0';
                end if; 
            end if;
            
            sendingData_del1 <= sendingData;
            sendingData_del2 <= sendingData_del1;
            sendingData_del3 <= sendingData_del2;
            sendingData_del4 <= sendingData_del3;
            
            frameCount_del1 <= frameCount_hold;
            frameCount_del2 <= frameCount_del1;
            frameCount_del3 <= frameCount_del2;
            
            if sendingData_del3 = '1' then
                if pStoreReadSel = '0' then
                    o_BFdata <= pStoreDout(63 downto 0);
                    o_BFscale <= pStoreDout(67 downto 64);
                else
                    o_BFdata <= pStoreDout(135 downto 72);
                    o_BFscale <= pStoreDout(139 downto 136);
                end if;
                o_BFpacketCount <= pPacketCount;
                -- See comments about beam numbering in the generics section of this module for more information about how BFbeam is assigned.
                case g_PIPE is
                    when 0 => o_BFbeam(1 downto 0) <= "00";
                    when 1 => o_BFbeam(1 downto 0) <= "01";
                    when 2 => o_BFbeam(1 downto 0) <= "10";
                    when others => o_BFbeam(1 downto 0) <= "11";
                end case;
                o_BFbeam(3 downto 2) <= frameCount_del3;
                o_BFbeam(9 downto 4) <= std_logic_vector(to_unsigned(g_DAISY_CHAIN, 6));
                o_BFfreqIndex <= '0' & pCoarse;
                o_BFvalid <= '1';
            else
                o_BFdata <= i_BFdata;
                o_BFScale <= i_BFScale;
                o_BFpacketCount <= i_BFpacketCount;
                o_BFBeam <= i_BFbeam;
                o_BFfreqIndex <= i_BFfreqIndex;
                o_BFvalid <= i_BFvalid;
            end if;
            
        end if;
    end process;
    
    pss_triggeri : entity bf_lib.PSSbeamTrigger
    generic map(
        g_DAISY_CHAIN => g_DAISY_CHAIN, -- integer;
        -- The number of clocks to output a packet is 864 (=(54 fine channels)*(16 times))
        -- g_PACKET_GAP is the gap from one packet output to the next, so 960 allows for 960-864 = 96 clocks idle time between packets
        g_PACKET_GAP => g_PACKET_GAP  --  natural range 0 to 1048575
    ) port map(
        i_BF_clk => i_BF_clk, -- 400 MHz clock
        ---------------------------------------------------------------------------
        -- configuration
        -- Number of beams enabled in this module
        i_beamsEnabled => beamsEnabledHere, -- in (2:0); 
        -- pulse high to indicate data is available to read out
        i_readStart => read_trigger, --  in std_logic;
        -- Sequence of up to 4 pulses to trigger reading of a frame
        o_frameSend => frameSend, -- out std_logic;
        -- Which of the 4 possible frames to send (0, 1, 2 or 3)
        o_frameCount => frameCount -- out (1:0)
    );
    
end Behavioral;
