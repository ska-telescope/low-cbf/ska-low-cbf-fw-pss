----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 4 July 2024
-- Module Name: axi_full_64k - Behavioral
-- Description: 
--   axi full interface to a 64kbyte memory
-- 
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib, bf_lib, xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;

entity axi_full_64k is
    port(
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        -- Station Jones registers
        i_axi_mosi : in  t_axi4_full_mosi;
        o_axi_miso : out t_axi4_full_miso;
        ----------------------------------------------------------------------
        -- clock for the memory
        i_BF_clk : in std_logic;
        -- 4k deep x 16 byte wide read interface ?
        i_rd_addr : in std_logic_vector(15 downto 0); -- byte address
        o_rd_data : out std_logic_vector(63 downto 0)
    );
end axi_full_64k;

architecture Behavioral of axi_full_64k is

    -- create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_BF
    -- set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_BF} CONFIG.MEM_DEPTH {262144} CONFIG.READ_LATENCY {5}] [get_ips axi_bram_ctrl_BF]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201123_161551/vitisAccelCore.srcs/sources_1/ip/axi_bram_ctrl_BF/axi_bram_ctrl_BF.xci]
    component axi_bram_ctrl_BF
    port (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;

    COMPONENT axi_clock_converter_BF
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        m_axi_aclk : IN STD_LOGIC;
        m_axi_aresetn : IN STD_LOGIC;
        m_axi_awaddr : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
        m_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid : OUT STD_LOGIC;
        m_axi_awready : IN STD_LOGIC;
        m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast : OUT STD_LOGIC;
        m_axi_wvalid : OUT STD_LOGIC;
        m_axi_wready : IN STD_LOGIC;
        m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid : IN STD_LOGIC;
        m_axi_bready : OUT STD_LOGIC;
        m_axi_araddr : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
        m_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid : OUT STD_LOGIC;
        m_axi_arready : IN STD_LOGIC;
        m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast : IN STD_LOGIC;
        m_axi_rvalid : IN STD_LOGIC;
        m_axi_rready : OUT STD_LOGIC);
    end component;
       
    
    signal BFmemRst   : std_logic;
    signal BFmemClk   : std_logic;
    signal BFmemEn    : std_logic;
    signal BFmemWrEn  : std_logic_vector(3 downto 0);
    signal BFmemAddr  : std_logic_vector(17 downto 0);
    signal BFmemDin, BFmemDout   : std_logic_vector(31 downto 0);
    
    signal MACE_rstn : std_logic;
    
    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;    
    signal awlock_slv : std_logic_vector(0 downto 0);
    signal arlock_slv : std_logic_vector(0 downto 0);
    
    signal axi_mosi_arlock : std_logic_vector(0 downto 0);
    signal axi_mosi_awlock : std_logic_vector(0 downto 0);
    signal highbits : std_logic_vector(1 downto 0) := "00";
    
    signal MACE_rstn_BFclk : std_logic;
    signal rst_BFclk : std_logic;
    
    signal Jones_status : t_slv_2_arr(3 downto 0);
    signal poly_ok : t_slv_2_arr(3 downto 0);
    signal regWrEn : std_logic;
    
begin

    MACE_rstn <= not i_MACE_rst;

    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => rst_BFclk,  -- 1-bit output
        dest_clk => i_BF_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',
        src_clk => i_MACE_clk,    -- 1-bit input: Source clock.
        src_pulse => i_MACE_rst,  -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            MACE_rstn_BFclk <= not rst_BFclk;
        end if;
    end process;
    
    awlock_slv(0) <= i_axi_mosi.awlock;
    arlock_slv(0) <= i_axi_mosi.arlock;
    

    BF_cci : axi_clock_converter_BF
    port map (
        s_axi_aclk    => i_MACE_clk, -- IN STD_LOGIC;
        s_axi_aresetn => MACE_rstn, -- IN STD_LOGIC;
        s_axi_awaddr    => i_axi_mosi.awaddr(18 downto 0),
        s_axi_awlen     => i_axi_mosi.awlen,
        s_axi_awsize    => i_axi_mosi.awsize,
        s_axi_awburst   => i_axi_mosi.awburst,
        s_axi_awlock    => awlock_slv,
        s_axi_awcache   => i_axi_mosi.awcache,
        s_axi_awprot    => i_axi_mosi.awprot,
        s_axi_awregion => (others => '0'), -- in(3:0);
        s_axi_awqos    => (others => '0'), -- in(3:0);
        s_axi_awvalid   => i_axi_mosi.awvalid,
        s_axi_awready   => o_axi_miso.awready,        
        
        s_axi_wdata     => i_axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => i_axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => i_axi_mosi.wlast,
        s_axi_wvalid    => i_axi_mosi.wvalid,
        s_axi_wready    => o_axi_miso.wready,
        
        s_axi_bresp     => o_axi_miso.bresp,
        s_axi_bvalid    => o_axi_miso.bvalid,
        s_axi_bready    => i_axi_mosi.bready ,

        s_axi_araddr    => i_axi_mosi.araddr(18 downto 0),
        s_axi_arlen     => i_axi_mosi.arlen,
        s_axi_arsize    => i_axi_mosi.arsize,
        s_axi_arburst   => i_axi_mosi.arburst,
        s_axi_arlock    => arlock_slv,
        s_axi_arcache   => i_axi_mosi.arcache,
        s_axi_arprot    => i_axi_mosi.arprot,
        s_axi_arregion  => "0000", -- in(3:0),
        s_axi_arqos     => "0000", -- in(3:0),
        s_axi_arvalid   => i_axi_mosi.arvalid,
        s_axi_arready   => o_axi_miso.arready,
          
        s_axi_rdata     => o_axi_miso.rdata(31 downto 0),
        s_axi_rresp     => o_axi_miso.rresp,
        s_axi_rlast     => o_axi_miso.rlast,
        s_axi_rvalid    => o_axi_miso.rvalid,
        s_axi_rready    => i_axi_mosi.rready,
        -- master interface

        m_axi_aclk    => i_BF_clk, -- in std_logic;
        m_axi_aresetn => MACE_rstn_BFclk, -- in std_logic;
        m_axi_awaddr  => axi_mosi.awaddr(18 downto 0), -- out STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_awlen   => axi_mosi.awlen, -- out STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize  => axi_mosi.awsize, -- out STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst => axi_mosi.awburst, -- out STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock  => axi_mosi_awlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache => axi_mosi.awcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot  => axi_mosi.awprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion => axi_mosi.awregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos   => axi_mosi.awqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid => axi_mosi.awvalid, -- OUT STD_LOGIC;
        m_axi_awready => axi_miso.awready, -- IN STD_LOGIC;
        m_axi_wdata  => axi_mosi.wdata(31 downto 0), -- OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb  => axi_mosi.wstrb(3 downto 0), -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast  => axi_mosi.wlast, -- OUT STD_LOGIC;
        m_axi_wvalid => axi_mosi.wvalid, -- OUT STD_LOGIC;
        m_axi_wready => axi_miso.wready, -- IN STD_LOGIC;
        m_axi_bresp  => axi_miso.bresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid => axi_miso.bvalid, -- IN STD_LOGIC;
        m_axi_bready => axi_mosi.bready, -- OUT STD_LOGIC;
        m_axi_araddr => axi_mosi.araddr(18 downto 0), -- OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_arlen  => axi_mosi.arlen, -- OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize => axi_mosi.arsize, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst => axi_mosi.arburst, -- OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock  => axi_mosi_arlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache => axi_mosi.arcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot  => axi_mosi.arprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion => axi_mosi.arregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos => axi_mosi.arqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid => axi_mosi.arvalid, -- OUT STD_LOGIC;
        m_axi_arready => axi_miso.arready, -- IN STD_LOGIC;
        m_axi_rdata   => axi_miso.rdata(31 downto 0), -- IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp   => axi_miso.rresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast   => axi_miso.rlast, -- IN STD_LOGIC;
        m_axi_rvalid  => axi_miso.rvalid, -- IN STD_LOGIC;
        m_axi_rready => axi_mosi.rready  -- OUT STD_LOGIC
    );

    axi_mosi.awlock <= axi_mosi_awlock(0);
    axi_mosi.arlock <= axi_mosi_arlock(0);
    
    -- Convert register interface from AXI full to address + data
    BF_ctrli : axi_bram_ctrl_BF
    port map (
        s_axi_aclk      => i_BF_clk,
        s_axi_aresetn   => MACE_rstn_BFclk, -- in std_logic;
        s_axi_awaddr    => axi_mosi.awaddr(17 downto 0),
        s_axi_awlen     => axi_mosi.awlen,
        s_axi_awsize    => axi_mosi.awsize,
        s_axi_awburst   => axi_mosi.awburst,
        s_axi_awlock    => axi_mosi.awlock ,
        s_axi_awcache   => axi_mosi.awcache,
        s_axi_awprot    => axi_mosi.awprot,
        s_axi_awvalid   => axi_mosi.awvalid,
        s_axi_awready   => axi_miso.awready,
        s_axi_wdata     => axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => axi_mosi.wlast,
        s_axi_wvalid    => axi_mosi.wvalid,
        s_axi_wready    => axi_miso.wready,
        s_axi_bresp     => axi_miso.bresp,
        s_axi_bvalid    => axi_miso.bvalid,
        s_axi_bready    => axi_mosi.bready ,
        s_axi_araddr    => axi_mosi.araddr(17 downto 0),
        s_axi_arlen     => axi_mosi.arlen,
        s_axi_arsize    => axi_mosi.arsize,
        s_axi_arburst   => axi_mosi.arburst,
        s_axi_arlock    => axi_mosi.arlock ,
        s_axi_arcache   => axi_mosi.arcache,
        s_axi_arprot    => axi_mosi.arprot,
        s_axi_arvalid   => axi_mosi.arvalid,
        s_axi_arready   => axi_miso.arready,
        s_axi_rdata     => axi_miso.rdata(31 downto 0),
        s_axi_rresp     => axi_miso.rresp,
        s_axi_rlast     => axi_miso.rlast,
        s_axi_rvalid    => axi_miso.rvalid,
        s_axi_rready    => axi_mosi.rready,
        bram_rst_a      => BFmemRst,   -- out std_logic;
        bram_clk_a      => BFmemClk,   -- out std_logic;
        bram_en_a       => BFmemEn,     -- out std_logic;
        bram_we_a       => BFmemWrEn,   -- out (3:0)
        bram_addr_a     => BFmemAddr,  -- out (19:0)
        bram_wrdata_a   => BFmemDin,   -- out (31:0)
        bram_rddata_a   => BFmemDout   -- in (31:0)
    );

    -- First stage of the daisy chained register interface

    regWrEn <= BFmemWrEn(0) and BFmemEn;  -- BFmemWrEn is 4 bits wide, one bit per byte; This assumes that all writes are 4 bytes wide. 
        
    -- ultraRAM blocks for the memory  
    urams : entity bf_lib.PSSBF_uram_wrapper
    port map(
        i_clk => i_BF_clk, --  in std_logic;
        -- Port A. Both reads and writes are 32 bits wide.
        i_addrA => BFmemAddr(15 downto 0), -- in std_logic_vector(15 downto 0); -- byte address
        i_dataA => BFmemDin,  -- in std_logic_vector(31 downto 0); -- 
        o_dataA => BFmemDout, -- out std_logic_vector(31 downto 0); -- 3 cycle latency
        i_wrEnA  => regWrEn, -- in std_logic;
        -- Port B. Reads are 64 bits wide
        i_addrB => i_rd_addr(15 downto 0), -- in std_logic_vector(15 downto 0); -- byte address
        o_dataB => o_rd_data  -- out std_ogic_vector(63 downto 0); -- 3 cycle latency
    );
    
            
end Behavioral;


