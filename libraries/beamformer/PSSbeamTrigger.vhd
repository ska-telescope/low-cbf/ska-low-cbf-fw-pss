----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08.11.2020 09:04:05
-- Module Name: PSSbeamformer - Behavioral
-- Description: 
--
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSSbeamTrigger is
    generic(
        -- Where in the daisy chain this module sits.
        g_DAISY_CHAIN : integer;
        -- The number of clocks to output a packet is 864 (=(54 fine channels)*(16 times))
        -- g_PACKET_GAP is the gap from one packet output to the next, so 960 allows for 960-864 = 96 clocks idle time between packets
        g_PACKET_GAP : natural range 0 to 1048575
    );
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        ---------------------------------------------------------------------------
        -- configuration
        -- Number of beams enabled in this module (up to 4)
        i_beamsEnabled : in std_logic_vector(2 downto 0); 
        -- pulse high to indicate data is available to read out
        i_readStart : in std_logic;
        -- Sequence of up to 4 pulses to trigger reading of a frame
        o_frameSend : out std_logic;
        -- Which of the 4 possible frames to send (0, 1, 2 or 3)
        o_frameCount : out std_logic_vector(1 downto 0)
    );
end PSSbeamTrigger;

architecture Behavioral of PSSbeamTrigger is
    
    -- Time to wait before sending the first packet; each module ahead of this module in the daisy chain
    -- needs to send 4 packets, so total time required is (4 * g_PACKET_GAP) per module.
    constant c_PACKET_OFFSET : integer := g_DAISY_CHAIN * 4 * g_PACKET_GAP;
    signal packetGap : std_logic_vector(15 downto 0);
    signal packetOffset : std_logic_vector(15 downto 0);
    signal waitCount : std_logic_vector(19 downto 0) := (others => '0');
    signal frameCount, frameCountSend : std_logic_vector(1 downto 0) := (others => '0');
    signal output_enabled : std_logic;
    signal running : std_logic := '0';
    signal frameSend : std_logic := '0';
    signal lastBeam : std_logic_vector(2 downto 0);
    
begin
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            
            -- Timing of transmission of the output packets
            -- ----------------------------------------
            --
            -- Output packets are 864 words long, made up of (54 fine channels) * (16 time samples)
            --   - Each output word is dual-pol complex data, so (2 pol)*(2 complex)*(16 bits) = 64 bits for 16-bit values.
            -- 
            -- We allow 900 clocks between packets to have some margin for pipeline delays.
            -- 
            -- The beamformers take turns to output packets, and each beamformer instance supports 4 beams.
            --
            -- So this beamformer instance needs to:
            --  (1) Wait c_PACKET_OFFSET
            --  (2) Output a packet (first beam)
            --  (3) Wait g_PACKET_GAP
            --  (4) Repeat (2) and (3) until all i_beamsEnabled packets have been sent (up to 4 of them).
            --
          
            if unsigned(i_beamsEnabled) /= "000" then
                output_enabled <= '1';
            else
                output_enabled <= '0';
            end if;
            lastBeam <= std_logic_vector(unsigned(i_beamsEnabled) - 1);
            
            if i_readStart = '1' and output_enabled = '1' then
                waitCount <= std_logic_vector(to_unsigned(c_PACKET_OFFSET,20));
                frameCount <= (others => '0');
                running <= '1';
                frameSend <= '0';
            elsif running = '1' then
                if unsigned(waitCount) = 0 then
                    frameSend <= '1';
                    frameCountSend <= frameCount;
                    if frameCount = lastBeam(1 downto 0) then
                        frameCount <= (others => '0');
                        waitCount <= (others => '0');
                        running <= '0';
                    else
                        frameCount <= std_logic_vector(unsigned(frameCount) + 1);
                        waitCount <= std_logic_vector(to_unsigned(g_PACKET_GAP,20));
                    end if;
                else
                    frameSend <= '0';
                    waitCount <= std_logic_vector(unsigned(waitCount) - 1);
                end if;
            else
                frameSend <= '0';
            end if;
            
        end if;
    end process;
    
    o_frameCount <= frameCountSend;
    o_frameSend <= frameSend;
    
end Behavioral;

