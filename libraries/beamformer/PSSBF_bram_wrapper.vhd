----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 08.11.2020 12:27:48
-- Design Name: 
-- Module Name: PSSBF_Bram_wrapper - Behavioral
-- Description: 
--   Wrapper to instantiate a BRAM, with 2 ports, 32 bit wide writes, 16 bit wide reads.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library xpm;
use xpm.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PSSBF_Bram_wrapper is
    port(
        i_clk : in std_logic;
        -- Port A. Both reads and writes are 32 bits wide.
        i_addrA : in std_logic_vector(11 downto 0); -- byte address
        i_dataA : in std_logic_vector(31 downto 0); -- 
        o_dataA : out std_logic_vector(31 downto 0); -- 3 cycle latency
        i_wrEnA  : in std_logic;
        -- Port B. Reads are 16 bits wide
        i_addrB : in std_logic_vector(11 downto 0); -- byte address
        o_dataB : out std_logic_vector(15 downto 0) -- 3 cycle latency
    );
end PSSBF_Bram_wrapper;

architecture Behavioral of PSSBF_Bram_wrapper is

    signal wea : std_logic_vector(0 downto 0);
    signal web : std_logic_vector(0 downto 0);
    signal dina, dinb : std_logic_vector(31 downto 0);
    signal addrA, addrB : std_logic_vector(9 downto 0);
    signal dataB : std_logic_vector(31 downto 0);
    signal addrBDel1, addrBDel2, addrBbit1Del : std_logic;
    
begin
    
    o_dataB <= dataB(15 downto 0) when addrBDel2 = '0' else dataB(31 downto 16);
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            dina <= i_dataA;
            wea(0) <= i_wrEnA;
            addrA <= i_addrA(11 downto 2);  -- byte address -> 4-byte word address.
            
            addrB <= i_addrB(11 downto 2);
            addrBbit1Del <= i_addrB(1);
            
            addrBDel1 <= addrBbit1Del;
            addrBDel2 <= addrBDel1;
            
        end if;
    end process;
    
    web(0) <= '0';
    dinb <= (others => '0');
    
    xpm_memory_tdpram_inst : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 10,              -- DECIMAL
        ADDR_WIDTH_B => 10,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 32,        -- DECIMAL
        BYTE_WRITE_WIDTH_B => 32,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "block",     -- String
        MEMORY_SIZE => 32768,            -- DECIMAL  -- Total bits in the memory; 1024 * 32 = 32768 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 32,         -- DECIMAL
        READ_DATA_WIDTH_B => 32,         -- DECIMAL
        READ_LATENCY_A => 2,             -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 32,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 32,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change"      -- String
    )
    port map (
        dbiterra => open,       -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port A.
        dbiterrb => open,       -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port A.
        douta => o_dataA,       -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => dataB,         -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,       -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port A.
        sbiterrb => open,       -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra => addra,         -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => addrb,         -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_clk,          -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb => i_clk,          -- Unused when parameter CLOCKING_MODE is "common_clock".
        dina => dina,           -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => dinb,           -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',             -- 1-bit input: Memory enable signal for port A.
        enb => '1',             -- 1-bit input: Memory enable signal for port B.
        injectdbiterra => '0',  -- 1-bit input: Controls double bit error injection on input data
        injectdbiterrb => '0',  -- 1-bit input: Controls double bit error injection on input data 
        injectsbiterra => '0',  -- 1-bit input: Controls single bit error injection on input data
        injectsbiterrb => '0',  -- 1-bit input: Controls single bit error injection on input data 
        regcea => '1',          -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',          -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',            -- 1-bit input: Reset signal for the final port A output register
        rstb => '0',            -- 1-bit input: Reset signal for the final port B output register
        sleep => '0',           -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => wea,    -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector
                     -- for port A input data port dina. In byte-wide write configurations, each bit controls the
                     -- writing one byte of dina to address addra. For example,
                     -- to synchronously write only bits [15-8] of dina when WRITE_DATA_WIDTH_A is 32, wea would be 4'b0010.
        web => web
   );



end Behavioral;
