----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 4 July 2024
-- Module Name: PSSbeamformerTop - Behavioral
-- Description: 
--   Top level for the PSS beamformer
--
-- Registers:
--   The registers are used for the station Jones corrections and beam weights.
--   The 
--   This module supports up to 64 beams via the g_PSS_BEAMS generic.
--   So 4Mbytes of address space is assumed here (64 * 64Kbytes = 4 MBytes). 4Mbytes = 22 address bits.
-- 
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib, bf_lib, xpm, scaling_lib;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;


entity PSSbeamformerTop is
    generic (
        g_DEBUG_ILA         : BOOLEAN := FALSE;
        g_PIPE_INSTANCE     : integer := 0;
        -- The number of beams supported is 16x g_BEAMFORMER_DAISYCHAIN_DEPTH
        -- (There are 4 parallel pipelines, with 4 beams in each instance that is instantiated)
        g_BEAMFORMER_DAISYCHAIN_DEPTH : integer := 1;
        g_ENABLE_BF_LOGIC   : BOOLEAN := FALSE;
        -- The number of clocks to output a packet is 864 (=(54 fine channels)*(16 times))
        -- g_PACKET_GAP is the gap from one packet output to the next, so 960 allows for 960-864 = 96 clocks idle time between packets
        g_PACKET_GAP        : integer
    );
    port(
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        -- Station Jones registers
        i_axi_mosi_sj : in  t_axi4_full_mosi;
        o_axi_miso_sj : out t_axi4_full_miso;
        -- Beam Jones registers
        i_axi_mosi_bj : in  t_axi4_full_mosi;
        o_axi_miso_bj : out t_axi4_full_miso;
        ----------------------------------------------------------------------
        -- Beamformer data from the corner turn
        i_BF_clk  : in std_logic;
        i_BF_rst  : in std_logic;
        i_data    : in std_logic_vector(127 downto 0); -- 2 stations delivered every clock.
        i_flagged : in std_logic_vector(1 downto 0);   -- aligns with i_data, indicates if either of the stations in i_data are flagged as RFI.
        i_fine    : in std_logic_vector(7 downto 0);   -- fine channel, 0 to 53
        i_coarse  : in std_logic_vector(9 downto 0);   -- index of the coarse channel.
        i_stations01 : in std_logic;  -- Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
        i_firstStation : in std_logic;                -- First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation  : in std_logic;
        i_timeStep     : in std_logic_vector(3 downto 0); -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        i_station      : in std_logic_vector(9 downto 0); -- Station
        i_virtualChannel : in std_logic_vector(9 downto 0); -- note this takes steps of 4, associated with the 4 stations delivered in a burst of 2 clocks.
        -- The PSS output packet count for this packet, based on the original packet count from LFAA.
        -- Each PSS output packet is 16 time samples = 16 * 69.12us = 1.105920 ms of data. There are 48 PSS output packets per corner turn frame.
        i_packetCount  : in std_logic_vector(47 downto 0);
        i_outputPktOdd : in std_logic;
        i_valid   : in std_logic;
        ----------------------------------------------------------------------
        -- polynomial data (4 parallel streams)
        i_phase_virtualChannel : in t_slv_10_arr(3 downto 0);
        i_phase_timeStep : in t_slv_10_arr(3 downto 0);
        i_phase_beam  : in t_slv_7_arr(3 downto 0);
        i_phase       : in t_slv_24_arr(3 downto 0);      -- Phase at the start of the coarse channel.
        i_phase_step  : in t_slv_24_arr(3 downto 0); -- Phase step per fine channel.
        i_phase_valid : in std_logic;
        i_phase_clear : in std_logic;    -- clear registers that hold the current phase to prevent old values being used. This should be set once at the start of each corner turn frame 
        -----------------------------------------------------------------------
        -- Other data from the corner turn
        i_station_jones_buffer : in std_logic;
        i_station_jones_status : in std_logic_vector(1 downto 0);  -- bit 0 = used default, bit 1 = jones valid;
        i_beam_jones_buffer : in std_logic;
        i_beam_jones_status : in std_logic_vector(1 downto 0);
        i_poly_ok      : in std_logic_vector(1 downto 0);  -- The polynomials used are within their valid time range;
        i_beamsEnabled : in std_logic_vector(9 downto 0);  -- Number of beams which are enabled. 
        i_scale_float  : in std_logic_vector(31 downto 0); -- Floating point scale factor as configured from ARGs.
        i_beamTestEnable : in std_logic;                   -- Enable beam test mode.
        -----------------------------------------------------------------------
        -- 4x 64 bit bus out to the 100GE Packetiser
        o_BFdata         : out t_slv_64_arr(3 downto 0);
        o_BFScale        : out t_slv_4_arr(3 downto 0);
        o_BFpacketCount  : out t_slv_48_arr(3 downto 0);
        o_BFBeam         : out t_slv_10_arr(3 downto 0);
        o_BFFreqIndex    : out t_slv_11_arr(3 downto 0);
        o_BFvalid        : out std_logic_vector(3 downto 0);
        o_BFjones_status : out std_logic_vector(3 downto 0);
        o_BFpoly_ok      : out std_logic_vector(1 downto 0);
        -- Weights bus out to the packetiser, common across all o_BFdata buses
        o_weights        : out std_logic_vector(15 downto 0);
        o_weights_valid  : out std_logic;
        o_weights_sop    : out std_logic;
        -- Debug signals
        i_badPacket : in std_logic
    );
end PSSbeamformerTop;

architecture Behavioral of PSSbeamformerTop is
    
    COMPONENT ila_pst
    PORT (
   	    clk : IN STD_LOGIC;
   	    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;    
    
    
    signal MACE_rstn : std_logic;
    
    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;    
    signal awlock_slv : std_logic_vector(0 downto 0);
    signal arlock_slv : std_logic_vector(0 downto 0);
    
    signal MACE_rstn_BFclk : std_logic;
    signal rst_BFclk : std_logic;
    
    type t_1 is array(3 downto 0) of std_logic_vector(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_2 is array(3 downto 0) of t_slv_2_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_3 is array(3 downto 0) of t_slv_3_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_4 is array(3 downto 0) of t_slv_4_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_6 is array(3 downto 0) of t_slv_6_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_7 is array(3 downto 0) of t_slv_7_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_8 is array(3 downto 0) of t_slv_8_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_10 is array(3 downto 0) of t_slv_10_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_11 is array(3 downto 0) of t_slv_11_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_24 is array(3 downto 0) of t_slv_24_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_64 is array(3 downto 0) of t_slv_64_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_40 is array(3 downto 0) of t_slv_40_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_48 is array(3 downto 0) of t_slv_48_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    type t_128 is array(3 downto 0) of t_slv_128_arr(g_BEAMFORMER_DAISYCHAIN_DEPTH downto 0);
    signal badPacket : t_1;
    signal phase_virtualChannel : t_10;
    signal phase_timeStep : t_10;
    signal phase_beam : t_8;
    signal phase : t_24;
    signal phase_step : t_24;
    signal phase_valid, phase_clear : t_1;
    
    signal PData : t_64;
    signal Pscale : t_4;
    signal PvirtualChannel : t_10;
    signal PpacketCount : t_48;
    signal PBeam : t_10;
    signal PFreqIndex : t_11;
    signal Pvalid : t_1;
    
    signal BFdata : t_128;
    signal BFfine : t_6;
    signal BFCoarse : t_10;
    signal BFStations01 : t_1;
    signal BFfirstStation : t_1;
    signal BFlastStation : t_1;
    signal BFstation : t_10;
    signal BFpacketCount : t_48;
    signal BFvalid, BFpktOdd, BFBeamTestEnable : t_1;
    signal BFtimeStep : t_4;
    
    signal beamsEnabled : t_10;
    signal Jones_status : t_slv_4_arr(3 downto 0);
    signal poly_ok : t_slv_2_arr(3 downto 0);

    signal sj_data : std_logic_vector(127 downto 0); -- 2 stations delivered every clock.
    signal sj_fine : std_logic_vector(7 downto 0);   -- fine channel, 0 to 53
    signal sj_coarse : std_logic_vector(9 downto 0); -- index of the coarse channel.
    signal sj_stations01 : std_logic; -- Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
    signal sj_firstStation : std_logic; -- First station (used to trigger a new accumulator cycle in the beamformers).
    signal sj_lastStation : std_logic;
    signal sj_timestep : std_logic_vector(3 downto 0); -- Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
    signal sj_station : std_logic_vector(9 downto 0); -- Station
    signal sj_packetCount : std_logic_vector(47 downto 0);
    signal sj_outputPktOdd : std_logic;
    signal sj_valid : std_logic;
    signal BFpacketCount_out_final : std_logic_vector(47 downto 0);
    signal Pdata_final : t_slv_64_arr(3 downto 0);
    signal PBeam_final : t_slv_10_arr(3 downto 0);
    signal BFFreqIndex_out_final : std_logic_vector(10 downto 0);
    signal Pscale_final : t_slv_4_arr(3 downto 0);
    signal BFvalid_out_final : std_logic;
    signal Pvalid_slv : std_logic_vector(3 downto 0);
    
begin
    
    ---------------------------------------------------
    
    sji : entity bf_lib.station_jones
    port map(
        i_clk         => i_BF_clk, -- in std_logic;
        i_jonesBuffer => i_station_jones_buffer, --  in std_logic;
        ----------------------------------------------------------------------
        -- Beamformer data from the corner turn
        -- i_data, i_flagged have an 8-cycle latency relative to the control signals (i_fine, i_coarse etc).
        i_data    => i_data, -- in (127:0);  2 stations delivered every clock,
        i_flagged => i_flagged, -- in (1:0); aligns with i_data, indicates if either of the stations in i_data are flagged as RFI.
        --
        i_fine    => i_fine,  --  in (7:0); fine channel, 0 to 53
        i_coarse  => i_coarse, -- in (9:0); index of the coarse channel.
        i_stations01 => i_stations01, -- in std_logic; Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
        i_firstStation => i_firstStation, -- in std_logic; First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation  => i_lastStation,  -- in std_logic;
        i_timeStep     => i_timeStep,     -- in (3:0);  Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        i_station      => i_station,      -- in (9:0); -- Station
        i_virtualChannel => i_virtualChannel, -- in (9:0); -- note this takes steps of 4, associated with the 4 stations delivered in a burst of 2 clocks.
        -- The PSS output packet count for this packet, based on the original packet count from LFAA.
        -- Each PSS output packet is 16 time samples = 16 * 69.12us = 1.105920 ms of data. There are 48 PSS output packets per corner turn frame.
        i_packetCount  => i_packetCount,  -- in (47:0);
        i_outputPktOdd => i_outputPktOdd, -- in std_logic;
        i_valid   => i_valid,             -- in std_logic;
        -----------------------------------------------------------------------
        -- Data output
        o_data    => sj_data, -- out (127:0); -- 2 stations delivered every clock.
        o_fine    => sj_fine, -- out (7:0);   -- fine channel, 0 to 53
        o_coarse  => sj_coarse, -- out (9:0); -- index of the coarse channel.
        o_stations01 => sj_stations01, -- out std_logic; Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
        o_firstStation => sj_firstStation, -- out std_logic; First station (used to trigger a new accumulator cycle in the beamformers).
        o_lastStation  => sj_lastStation,  -- out std_logic;
        o_timeStep     => sj_timestep,     -- out (3:0); Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        o_station      => sj_station,      -- out (9:0); -- Station
        o_packetCount  => sj_packetCount, -- out (47:0);
        o_outputPktOdd => sj_outputPktOdd, -- out std_logic;
        o_valid        => sj_valid,        -- out std_logic;
        --------------------------------------------------------------------------
        -- Weights output
        -- burst of 
        o_weights       => o_weights,       -- out (15:0);
        o_weights_valid => o_weights_valid, -- out std_logic;
        o_weights_sop   => o_weights_sop,
        --------------------------------------------------------------------------
        -- Register interface
        -- Station Jones registers
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk => i_MACE_clk, -- in std_logic;
        i_MACE_rst => i_MACE_rst, -- in std_logic;
        i_axi_mosi_sj => i_axi_mosi_sj, -- in  t_axi4_full_mosi;
        o_axi_miso_sj => o_axi_miso_sj  -- out t_axi4_full_miso
    );
    --NOTE:
    -- 5 clock latency for the phase information is needed to match the data latency through the station jones module
    -- This is done in CT2, where the trigger to read from the FIFOs holding the phase information is delayed by 5 clocks.
    
    
    
    ---------------------------------------------------
    
    MACE_rstn <= not i_MACE_rst;
    
    -- Instantiate one module for each beam we are making
    beamPipeGen : for pipe in 0 to 3 generate  -- 4 parallel pipelines
    
        -- start of the station data pipeline
        BFdata(pipe)(0) <= sj_data;  -- in (127:0);  2 stations delivered every clock
        BFfine(pipe)(0) <= sj_fine(5 downto 0);  -- in (5:0); fine channel, 0 to 53
        BFCoarse(pipe)(0) <= sj_coarse;   -- in(9:0)
        BFStations01(pipe)(0) <= sj_stations01;
        BFfirstStation(pipe)(0) <= sj_firstStation;  -- in std_logic; First station in a burst.
        BFlastStation(pipe)(0) <= sj_lastStation;    -- in std_logic; Last station
        BFtimeStep(pipe)(0) <= sj_timeStep;       -- in (3:0); Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        BFstation(pipe)(0) <= sj_station;         -- in (9:0); Station
        BFpacketCount(pipe)(0) <= sj_packetCount; -- in (47:0); The output packet count for this packet, in units of 6.63552 ms since the SKA epoch (Each PST packet is 6.63552 ms)
        BFpktOdd(pipe)(0) <= sj_outputPktOdd;     -- in std_logic;
        BFvalid(pipe)(0) <= sj_valid;
        BFBeamTestEnable(pipe)(0) <= i_beamTestEnable;   
        
        -- start of the phase information pipeline
        phase_virtualChannel(pipe)(0) <= i_phase_virtualChannel(pipe);  -- in t_slv_10_arr(3:0);
        phase_timeStep(pipe)(0) <= i_phase_timeStep(pipe);  -- in t_slv_10_arr(3:0);
        phase_beam(pipe)(0) <= '0' & i_phase_beam(pipe);    -- in (7:0);
        phase(pipe)(0) <= i_phase(pipe);             -- in (23:0); Phase at the start of the coarse channel.
        phase_step(pipe)(0) <= i_phase_step(pipe);   -- in (23:0); Phase step per fine channel.
        phase_valid(pipe)(0) <= i_phase_valid; -- in std_logic;
        phase_clear(pipe)(0) <= i_phase_clear; -- in std_logic;
        
        -- First stage of the beamformed data pipeline 
        Pdata(pipe)(0) <= (others => '0');        -- (63:0);
        Pscale(pipe)(0) <= (others => '0');
        PpacketCount(pipe)(0) <= (others => '0'); -- in (47:0);
        PBeam(pipe)(0) <= (others => '0');        -- in (9:0); 
        PFreqIndex(pipe)(0) <= (others => '0'); 
        Pvalid(pipe)(0) <= '0';
        
        beamsEnabled(pipe)(0) <= i_beamsEnabled;
        badPacket(pipe)(0) <= i_badPacket;
    
        beamGen : for i in 0 to (g_BEAMFORMER_DAISYCHAIN_DEPTH-1) generate  -- generic-configurable number of beamformer modules per pipeline
        
            bfinst: entity bf_lib.PSSbeamformer
            generic map (
                g_PIPE => pipe, -- integer
                g_DAISY_CHAIN => i,    -- integer
                -- Number of clock cycles to wait between start of output packets
                g_PACKET_GAP => g_PACKET_GAP --  integer
            ) port map(
                i_BF_clk  => i_BF_clk, --  in std_logic; -- 400MHz clock
                -- Data in to the beamformer
                i_data    => BFdata(pipe)(i),    -- in (127:0);  2 stations delivered every clock
                i_flagged => "00", -- in (1:0); unused. Flagging is only used in station jones to generate weights.
                i_fine    => BFfine(pipe)(i),    -- in (5:0); fine channel, 0 to 53
                i_coarse  => BFCoarse(pipe)(i),  -- in(9:0)
                i_stations01 => BFStations01(pipe)(i), -- in std_logic;
                i_firstStation => BFfirstStation(pipe)(i),  -- in std_logic; First station in a burst.
                i_lastStation => BFlastStation(pipe)(i), -- in std_logic; Last station
                i_timeStep    => BFtimeStep(pipe)(i),    -- in (3:0); Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
                i_station     => BFstation(pipe)(i),     -- in (9:0); station
                i_packetCount => BFpacketCount(pipe)(i), -- in (47:0); The output packet count for this packet, in units of 6.63552 ms since the SKA epoch (Each PST packet is 6.63552 ms)
                i_pktOdd  => BFpktOdd(pipe)(i), -- in std_logic;
                i_valid   => BFvalid(pipe)(i), -- in std_logic;
                i_beamTestEnable => BFBeamTestEnable(pipe)(i), -- in std_logic;
                --
                -- Data out to the next beamformer (pipelined version of the data in) (2 pipeline stages in this module)
                o_data    => BFdata(pipe)(i+1),    -- out (95:0); 3 consecutive fine channels delivered every clock; 
                o_flagged => open, -- out (2:0);
                o_fine    => BFfine(pipe)(i+1),    -- out (7:0); 
                o_coarse  => BFCoarse(pipe)(i+1),  -- out(9:0)
                o_firstStation => BFfirstStation(pipe)(i+1), -- out std_logic; pipelined i_firstStation
                o_lastStation  => BFlastStation(pipe)(i+1),  -- out std_logic; pipelined i_lastStation
                o_timeStep     => BFtimeStep(pipe)(i+1),     -- out(4:0); pipelined i_timeStep
                o_station     => BFstation(pipe)(i+1),       -- out (9:0);
                o_packetCount => BFpacketCount(pipe)(i+1),   -- out (39:0); The packet count for this packet, based on the original packet count from LFAA.
                o_pktOdd  => BFpktOdd(pipe)(i+1), -- out std_logic;
                o_valid   => BFvalid(pipe)(i+1),  -- out std_logic;
                o_beamTestEnable => BFBeamTestEnable(pipe)(i+1), -- out std_logic;
                --------------------------------------------------------------------------
                -- Phase data
                -- This is captured in a pair of registers, which form a 2-deep FIFO, so data for the next station or time can be
                -- loaded while the current station/time is being used.
                i_phase_virtualChannel => phase_virtualChannel(pipe)(i), -- in (9:0);
                i_phase_timeStep       => phase_timestep(pipe)(i),       -- in (7:0);
                i_phase_beam           => phase_beam(pipe)(i),           -- in (7:0);
                i_phase                => phase(pipe)(i),                -- in (23:0);  Phase at the start of the coarse channel.
                i_phase_step           => phase_step(pipe)(i),           -- in (23:0);  Phase step per fine channel.
                i_phase_valid          => phase_valid(pipe)(i),          -- in std_logic;
                i_phase_clear          => phase_clear(pipe)(i),          -- in std_logic;
                -- Pass on phase data to the next beamformer.
                o_phase_virtualChannel => phase_virtualChannel(pipe)(i+1), -- out (9:0);
                o_phase_timeStep       => phase_timestep(pipe)(i+1),       -- out (7:0);
                o_phase_beam           => phase_beam(pipe)(i+1),           -- out (7:0);
                o_phase                => phase(pipe)(i+1),                -- out (23:0);  Phase at the start of the coarse channel.
                o_phase_step           => phase_step(pipe)(i+1),           -- out (23:0);  Phase step per fine channel.
                o_phase_valid          => phase_valid(pipe)(i+1),          -- out std_logic;
                o_phase_clear          => phase_clear(pipe)(i+1),          -- out std_logic;
                ---------------------------------------------------------------------
                -- Packets of data from the previous beamformer, to be passed on to the next beamformer.
                i_BFdata        => Pdata(pipe)(i),        -- in(63:0);
                i_BFscale       => Pscale(pipe)(i),       -- in(3:0);
                i_BFpacketCount => PpacketCount(pipe)(i), -- out(47:0);
                i_BFBeam        => PBeam(pipe)(i),        -- in(9:0);  
                i_BFFreqIndex   => PFreqIndex(pipe)(i),   -- in(10:0);
                i_BFvalid       => PValid(pipe)(i),       -- in std_logic;
                -- Packets of data out to the 100G interface
                o_BFdata        => Pdata(pipe)(i+1),        -- out(63:0);
                o_BFscale       => Pscale(pipe)(i+1),       -- out(3:0);
                o_BFpacketCount => PpacketCount(pipe)(i+1), -- out(47:0);
                o_BFFreqIndex   => PFreqIndex(pipe)(i+1),   -- out(10:0);
                o_BFBeam        => PBeam(pipe)(i+1),        -- out(9:0);  
                o_BFvalid       => Pvalid(pipe)(i+1),       -- out std_logic

                ----------------------------------------------------------------------------
                -- Configuration
                i_beamsEnabled => beamsEnabled(pipe)(i), -- in (9:0); -- Number of beams enabled; This beamformer will output data packets if g_BEAM_NUMBER < i_beamsEnabled
                -- pipelined outputs for buffer settings
                o_beamsEnabled => beamsEnabled(pipe)(i+1), -- out (9:0);
                
                -- debug stuff
                i_badPacket     => badPacket(pipe)(i),
                o_badPacket     => badPacket(pipe)(i+1)
            );
        end generate;
        
        
        Pdata_final(pipe) <= Pdata(pipe)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
        Pscale_final(pipe) <= PScale(pipe)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
        PBeam_final(pipe) <= PBeam(pipe)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
        
    end generate;
    
    
    Pvalid_slv(0) <= Pvalid(0)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
    Pvalid_slv(1) <= Pvalid(1)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
    Pvalid_slv(2) <= Pvalid(2)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
    Pvalid_slv(3) <= Pvalid(3)(g_BEAMFORMER_DAISYCHAIN_DEPTH);
    
    beamjonesi : entity bf_lib.beam_jones
    port map(
        i_clk         => i_BF_clk, -- in std_logic;
        i_rst         => i_BF_rst, -- in std_logic;
        i_jonesBuffer => i_beam_jones_buffer, -- in std_logic;
        i_beamsEnabled => i_beamsEnabled, -- in std_logic_vector(9 downto 0); -- Number of beams enabled.
        -- Beam Jones registers
        -- Registers axi full interface, used for Jones matrices for the station correction
        i_MACE_clk => i_MACE_clk, -- in std_logic;
        i_MACE_rst => i_MACE_rst, -- in std_logic;
        i_axi_mosi_bj => i_axi_mosi_bj, -- in  t_axi4_full_mosi;
        o_axi_miso_bj => o_axi_miso_bj, -- out t_axi4_full_miso;
        --
        i_BFdata         => Pdata_final, --  in t_slv_64_arr(3 downto 0);
        i_BFpacketCount  => PPacketCount(0)(g_BEAMFORMER_DAISYCHAIN_DEPTH), -- in std_logic_vector(47 downto 0); -- packetCount is the same for all four parallel inputs
        i_BFBeam         => PBeam_final, -- in t_slv_10_arr(3 downto 0);
        i_BFFreqIndex    => PFreqIndex(0)(g_BEAMFORMER_DAISYCHAIN_DEPTH), -- in std_logic_vector(10 downto 0); -- freqIndex is the same for all four parallel inputs
        i_BFvalid        => Pvalid_slv, -- in (3:0);
	    i_BFscale	     => Pscale_final, -- in t_slv_4_arr(3 downto 0);
        --
        o_BFdata         => o_BFdata,  -- out t_slv_64_arr(3 downto 0);
        o_BFscale	     => o_BFScale, -- out t_slv_4_arr(3 downto 0)
        o_BFpacketCount  => BFpacketCount_out_final, --  out std_logic_vector(47 downto 0);
        o_BFBeam         => o_BFBeam,  -- out t_slv_10_arr(3 downto 0);
        o_BFFreqIndex    => BFFreqIndex_out_final, -- out std_logic_vector(10 downto 0);
        o_BFvalid        => o_BFvalid  --  out std_logic_vector(3 downto 0);
    );
    
    o_BFpacketCount(0) <= BFpacketCount_out_final;
    o_BFpacketCount(1) <= BFpacketCount_out_final;
    o_BFpacketCount(2) <= BFpacketCount_out_final;
    o_BFpacketCount(3) <= BFpacketCount_out_final;
    
    o_BFFreqIndex(0) <= BFFreqIndex_out_final;
    o_BFFreqIndex(1) <= BFFreqIndex_out_final;
    o_BFFreqIndex(2) <= BFFreqIndex_out_final;
    o_BFFreqIndex(3) <= BFFreqIndex_out_final;
    
    -----------------------------------------------------------------------------------------
    
    process(i_BF_clk)
    begin
        if rising_Edge(i_BF_clk) then
            -- Notification of jones and polynomial status, just goes straight to the packetiser.
            -- These are set for the duration of the corner turn (53 ms)
            -- Providing there is a gap between corner turn frames a small delay line is fine 
            -- delay line is just to make timing easy to meet.
            Jones_status(0) <= i_station_jones_status & i_beam_jones_status; -- both 2 bit inputs; bit 0 = used default, bit 1 = jones valid;
            poly_ok(0) <= i_poly_ok;           -- 2 bits, for station and beam polynomials within their valid time range;
            
            jones_status(3 downto 1) <= jones_status(2 downto 0);
            poly_ok(3 downto 1) <= poly_ok(2 downto 0);
            
            o_BFjones_status <= jones_status(3);
            o_BFpoly_ok <= poly_ok(3);
        end if;
    end process;
                
end Behavioral;
