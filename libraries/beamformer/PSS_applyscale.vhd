----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: August 2024
-- Module Name: PSS_scalefactor - Behavioral
-- Description: 
--  Scale down a 32 bit value to a 16 bi value.
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSS_ApplyScale is
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        ---------------------------------------------------------------------------
        -- Data in to the beamformer
        i_data  : in std_logic_vector(31 downto 0);
        i_scale : in std_logic_vector(4 downto 0);
        --
        o_data : out std_logic_vector(15 downto 0)  -- 1 clock latency
    );
end PSS_ApplyScale;

architecture Behavioral of PSS_ApplyScale is

    signal scale0, scale1, scale2, scale3 : std_logic_vector(4 downto 0);
    signal use_scale0, use_scale1, use_scale2 : std_logic;
    
begin
    
    -- Pipelines that go through to the next beamformer
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            case i_scale is
                when "00000" => o_data <= i_data(15 downto 0);
                when "00001" => o_data <= i_data(16 downto 1);
                when "00010" => o_data <= i_data(17 downto 2);
                when "00011" => o_data <= i_data(18 downto 3);
                when "00100" => o_data <= i_data(19 downto 4);
                when "00101" => o_data <= i_data(20 downto 5);
                when "00110" => o_data <= i_data(21 downto 6);
                when "00111" => o_data <= i_data(22 downto 7);
                when "01000" => o_data <= i_data(23 downto 8);
                when "01001" => o_data <= i_data(24 downto 9);
                when "01010" => o_data <= i_data(25 downto 10);
                when "01011" => o_data <= i_data(26 downto 11);
                when "01100" => o_data <= i_data(27 downto 12);
                when "01101" => o_data <= i_data(28 downto 13);
                when "01110" => o_data <= i_data(29 downto 14);
                when "01111" => o_data <= i_data(30 downto 15);
                when others => o_data <= i_data(31 downto 16);
            end case;
        end if;
    end process;
    
end Behavioral;
