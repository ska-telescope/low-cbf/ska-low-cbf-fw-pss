----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08/14/2023 10:14:24 PM
-- Module Name: ct1_tb - Behavioral
-- Description: 
--  Standalone testbench for PSS corner turn 2 + beamformer
-- 
----------------------------------------------------------------------------------

library IEEE, correlator_lib, ct_lib, BF_lib, common_lib, filterbanks_lib, pst_lib, tbmodels_lib, PSR_Packetiser_lib, technology_lib;
use IEEE.STD_LOGIC_1164.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio.all;
USE ct_lib.ct2_reg_pkg.ALL;
USE common_lib.common_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
USE technology_lib.tech_mac_100g_pkg.ALL;
--library DSP_top_lib;
--use DSP_top_lib.DSP_top_pkg.all;

entity file2_axifull is
    generic(
        -- The file should have blocks of (g_AWLEN+1) lines of text
        -- first line in the block is [addr_in_hex] 
        -- then g_AWLEN words of data
        -- e.g. for g_AWLEN = 6, blocks of text like : 
        -- [00000000]
        -- 41200000
        -- 3dcccccd
        -- 00000000
        -- 00000000
        -- 00000000
        -- 00000000
        g_INIT_FILENAME : string; 
        g_AWLEN : integer  -- number of words to write in a burst; 
    );
    port (
        clk300 : in std_logic;
        axi_full_mosi : out t_axi4_full_mosi;
        axi_full_miso : in t_axi4_full_miso;
        o_done : out std_logic
    );
end file2_axifull;

architecture Behavioral of file2_axifull is
    
    
begin
    
    --------------------------------------------------------------------
    -- Programming of axi-full registers
    --
    process
        file polyConfigfile: TEXT;
        variable PolyLine_in : Line;
        variable polyData, polyAddr : std_logic_vector(31 downto 0);
        variable polyGood : boolean;
        variable c2 : character;
    begin
        o_done <= '0';
        axi_full_mosi.awaddr <= (others => '0');
        axi_full_mosi.awvalid <= '0';
        -- Always write bursts of 24 bytes = g_AWLEN x 4-byte words.
        -- Corresponds to one full polynomial.
        axi_full_mosi.awlen <= std_logic_vector(to_unsigned(g_AWLEN - 1,8)); -- e.g. for g_AWLEN=6, will be "00000101";
        axi_full_mosi.wvalid <= '0';
        axi_full_mosi.wdata <= (others => '0');
        axi_full_mosi.wlast <= '0';
        axi_full_mosi.bready <= '1';
        axi_full_mosi.arvalid <= '0';
        axi_full_mosi.awid <= x"00";
        axi_full_mosi.awprot <= "000";
        axi_full_mosi.awsize <= "010";   -- "010" = 32 bit wide data bus
        axi_full_mosi.awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
        axi_full_mosi.awcache <= "0011"; -- "0011" = bufferable transaction
        axi_full_mosi.awuser <= "0000";
        axi_full_mosi.awlock <= '0';
        axi_full_mosi.wid <= "00000000";
        axi_full_mosi.arid <= "00000000";
        axi_full_mosi.araddr <= (others => '0');
        axi_full_mosi.arprot <= "000";
        axi_full_mosi.arlen <= "00000101";
        axi_full_mosi.arsize <= "010";
        axi_full_mosi.arburst <= "01";
        axi_full_mosi.arcache <= "0011";
        axi_full_mosi.aruser <= "0000";
        axi_full_mosi.arlock <= '0';
        axi_full_mosi.rready <= '1';
        axi_full_mosi.awregion <= "0000";
        axi_full_mosi.arregion <= "0000";
        axi_full_mosi.arqos <= "0000";
        axi_full_mosi.awqos <= "0000";
        axi_full_mosi.wstrb <= (others => '1');
        
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        
        if g_INIT_FILENAME'length > 0 then
            -- write configuration data to memory over the axi full interface.
            FILE_OPEN(polyConfigfile, g_INIT_FILENAME, READ_MODE);
            
            while (not endfile(polyConfigfile)) loop
                -- Get data for 1 source (64 bytes = 16 x 4byte words)
                readline(polyConfigFile, polyLine_in);
                -- drop "[" character that indicates the address
                read(polyLine_in,c2,polyGood);
                -- get the address
                hread(polyLine_in,polyAddr,polyGood);
                axi_full_mosi.awaddr(31 downto 0) <= polyAddr;
                axi_full_mosi.awvalid <= '1';
                wait until (rising_edge(clk300) and axi_full_miso.awready='1');
                wait for 1 ps;
                axi_full_mosi.awvalid <= '0';
                for i in 0 to (g_AWLEN-1) loop
                    readline(polyConfigFile, polyLine_in);
                    hread(polyLine_in,polyData,polyGood);
                    axi_full_mosi.wvalid <= '1';
                    axi_full_mosi.wdata(31 downto 0) <= polyData;
                    if i=(g_AWLEN-1) then
                        axi_full_mosi.wlast <= '1';
                    else
                        axi_full_mosi.wlast <= '0';
                    end if;
                    wait until (rising_edge(clk300) and axi_full_miso.wready = '1');
                    wait for 1 ps;
                end loop;
                axi_full_mosi.wvalid <= '0';
                wait until rising_edge(clk300);
                wait until rising_edge(clk300);
            end loop;
            wait until rising_edge(clk300);
            wait for 100 ns;
            wait until rising_edge(clk300);
            
            ---------------------------------------------------------------
            -- read back some data 
            ---------------------------------------------------------------
            axi_full_mosi.arlen <= "00000111";
            axi_full_mosi.araddr(31 downto 0) <= (others => '0');
            axi_full_mosi.arvalid <= '1';
            wait until (rising_edge(clk300) and axi_full_miso.arready='1');
            wait for 1 ps;
            axi_full_mosi.arvalid <= '0';
            
            wait until (rising_edge(clk300) and axi_full_miso.rvalid = '1');
            wait for 200 ns;
            wait until rising_edge(clk300);
    
            ---------------------------------------------------------------
            axi_full_mosi.arlen <= "00000111";
            axi_full_mosi.araddr(31 downto 0) <= x"00000030";
            axi_full_mosi.arvalid <= '1';
            wait until (rising_edge(clk300) and axi_full_miso.arready='1');
            wait for 1 ps;
            axi_full_mosi.arvalid <= '0';
            wait until (rising_edge(clk300) and axi_full_miso.rvalid = '1');
            wait for 200 ns;
            wait until rising_edge(clk300);        
    
            ---------------------------------------------------------------
            axi_full_mosi.arlen <= "00001111";
            axi_full_mosi.araddr(31 downto 0) <= x"00060000";
            axi_full_mosi.arvalid <= '1';
            wait until (rising_edge(clk300) and axi_full_miso.arready='1');
            wait for 1 ps;
            axi_full_mosi.arvalid <= '0';
            wait until (rising_edge(clk300) and axi_full_miso.rvalid = '1');
            wait for 200 ns;
            wait until rising_edge(clk300);         
            ---------------------------------------------------------------
        end if;
        
        wait until rising_edge(clk300);
        o_done <= '1';
        wait;
    end process;
    
    
end Behavioral;
