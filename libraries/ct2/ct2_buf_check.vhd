----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 10 september 2024
-- Module Name: ct2_buf_check - Behavioral
-- Description: 
--  work out which jones buffer to use from register settings and current frame
----------------------------------------------------------------------------------

library IEEE, axi4_lib, common_lib, ct_lib;
use IEEE.STD_LOGIC_1164.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
USE common_lib.common_pkg.ALL;
--library DSP_top_lib;
--use DSP_top_lib.DSP_top_pkg.all;
use ct_lib.ct2_reg_pkg.all;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct2_buf_check is
    Port(
        -- inputs in the registers clock domain
        i_clk : in std_logic;
        i_buf0_valid_frame : in std_logic_vector(36 downto 0); -- reg_rw.jones_buffer0_valid_frame_high(4 downto 0) & reg_rw.jones_buffer0_valid_frame_low(31 downto 0);
        i_buf0_valid_duration : in std_logic_vector(31 downto 0);
        i_buf1_valid_frame : in std_logic_vector(36 downto 0); -- reg_rw.jones_buffer1_valid_frame_high(4 downto 0) & reg_rw.jones_buffer1_valid_frame_low(31 downto 0);
        i_buf1_valid_duration : in std_logic_vector(31 downto 0);
        i_ctframe : in std_logic_vector(36 downto 0); -- readout_ctframe_axi_hold
        i_valid : in std_logic;
        -- Results in the beamformer clock domain, stable when o_valid = '1', which will be after about 6 i_clk clocks 
        i_BF_clk : in std_logic;
        o_valid  : out std_logic;      -- 
        o_buffer : out std_logic;      -- Which buffer to use (0 or 1)
        o_sel_default : out std_logic; -- indicates that valid_duration = xffffffff
        o_buffer_valid : out std_logic -- Selected a valid jones buffer with valid_duration < xffffffff
    );
end ct2_buf_check;

architecture Behavioral of ct2_buf_check is
    
    signal cur_frame_gt_buf0_start, buf_selected : std_logic;
    signal valid_del0, valid_del1, valid_del2, valid_del3, valid_del4, buf0_valid, buf1_valid : std_logic;
    signal cur_frame_gt_buf1_start, selected_buf_valid : std_logic;
    signal buf0_more_recent, buf0_more_recent_del1 : std_logic;
    signal buf0_valid_forever, buf1_valid_forever, buf0_valid_forever_del1, buf1_valid_forever_del1 : std_logic;
    signal ctframe_hold, buf0_end_frame, buf1_end_frame : std_logic_vector(36 downto 0);
    signal buf0_valid_duration, buf1_valid_duration : std_logic_vector(36 downto 0);
    signal use_default : std_logic;
    signal sel_bits, sel_bits_BF_clk : std_logic_vector(3 downto 0);
    
begin
    
    buf0_valid_duration <= "00000" & i_buf0_valid_duration;
    buf1_valid_duration <= "00000" & i_buf1_valid_duration;
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            ----------------------------------------------------------------------
            -- Pipelined calculation of which Jones buffer to use:
            -- 1st pipeline stage
            if (unsigned(i_ctframe) >= unsigned(i_buf0_valid_frame)) then
                cur_frame_gt_buf0_start <= '1';
            else
                cur_frame_gt_buf0_start <= '0';
            end if;
            buf0_end_frame <= std_logic_vector(unsigned(i_buf0_valid_frame) + unsigned(i_buf0_valid_duration));
            
            if (unsigned(i_ctframe) >= unsigned(i_buf1_valid_frame)) then
                cur_frame_gt_buf1_start <= '1';
            else
                cur_frame_gt_buf1_start <= '0';
            end if;
            buf1_end_frame <= std_logic_vector(unsigned(i_buf1_valid_frame) + unsigned(i_buf1_valid_duration));
            
            if i_buf0_valid_duration = x"ffffffff" then
                buf0_valid_forever <= '1';
            else
                buf0_valid_forever <= '0';
            end if;
            
            if i_buf1_valid_duration = x"ffffffff" then
                buf1_valid_forever <= '1';
            else
                buf1_valid_forever <= '0';
            end if;
            
            if unsigned(i_buf0_valid_frame) > unsigned(i_buf1_valid_frame) then
                buf0_more_recent <= '1';
            else
                buf0_more_recent <= '0';
            end if;
            
            valid_del0 <= i_valid;
            
            ctframe_hold <= i_ctframe;
            
            -------------------------------------------------------------------------
            -- 2nd pipeline stage
            if (cur_frame_gt_buf0_start = '1') and (unsigned(ctframe_hold) < unsigned(buf0_end_frame)) then
                buf0_valid <= '1';
            else
                buf0_valid <= '0';
            end if;
            if (cur_frame_gt_buf1_start = '1') and (unsigned(ctframe_hold) < unsigned(buf1_end_frame)) then
                buf1_valid <= '1';
            else
                buf1_valid <= '0';
            end if;
            buf0_more_recent_del1 <= buf0_more_recent;
            buf0_valid_forever_del1 <= buf0_valid_forever;
            buf1_valid_forever_del1 <= buf1_valid_forever;
            valid_del1 <= valid_del0;
            
            --------------------------------------------------------------------------
            -- 3rd pipeline stage
            -- calculates which buffer to use ("sel_jones_buf") 
            -- and also flags that go in the output PST packets 
            -- "jones_valid" : selected a valid jones buffer with validity time < xffffffff
            -- "jones_used_default" : 
            if valid_del1 = '1' then
                if ((buf0_valid = '1' and (buf0_more_recent_del1 = '1' or buf1_valid = '0')) or
                    (buf0_valid_forever_del1 = '1' and buf1_valid = '0')) then
                    buf_selected <= '0'; -- select Jones buffer 0
                    use_default <= buf0_valid_forever_del1;
                    selected_buf_valid <= buf0_valid and (not buf0_valid_forever_del1);
                else
                    buf_selected <= '1'; -- select Jones buffer 1
                    use_default <= buf1_valid_forever_del1;
                    selected_buf_valid <= buf1_valid and (not buf1_valid_forever_del1);
                end if;
            end if;
            valid_del2 <= valid_del1;
            
            --------------------------------------------------------------------------
            -- Pass to the BF clock domain to output to the beamformer.
            -- This should occur well in advance of the first data going to the beamformer, 
            -- because both are triggered by the same thing, and this only has a few clocks latency,
            -- while the beamformer data output has to calculate polynomials and fetch data from the HBM. 
            sel_bits(0) <= buf_selected;
            sel_bits(1) <= use_default;
            sel_bits(2) <= selected_buf_valid;
            valid_del3 <= valid_del2;
            ------------------------------------------------------------------------------------------
            valid_del4 <= valid_del3;
            --Set bit 3 a few clocks later relative to jones_sel_bits, so that the output will be stable by the time this value is set.
            sel_bits(3) <= valid_del4;
            
        end if;
    end process;
    
    
    xpm_cdc_array_single2 : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 4           -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => sel_bits_BF_clk,   -- WIDTH-bit output: src_in synchronized to the destination clock domain.
        dest_clk => i_BF_clk,  -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => i_clk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => sel_bits -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain. 
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            o_Buffer <= sel_bits_BF_clk(0);  -- Which jones buffer to use.
            o_sel_default <= sel_bits_BF_clk(1);  -- Used the defaults; i.e. valid_duration register was xffffffff
            o_buffer_valid <= sel_bits_BF_clk(2); -- Jones matrices are valid
            o_valid <= sel_bits_BF_clk(3); -- The other 3 outputs are valid
        end if;
    end process;
    
end Behavioral;
