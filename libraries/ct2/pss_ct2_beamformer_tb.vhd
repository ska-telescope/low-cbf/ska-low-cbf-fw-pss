----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08/14/2023 10:14:24 PM
-- Module Name: ct1_tb - Behavioral
-- Description: 
--  Standalone testbench for PSS corner turn 2 + beamformer
-- 
----------------------------------------------------------------------------------

library IEEE, correlator_lib, ct_lib, BF_lib, common_lib, filterbanks_lib, scaling_lib, tbmodels_lib, PSR_Packetiser_lib, technology_lib, pss_lib;
use IEEE.STD_LOGIC_1164.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio.all;
USE ct_lib.ct2_reg_pkg.ALL;
USE common_lib.common_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
USE technology_lib.tech_mac_100g_pkg.ALL;
--library DSP_top_lib;
--use DSP_top_lib.DSP_top_pkg.all;

entity pss_ct2_beamformer_tb is
    generic(
        
        -----------------------------------------------------------------------------------------------
        -- test 8 (8 stations, 1 coarse channel)
        -----------------------------------------------------------------------------------------------
--        -- Number of virtual channels to use
--        g_STATIONS : integer := 8;
--        g_COARSE_CHANNELS : integer := 1;
--        g_PACKET_GAP : integer := 960;
--        -- 
--        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
--        -- register configuration
--        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
--        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
--        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
--        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
--        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
--        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
--        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
--        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
--        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
--        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
--        g_TEST_CASE                 : string := "../../../../../../../";    -- get to root of repo.
        
--        g_POLYNOMIAL0_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test8_poly0.txt";
--        g_POLYNOMIAL1_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test8_poly1.txt";
--        g_POLYNOMIAL2_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test8_poly2.txt";
--        g_POLYNOMIAL3_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test8_poly3.txt";
        
--        g_STATION_JONES_INIT_FILENAME : string := ""; -- "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_station_jones.txt";
--        g_BEAM_JONES_INIT_FILENAME    : string := ""; -- "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_beam_jones.txt";
--        g_BF_OUT_FILENAME0          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets0.txt";
--        g_BF_OUT_FILENAME1          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets1.txt";
--        g_BF_OUT_FILENAME2          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets2.txt";
--        g_BF_OUT_FILENAME3          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets3.txt";
--        g_BEAMFORMER_DAISYCHAIN_DEPTH : integer := 2; -- Generic configures number of beamformers instantiated
--        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000002"; -- register setting in CT2
--        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000";   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
--        g_INCLUDE_POST_BF : boolean := false
        -----------------------------------------------------------------------------------------------

        -----------------------------------------------------------------------------------------------
        -- test 8 (8 stations, 1 coarse channel, 11 beams)
        -----------------------------------------------------------------------------------------------
        -- Number of virtual channels to use
--        g_STATIONS : integer := 8;
--        g_COARSE_CHANNELS : integer := 1;
--        g_PACKET_GAP : integer := 960;
--        -- 
--        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
--        -- register configuration
--        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
--        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
--        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
--        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
--        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
--        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
--        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
--        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
--        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
--        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
--        g_TEST_CASE                 : string := "../../../../../../../";    -- get to root of repo.
        
--        g_POLYNOMIAL0_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8station_11beam_poly0.txt";
--        g_POLYNOMIAL1_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8station_11beam_poly1.txt";
--        g_POLYNOMIAL2_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8station_11beam_poly2.txt";
--        g_POLYNOMIAL3_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8station_11beam_poly3.txt";
        
--        g_STATION_JONES_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8station_11beam_station_jones.txt";
--        g_BEAM_JONES_INIT_FILENAME    : string := "libraries/ct2/test_ct2_bf/test_8station_11beam_beam_jones.txt";
--        g_BF_OUT_FILENAME0          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets0.txt";
--        g_BF_OUT_FILENAME1          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets1.txt";
--        g_BF_OUT_FILENAME2          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets2.txt";
--        g_BF_OUT_FILENAME3          : string := "libraries/ct2/test_ct2_bf/test8_pss_packets3.txt";
--        g_BEAMFORMER_DAISYCHAIN_DEPTH : integer := 2; -- Generic configures number of beamformers instantiated
--        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"0000000B"; -- register setting in CT2
--        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000";   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
--        g_INCLUDE_POST_BF : boolean := true


        -----------------------------------------------------------------------------------------------
        -- test 8 (8 stations, 1 coarse channel, 11 beams)
        --  BUT with 2 beams enabled only.
        -----------------------------------------------------------------------------------------------
        -- Number of virtual channels to use
--        g_STATIONS : integer := 4;
--        g_COARSE_CHANNELS : integer := 1;
--        g_PACKET_GAP : integer := 960;
--        g_DATATYPE : integer := 2;
--        -- 
--        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
--        -- register configuration
--        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
--        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
--        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
--        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
--        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
--        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
--        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
--        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
--        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
--        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
--        g_TEST_CASE                 : string := "../../../../../../../";    -- get to root of repo.
        
--        g_POLYNOMIAL0_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_4s_2b_poly0.txt";
--        g_POLYNOMIAL1_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_4s_2b_poly1.txt";
--        g_POLYNOMIAL2_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_4s_2b_poly2.txt";
--        g_POLYNOMIAL3_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_4s_2b_poly3.txt";
        
--        g_STATION_JONES_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_4s_2b_station_jones.txt";
--        g_BEAM_JONES_INIT_FILENAME    : string := "libraries/ct2/test_ct2_bf/test_4s_2b_beam_jones.txt";
--        g_BF_OUT_FILENAME0          : string := "libraries/ct2/test_ct2_bf/test_4s_2b_packets0.txt";
--        g_BF_OUT_FILENAME1          : string := "libraries/ct2/test_ct2_bf/test_4s_2b_packets1.txt";
--        g_BF_OUT_FILENAME2          : string := "libraries/ct2/test_ct2_bf/test_4s_2b_packets2.txt";
--        g_BF_OUT_FILENAME3          : string := "libraries/ct2/test_ct2_bf/test_4s_2b_packets3.txt";
        
--        g_SCALING_OUT_FILENAME0  : string := "libraries/ct2/test_ct2_bf/test_4s_2b_scaled_packets0.txt";
--        g_SCALING_OUT_FILENAME1  : string := "libraries/ct2/test_ct2_bf/test_4s_2b_scaled_packets1.txt";
--        g_SCALING_OUT_FILENAME2  : string := "libraries/ct2/test_ct2_bf/test_4s_2b_scaled_packets2.txt";
--        g_SCALING_OUT_FILENAME3  : string := "libraries/ct2/test_ct2_bf/test_4s_2b_scaled_packets3.txt";
        
--        g_BEAMFORMER_DAISYCHAIN_DEPTH : integer := 1; -- Generic configures number of beamformers instantiated
--        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000002"; -- register setting in CT2
--        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000";   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
--        g_INCLUDE_POST_BF : boolean := true

        -- Number of virtual channels to use
        g_STATIONS : integer := 8;
        g_COARSE_CHANNELS : integer := 1;
        g_PACKET_GAP : integer := 960;
        g_DATATYPE : integer := 0;
        -- 
        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
        -- register configuration
        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
        g_TEST_CASE                 : string := "../../../../../../../";    -- get to root of repo.
        
        g_POLYNOMIAL0_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8s_4b_poly0.txt";
        g_POLYNOMIAL1_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8s_4b_poly1.txt";
        g_POLYNOMIAL2_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8s_4b_poly2.txt";
        g_POLYNOMIAL3_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8s_4b_poly3.txt";
        
        g_STATION_JONES_INIT_FILENAME : string := "libraries/ct2/test_ct2_bf/test_8s_4b_station_jones.txt";
        g_BEAM_JONES_INIT_FILENAME    : string := "libraries/ct2/test_ct2_bf/test_8s_4b_beam_jones.txt";
        g_BF_OUT_FILENAME0          : string := "libraries/ct2/test_ct2_bf/test_8s_4b_packets0.txt";
        g_BF_OUT_FILENAME1          : string := "libraries/ct2/test_ct2_bf/test_8s_4b_packets1.txt";
        g_BF_OUT_FILENAME2          : string := "libraries/ct2/test_ct2_bf/test_8s_4b_packets2.txt";
        g_BF_OUT_FILENAME3          : string := "libraries/ct2/test_ct2_bf/test_8s_4b_packets3.txt";
        
        g_SCALING_OUT_FILENAME0  : string := "libraries/ct2/test_ct2_bf/test_8s_4b_scaled_packets0.txt";
        g_SCALING_OUT_FILENAME1  : string := "libraries/ct2/test_ct2_bf/test_8s_4b_scaled_packets1.txt";
        g_SCALING_OUT_FILENAME2  : string := "libraries/ct2/test_ct2_bf/test_8s_4b_scaled_packets2.txt";
        g_SCALING_OUT_FILENAME3  : string := "libraries/ct2/test_ct2_bf/test_8s_4b_scaled_packets3.txt";
        
        g_BEAMFORMER_DAISYCHAIN_DEPTH : integer := 1; -- Generic configures number of beamformers instantiated
        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000002"; -- register setting in CT2
        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000";   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
        g_INCLUDE_POST_BF : boolean := true

    );
end pss_ct2_beamformer_tb;

architecture Behavioral of pss_ct2_beamformer_tb is

    function get_axi_size(AXI_DATA_WIDTH : integer) return std_logic_vector is
    begin
        if AXI_DATA_WIDTH = 8 then
            return "000";
        elsif AXI_DATA_WIDTH = 16 then
            return "001";
        elsif AXI_DATA_WIDTH = 32 then
            return "010";
        elsif AXI_DATA_WIDTH = 64 then
            return "011";
        elsif AXI_DATA_WIDTH = 128 then
            return "100";
        elsif AXI_DATA_WIDTH = 256 then
            return "101";
        elsif AXI_DATA_WIDTH = 512 then
            return "110";    -- size of 6 indicates 64 bytes in each beat (i.e. 512 bit wide bus) -- out std_logic_vector(2 downto 0);
        elsif AXI_DATA_WIDTH = 1024 then
            return "111";
        else
            assert FALSE report "Bad AXI data width" severity failure;
            return "000";
        end if;
    end get_axi_size;
    constant M02_DATA_WIDTH : integer := 512;
    -- c_minGapData is a bit more than 0x2000 used in CT2, total clocks per CT2 frame needs to be this * beams * coarse channels.
    -- It counts against the 450MHz filterbank clock vs the 400 MHz beamformer clock, so needs to be larger than the
    -- beamformer gap by a factor of 450/400
    constant c_minGapData  : integer  := 9450;  
    
    constant c_VIRTUAL_CHANNELS : integer := g_STATIONS * g_COARSE_CHANNELS;
    
    signal m02_axi_awvalid : std_logic;
    signal m02_axi_awready : std_logic;
    signal m02_axi_awaddr : std_logic_vector(31 downto 0);
    signal m02_axi_awid : std_logic_vector(0 downto 0);
    signal m02_axi_awlen : std_logic_vector(7 downto 0);
    signal m02_axi_awsize : std_logic_vector(2 downto 0);
    signal m02_axi_awburst : std_logic_vector(1 downto 0);
    signal m02_axi_awlock :  std_logic_vector(1 downto 0);
    signal m02_axi_awcache :  std_logic_vector(3 downto 0);
    signal m02_axi_awprot :  std_logic_vector(2 downto 0);
    signal m02_axi_awqos :  std_logic_vector(3 downto 0);
    signal m02_axi_awregion :  std_logic_vector(3 downto 0);
    signal m02_axi_wready :  std_logic;
    signal m02_axi_wstrb :  std_logic_vector(63 downto 0);
    signal m02_axi_bvalid : std_logic;
    signal m02_axi_bready :  std_logic;
    signal m02_axi_bresp :  std_logic_vector(1 downto 0);
    signal m02_axi_bid :  std_logic_vector(0 downto 0);
    signal m02_axi_arvalid :  std_logic;
    signal m02_axi_arready :  std_logic;
    signal m02_axi_araddr :  std_logic_vector(31 downto 0);
    signal m02_axi_arid :  std_logic_vector(0 downto 0);
    signal m02_axi_arlen :  std_logic_vector(7 downto 0);
    signal m02_axi_arsize :  std_logic_vector(2 downto 0);
    signal m02_axi_arburst : std_logic_vector(1 downto 0);
    signal m02_axi_arlock :  std_logic_vector(1 downto 0);
    signal m02_axi_arcache :  std_logic_vector(3 downto 0);
    signal m02_axi_arprot :  std_logic_Vector(2 downto 0);
    signal m02_axi_arqos :  std_logic_vector(3 downto 0);
    signal m02_axi_arregion :  std_logic_vector(3 downto 0);
    signal m02_axi_rvalid :  std_logic;
    signal m02_axi_rready :  std_logic;
    signal m02_axi_rdata :  std_logic_vector((M02_DATA_WIDTH-1) downto 0);
    signal m02_axi_rlast :  std_logic;
    signal m02_axi_rid :  std_logic_vector(0 downto 0);
    signal m02_axi_rresp :  std_logic_vector(1 downto 0);

    signal clk300, clk300_rst, data_rst : std_logic := '0';
    signal clk400 : std_logic := '0';
    signal clk450 : std_logic := '0';
    
    signal axi_lite_mosi : t_axi4_lite_mosi;
    signal axi_lite_miso : t_axi4_lite_miso;
    signal axi_full0_mosi, axi_full1_mosi, axi_full2_mosi, axi_full3_mosi, axi_full_sj_mosi, axi_full_bj_mosi : t_axi4_full_mosi;
    signal axi_full0_miso, axi_full1_miso, axi_full2_miso, axi_full3_miso, axi_full_sj_miso, axi_full_bj_miso : t_axi4_full_miso;
    signal axi_full_bf_mosi : t_axi4_full_mosi;
    signal axi_full_bf_miso : t_axi4_full_miso;
    
    signal pkt_packet_count : integer;
    signal pkt_virtual_channel : integer;
    signal pkt_wait_count : integer := 0;
    signal pkt_valid : std_logic := '0';
    signal absolute_sample : std_logic_vector(52 downto 0);
    
    signal write_HBM_to_disk : std_logic;
    signal hbm_dump_addr : integer;
    signal hbm_dump_filename, init_fname : string(1 to 9) := "dummy.txt";
    signal init_mem : std_logic;
    signal rst_n : std_logic;
    
    signal FD_virtualChannel : t_slv_16_arr(3 downto 0); -- 3 virtual channels, one for each of the PST data streams.
    signal FD_headerValid : std_logic_vector(3 downto 0);
    
    signal FD_frameCount : std_logic_vector(36 downto 0);
    
    type fb_pkt_fsm_type is (idle, send_pkt, inter_packet_gap, update_counts, pkt_gap, pkt_SOF, pkt_SOF_wait);
    signal fb_pkt_fsm : fb_pkt_fsm_type := idle;
    signal pkt_frame_count : std_logic_vector(36 downto 0);
    signal pkt_fine_channel : integer;
    
    signal CT_sofFinal : std_logic := '0';
    signal FD_data : t_slv_64_arr(3 downto 0);
    signal FD_dataValid : std_logic := '0';
    signal totalStations, totalCoarse, totalChannels : std_logic_vector(11 downto 0);
    signal m02_axi_wdata : std_logic_vector(511 downto 0);
    signal m02_axi_wlast, m02_axi_wvalid : std_logic;
    signal BFdataIn : std_logic_vector(127 downto 0);
    signal BFflagged : std_logic_vector(1 downto 0);
    signal BFFine : std_logic_vector(7 downto 0);
    signal BFCoarse : std_logic_vector(9 downto 0);
    signal BFFirstStation, BFLastStation : std_logic;
    signal BFTimeStep : std_logic_vector(3 downto 0);
    signal BFstation : std_logic_vector(9 downto 0);
    signal BFPacketCount : std_logic_vector(47 downto 0);
    signal BFValidIn : std_logic;
    signal BF_phase_virtualChannel : t_slv_10_arr(3 downto 0); -- std_logic_vector(9 downto 0);
    signal BF_phase_timeStep : t_slv_10_arr(3 downto 0);
    signal BF_phase_beam : t_slv_7_arr(3 downto 0);
    signal BF_phase : t_slv_24_arr(3 downto 0);
    signal BF_phase_step : t_slv_24_arr(3 downto 0);
    signal BF_phase_valid, BF_phase_clear : std_logic;
    signal BFsJonesBuffer, BFbJonesBuffer : std_logic;
    signal BFsJones_status, BFbJones_status : std_logic_vector(1 downto 0);
    signal BFpoly_ok : std_logic_vector(1 downto 0);
    signal BFBeamsEnabled : std_logic_vector(9 downto 0);
    signal BF_scale_exp_frac : std_logic_vector(7 downto 0);
    signal dataMisMatch, dataMisMatchBFclk : std_logic;
    
    signal beamData : t_slv_64_arr(3 downto 0);
    signal beamPacketCount : t_slv_48_arr(3 downto 0);
    signal beamBeam : t_slv_10_arr(3 downto 0);
    signal beamFreqIndex : t_slv_11_arr(3 downto 0);
    signal beamValid : std_logic_vector(3 downto 0);
    signal beamJonesStatus : std_logic_vector(3 downto 0);
    signal beamPoly_ok : std_logic_vector(1 downto 0);
    signal bdbg_ILA_trigger : std_logic;
    
    signal axi_lite_done : std_logic := '0';
    signal ct2_axi_full_done : std_logic := '0';
    signal bf_axi_full_done : std_logic := '0';
    signal frame_gap_count, frame_gap : integer;
    signal BFPacketOdd : std_logic := '0';
    signal bad_polynomials : std_logic;
    
    signal clock_322     : std_logic := '0';
    signal clock_322_rst : std_logic := '1';
    signal clock_300_rst : std_logic := '1';
    signal clock_400_rst : std_logic := '1';
    signal testCount_322 : integer   := 0;
    signal testCount_300 : integer   := 0;
    signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := (others => '1');
    signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := (others => '1');
    signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := (others => '1');
    signal o_data_to_transmit      : t_lbus_sosi;
    signal i_data_to_transmit_ctl  : t_lbus_siso;
    
    signal packetiser_ctrl          : packetiser_stream_ctrl;
    signal packetiser_config        : packetiser_config_in;
    
    signal beamformer_to_packetiser_data    :  packetiser_stream_in; 
    signal beamformer_to_packetiser_data_2  :  packetiser_stream_in;
    signal beamformer_to_packetiser_data_3  :  packetiser_stream_in;
     
    signal packet_stream_stats              :  t_packetiser_stats(2 downto 0);
    
    signal packetiser_stream_1_host_bus_in  : packetiser_config_in;
    signal packetiser_stream_2_host_bus_in  : packetiser_config_in;
    signal packetiser_stream_3_host_bus_in  : packetiser_config_in;
      
    signal packetiser_host_bus_out          : packetiser_config_out;  
    signal packetiser_host_bus_out_2        : packetiser_config_out;
    signal packetiser_host_bus_out_3        : packetiser_config_out;  
    
    signal packetiser_host_bus_ctrl         :  packetiser_stream_ctrl;
    signal packetiser_host_bus_ctrl_2       :  packetiser_stream_ctrl;
    signal packetiser_host_bus_ctrl_3       :  packetiser_stream_ctrl;
    signal i_tx_axis_tready : std_logic;
    
    -- AXI to CMAC interface to be implemented
    signal o_tx_axis_tdata          : STD_LOGIC_VECTOR(511 downto 0);
    signal o_tx_axis_tkeep          : STD_LOGIC_VECTOR(63 downto 0);
    signal o_tx_axis_tvalid         : STD_LOGIC;
    signal o_tx_axis_tlast          : STD_LOGIC;
    signal o_tx_axis_tuser          : STD_LOGIC;
    
    signal pkt_word : std_logic_vector(15 downto 0) := x"0000";
    
    signal tdata_del : std_logic_vector(511 downto 0);
    signal d_samples : t_slv_32_arr(15 downto 0);
    signal pkt_data_word : std_logic_vector(15 downto 0);
    signal d_samples_pol : std_logic;
    signal d_samples_fine : std_logic_vector(13 downto 0);
    
    signal count1 : std_logic_vector(15 downto 0) := x"0000";
    signal count2 : std_logic_vector(15 downto 0) := x"0100";
    signal count3 : std_logic_vector(15 downto 0) := x"0200";
    signal count4 : std_logic_vector(15 downto 0) := x"0300";
    signal count5 : std_logic_vector(15 downto 0) := x"4000";
    signal count6 : std_logic_vector(15 downto 0) := x"4100";
    signal count7 : std_logic_vector(15 downto 0) := x"4200";
    signal count8 : std_logic_vector(15 downto 0) := x"4300";
    signal count9 : std_logic_vector(15 downto 0) := x"8000";
    signal count10 : std_logic_vector(15 downto 0) := x"8100";
    signal count11 : std_logic_vector(15 downto 0) := x"8200";
    signal count12 : std_logic_vector(15 downto 0) := x"8300";
    signal count13 : std_logic_vector(15 downto 0) := x"C000";
    signal count14 : std_logic_vector(15 downto 0) := x"C100";
    signal count15 : std_logic_vector(15 downto 0) := x"C200";
    signal count16 : std_logic_vector(15 downto 0) := x"C300";
    
    signal BF_scale_float : std_logic_vector(31 downto 0);
    signal full0_done, full1_done, full2_done, full3_done, full_sj_done, full_bj_done : std_logic;
    signal BFStations01 : std_logic;
    signal beamScale : t_slv_4_arr(3 downto 0);
    
    signal zero12 : std_logic_vector(11 downto 0);
    signal pcount, scaling_pcount : t_slv_12_arr(3 downto 0);
    signal beamValid_del1 : std_logic_Vector(3 downto 0);
    signal scaling_beamData         : t_slv_64_arr(3 downto 0);
    signal scaling_beamPacketCount  : t_slv_48_arr(3 downto 0);
    signal scaling_beamBeam         : t_slv_10_arr(3 downto 0);
    signal scaling_beamFreqIndex    : t_slv_11_arr(3 downto 0);
    signal scaling_beamValid, scaling_beamValid_del1 : std_logic_vector(3 downto 0);
    signal scaling_beamJonesStatus  : t_slv_4_arr(3 downto 0);
    signal scaling_beamPoly_ok      : t_slv_2_arr(3 downto 0);
    
    signal o_beam_output_stream     : t_bf_output_stream;
    
    signal testCount                : integer   := 0;
    signal rst400                   : std_logic := '1';
    
    signal i_beam_output_stream_1           : t_bf_output_stream;
    signal i_beam_output_stream_2           : t_bf_output_stream;
    
    signal i_PSR_packetiser_Lite_axi_mosi   : t_axi4_lite_mosi_arr((2-1) downto 0);
    signal o_PSR_packetiser_Lite_axi_miso   : t_axi4_lite_miso_arr((2-1) downto 0);
            
    signal i_PSR_packetiser_Full_axi_mosi   : t_axi4_full_mosi_arr((2-1) downto 0);
    signal o_PSR_packetiser_Full_axi_miso   : t_axi4_full_miso_arr((2-1) downto 0);
    
    signal packetiser_host_bus_ctrl_hold    :  packetiser_stream_ctrl;
    signal packetiser_stream_1_host_bus_in_hold : packetiser_config_in;
    
    signal BF_beamTestEnable : std_logic;
    signal BFvirtualChannel : std_logic_vector(9 downto 0);

    signal weights        : std_logic_vector(15 downto 0);
    signal weights_valid  : std_logic;
    signal weights_sop    : std_logic;
    signal HBM_reset   : std_logic;
    signal HBM_status  : std_logic_vector(7 downto 0);
    signal pol0_fine_active, pol1_fine_active : std_logic := '0';
    signal amp : integer := 0;
    
begin
    
    clk300 <= not clk300 after 1.666 ns;
    clock_322 <= not clock_322 after 3.11 ns;
    clk400 <= not clk400 after 1.25 ns;
    clk450 <= not clk450 after 1.111 ns;
    
    rst_n <= not clk300_rst;
    
    clock_300_rst   <= clk300_rst;
    -------------------------------------------------------------------------------
    test_runner_proc_clk300: process(clk300)
    begin
        if rising_edge(clk300) then
            -- power up reset logic
            if power_up_rst_clock_300(31) = '1' then
                power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
                clock_300_rst   <= '1';
                testCount_300   <= 0;
            else
                clock_300_rst   <= '0';
                testCount_300   <= testCount_300 + 1;
            end if;
        end if;
    end process;

    test_runner_proc_clk322: process(clock_322)
    begin
        if rising_edge(clock_322) then
            -- power up reset logic
            if power_up_rst_clock_322(31) = '1' then
                power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
                clock_322_rst   <= '1';
                testCount_322   <= 0;
            else
                clock_322_rst   <= '0';
                testCount_322   <= testCount_322 + 1;
            end if;
        end if;
    end process;

    test_runner_proc_clk400: process(clk400)
    begin
        if rising_edge(clk400) then
            -- power up reset logic
            if power_up_rst_clock_400(31) = '1' then
                power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
                testCount       <= 0;
                clock_400_rst   <= '1';
            else
                testCount       <= testCount + 1;
                clock_400_rst   <= '0';
             
            end if;
        end if;
    end process;

    rst400  <= clock_400_rst;
    
    -------------------------------------------------------------------------------
    -- Programming of the corner turn 2 axi-lite registers :
    --    - jones_buffer0_valid_frame_low, jones_buffer0_valid_frame_high, jones_buffer0_valid_duration
    --    - jones_buffer1_valid_frame_low, jones_buffer1_valid_frame_high, jones_buffer1_valid_duration
    --    - poly_buffer0_info_valid, poly_buffer0_valid_frame_low, poly_buffer0_valid_frame_high, poly_buffer0_valid_duration, poly_buffer0_offset_ns
    --    - poly_buffer1_info_valid, poly_buffer1_valid_frame_low, poly_buffer1_valid_frame_high, poly_buffer1_valid_duration, poly_buffer1_offset_ns
    --    - BeamsEnabled
    
    --
    process
        file RegCmdfile: TEXT;
        variable RegLine_in : Line;
        variable regData, regAddr : std_logic_vector(31 downto 0);
        variable RegGood : boolean;
        variable c : character;
    begin
        clk300_rst <= '1';
        axi_lite_done <= '0';
        -- axi_lite for writes to corner turn 2
        axi_lite_mosi.awaddr <= (others => '0');
        axi_lite_mosi.awprot <= "000";
        axi_lite_mosi.awvalid <= '0';
        axi_lite_mosi.wdata <= (others => '0');
        axi_lite_mosi.wstrb <= "1111";
        axi_lite_mosi.wvalid <= '0';
        axi_lite_mosi.bready <= '0';
        axi_lite_mosi.araddr <= (others => '0');
        axi_lite_mosi.arprot <= "000";
        axi_lite_mosi.arvalid <= '0';
        axi_lite_mosi.rready <= '0';
        
        wait until rising_edge(clk300);
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        clk300_rst <= '0';
        wait until rising_edge(clk300);
        wait until rising_edge(clk300);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer0_valid_frame_low_address.address, true, g_JONES_BUF0_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer0_valid_frame_high_address.address, true, g_JONES_BUF0_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer0_valid_duration_address.address, true, g_JONES_BUF0_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer1_valid_frame_low_address.address, true, g_JONES_BUF1_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer1_valid_frame_high_address.address, true, g_JONES_BUF1_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer1_valid_duration_address.address, true, g_JONES_BUF1_VALID_DURATION);
        wait until rising_edge(clk300);
        
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_beamjones_buffer0_valid_frame_low_address.address, true, g_JONES_BUF0_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_beamjones_buffer0_valid_frame_high_address.address, true, g_JONES_BUF0_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_beamjones_buffer0_valid_duration_address.address, true, g_JONES_BUF0_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_beamjones_buffer1_valid_frame_low_address.address, true, g_JONES_BUF1_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_beamjones_buffer1_valid_frame_high_address.address, true, g_JONES_BUF1_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_beamjones_buffer1_valid_duration_address.address, true, g_JONES_BUF1_VALID_DURATION);
        wait until rising_edge(clk300);        
        
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_valid_frame_low_address.address, true, g_POLY_BUF0_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_valid_frame_high_address.address, true, g_POLY_BUF0_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_valid_duration_address.address, true, g_POLY_BUF0_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_offset_ns_address.address, true, g_POLY_BUF0_OFFSET_NS);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_info_valid_address.address, true, x"00000001");
        wait until rising_edge(clk300);
        
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_valid_frame_low_address.address, true, g_POLY_BUF1_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_valid_frame_high_address.address, true, g_POLY_BUF1_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_valid_duration_address.address, true, g_POLY_BUF1_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_offset_ns_address.address, true, g_POLY_BUF1_OFFSET_NS);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_info_valid_address.address, true, x"00000001");
        wait until rising_edge(clk300);
        
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_BeamsEnabled_address.address, true, g_BEAMS_ENABLED);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_scaleFactor_address.address, true, g_SCALE_FACTOR);
        wait until rising_edge(clk300);
        
        axi_lite_done <= '1';
        wait;
    end process;
    
    --------------------------------------------------------------------
    -- Programming of the corner turn 2 axi-full registers
    --  - Holds the polynomial configuration
    --
    
    poly0_reg : entity pss_lib.file2_axifull
    generic map(
        g_AWLEN => 6, -- integer; Number of words to write in a burst; 
        g_INIT_FILENAME => g_TEST_CASE & g_POLYNOMIAL0_INIT_FILENAME
    ) port map (
        clk300 => clk300, -- in std_logic;
        axi_full_mosi => axi_full0_mosi, -- out t_axi4_full_mosi;
        axi_full_miso => axi_full0_miso, -- in t_axi4_full_miso;
        o_done => full0_done             -- out std_logic
    );
    
    poly1_reg : entity pss_lib.file2_axifull
    generic map(
        g_AWLEN => 6, -- integer; Number of words to write in a burst; 
        g_INIT_FILENAME => g_TEST_CASE & g_POLYNOMIAL1_INIT_FILENAME
    ) port map (
        clk300 => clk300, -- in std_logic;
        axi_full_mosi => axi_full1_mosi, -- out t_axi4_full_mosi;
        axi_full_miso => axi_full1_miso, -- in t_axi4_full_miso;
        o_done => full1_done             -- out std_logic
    );
    
    poly2_reg : entity pss_lib.file2_axifull
    generic map(
        g_AWLEN => 6, -- integer; Number of words to write in a burst; 
        g_INIT_FILENAME => g_TEST_CASE & g_POLYNOMIAL2_INIT_FILENAME
    ) port map (
        clk300 => clk300, -- in std_logic;
        axi_full_mosi => axi_full2_mosi, -- out t_axi4_full_mosi;
        axi_full_miso => axi_full2_miso, -- in t_axi4_full_miso;
        o_done => full2_done             -- out std_logic
    );
    
    poly3_reg : entity pss_lib.file2_axifull
    generic map(
        g_AWLEN => 6, -- integer; Number of words to write in a burst; 
        g_INIT_FILENAME => g_TEST_CASE & g_POLYNOMIAL3_INIT_FILENAME
    ) port map (
        clk300 => clk300, -- in std_logic;
        axi_full_mosi => axi_full3_mosi, -- out t_axi4_full_mosi;
        axi_full_miso => axi_full3_miso, -- in t_axi4_full_miso;
        o_done => full3_done             -- out std_logic
    );
    
    
    stationjones_reg : entity pss_lib.file2_axifull
    generic map(
        g_AWLEN => 16, -- integer; Number of words to write in a burst; 
        g_INIT_FILENAME => g_TEST_CASE & g_STATION_JONES_INIT_FILENAME
    ) port map (
        clk300 => clk300, -- in std_logic;
        axi_full_mosi => axi_full_sj_mosi, -- out t_axi4_full_mosi;
        axi_full_miso => axi_full_sj_miso, -- in t_axi4_full_miso;
        o_done => full_sj_done             -- out std_logic
    );
    
    beamjones_reg : entity pss_lib.file2_axifull
    generic map(
        g_AWLEN => 16, -- integer; Number of words to write in a burst; 
        g_INIT_FILENAME => g_TEST_CASE & g_BEAM_JONES_INIT_FILENAME
    ) port map (
        clk300 => clk300, -- in std_logic;
        axi_full_mosi => axi_full_bj_mosi, -- out t_axi4_full_mosi;
        axi_full_miso => axi_full_bj_miso, -- in t_axi4_full_miso;
        o_done => full_bj_done             -- out std_logic
    );
    
    
    process(clk300)
    begin
        if rising_edge(clk300) then
            if axi_lite_done = '0' or full0_done = '0' or full1_done = '0' or full2_done = '0' or full3_done = '0' or full_sj_done = '0' or full_bj_done = '0' then
                data_rst <= '1';
            else
                data_rst <= '0';
            end if;
        end if;
    end process;
    
    --------------------------------------------------------------------------------
    -- Emulate the Filterbanks
    --  Filterbank output signals on clk450 :
    --    CT_sofFinal       ; Pulse high at the start of every frame, prior to FD_dataValid (1 frame is 53 ms of data).
    --    FD_frameCount     ; std_logic_vector(36 downto 0); Corner turn frame count; Stable for the entire corner turn frame.
    --    FD_virtualChannel ; t_slv_16_arr(2 downto 0); 3 virtual channels, one for each of the PST data streams.
    --    FD_headerValid    ; std_logic_vector(2 downto 0); goes to "111" for the first clock cycle of each burst of 216 fine channels.
    --    FD_data           ; t_slv_64_arr(2 downto 0); (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
    --    FD_dataValid      ; std_logic; Goes high in bursts of 216 out of every 258 clocks.
    --
    
    -- Per corner turn frame, beamformer has to output :
    --  (c_minGapData = time allowance for 9 packets = 1 coarse channel) * 
    --  (g_BEAMS_ENABLED beams) * (g_COARSE_CHANNELS coarse channels) * (8 packets per corner turn [each 32 time samples])
    frame_gap <= c_minGapData * TO_INTEGER(unsigned(g_BEAMS_ENABLED)) * g_COARSE_CHANNELS * 8;
    
    process(clk300)
    begin
        if rising_Edge(clk300) then
            -- generate data on pkt_virtual_channel, pkt_packet_count, pkt_valid
            -- Time between packets is at least 200 clocks
            -- (8300byte packets at 100Gbit/sec, 3.333 ns clock period)
            if data_rst = '1' then
                fb_pkt_fsm <= pkt_gap;
                pkt_frame_count <= g_FRAME_COUNT_START(36 downto 0); -- Count of corner turn frames since the SKA epoch
                pkt_packet_count <= 0;     -- Count through 768 packets for each virtual channel
                pkt_virtual_channel <= 0;  -- Count through virtual channels in steps of 3 
                pkt_wait_count <= 0;
                pkt_fine_channel <= 0;
                frame_gap_count <= frame_gap-10;
            else
                case fb_pkt_fsm is
                    when idle => -- do nothing until data_rst kicks off the data input.
                        fb_pkt_fsm <= idle;
                        
                    when send_pkt =>
                        if pkt_fine_channel = 53 then
                            pkt_fine_channel <= 0;
                            fb_pkt_fsm <= inter_packet_gap;
                        else
                            pkt_fine_channel <= pkt_fine_channel + 1;
                        end if;
                        pkt_wait_count <= 0;
                        frame_gap_count <= frame_gap_count + 1;
                    
                    when inter_packet_gap =>
                        pkt_fine_channel <= 0;
                        if pkt_wait_count = 10 then
                            -- total count of clocks when not sending is 11 (this state) + 1 (update_counts)
                            --  = 12 clocks; 54+12 = 66 clocks total between packets (as per the real filterbank) 
                            fb_pkt_fsm <= update_counts;
                        else
                            pkt_wait_count <= pkt_wait_count + 1;
                        end if;
                        frame_gap_count <= frame_gap_count + 1;
                    
                    when update_counts =>
                        if pkt_packet_count = 767 then
                            pkt_packet_count <= 0;
                            -- Go to the next virtual channel
                            if ((pkt_virtual_channel+4) >= c_VIRTUAL_CHANNELS) then
                                -- Just finished sending all the virtual channels
                                --   e.g. pkt_virtual_channel = 0, then we've just sent 3 channels.
                                -- Go to the next corner turn frame.
                                pkt_virtual_channel <= 0;
                                pkt_frame_count <= std_logic_vector(unsigned(pkt_frame_count) + 1);
                                fb_pkt_fsm <= pkt_gap;
                            else
                                pkt_virtual_channel <= pkt_virtual_channel + 4;
                                fb_pkt_fsm <= send_pkt;
                            end if;
                        else
                            pkt_packet_count <= pkt_packet_count + 1;
                            fb_pkt_fsm <= send_pkt;
                        end if;
                        frame_gap_count <= frame_gap_count + 1;
                        
                    when pkt_gap =>
                        -- Gap between corner turn frames
                        pkt_wait_count <= pkt_wait_count + 1;
                        pkt_fine_channel <= 0;
                        frame_gap_count <= frame_gap_count + 1;
                        if (frame_gap_count > frame_gap) then
                            fb_pkt_fsm <= pkt_SOF;
                        end if;
                        
                    when pkt_SOF =>
                        -- set start of frame before sending a new corner turn frames worth of data.
                        fb_pkt_fsm <= pkt_SOF_wait;
                        pkt_wait_count <= 0;
                        frame_gap_count <= 0;
                        
                    when pkt_SOF_wait =>
                        frame_gap_count <= frame_gap_count + 1;
                        pkt_wait_count <= pkt_wait_count + 1;
                        if (pkt_wait_count > 20) then
                            fb_pkt_fsm <= send_pkt;
                        end if;
                        
                end case;
            end if;
            
        end if;
    end process;
    
    absolute_sample <= std_logic_vector(unsigned(pkt_frame_count) * to_unsigned(256,16) + to_unsigned(pkt_packet_count,53));
    
    -- Construct the emulated filterbank data
    CT_sofFinal <= '1' when fb_pkt_fsm = pkt_SOF else '0';
    FD_frameCount <= pkt_frame_count;
    FD_virtualChannel(0) <= std_logic_vector(to_unsigned(pkt_virtual_channel,16));
    FD_virtualChannel(1) <= std_logic_vector(to_unsigned(pkt_virtual_channel+1,16));
    FD_virtualChannel(2) <= std_logic_vector(to_unsigned(pkt_virtual_channel+2,16));
    FD_virtualChannel(3) <= std_logic_vector(to_unsigned(pkt_virtual_channel+3,16));
    FD_headerValid <= "1111" when (fb_pkt_fsm = send_pkt and pkt_fine_channel = 0) else "0000";
--    FD_data(0)(15 downto 0)  <= absolute_sample(15 downto 0); 
--    FD_data(0)(31 downto 16) <= std_logic_vector(to_unsigned(pkt_virtual_channel,16));
--    FD_data(0)(47 downto 32) <= absolute_sample(31 downto 16);
--    FD_data(0)(63 downto 48) <= std_logic_vector(to_unsigned(pkt_fine_channel,16));
--    FD_data(1)(15 downto 0)  <= absolute_sample(15 downto 0);
--    FD_data(1)(31 downto 16) <= std_logic_vector(to_unsigned(pkt_virtual_channel+1,16));
--    FD_data(1)(47 downto 32) <= absolute_sample(31 downto 16);
--    FD_data(1)(63 downto 48) <= std_logic_vector(to_unsigned(pkt_fine_channel,16));
--    FD_data(2)(15 downto 0)  <= absolute_sample(15 downto 0);
--    FD_data(2)(31 downto 16) <= std_logic_vector(to_unsigned(pkt_virtual_channel+2,16));
--    FD_data(2)(47 downto 32) <= absolute_sample(31 downto 16);
--    FD_data(2)(63 downto 48) <= std_logic_vector(to_unsigned(pkt_fine_channel,16));
    FD_dataValid <= '1' when fb_pkt_fsm = send_pkt else '0';
    
    process(clk300)
    begin
        if rising_edge(clk300) then
            if FD_datavalid = '1' then
                count1 <= std_logic_vector(unsigned(count1) + 1);
                count2 <= std_logic_vector(unsigned(count2) + 1);
                count3 <= std_logic_vector(unsigned(count3) + 1);
                count4 <= std_logic_vector(unsigned(count4) + 1);
                count5 <= std_logic_vector(unsigned(count5) + 1);
                count6 <= std_logic_vector(unsigned(count6) + 1);
                count7 <= std_logic_vector(unsigned(count7) + 1);
                count8 <= std_logic_vector(unsigned(count8) + 1);
                count9 <= std_logic_vector(unsigned(count9) + 1);
                count10 <= std_logic_vector(unsigned(count10) + 1);
                count11 <= std_logic_vector(unsigned(count11) + 1);
                count12 <= std_logic_vector(unsigned(count12) + 1);
                count13 <= std_logic_vector(unsigned(count13) + 1);
                count14 <= std_logic_vector(unsigned(count14) + 1);
                count15 <= std_logic_vector(unsigned(count15) + 1);
                count16 <= std_logic_vector(unsigned(count16) + 1);
            end if;
            --count1 <= std_logic_vector(unsigned(count1) + 23753);
            --count2 <= std_logic_vector(unsigned(count2) + 13751);
            --count3 <= std_logic_vector(unsigned(count3) + 11743);
            --count4 <= std_logic_vector(unsigned(count4) + 9743);
            --count5 <= std_logic_vector(unsigned(count4) + 7753);
        end if;
    end process;
    
    dgen0 : if g_DATATYPE = 0 generate
        -- simple data based on a wrapping counter.
        FD_data(0)(15 downto 0)  <= count1; 
        FD_data(0)(31 downto 16) <= count2;
        FD_data(0)(47 downto 32) <= count3;
        FD_data(0)(63 downto 48) <= count4;
        FD_data(1)(15 downto 0)  <= count5;
        FD_data(1)(31 downto 16) <= count6;
        FD_data(1)(47 downto 32) <= count7;
        FD_data(1)(63 downto 48) <= count8;
        FD_data(2)(15 downto 0)  <= count9;
        FD_data(2)(31 downto 16) <= count10;
        FD_data(2)(47 downto 32) <= count11;
        FD_data(2)(63 downto 48) <= count12;
        FD_data(3)(15 downto 0)  <= count13;
        FD_data(3)(31 downto 16) <= count14;
        FD_data(3)(47 downto 32) <= count15;
        FD_data(3)(63 downto 48) <= count16;
    end generate;
    dgen1 : if g_DATATYPE = 1 generate
        -- Alternate - triangular wave in one polarisation, real only, just for stations 0, 4, 8 etc
        -- over one corner turn frame = 768 time samples, this runs from 0 to 32767 to -32641
        -- to exercise the full range for 16-bit input data.
        FD_data(0)(15 downto 0) <= 
            std_logic_vector(to_unsigned(128 * pkt_packet_count,16)) when pkt_packet_count < 256 and pkt_fine_channel = 40 else
            std_logic_vector(to_unsigned(32767 - 128 * (pkt_packet_count-256),16)) when pkt_fine_channel = 40 else
            (others => '0');
        FD_data(0)(31 downto 16) <= (others => '0'); -- std_logic_vector(to_unsigned(32 * pkt_packet_count - 32768,16)) when pkt_fine_channel = 40 else (others => '0');
        FD_data(0)(47 downto 32) <= (others => '0');
        FD_data(0)(63 downto 48) <= (others => '0');
        
        FD_data(1)(15 downto 0) <=  (others => '0');
        FD_data(1)(31 downto 16) <= (others => '0');
        FD_data(1)(47 downto 32) <= (others => '0');
        FD_data(1)(63 downto 48) <= (others => '0');
        
        FD_data(2)(15 downto 0) <=  (others => '0');
        FD_data(2)(31 downto 16) <= (others => '0');
        FD_data(2)(47 downto 32) <= (others => '0');
        FD_data(2)(63 downto 48) <= (others => '0');
    
        FD_data(3)(15 downto 0) <=  (others => '0');
        FD_data(3)(31 downto 16) <= (others => '0');
        FD_data(3)(47 downto 32) <= (others => '0');
        FD_data(3)(63 downto 48) <= (others => '0');
    end generate;
    dgen2 : if g_DATATYPE = 2 generate
        -- Alternate - "complex" triangular wave in one polarisation, real only, just for stations 0, 4, 8 etc
        -- over one corner turn frame = 768 time samples, this runs from 0 to 8064 to -8022 back to 0
        -- to exercise the full range for 16-bit input data.
        FD_data(0)(15 downto 0) <= 
            std_logic_vector(to_unsigned(42 * pkt_packet_count,16)) when pkt_packet_count < 192 and pkt_fine_channel = 0 else
            std_logic_vector(to_unsigned(8064 - 42 * (pkt_packet_count-192),16)) when pkt_packet_count < 576 and pkt_fine_channel = 0 else
            std_logic_vector(to_unsigned(-8022 + 42 * (pkt_packet_count-576),16)) when pkt_fine_channel = 0 else
            (others => '0');
        -- imaginary part, runs -8022 to +8022 to -8022
        FD_data(0)(31 downto 16) <= 
            std_logic_vector(to_unsigned(-8022 + 42 * (pkt_packet_count),16)) when pkt_packet_count < 384 and pkt_fine_channel = 0 else
            std_logic_vector(to_unsigned(8064 - 42 * (pkt_packet_count-384),16)) when pkt_fine_channel = 0 else
            (others => '0');        
        
        FD_data(0)(47 downto 32) <= (others => '0');
        FD_data(0)(63 downto 48) <= (others => '0');
        
        FD_data(1)(15 downto 0)  <= FD_data(0)(15 downto 0);
        FD_data(1)(31 downto 16) <= FD_data(0)(31 downto 16);
        FD_data(1)(47 downto 32) <= (others => '0');
        FD_data(1)(63 downto 48) <= (others => '0');
        
        FD_data(2)(15 downto 0) <=  FD_data(0)(15 downto 0);
        FD_data(2)(31 downto 16) <= FD_data(0)(31 downto 16);
        FD_data(2)(47 downto 32) <= (others => '0');
        FD_data(2)(63 downto 48) <= (others => '0');
    
        FD_data(3)(15 downto 0) <=  FD_data(0)(15 downto 0);
        FD_data(3)(31 downto 16) <= FD_data(0)(31 downto 16);
        FD_data(3)(47 downto 32) <= (others => '0');
        FD_data(3)(63 downto 48) <= (others => '0');
    
    end generate;

    dgen3 : if g_DATATYPE = 3 generate
        -- Alternate - "complex" triangular wave in one polarisation, real only, just for stations 0, 4, 8 etc
        -- over one corner turn frame = 768 time samples, this runs from 0 to 192
        pol0_fine_active <= '1' when pkt_fine_channel = 40 or pkt_fine_channel = 44 or pkt_fine_channel = 48 or pkt_fine_channel = 52 else '0';
        pol1_fine_active <= '1' when pkt_fine_channel = 16 or pkt_fine_channel = 20 or pkt_fine_channel = 24 or pkt_fine_channel = 28 else '0';
        amp <= 
            4 when pkt_fine_channel = 16 else
            3 when pkt_fine_channel = 20 else
            2 when pkt_fine_channel = 24 else
            1 when pkt_fine_channel = 28 else
            2 when pkt_fine_channel = 40 else
            3 when pkt_fine_channel = 44 else
            4 when pkt_fine_channel = 48 else
            5 when pkt_fine_channel = 52 else
            0;
        
        FD_data(0)(15 downto 0) <=
            std_logic_vector(to_unsigned(amp*pkt_packet_count,16)) when pkt_packet_count < 192 and pol0_fine_active = '1' else
            std_logic_vector(to_unsigned(192*amp - amp*(pkt_packet_count-192),16)) when pkt_packet_count < 576 and pol0_fine_active = '1' else
            std_logic_vector(to_unsigned(-192*amp + amp*(pkt_packet_count-576),16)) when pol0_fine_active = '1' else
            (others => '0');
        -- imaginary part, runs -192 to 192 to -192
        FD_data(0)(31 downto 16) <= 
            std_logic_vector(to_unsigned(-192*amp + amp*(pkt_packet_count),16)) when pkt_packet_count < 384 and pol0_fine_active = '1' else
            std_logic_vector(to_unsigned(192*amp - amp*(pkt_packet_count-384),16)) when pol0_fine_active = '1' else
            (others => '0');
        
        FD_data(0)(47 downto 32) <= 
            std_logic_vector(to_unsigned(amp*pkt_packet_count,16)) when pkt_packet_count < 192 and pol1_fine_active = '1' else
            std_logic_vector(to_unsigned(192*amp - amp*(pkt_packet_count-192),16)) when pkt_packet_count < 576 and pol1_fine_active = '1' else
            std_logic_vector(to_unsigned(-192*amp + amp*(pkt_packet_count-576),16)) when pol1_fine_active = '1' else
            (others => '0');
        
        FD_data(0)(63 downto 48) <=
            std_logic_vector(to_unsigned(-192*amp + amp*(pkt_packet_count),16)) when pkt_packet_count < 384 and pol1_fine_active = '1' else
            std_logic_vector(to_unsigned(192*amp - amp*(pkt_packet_count-384),16)) when pol1_fine_active = '1' else
            (others => '0'); 
        
        FD_data(1)(15 downto 0)  <= FD_data(0)(15 downto 0);
        FD_data(1)(31 downto 16) <= FD_data(0)(31 downto 16);
        FD_data(1)(47 downto 32) <= FD_data(0)(47 downto 32);
        FD_data(1)(63 downto 48) <= FD_data(0)(63 downto 48);
        
        FD_data(2)(15 downto 0) <=  FD_data(0)(15 downto 0);
        FD_data(2)(31 downto 16) <= FD_data(0)(31 downto 16);
        FD_data(2)(47 downto 32) <= FD_data(0)(47 downto 32);
        FD_data(2)(63 downto 48) <= FD_data(0)(63 downto 48);
    
        FD_data(3)(15 downto 0) <=  FD_data(0)(15 downto 0);
        FD_data(3)(31 downto 16) <= FD_data(0)(31 downto 16);
        FD_data(3)(47 downto 32) <= FD_data(0)(47 downto 32);
        FD_data(3)(63 downto 48) <= FD_data(0)(63 downto 48);
    
    end generate;
    
        
    totalStations <= std_logic_vector(to_unsigned(g_STATIONS,12));
    totalCoarse <= std_logic_vector(to_unsigned(g_COARSE_CHANNELS,12));
    totalChannels <= std_logic_vector(to_unsigned(g_STATIONS * g_COARSE_CHANNELS,12));
    bad_polynomials <= '0';
    
    BFCT : entity ct_lib.ct2_wrapper
    generic map (
        g_BEAMFORMER_DAISYCHAIN_DEPTH => g_BEAMFORMER_DAISYCHAIN_DEPTH,
        g_PACKET_GAP => g_PACKET_GAP,
        g_USE_META => False
    ) port map (
        -- Parameters, in the i_axi_clk domain.
        i_stations => totalStations(10 downto 0), -- in (10:0); Up to 1024 stations
        i_coarse   => totalCoarse(9 downto 0),    -- in (9:0); Number of coarse channels.
        i_virtualChannels => totalChannels(10 downto 0), -- in (10:0); Total virtual channels (= i_stations * i_coarse)
        i_bad_polynomials => bad_polynomials, --  in std_logic;
        -- Registers AXI Lite Interface (uses i_axi_clk)
        i_axi_mosi => axi_lite_mosi, -- in t_axi4_lite_mosi;
        o_axi_miso => axi_lite_miso, -- out t_axi4_lite_miso;
        i_axi_rst  => clk300_rst,    -- in std_logic;
        -- Polynomial memory axi full interfaces, for 4 sets of beam polynomials
        i_axi_full0_mosi => axi_full0_mosi, -- in t_axi4_full_mosi;
        o_axi_full0_miso => axi_full0_miso, -- out t_axi4_full_miso;
        i_axi_full1_mosi => axi_full1_mosi, -- in t_axi4_full_mosi;
        o_axi_full1_miso => axi_full1_miso, -- out t_axi4_full_miso;
        i_axi_full2_mosi => axi_full2_mosi, -- in t_axi4_full_mosi;
        o_axi_full2_miso => axi_full2_miso, -- out t_axi4_full_miso;
        i_axi_full3_mosi => axi_full3_mosi, -- in t_axi4_full_mosi;
        o_axi_full3_miso => axi_full3_miso, -- out t_axi4_full_miso;
        -- Reset passed in from upstream.
        i_rst => data_rst,
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        i_sof          => CT_sofFinal,     -- in std_logic; Pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       => clk300,          -- in std_logic; Filterbank clock, expected to be 300 MHz
        i_frameCount     => FD_frameCount, -- in (36:0); Frame count is the same for all simultaneous output streams.
        i_virtualChannel => FD_virtualChannel, -- in t_slv_16_arr(3:0); 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid => FD_headerValid,   -- in (3:0);
        i_data        => FD_data,          -- in t_slv_64_arr(3:0); (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
        i_dataValid   => FD_dataValid,     -- in std_logic;
        -- Data out to the beamformer
        i_BF_clk  => clk400,    -- in std_logic; Beamformer clock, expected to be 400 MHz
        --
        o_data    => BFdataIn,  -- out (127:0);
        o_flagged => BFflagged, -- out (1:0);  -- "o_flagged" aligns with "o_data"
        o_fine    => BFFine,    -- out (7:0);
        o_coarse  => BFCoarse,  -- out (9:0);
        o_stations01 => BFstations01,
        o_firstStation => BFFirstStation, -- out std_logic;
        o_lastStation => BFLastStation,   -- out std_logic;
        o_timeStep => BFtimeStep,         -- out (3:0)
        o_station => BFStation,           -- out (9:0); station
        o_virtualChannel => BFvirtualChannel, -- out (9:0);   -- Note takes steps of 4
        o_packetCount => BFPacketCount, -- out (47:0); The PST output packet count for this packet, based on the original packet count from LFAA. Each PST output packet is 6.63552 ms
        o_outputPktOdd => BFPacketOdd,  -- out std_logic;
        o_valid   => BFValidIn,  -- out std_logic;
        -- Polynomial data 
        o_phase_virtualChannel => BF_phase_virtualChannel, -- out t_slv_10_arr(3 downto 0);
        o_phase_timeStep       => BF_phase_timeStep,       -- out t_slv_10_arr(3 downto 0); Timestep within the corner turn frame, 0 to 767
        o_phase_beam           => BF_phase_beam,           -- out t_slv_7_arr(3 downto 0); Data for up to 128 beams on each bus, 4 busses so 512 beams total.
        o_phase                => BF_phase,                -- out t_slv_24_arr(3 downto 0); Phase at the start of the coarse channel.
        o_phase_step           => BF_phase_step,           -- out t_slv_24_arr(3 downto 0); Phase step per fine channel.
        o_phase_valid          => BF_phase_valid,          -- out std_logic;
        o_phase_clear          => BF_phase_clear,          -- out std_logic;
        -- Configuration to the beamformers
        o_sjonesBuffer  => BFsJonesBuffer, -- out std_logic;                    -- Which station jones buffer to use.
        o_sjones_status => BFsJones_status, -- out std_logic_vector(1 downto 0); -- bit 0 = used default, bit 1 = jones valid
        o_bjonesBuffer  => BFbJonesBuffer, -- out std_logic;                    -- Which beam jones buffer to use.
        o_bjones_status => BFbJones_status, -- out std_logic_vector(1 downto 0); -- bit 0 = used default, bit 1 = jones valid
        o_poly_ok      => BFpoly_ok,      -- out (1:0); The polynomials used are within their valid time range;
        o_beamsEnabled => BFBeamsEnabled, -- out (9:0)
        o_scale_float  => BF_scale_float, -- out (31:0); bit 3 = 0 indicates firmware should calculate the scale factor.
        o_beamTestEnable => BF_beamTestEnable, -- out std_logic;  beam test mode - each beam only uses the station with the same number as the beam number.
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        -- aw bus = write address
        i_axi_clk => clk300, -- in std_logic;
        m0_axi_awvalid  => m02_axi_awvalid, --  out std_logic;
        m0_axi_awready  => m02_axi_awready, -- in std_logic;
        m0_axi_awaddr   => m02_axi_awaddr(29 downto 0),  -- out (29:0);
        m0_axi_awlen    => m02_axi_awlen,   -- out (7:0);
        -- w bus - write data
        m0_axi_wvalid   => m02_axi_wvalid, -- out std_logic;
        m0_axi_wready   => m02_axi_wready, -- in std_logic;
        m0_axi_wdata    => m02_axi_wdata,  -- out (511:0);
        m0_axi_wlast    => m02_axi_wlast,  -- out std_logic;
        -- b bus - write response
        m0_axi_bvalid    => m02_axi_bvalid, -- in std_logic;
        m0_axi_bresp     => m02_axi_bresp,  -- in (1:0);
        -- ar bus - read address
        m0_axi_arvalid   => m02_axi_arvalid, -- out std_logic;
        m0_axi_arready   => m02_axi_arready, -- in std_logic;
        m0_axi_araddr    => m02_axi_araddr(29 downto 0),  -- out (29:0);
        m0_axi_arlen     => m02_axi_arlen,   -- out (7:0);
        -- r bus - read data
        m0_axi_rvalid    => m02_axi_rvalid, -- in std_logic;
        m0_axi_rready    => m02_axi_rready, -- out std_logic;
        m0_axi_rdata     => m02_axi_rdata,  -- in (511:0);
        m0_axi_rlast     => m02_axi_rlast,  -- in std_logic;
        m0_axi_rresp     => m02_axi_rresp,  -- in (1:0)
        --
        o_dataMismatch => dataMismatch,  -- out std_logic;
        o_dataMismatchBFclk => datamismatchBFclk, -- out std_logic;
        -- hbm reset
        o_hbm_reset  => HBM_reset, -- out std_logic;
        i_hbm_status => HBM_status -- in std_logic_vector(7 downto 0)
    );
    
    HBM_status <= (others => '0');
    
    -- Beamformer
    BFi : entity bf_lib.PSSbeamformerTop
    generic map (
        g_PIPE_INSTANCE => 0,  -- only used for instantiating ILAs
        g_PACKET_GAP => g_PACKET_GAP,
        g_BEAMFORMER_DAISYCHAIN_DEPTH => g_BEAMFORMER_DAISYCHAIN_DEPTH
    ) port map (
        -- Registers axi full interface
        i_MACE_clk => clk300, -- in std_logic;
        i_MACE_rst => clk300_rst, -- in std_logic;
        -- station jones configuration
        i_axi_mosi_sj => axi_full_sj_mosi, -- in  t_axi4_full_mosi;
        o_axi_miso_sj => axi_full_sj_miso, -- out t_axi4_full_miso;
        -- Beam Jones Configuration
        i_axi_mosi_bj => axi_full_bj_mosi, -- in  t_axi4_full_mosi;
        o_axi_miso_bj => axi_full_bj_miso, -- out t_axi4_full_miso;
        -- Beamformer data from the corner turn
        i_BF_clk         => clk400,        -- in std_logic;
        i_BF_rst         => '0',        -- in std_logic;
        i_data           => BFdataIn,        -- in (127:0); Data for 2 stations delivered every clock.
        i_flagged        => BFflagged,       -- in (1:0);
        i_fine           => BFFine,          -- in (7:0); Fine channel, 0 to 53
        i_coarse         => BFCoarse,        -- in (9:0);
        i_stations01     => BFStations01,    -- in std_logic;
        i_firstStation   => BFFirstStation,  -- in std_logic; First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation    => BFLastStation,   -- in std_logic;
        i_timeStep       => BFTimeStep,      -- in (3:0); Timestep, runs from 0 to 15. There are 16 timesteps per output packet.
        i_station        => BFstation,       -- in (9:0); station
        i_virtualChannel => BFvirtualChannel, -- out (9:0);  Note takes steps of 4
        i_packetCount    => BFPacketCount,   -- in (47:0); The packet count for this packet, based on the original packet count from LFAA.
        i_outputPktOdd   => BFPacketOdd,     -- in std_logic;
        i_valid          => BFValidIn,       -- in std_logic;
        -- Polynomial data 
        i_phase_virtualChannel => BF_phase_virtualChannel, -- in t_slv_10_arr(3:0);
        i_phase_timeStep       => BF_phase_timeStep,       -- in t_slv_10_arr(3:0);
        i_phase_beam           => BF_phase_beam,           -- in t_slv_7_arr(3:0);
        i_phase                => BF_phase,                -- in t_slv_24_arr(3:0); Phase at the start of the coarse channel.
        i_phase_step           => BF_phase_step,           -- in t_slv_24_arr(3:0); Phase step per fine channel.
        i_phase_valid          => BF_phase_valid,          -- in std_logic;
        i_phase_clear          => BF_phase_clear,          -- in std_logic;
        -- Other data from the corner turn
        i_station_jones_Buffer => BFsJonesBuffer,    -- in std_logic;
        i_station_jones_status => BFsjones_status,   -- in (1:0); bit 0 = used default, bit 1 = jones valid;
        i_beam_jones_Buffer => BFbJonesBuffer,    -- in std_logic;
        i_beam_jones_status => BFbjones_status,   -- in (1:0); bit 0 = used default, bit 1 = jones valid;
        i_poly_ok        => BFpoly_ok,        -- in (1:0); The polynomials used are within their valid time range;
        i_beamsEnabled   => BFBeamsEnabled,   -- in (9:0)
        i_scale_float    => BF_scale_float,   -- in (31:0); -- sw configured scale PSS packet scale factor; 0 indicates firmware should calculate the scale factor.
        i_beamTestEnable => BF_beamTestEnable,
        -- 64 bit bus out to the 100GE Packetiser
        o_BFdata         => beamData,         -- out t_slv_64_arr(3:0);
        o_BFscale        => beamScale,        -- out t_slv_4_arr(3:0);
        o_BFpacketCount  => beamPacketCount,  -- out t_slv_48_arr(3:0);
        o_BFBeam         => beamBeam,         -- out t_slv_10_arr(3:0);
        o_BFFreqIndex    => beamFreqIndex,    -- out t_slv_11_arr(3:0);
        o_BFvalid        => beamValid,        -- out (3:0);
        o_BFjones_status => beamJonesStatus,  -- out (3:0); Common to all 4 busses
        o_BFpoly_ok      => beamPoly_ok,      -- out (1:0); Common to all 4 busses
        -- Weights bus out to the packetiser, common across all o_BFdata buses
        o_weights        => weights,
        o_weights_valid  => weights_valid,
        o_weights_sop    => weights_sop,
        --
        i_badPacket => bdbg_ILA_trigger
    );
    
    post_BF_gen : if g_INCLUDE_POST_BF generate
        gen_scales_output : FOR i in 0 to 3 GENERATE
        
            i_output_scaling : entity scaling_lib.output_scaling 
            generic map (
                g_SIM     => TRUE
            )
            port map (
                i_clk               => clk400,
                i_rst               => rst400,
                -- Data from the beamformers
                i_BFdata            => beamData(i),
                i_BFpacketCount     => beamPacketCount(i),
                i_BFBeam            => beamBeam(i),
                i_BFFreqIndex       => beamFreqIndex(i),
                i_BFjones_status    => beamJonesStatus,
                i_BFpoly_ok         => beamPoly_ok,
                i_BFscale	        => beamScale(i),
                i_BFvalid           => beamValid(i),
                i_scale_data        => BF_scale_float, -- only use when not zero. If zero, the scale factor is calculated internally.                
                -- Output to the packetiser
                o_BFdata            => scaling_beamData(i),
                o_BFpacketCount     => scaling_beamPacketCount(i),
                o_BFBeam            => scaling_beamBeam(i),
                o_BFFreqIndex       => scaling_beamFreqIndex(i),
                o_BFjones_status    => scaling_beamJonesStatus(i),
                o_BFpoly_ok         => scaling_beamPoly_ok(i),
                o_BFvalid           => scaling_beamValid(i)    
            );
        
            o_beam_output_stream(i).beamData        <= scaling_beamData(i);
            o_beam_output_stream(i).beamPacketCount <= scaling_beamPacketCount(i);
            o_beam_output_stream(i).beamBeam        <= scaling_beamBeam(i);
            o_beam_output_stream(i).beamFreqIndex   <= scaling_beamFreqIndex(i);
            o_beam_output_stream(i).beamValid       <= scaling_beamValid(i);
            o_beam_output_stream(i).beamJonesStatus <= scaling_beamJonesStatus(i);
            o_beam_output_stream(i).beamPoly_ok     <= scaling_beamPoly_ok(i);
            
            o_beam_output_stream(i).weights_data    <= weights;
            o_beam_output_stream(i).weights_data_wr <= weights_valid;
            o_beam_output_stream(i).weights_sop     <= weights_sop;
        END GENERATE;
        
        
        -- Log the output of the scaling module
        process(clk400)
        begin
            if rising_Edge(clk400) then
                scaling_beamValid_del1 <= scaling_beamValid;
                for i in 0 to 3 loop
                    if scaling_beamValid(i) = '1' and scaling_beamValid_del1(i) = '0' then
                        scaling_pcount(i) <= x"001";
                    elsif scaling_beamValid(i) = '1' then
                        scaling_pcount(i) <= std_logic_vector(unsigned(scaling_pcount(i)) + 1);
                    end if;
                end loop;
            end if;
        end process;        
        
        process
            file logfile: TEXT;
            --variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
            variable line_out : Line;
        begin
            FILE_OPEN(logfile, g_TEST_CASE & g_SCALING_OUT_FILENAME0, WRITE_MODE);
            loop
                wait until rising_edge(clk400);
    
                if scaling_beamValid(0) = '1' then
                    line_out := "";
                    hwrite(line_out,scaling_beamPacketCount(0),RIGHT,12);
                    if scaling_beamValid(0) = '1' and scaling_beamValid_del1(0) = '0' then
                        hwrite(line_out,zero12,RIGHT,4);
                    else
                        hwrite(line_out,scaling_pcount(0),RIGHT,4);
                    end if;
                    hwrite(line_out,scaling_beamBeam(0),RIGHT,4);
                    hwrite(line_out,scaling_beamFreqIndex(0),RIGHT,4);
                    hwrite(line_out,scaling_beamData(0)(63 downto 56),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(55 downto 48),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(47 downto 40),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(39 downto 32),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(31 downto 24),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(23 downto 16),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(15 downto 8),RIGHT,3);
                    hwrite(line_out,scaling_beamData(0)(7 downto 0),RIGHT,3);
                    hwrite(line_out,scaling_beamPoly_ok(0) & scaling_beamJonesStatus(0),RIGHT,3);
                    writeline(logfile,line_out);
                end if;
            end loop;
        end process;        
        
        process
            file logfile: TEXT;
            --variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
            variable line_out : Line;
        begin
            FILE_OPEN(logfile, g_TEST_CASE & g_SCALING_OUT_FILENAME1, WRITE_MODE);
            loop
                wait until rising_edge(clk400);
    
                if scaling_beamValid(1) = '1' then
                    line_out := "";
                    hwrite(line_out,scaling_beamPacketCount(1),RIGHT,12);
                    if scaling_beamValid(1) = '1' and scaling_beamValid_del1(1) = '0' then
                        hwrite(line_out,zero12,RIGHT,4);
                    else
                        hwrite(line_out,scaling_pcount(1),RIGHT,4);
                    end if;
                    hwrite(line_out,scaling_beamBeam(1),RIGHT,4);
                    hwrite(line_out,scaling_beamFreqIndex(1),RIGHT,4);
                    hwrite(line_out,scaling_beamData(1)(63 downto 56),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(55 downto 48),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(47 downto 40),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(39 downto 32),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(31 downto 24),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(23 downto 16),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(15 downto 8),RIGHT,3);
                    hwrite(line_out,scaling_beamData(1)(7 downto 0),RIGHT,3);
                    hwrite(line_out,scaling_beamPoly_ok(1) & scaling_beamJonesStatus(1),RIGHT,3);
                    writeline(logfile,line_out);
                end if;
            end loop;
        end process;
        
        process
            file logfile: TEXT;
            --variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
            variable line_out : Line;
        begin
            FILE_OPEN(logfile, g_TEST_CASE & g_SCALING_OUT_FILENAME2, WRITE_MODE);
            loop
                wait until rising_edge(clk400);
    
                if scaling_beamValid(2) = '1' then
                    line_out := "";
                    hwrite(line_out,scaling_beamPacketCount(2),RIGHT,12);
                    if scaling_beamValid(2) = '1' and scaling_beamValid_del1(2) = '0' then
                        hwrite(line_out,zero12,RIGHT,4);
                    else
                        hwrite(line_out,scaling_pcount(2),RIGHT,4);
                    end if;
                    hwrite(line_out,scaling_beamBeam(2),RIGHT,4);
                    hwrite(line_out,scaling_beamFreqIndex(2),RIGHT,4);
                    hwrite(line_out,scaling_beamData(2)(63 downto 56),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(55 downto 48),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(47 downto 40),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(39 downto 32),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(31 downto 24),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(23 downto 16),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(15 downto 8),RIGHT,3);
                    hwrite(line_out,scaling_beamData(2)(7 downto 0),RIGHT,3);
                    hwrite(line_out,scaling_beamPoly_ok(2) & scaling_beamJonesStatus(2),RIGHT,3);
                    writeline(logfile,line_out);
                end if;
            end loop;
        end process;
        
        process
            file logfile: TEXT;
            --variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
            variable line_out : Line;
        begin
            FILE_OPEN(logfile, g_TEST_CASE & g_SCALING_OUT_FILENAME3, WRITE_MODE);
            loop
                wait until rising_edge(clk400);
    
                if scaling_beamValid(3) = '1' then
                    line_out := "";
                    hwrite(line_out,scaling_beamPacketCount(3),RIGHT,12);
                    if scaling_beamValid(3) = '1' and scaling_beamValid_del1(3) = '0' then
                        hwrite(line_out,zero12,RIGHT,4);
                    else
                        hwrite(line_out,scaling_pcount(3),RIGHT,4);
                    end if;
                    hwrite(line_out,scaling_beamBeam(3),RIGHT,4);
                    hwrite(line_out,scaling_beamFreqIndex(3),RIGHT,4);
                    hwrite(line_out,scaling_beamData(3)(63 downto 56),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(55 downto 48),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(47 downto 40),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(39 downto 32),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(31 downto 24),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(23 downto 16),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(15 downto 8),RIGHT,3);
                    hwrite(line_out,scaling_beamData(3)(7 downto 0),RIGHT,3);
                    hwrite(line_out,scaling_beamPoly_ok(3) & scaling_beamJonesStatus(3),RIGHT,3);
                    writeline(logfile,line_out);
                end if;
            end loop;
        end process;
        
        
    end generate;
    
    
    -----------------------------------------------------------------------------------------
    
    m02_axi_awaddr(31 downto 30) <= "00";
    m02_axi_araddr(31 downto 30) <= "00";
    
    -- Other unused axi ports
    m02_axi_awsize <= get_axi_size(M02_DATA_WIDTH);
    m02_axi_awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
    m02_axi_bready  <= '1';  -- Always accept acknowledgement of write transactions. -- out std_logic;
    m02_axi_wstrb  <= (others => '1');  -- We always write all bytes in the bus. --  out std_logic_vector(63 downto 0);
    m02_axi_arsize  <= get_axi_size(M02_DATA_WIDTH);   -- 6 = 64 bytes per beat = 512 bit wide bus. -- out std_logic_vector(2 downto 0);
    m02_axi_arburst <= "01";    -- "01" = incrementing address for each beat in the burst. -- out std_logic_vector(1 downto 0);
    
    m02_axi_arlock <= "00";
    m02_axi_awlock <= "00";
    m02_axi_awid(0) <= '0';   -- We only use a single ID -- out std_logic_vector(0 downto 0);
    m02_axi_awcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    m02_axi_awprot  <= "000";   -- Has no effect in Vitis environment. -- out std_logic_vector(2 downto 0);
    m02_axi_awqos   <= "0000";  -- Has no effect in vitis environment, -- out std_logic_vector(3 downto 0);
    m02_axi_awregion <= "0000"; -- Has no effect in Vitis environment. -- out std_logic_vector(3 downto 0);
    m02_axi_arid(0) <= '0';     -- ID are not used. -- out std_logic_vector(0 downto 0);
    m02_axi_arcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    m02_axi_arprot  <= "000";   -- Has no effect in vitis environment; out std_logic_Vector(2 downto 0);
    m02_axi_arqos    <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    m02_axi_arregion <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    
    -- m02 HBM interface : 2nd stage corner turn between filterbanks and beamformer, 1 GByte
    HBM3G_1 : entity pss_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 32, -- : integer := 32;   -- Byte address width. This also defines the amount of data. Use the correct width for the HBM memory block, e.g. 28 bits for 256 MBytes.
        AXI_ID_WIDTH => 1, -- integer := 1;
        AXI_DATA_WIDTH => 512, -- integer := 256;  -- Must be a multiple of 32 bits.
        READ_QUEUE_SIZE => 16, --  integer := 16;
        MIN_LAG => 60,  -- integer := 80   
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 90, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 80 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk        => clk300,
        i_rst_n      => rst_n,
        axi_awaddr   => m02_axi_awaddr(31 downto 0),
        axi_awid     => m02_axi_awid, -- in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen    => m02_axi_awlen,
        axi_awsize   => m02_axi_awsize,
        axi_awburst  => m02_axi_awburst,
        axi_awlock   => m02_axi_awlock,
        axi_awcache  => m02_axi_awcache,
        axi_awprot   => m02_axi_awprot,
        axi_awqos    => m02_axi_awqos, -- in(3:0)
        axi_awregion => m02_axi_awregion, -- in(3:0)
        axi_awvalid  => m02_axi_awvalid,
        axi_awready  => m02_axi_awready,
        axi_wdata    => m02_axi_wdata,
        axi_wstrb    => m02_axi_wstrb,
        axi_wlast    => m02_axi_wlast,
        axi_wvalid   => m02_axi_wvalid,
        axi_wready   => m02_axi_wready,
        axi_bresp    => m02_axi_bresp,
        axi_bvalid   => m02_axi_bvalid,
        axi_bready   => m02_axi_bready,
        axi_bid      => m02_axi_bid, -- out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_araddr   => m02_axi_araddr(31 downto 0),
        axi_arlen    => m02_axi_arlen,
        axi_arsize   => m02_axi_arsize,
        axi_arburst  => m02_axi_arburst,
        axi_arlock   => m02_axi_arlock,
        axi_arcache  => m02_axi_arcache,
        axi_arprot   => m02_axi_arprot,
        axi_arvalid  => m02_axi_arvalid,
        axi_arready  => m02_axi_arready,
        axi_arqos    => m02_axi_arqos,
        axi_arid     => m02_axi_arid,
        axi_arregion => m02_axi_arregion,
        axi_rdata    => m02_axi_rdata,
        axi_rresp    => m02_axi_rresp,
        axi_rlast    => m02_axi_rlast,
        axi_rvalid   => m02_axi_rvalid,
        axi_rready   => m02_axi_rready,
        i_write_to_disk => write_HBM_to_disk, -- in std_logic;
        i_write_to_disk_addr => hbm_dump_addr, -- address to start the memory dump at.
        -- 4 Mbytes = first 8 data streams
        i_write_to_disk_size => 4194304, -- size in bytes
        i_fname => hbm_dump_filename, -- : in string
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem   => init_mem, --  in std_logic;
        i_init_fname => init_fname -- : in string
    );

    write_HBM_to_disk <= '0';
    hbm_dump_addr <= 0;
    init_mem <= '0';

    --------------------------------------------------------------------------
    -- Write the beamformer output to a file
    
    process(clk400)
    begin
        if rising_Edge(clk400) then
            beamValid_del1 <= beamValid;
            for i in 0 to 3 loop
                if beamValid(i) = '1' and beamValid_del1(i) = '0' then
                    pcount(i) <= x"001";
                elsif beamValid(i) = '1' then
                    pcount(i) <= std_logic_vector(unsigned(pcount(i)) + 1);
                end if;
            end loop;
        end if;
    end process;
    zero12 <= x"000";
    
    process
		file logfile: TEXT;
		--variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
		variable line_out : Line;
    begin
	    FILE_OPEN(logfile, g_TEST_CASE & g_BF_OUT_FILENAME0, WRITE_MODE);
		loop
            wait until rising_edge(clk400);

            if beamValid(0) = '1' then
                line_out := "";
                hwrite(line_out,beamPacketCount(0),RIGHT,12);
                if beamValid(0) = '1' and beamValid_del1(0) = '0' then
                    hwrite(line_out,zero12,RIGHT,4);
                else
                    hwrite(line_out,pcount(0),RIGHT,4);
                end if;
                hwrite(line_out,beamBeam(0),RIGHT,4);
                hwrite(line_out,beamFreqIndex(0),RIGHT,4);
                hwrite(line_out,beamScale(0),RIGHT,2);
                hwrite(line_out,beamData(0)(63 downto 48),RIGHT,5);
                hwrite(line_out,beamData(0)(47 downto 32),RIGHT,5);
                hwrite(line_out,beamData(0)(31 downto 16),RIGHT,5);
                hwrite(line_out,beamData(0)(15 downto 0),RIGHT,5);
                hwrite(line_out,beamPoly_ok & beamJonesStatus,RIGHT,3);
                writeline(logfile,line_out);
            end if;
        end loop;
    end process;

    process
		file logfile: TEXT;
		--variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
		variable line_out : Line;
    begin
	    FILE_OPEN(logfile, g_TEST_CASE & g_BF_OUT_FILENAME1, WRITE_MODE);
		loop
            wait until rising_edge(clk400);
            if beamValid(1) = '1' then
                line_out := "";
                hwrite(line_out,beamPacketCount(1),RIGHT,12);
                if beamValid(1) = '1' and beamValid_del1(1) = '0' then
                    hwrite(line_out,zero12,RIGHT,4);
                else
                    hwrite(line_out,pcount(1),RIGHT,4);
                end if;
                hwrite(line_out,beamBeam(1),RIGHT,4);
                hwrite(line_out,beamFreqIndex(1),RIGHT,4);
                hwrite(line_out,beamScale(1),RIGHT,2);
                hwrite(line_out,beamData(1)(63 downto 48),RIGHT,5);
                hwrite(line_out,beamData(1)(47 downto 32),RIGHT,5);
                hwrite(line_out,beamData(1)(31 downto 16),RIGHT,5);
                hwrite(line_out,beamData(1)(15 downto 0),RIGHT,5);
                hwrite(line_out,beamPoly_ok & beamJonesStatus,RIGHT,3);
                writeline(logfile,line_out);
            end if;
        end loop;
    end process;
    
   process
		file logfile: TEXT;
		--variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
		variable line_out : Line;
    begin
	    FILE_OPEN(logfile, g_TEST_CASE & g_BF_OUT_FILENAME2, WRITE_MODE);
		loop
            wait until rising_edge(clk400);
            if beamValid(2) = '1' then
                line_out := "";
                hwrite(line_out,beamPacketCount(2),RIGHT,12);
                if beamValid(2) = '1' and beamValid_del1(2) = '0' then
                    hwrite(line_out,zero12,RIGHT,4);
                else
                    hwrite(line_out,pcount(2),RIGHT,4);
                end if;
                hwrite(line_out,beamBeam(2),RIGHT,4);
                hwrite(line_out,beamFreqIndex(2),RIGHT,4);
                hwrite(line_out,beamScale(2),RIGHT,2);
                hwrite(line_out,beamData(2)(63 downto 48),RIGHT,5);
                hwrite(line_out,beamData(2)(47 downto 32),RIGHT,5);
                hwrite(line_out,beamData(2)(31 downto 16),RIGHT,5);
                hwrite(line_out,beamData(2)(15 downto 0),RIGHT,5);
                hwrite(line_out,beamPoly_ok & beamJonesStatus,RIGHT,3);
                writeline(logfile,line_out);
            end if;
        end loop;
    end process;
    
   process
		file logfile: TEXT;
		--variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
		variable line_out : Line;
    begin
	    FILE_OPEN(logfile, g_TEST_CASE & g_BF_OUT_FILENAME3, WRITE_MODE);
		loop
            wait until rising_edge(clk400);
            if beamValid(3) = '1' then
                line_out := "";
                hwrite(line_out,beamPacketCount(3),RIGHT,12);
                if beamValid(3) = '1' and beamValid_del1(3) = '0' then
                    hwrite(line_out,zero12,RIGHT,4);
                else
                    hwrite(line_out,pcount(3),RIGHT,4);
                end if;
                hwrite(line_out,beamBeam(3),RIGHT,4);
                hwrite(line_out,beamFreqIndex(3),RIGHT,4);
                hwrite(line_out,beamScale(3),RIGHT,2);
                hwrite(line_out,beamData(3)(63 downto 48),RIGHT,5);
                hwrite(line_out,beamData(3)(47 downto 32),RIGHT,5);
                hwrite(line_out,beamData(3)(31 downto 16),RIGHT,5);
                hwrite(line_out,beamData(3)(15 downto 0),RIGHT,5);
                hwrite(line_out,beamPoly_ok & beamJonesStatus,RIGHT,3);
                writeline(logfile,line_out);
            end if;
        end loop;
    end process;    
    
    -------------------------------------------------------------------------------------------------------------
    
    post_BF_gen2 : if g_INCLUDE_POST_BF generate    
        
        args_packetiser_enable_proc : process(clk300)
        begin
            if rising_edge(clk300) then
                -- 0 - enable packetiser
                -- 1 - enable test gen
                -- 2 - limited runs
                -- 3 - use defaults
                --  0x03 = Run Test gen
                --  0x09 = Run packetiser with defaults, assume BF sourced data.
                if (clock_300_rst = '1') then
                    packetiser_host_bus_ctrl.instruct    <= x"00";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                else 
                    if (testCount_300 = 1000) then
                        packetiser_host_bus_ctrl.instruct    <= x"01";
                        packetiser_host_bus_ctrl_2.instruct  <= x"00";
                        packetiser_host_bus_ctrl_3.instruct  <= x"00";
                    end if;
                    if (testCount_300 = 20000) then
                        packetiser_host_bus_ctrl.instruct    <= x"01";
                        packetiser_host_bus_ctrl_2.instruct  <= x"00";
                        packetiser_host_bus_ctrl_3.instruct  <= x"00";
                    end if;
                    if (testCount_300 = 200000) then
                        packetiser_host_bus_ctrl.instruct    <= x"01";
                        packetiser_host_bus_ctrl_2.instruct  <= x"00";
                        packetiser_host_bus_ctrl_3.instruct  <= x"00";
                    end if;
                end if;
            end if;
        end process;
         
        cmac_emulator : process(clock_322)
        begin
            if rising_edge(clock_322) then
                if (testCount_322 > 15) then
                    if testCount_322 = 200 or testCount_322 = 250 then
                        i_data_to_transmit_ctl.ready    <= '0';
                        i_tx_axis_tready                <= '0';
                    else    
                        i_data_to_transmit_ctl.ready    <= '1';
                        i_tx_axis_tready                <= '1';
                    end if;
                else
                    i_data_to_transmit_ctl.ready        <= '0';   
                    i_data_to_transmit_ctl.overflow     <= '0';
                    i_data_to_transmit_ctl.underflow    <= '0';
                    i_tx_axis_tready                    <= '0';     
                end if;
            end if;
        end process;
        
        packetiser_stream_1_host_bus_in.config_data_clk <= clk400;
        packetiser_stream_2_host_bus_in.config_data_clk <= clk400;
        packetiser_stream_3_host_bus_in.config_data_clk <= clk400;
        
        ---------------------------------------------------------
        i_beam_output_stream_1      <= o_beam_output_stream;
        
        -- DUT/UUT
        DUT_PSR : entity PSR_Packetiser_lib.psr_packetiser100G_Top 
        Generic Map (
            g_DEBUG_ILA                 => TRUE,
            g_Number_of_streams         => 2,
            g_PST_config                => FALSE
        )
        Port Map ( 
            -- ~322 MHz
            i_cmac_clk                  => clock_322,
            i_cmac_rst                  => clock_322_rst,
            
            i_packetiser_clk            => clk400,
            i_packetiser_rst            => clock_400_rst,
            
            -- Lbus to MAC
            o_data_to_transmit          => o_data_to_transmit,
            i_data_to_transmit_ctl      => i_data_to_transmit_ctl,
            
            -- AXI to CMAC interface to be implemented
            o_tx_axis_tdata             => o_tx_axis_tdata,
            o_tx_axis_tkeep             => o_tx_axis_tkeep,
            o_tx_axis_tvalid            => o_tx_axis_tvalid,
            o_tx_axis_tlast             => o_tx_axis_tlast,
            o_tx_axis_tuser             => o_tx_axis_tuser,
            i_tx_axis_tready            => i_tx_axis_tready,
            
            -- beamformer output streams
            i_beam_output_stream_1      => i_beam_output_stream_1,
            i_beam_output_stream_2      => i_beam_output_stream_2,
    
            -- signals from signal processing/HBM/the moon/etc
            i_packet_stream_ctrl(0)     => packetiser_host_bus_ctrl,
            i_packet_stream_ctrl(1)     => packetiser_host_bus_ctrl_2,
            i_packet_stream_ctrl(2)     => packetiser_host_bus_ctrl_3,
            
            o_packet_stream_stats       => packet_stream_stats,
                    
            i_packet_stream(0)          => beamformer_to_packetiser_data,
            i_packet_stream(1)          => beamformer_to_packetiser_data,
            i_packet_stream(2)          => beamformer_to_packetiser_data,
            o_packet_stream_out         => open,
            
            -- AXI BRAM to packetiser
            i_packet_config_in_stream_1 => packetiser_stream_1_host_bus_in,
            i_packet_config_in_stream_2 => packetiser_stream_2_host_bus_in,
            i_packet_config_in_stream_3 => packetiser_stream_3_host_bus_in,
            
            -- AXI BRAM return path from packetiser 
            o_packet_config_stream_1    => packetiser_host_bus_out.config_data_out,
            o_packet_config_stream_2    => packetiser_host_bus_out_2.config_data_out,
            o_packet_config_stream_3    => packetiser_host_bus_out_3.config_data_out
            
        );
        
        --------------------------------------------------------------------------------------------------
        packetiser_host : entity PSR_Packetiser_lib.cmac_args_axi_wrapper 
            Port Map ( 
            
                -- ARGS interface
                -- MACE clock is 300 MHz
                i_MACE_clk                          => clk300,
                i_MACE_rst                          => clock_300_rst,
                
                i_packetiser_clk                    => clk400,
                
                i_PSR_packetiser_Lite_axi_mosi      => i_PSR_packetiser_Lite_axi_mosi(0),
                o_PSR_packetiser_Lite_axi_miso      => o_PSR_packetiser_Lite_axi_miso(0),
                
                i_axi_mosi                          => i_PSR_packetiser_Full_axi_mosi(0),
                o_axi_miso                          => o_PSR_packetiser_Full_axi_miso(0),
                
                o_packet_stream_ctrl                => packetiser_host_bus_ctrl_hold,
                        
                i_packet_stream_stats               => packet_stream_stats(0),
                        
                o_packet_config                     => packetiser_stream_1_host_bus_in_hold,
                i_packet_config_out                 => packetiser_host_bus_out
        
            );
            
    end generate;
    
end Behavioral;
