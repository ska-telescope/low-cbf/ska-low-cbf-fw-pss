----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 19.03.2024
-- Module Name: ct2_poly_eval - Behavioral
-- Description: 
--  Polynomial configuration memory for the PST beamformer.
--  
--  Memory space to specify parameters for up to 512 stations x 16 beams, i.e. 8192 polynomials. 
--  Each polynomial is 6 single precision floating point values, i.e. 24 bytes
--  Total space allocated : (512 stations) * (16 beams) * (32 (rounded up from 24 bytes)) * (2 buffers) = 524288 bytes 
--
--  Byte Address 
--    (even indexed stations) = (buffer * 196608) + (beam * 12288) + ((station//2) * 24)
--    (odd  indexed stations) = (buffer * 196608) + (beam * 12288) + 6144 + ((station//2) * 24)
--
--  where buffer = 0 or 1, beam = 0:15, station = 0:511 
--  The total space allocated is 131072*4 = 524288 bytes, but only 393216 bytes are implemented 
--   
--  second block of data at byte address 393216: 
--   8192 bytes : 
--   for each of 1024 virtual channels, we need 
--    sky frequency - 4 byte single precision 
--    station number - 9 bits, used to look up the polynomial 
--   
-- 
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib, bf_lib, xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;


entity ct2_poly_mem is
    port(
        -- Registers axi full interface
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        i_axi_mosi : in  t_axi4_full_mosi;
        o_axi_miso : out t_axi4_full_miso;
        -- Memory interface
        i_BF_clk : in std_logic;
        -- 4 clock read latency
        i_vcmap_RdAddr : in std_logic_vector(9 downto 0);
        o_vcmap_dout : out std_logic_vector(63 downto 0);
        -- second port to read the same memory as i_vcmap_rdAddr
        i_vcmap_RdAddr2 : in std_logic_vector(9 downto 0);
        o_vcmap_dout2 : out std_logic_vector(63 downto 0);
        -- 15 clock read latency (chained ultraRAMs, 12 deep to get 49152 = 12 * 4096) 
        -- Read data for even indexed stations
        i_polymem_even_RdAddr : in std_logic_vector(14 downto 0);
        o_polymem_even_dout : out std_logic_vector(63 downto 0);
        -- Read data for odd indexed stations
        i_polymem_odd_RdAddr : in std_logic_vector(14 downto 0);
        o_polymem_odd_dout : out std_logic_vector(63 downto 0) 
    );
end ct2_poly_mem;

architecture Behavioral of ct2_poly_mem is

    component axi_bram_ctrl_ct2
    port (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;

    COMPONENT axi_clock_converter_ct2
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        m_axi_aclk : IN STD_LOGIC;
        m_axi_aresetn : IN STD_LOGIC;
        m_axi_awaddr : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
        m_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid : OUT STD_LOGIC;
        m_axi_awready : IN STD_LOGIC;
        m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast : OUT STD_LOGIC;
        m_axi_wvalid : OUT STD_LOGIC;
        m_axi_wready : IN STD_LOGIC;
        m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid : IN STD_LOGIC;
        m_axi_bready : OUT STD_LOGIC;
        m_axi_araddr : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
        m_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid : OUT STD_LOGIC;
        m_axi_arready : IN STD_LOGIC;
        m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast : IN STD_LOGIC;
        m_axi_rvalid : IN STD_LOGIC;
        m_axi_rready : OUT STD_LOGIC);
    end component;
    
    COMPONENT ila_pst
    PORT (
   	    clk : IN STD_LOGIC;
   	    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;    
    
    signal polyMemRst   : std_logic;
    signal polyMemClk   : std_logic;
    signal polyMemEn    : std_logic;
    signal polyMemAddr  : std_logic_vector(18 downto 0);
    signal polyMemDin   : std_logic_vector(31 downto 0);
    
    signal MACE_rstn : std_logic;
    
    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;    
    signal awlock_slv : std_logic_vector(0 downto 0);
    signal arlock_slv : std_logic_vector(0 downto 0);
    
    signal axi_mosi_arlock : std_logic_vector(0 downto 0);
    signal axi_mosi_awlock : std_logic_vector(0 downto 0);
    
    signal polyMemWrEn_even, polyMemWrEn_odd, vcmapMemWrEn : std_logic_vector(7 downto 0) := x"00";
    
    signal memEn : std_logic;
    signal memWrEn : std_logic_vector(3 downto 0);
    signal memAddr, memAddr_even, memAddr_odd : std_logic_vector(18 downto 0);
    signal memDin, memDin_del1 : std_logic_vector(31 downto 0);
    signal memDout : std_logic_vector(31 downto 0);
    signal MACE_rstn_BFclk, rst_BFclk : std_logic;
    signal memDoutSelDel : t_slv_3_arr(13 downto 0);
    signal polyMem_even_douta, polyMem_odd_douta : std_logic_vector(63 downto 0);
    signal vcmap_douta : std_logic_vector(63 downto 0);
    signal dina : std_logic_vector(63 downto 0);
    signal rd_even_del : std_logic_vector(13 downto 0);
    
begin

    MACE_rstn <= not i_MACE_rst;

    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => rst_BFclk,  -- 1-bit output
        dest_clk => i_BF_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',
        src_clk => i_MACE_clk,    -- 1-bit input: Source clock.
        src_pulse => i_MACE_rst,  -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            MACE_rstn_BFclk <= not rst_BFclk;
        end if;
    end process;
    
    awlock_slv(0) <= i_axi_mosi.awlock;
    arlock_slv(0) <= i_axi_mosi.arlock;
    

    BF_cci : axi_clock_converter_ct2
    port map (
        s_axi_aclk    => i_MACE_clk, -- IN STD_LOGIC;
        s_axi_aresetn => MACE_rstn, -- IN STD_LOGIC;
        s_axi_awaddr    => i_axi_mosi.awaddr(18 downto 0),
        s_axi_awlen     => i_axi_mosi.awlen,
        s_axi_awsize    => i_axi_mosi.awsize,
        s_axi_awburst   => i_axi_mosi.awburst,
        s_axi_awlock    => awlock_slv,
        s_axi_awcache   => i_axi_mosi.awcache,
        s_axi_awprot    => i_axi_mosi.awprot,
        s_axi_awregion => (others => '0'), -- in(3:0);
        s_axi_awqos    => (others => '0'), -- in(3:0);
        s_axi_awvalid   => i_axi_mosi.awvalid,
        s_axi_awready   => o_axi_miso.awready,        
        
        s_axi_wdata     => i_axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => i_axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => i_axi_mosi.wlast,
        s_axi_wvalid    => i_axi_mosi.wvalid,
        s_axi_wready    => o_axi_miso.wready,
        
        s_axi_bresp     => o_axi_miso.bresp,
        s_axi_bvalid    => o_axi_miso.bvalid,
        s_axi_bready    => i_axi_mosi.bready ,

        s_axi_araddr    => i_axi_mosi.araddr(18 downto 0),
        s_axi_arlen     => i_axi_mosi.arlen,
        s_axi_arsize    => i_axi_mosi.arsize,
        s_axi_arburst   => i_axi_mosi.arburst,
        s_axi_arlock    => arlock_slv,
        s_axi_arcache   => i_axi_mosi.arcache,
        s_axi_arprot    => i_axi_mosi.arprot,
        s_axi_arregion  => "0000", -- in(3:0),
        s_axi_arqos     => "0000", -- in(3:0),
        s_axi_arvalid   => i_axi_mosi.arvalid,
        s_axi_arready   => o_axi_miso.arready,
          
        s_axi_rdata     => o_axi_miso.rdata(31 downto 0),
        s_axi_rresp     => o_axi_miso.rresp,
        s_axi_rlast     => o_axi_miso.rlast,
        s_axi_rvalid    => o_axi_miso.rvalid,
        s_axi_rready    => i_axi_mosi.rready,
        -- master interface

        m_axi_aclk    => i_BF_clk, -- in std_logic;
        m_axi_aresetn => MACE_rstn_BFclk, -- in std_logic;
        m_axi_awaddr  => axi_mosi.awaddr(18 downto 0), -- out STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_awlen   => axi_mosi.awlen, -- out STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize  => axi_mosi.awsize, -- out STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst => axi_mosi.awburst, -- out STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock  => axi_mosi_awlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache => axi_mosi.awcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot  => axi_mosi.awprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion => axi_mosi.awregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos   => axi_mosi.awqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid => axi_mosi.awvalid, -- OUT STD_LOGIC;
        m_axi_awready => axi_miso.awready, -- IN STD_LOGIC;
        m_axi_wdata  => axi_mosi.wdata(31 downto 0), -- OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb  => axi_mosi.wstrb(3 downto 0), -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast  => axi_mosi.wlast, -- OUT STD_LOGIC;
        m_axi_wvalid => axi_mosi.wvalid, -- OUT STD_LOGIC;
        m_axi_wready => axi_miso.wready, -- IN STD_LOGIC;
        m_axi_bresp  => axi_miso.bresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid => axi_miso.bvalid, -- IN STD_LOGIC;
        m_axi_bready => axi_mosi.bready, -- OUT STD_LOGIC;
        m_axi_araddr => axi_mosi.araddr(18 downto 0), -- OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_arlen  => axi_mosi.arlen, -- OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize => axi_mosi.arsize, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst => axi_mosi.arburst, -- OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock  => axi_mosi_arlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache => axi_mosi.arcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot  => axi_mosi.arprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion => axi_mosi.arregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos => axi_mosi.arqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid => axi_mosi.arvalid, -- OUT STD_LOGIC;
        m_axi_arready => axi_miso.arready, -- IN STD_LOGIC;
        m_axi_rdata   => axi_miso.rdata(31 downto 0), -- IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp   => axi_miso.rresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast   => axi_miso.rlast, -- IN STD_LOGIC;
        m_axi_rvalid  => axi_miso.rvalid, -- IN STD_LOGIC;
        m_axi_rready => axi_mosi.rready  -- OUT STD_LOGIC
    );

    axi_mosi.awlock <= axi_mosi_awlock(0);
    axi_mosi.arlock <= axi_mosi_arlock(0);
    
    -- Convert register interface from AXI full to address + data
    -- 14 clock read latency to allow pipelining of the ultraRAMs
    BF_ctrli : axi_bram_ctrl_ct2
    port map (
        s_axi_aclk      => i_BF_clk,
        s_axi_aresetn   => MACE_rstn_BFclk, -- in std_logic;
        s_axi_awaddr    => axi_mosi.awaddr(18 downto 0),
        s_axi_awlen     => axi_mosi.awlen,
        s_axi_awsize    => axi_mosi.awsize,
        s_axi_awburst   => axi_mosi.awburst,
        s_axi_awlock    => axi_mosi.awlock ,
        s_axi_awcache   => axi_mosi.awcache,
        s_axi_awprot    => axi_mosi.awprot,
        s_axi_awvalid   => axi_mosi.awvalid,
        s_axi_awready   => axi_miso.awready,
        s_axi_wdata     => axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => axi_mosi.wlast,
        s_axi_wvalid    => axi_mosi.wvalid,
        s_axi_wready    => axi_miso.wready,
        s_axi_bresp     => axi_miso.bresp,
        s_axi_bvalid    => axi_miso.bvalid,
        s_axi_bready    => axi_mosi.bready ,
        s_axi_araddr    => axi_mosi.araddr(18 downto 0),
        s_axi_arlen     => axi_mosi.arlen,
        s_axi_arsize    => axi_mosi.arsize,
        s_axi_arburst   => axi_mosi.arburst,
        s_axi_arlock    => axi_mosi.arlock ,
        s_axi_arcache   => axi_mosi.arcache,
        s_axi_arprot    => axi_mosi.arprot,
        s_axi_arvalid   => axi_mosi.arvalid,
        s_axi_arready   => axi_miso.arready,
        s_axi_rdata     => axi_miso.rdata(31 downto 0),
        s_axi_rresp     => axi_miso.rresp,
        s_axi_rlast     => axi_miso.rlast,
        s_axi_rvalid    => axi_miso.rvalid,
        s_axi_rready    => axi_mosi.rready,
        bram_rst_a      => open,     -- out std_logic;
        bram_clk_a      => open,     -- !!! check what to do with this   -- out std_logic;  
        bram_en_a       => memEn,      -- out std_logic;
        bram_we_a       => memWrEn,    -- out (3:0)
        bram_addr_a     => memAddr,    -- out (18:0); 
        bram_wrdata_a   => memDin,     -- out (31:0)
        bram_rddata_a   => memDout     -- in (31:0)
    );
    
    process(i_BF_clk)
    begin
        if rising_Edge(i_BF_clk) then
            if (unsigned(memAddr) < 393216) then
                if memAddr(2) = '0' then
                    memDoutSelDel(0) <= "100"; -- for polyMem_douta(31 downto 0);
                else
                    memDoutSelDel(0) <= "101"; -- for polyMem_douta(63 downto 32);
                end if;
            elsif (unsigned(memAddr) < 401408) then
                if memAddr(2) = '0' then
                    memDoutSelDel(0) <= "110"; -- for vcmap_douta(31 downto 0);
                else
                    memDoutSelDel(0) <= "111"; -- for vcmap_douta(63 downto 32);
                end if;
            else
                memDoutSelDel(0) <= "000";
            end if;
            
            if ((unsigned(memAddr) < 98304) or ((unsigned(memAddr) >= 196608) and (unsigned(memAddr) < 294912))) then
                rd_even_del(0) <= '1';
            else
                rd_even_del(0) <= '0';
            end if;
            
            -- 14 cycle read latency to allow pipeline stages between the chained urams
            memDoutSelDel(13 downto 1) <= memDoutSelDel(12 downto 0);
            rd_even_del(13 downto 1) <= rd_even_del(12 downto 0);
            
            if (memDoutSelDel(13)(2) = '0') then
                memDout <= (others => '0'); -- read from address space that doesn't map to any actual memory
            else
                if (memDoutSelDel(13)(1 downto 0) = "00") then
                    if rd_even_del(13) = '1' then
                        memDout <= polyMem_even_douta(31 downto 0);
                    else
                        memDout <= polyMem_odd_douta(31 downto 0);
                    end if;
                elsif (memDoutSelDel(13)(1 downto 0) = "01") then
                    if rd_even_del(13) = '1' then
                        memDout <= polyMem_even_douta(63 downto 32);
                    else
                        memDout <= polyMem_odd_douta(63 downto 32);
                    end if;
                elsif (memDoutSelDel(13)(1 downto 0) = "10") then
                    memDout <= vcmap_douta(31 downto 0);
                else
                    memDout <= vcmap_douta(63 downto 32);
                end if;
            end if;
            
            -- polynomial memory is address 0 to 393215 = 0x0 to 0x5FFFF
            -- Byte address      0:98303  = even memory address 0:98303 
            --               98304:196607 = odd memory address 0:98303
            --              196608:294911 = Even memory address 98304:196607
            --              294912:393215 = Odd memory address  98304:196607
            if memEn = '1' and memWrEn(0) = '1' and ((unsigned(memAddr) < 98304) or ((unsigned(memAddr) >= 196608) and (unsigned(memAddr) < 294912))) then
                if memAddr(2) = '0' then
                    polyMemWrEn_even <= "00001111";
                else
                    polyMemWrEn_even <= "11110000";
                end if;
            else
                polyMemWrEn_even <= "00000000";
            end if;
            if unsigned(memAddr) < 98304 then
                memAddr_even <= memAddr;
            else
                memAddr_even <= std_logic_vector(unsigned(memAddr) - 98304);
            end if;
            
            memDin_del1 <= memDin;
            
            if memEn = '1' and memWrEn(0) = '1' and 
               (((unsigned(memAddr) >= 98304) and (unsigned(memAddr) < 196608)) or 
                ((unsigned(memAddr) >= 294912) and (unsigned(memAddr) < 393216))) then
                if memAddr(2) = '0' then
                    polyMemWrEn_odd <= "00001111";
                else
                    polyMemWrEn_odd <= "11110000";
                end if;
            else
                polyMemWrEn_odd <= "00000000";
            end if;
            if unsigned(memAddr) < 196608 then
                memAddr_odd <= std_logic_vector(unsigned(memAddr) - 98304);
            else
                memAddr_odd <= std_logic_vector(unsigned(memAddr) - 196608);
            end if;
            
            -- vcmap memory address is 393216 to 401407 = 0x60000 to 0x61FFF
            if memEn = '1' and memWrEn(0) = '1' and memAddr(18 downto 13) = "110000" then
                if memAddr(2) = '0' then
                    vcmapMemWrEn <= "00001111";
                else
                    vcmapMemWrEn <= "11110000";
                end if;
            else
                vcmapMemWrEn <= "00000000";
            end if;
            
        end if;
    end process;
    
    --------------------------------------------------------------------------
    -- ultraRAM for the polynomial coefficients
    --  (49152 deep) x (8 bytes wide)
    --
    -- xpm_memory_tdpram: True Dual Port RAM
    -- Xilinx Parameterized Macro, version 2022.2
    xpm_memory_tdpram_poly_i : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 15,              -- DECIMAL
        ADDR_WIDTH_B => 15,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,        -- DECIMAL
        BYTE_WRITE_WIDTH_B => 8,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => 24576*64,         -- DECIMAL
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 64,         -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_A => 13,            -- DECIMAL
        READ_LATENCY_B => 14,            -- DECIMAL
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 64,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        dbiterra => open,        -- 1-bit output: Status signal to indicate double bit error
        dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error
        douta => polyMem_even_douta,  -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => o_polymem_even_dout, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,        -- 1-bit output: Status signal to indicate single bit error occurrence
        sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error 
        addra => memAddr_even(17 downto 3), -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => i_polymem_even_RdAddr, -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_BF_clk,        -- 1-bit input: Clock signal for port A. Also clocks port B.
        clkb => i_BF_clk,        -- 1-bit input, unused since common clock
        dina => dina,            -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => (others => '0'), -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',              -- 1-bit input: Memory enable signal for port A
        enb => '1',              -- 1-bit input: Memory enable signal for port B. 
        injectdbiterra => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectdbiterrb => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectsbiterra => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        injectsbiterrb => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        regcea => '1',      -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',      -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',        -- 1-bit input: Reset signal for the final port A output register stage.
        rstb => '0',        -- 1-bit input: Reset signal for the final port B output register stage.
        sleep => '0',       -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => polyMemWrEn_even, -- write enable 8 bits; 1 bit per byte
        web => "00000000"   -- write enable 8 bits; 1 bit per byte.
    );

    xpm_memory_tdpram_polyodd_i : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 15,              -- DECIMAL
        ADDR_WIDTH_B => 15,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL
        BYTE_WRITE_WIDTH_B => 8,         -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => 24576*64,         -- DECIMAL
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 64,         -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_A => 13,            -- DECIMAL
        READ_LATENCY_B => 14,            -- DECIMAL
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 64,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        dbiterra => open,        -- 1-bit output: Status signal to indicate double bit error
        dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error
        douta => polyMem_odd_douta,  -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => o_polymem_odd_dout, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,        -- 1-bit output: Status signal to indicate single bit error occurrence
        sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error 
        addra => memAddr_odd(17 downto 3), -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => i_polymem_odd_RdAddr, -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_BF_clk,        -- 1-bit input: Clock signal for port A. Also clocks port B.
        clkb => i_BF_clk,        -- 1-bit input, unused since common clock
        dina => dina,            -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => (others => '0'), -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',              -- 1-bit input: Memory enable signal for port A
        enb => '1',              -- 1-bit input: Memory enable signal for port B. 
        injectdbiterra => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectdbiterrb => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectsbiterra => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        injectsbiterrb => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        regcea => '1',      -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',      -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',        -- 1-bit input: Reset signal for the final port A output register stage.
        rstb => '0',        -- 1-bit input: Reset signal for the final port B output register stage.
        sleep => '0',       -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => polyMemWrEn_odd, -- write enable 8 bits; 1 bit per byte
        web => "00000000"   -- write enable 8 bits; 1 bit per byte.
    );
    
    dina <= memDin_del1 & memDin_del1;
    --------------------------------------------------------------------------
    -- BRAM for the virtual channel mapping table
    --  = (1024 deep) x (8 byte wide)
    --
    xpm_memory_vcmapi : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 10,              -- DECIMAL
        ADDR_WIDTH_B => 10,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL
        BYTE_WRITE_WIDTH_B => 8,         -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 1024*64,          -- DECIMAL
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 64,         -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_A => 13,            -- DECIMAL -- port A used to read back over the register interface, latency needs to match the ultraRAM latency.
        READ_LATENCY_B => 3,             -- DECIMAL -- Port B used by the polynomial evaluation side, 3 cycle latency.
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 64,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        dbiterra => open,        -- 1-bit output: Status signal to indicate double bit error
        dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error
        douta => vcmap_douta,    -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => o_vcmap_dout,   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,        -- 1-bit output: Status signal to indicate single bit error occurrence
        sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error 
        addra => memAddr(12 downto 3), -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => i_vcmap_Rdaddr,  -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_BF_clk,        -- 1-bit input: Clock signal for port A. Also clocks port B.
        clkb => i_BF_clk,        -- 1-bit input, unused since common clock
        dina => dina,            -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => (others => '0'), -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',              -- 1-bit input: Memory enable signal for port A
        enb => '1',              -- 1-bit input: Memory enable signal for port B. 
        injectdbiterra => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectdbiterrb => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectsbiterra => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        injectsbiterrb => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        regcea => '1',    -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',    -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',      -- 1-bit input: Reset signal for the final port A output register stage.
        rstb => '0',      -- 1-bit input: Reset signal for the final port B output register stage.
        sleep => '0',     -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => vcmapMemWrEn, -- write enable 8 bits; 1 bit per byte
        web => "00000000"    -- write enable 8 bits; 1 bit per byte.
    );
    
    -- Duplicate the memory to provide a second read port
    xpm_memory_vcmap2i : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => 10,              -- DECIMAL
        ADDR_WIDTH_B => 10,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL
        BYTE_WRITE_WIDTH_B => 8,         -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 1024*64,          -- DECIMAL
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_A => 64,         -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_A => 13,            -- DECIMAL -- port A used to read back over the register interface, latency needs to match the ultraRAM latency.
        READ_LATENCY_B => 3,             -- DECIMAL -- Port B used by the polynomial evaluation side, 3 cycle latency.
        READ_RESET_VALUE_A => "0",       -- String
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_DATA_WIDTH_B => 64,        -- DECIMAL
        WRITE_MODE_A => "no_change",     -- String
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        dbiterra => open,        -- 1-bit output: Status signal to indicate double bit error
        dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error
        douta => open,    -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
        doutb => o_vcmap_dout2,  -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterra => open,        -- 1-bit output: Status signal to indicate single bit error occurrence
        sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error 
        addra => memAddr(12 downto 3), -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
        addrb => i_vcmap_Rdaddr2,  -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
        clka => i_BF_clk,        -- 1-bit input: Clock signal for port A. Also clocks port B.
        clkb => i_BF_clk,        -- 1-bit input, unused since common clock
        dina => dina,            -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        dinb => (others => '0'), -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
        ena => '1',              -- 1-bit input: Memory enable signal for port A
        enb => '1',              -- 1-bit input: Memory enable signal for port B. 
        injectdbiterra => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectdbiterrb => '0',   -- 1-bit input: Controls double bit error injection on input data 
        injectsbiterra => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        injectsbiterrb => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
        regcea => '1',    -- 1-bit input: Clock Enable for the last register stage on the output data path.
        regceb => '1',    -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rsta => '0',      -- 1-bit input: Reset signal for the final port A output register stage.
        rstb => '0',      -- 1-bit input: Reset signal for the final port B output register stage.
        sleep => '0',     -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => vcmapMemWrEn, -- write enable 8 bits; 1 bit per byte
        web => "00000000"    -- write enable 8 bits; 1 bit per byte.
    );
    
end Behavioral;


