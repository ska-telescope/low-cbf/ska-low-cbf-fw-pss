----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 19.03.2024
-- Module Name: ct2_poly_eval - Behavioral
-- Description: 
--   Polynomial evaluation for PST beamformer.
-- 
-- Data output :
--  The data output needs to match the order it is expected in the beamformer :
--
--    For timeGroup = 0:(<time samples per corner turn>/16 - 1) -- 53 ms corner turn, So 0:(768/16-1) = 0:47
--       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 16 + (0:15)                  -- 16 times needed for an output packet
--             For Station_block = 0:(i_stations/4 - 1)
--                For fine = 0:53                               -- 54 fine channels per SPS channel
--                   For station = 0:1
--                      Send a 128 bit word (i.e. 2 stations fine channels x (2 pol) x (16+16 bit complex data)).
--
--  This means the delays need to be generated in the following order :
--
--    For timeGroup = 0:(<time samples per corner turn>/16 - 1) -- 53 ms corner turn, So 0:(768/16-1) = 0:47
--       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 16 + (0:15)                  -- 16 times needed for an output packet
--
--             Note : Minimum gap between steps
--             in "Time" is 108 clocks
--
--             For Station = 0:(i_stations-1)                   -- Up to 512 stations
--                For beam = 0:(i_beams-1)                      -- Up to 16 beams
--                
--
-- Calculations :
--   - Determine the time to use for "t" in the polynomial.
--      - To calculate t :
--          - Find time as an integer in nanoseconds:
--              ct_offset_epoch =  i_time_base_ns + (i_timegroup * 32*192*1080ns) [note 32*192*1080= 6635520 ns]
--          - For each new time sample within a corner turn frame, 
--              add (64*1080ns) = 69120 ns
--              ct_offset_epoch = ct_offset_epoch + 69120
--      - t calculation uses integer nanoseconds precision
--          - Single precision cannot work - not enough bits to accurately add corner turn frame length after a few minutes 
--          - time since epoch needs to allow for at least 10 minutes = 6*10^11 = 40 bits required
--            But should allow for a day = 47 bits  
--      - Convert t to floating point t_ns = double(uint64(ct_offset_epoch)) 
--      - get t_seconds = double(1e-9) * t_ns
--      - Compute powers of t_seconds using double precision multiplier
--      - Convert to single precision
--
----------------------------------------------------------------------------------

library IEEE, axi4_lib, common_lib, bf_lib, xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;

entity ct2_poly_time is
    port(
        clk : in std_logic;
        ---------------------------------------------------------------------
        -- start getting times from (i_ct_frame + i_timeGroup)
        -- Time group within the corner turn frame. Each time group is 16 time samples = 16*64*1080ns = 1105920 ns
        i_start        : in std_logic;
        i_timeGroup    : in std_logic_vector(5 downto 0);   -- 48 time groups per frame, 16 step per group = 768 time samples per frame.
        i_time_base_ns : in std_logic_vector(63 downto 0);
        o_idle         : out std_logic;
        ---------------------------------------------------------------------
        -- Output times as single precision floating point values
        -- Each pulse on i_start generates 32 sets of times in the output FIFO. 
        -- for 16 times separated by 64*1080 = 69.12 us
        o_t1 : out std_logic_vector(31 downto 0);  -- time used in the polynomial in seconds
        o_t2 : out std_logic_vector(31 downto 0);  -- time squared
        o_t3 : out std_logic_vector(31 downto 0);  -- time cubed
        o_t4 : out std_logic_vector(31 downto 0);  -- time to the fourth
        o_t5 : out std_logic_vector(31 downto 0);  -- time to the fifth
        o_step : out std_logic_vector(9 downto 0); -- time sample within the frame for the current outputs, 0 to 767.
        o_t_valid : out std_logic;
        i_fifo_rdEn : in std_logic; -- outputs valid one clock after i_fifo_RdEn, if there is data in the fifo.
        i_fifo_rst : in std_logic;
        o_fifo_rd_count : out std_logic_vector(6 downto 0)    -- only sets o_t_valid is i_t_rdy is high.
    );
end ct2_poly_time;

architecture Behavioral of ct2_poly_time is
    
    -- double precision 1e-9
    constant c_fp64_1eMinus9 : std_logic_vector(63 downto 0) := x"3E112E0BE826D695";
    
    ---------------------------------------------------------------
    
    -- double precision multiplier, 12 cycle latency
    component fp64_mult
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    end component;
    
    -- uint64 to double, 6 cycle latency
    component uint64_to_double
    port (
        aclk : in std_logic;
        s_axis_a_tvalid : in std_logic;
        s_axis_a_tdata : in std_logic_vector(63 downto 0);
        m_axis_result_tvalid : out std_logic;
        m_axis_result_tdata : out std_logic_vector(63 downto 0));
    end component;
    
    component fp64_to_fp32
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;
    
    type times_fsm_type is (get_offset_epoch_start0, get_offset_epoch_start1, convert_to_fp64, convert_to_seconds, wait_mult_1e_minus9,
                            get_s, wait_s2, get_s2, wait_s3, get_s3, wait_s4, get_s4, wait_s5, get_s5, wait_fifo_write, get_next_s, done);
    signal times_fsm : times_fsm_type := done;
    signal times_fsm_del1, times_fsm_del2, times_fsm_del3, times_fsm_del4 : times_fsm_type := done;
    
    signal timeGroup : std_logic_vector(5 downto 0);
    signal timeGroup_x1105920 : std_logic_vector(63 downto 0);
    signal ct_frame_offset_ns : std_logic_vector(63 downto 0);
    signal fsm_waitcount : std_logic_vector(5 downto 0) := "000000";
    signal time_in_timegroup : std_logic_vector(5 downto 0) := "000000";
    signal time_in_frame_del1, time_in_frame_del2, time_in_frame_del3 : std_logic_vector(10 downto 0) := "00000000000";
    signal ct_offset_epoch_ns : std_logic_vector(63 downto 0);
    signal mult_dina, mult_dout, ct_offset_epoch_seconds_fp64, mult_dinb : std_logic_vector(63 downto 0);
    signal ct_offset_ns, ct_offset_epoch_ns_fp64 : std_logic_vector(63 downto 0);
    signal fp32_dout : std_logic_vector(31 downto 0);
    signal fifo_din, fifo_dout : std_logic_vector(170 downto 0);
    signal fifo_wrEn : std_logic;
    signal time_base_ns : std_logic_vector(63 downto 0);
    
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            if i_start = '1' then
                -- A pulse on i_start triggers calculation of 32 sets of time values
                -- (enough for a full PST output packet)
                timeGroup <= i_timeGroup;   -- 6 bit value
                time_base_ns <= i_time_base_ns;  -- 64 bit integer time in seconds for 't' in the polynomial at the start of the corner turn frame
                times_fsm <= get_offset_epoch_start0;
                fsm_waitCount <= (others => '0');
                time_in_timegroup <= (others => '0');
            else
                -- 
                case times_fsm is
                    when get_offset_epoch_start0 =>
                        -- one clock wait to get timeGroup_x1105920
                        times_fsm <= get_offset_epoch_start1;
                        fsm_waitCount <= (others => '0');
                        
                    when get_offset_epoch_start1 =>
                        -- Add the offset for i_timegroup to i_time_base_ns
                        times_fsm <= convert_to_fp64;
                        fsm_waitCount <= (others => '0');
                    
                    when convert_to_fp64 =>
                        -- wait 6 clocks for the uint64_to_double block to complete
                        -- The output is ct_offset_epoch_ns_fp64 
                        if unsigned(fsm_waitCount) = 5 then 
                            -- Input to uint64_to_double block is valid on waitCount = 0, 
                            -- output ("ct_offset_epoch_ns_fp64") will be valid one clock after waitCount = 5, i.e. when fsm is in the state convert_to_seconds
                            times_fsm <= convert_to_seconds;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;

                    when convert_to_seconds => 
                        -- Load the multiplier with time in ns (ct_offset_epoch_ns_fp64) and 1e-9, to get the time in seconds.
                        times_fsm <= wait_mult_1e_minus9;
                        fsm_waitCount <= (others => '0');
                        
                    when wait_mult_1e_minus9 =>
                        -- 12 clock latency for the fp64 multiplier
                        if unsigned(fsm_waitCount) = 11 then
                            -- fp64 input is valid when waitCount = 0, output will be valid one clock after waitCount = 11 
                            times_fsm <= get_s;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                        
                    when get_s =>
                        -- mult_dout is valid in this state, holding time in seconds.
                        -- mult_dout is the input to fp64_to_fp32.
                        -- Also set the inputs to fp64 multiplier in this state to find s^2
                        -- Also update ct_offset_epoch_ns for the next time sample (add 207360 ns)
                        times_fsm <= wait_s2;
                        fsm_waitCount <= (others => '0');
                    
                    when wait_s2 =>
                        -- 12 clock latency for the fp64 multiplier
                        if unsigned(fsm_waitCount) = 11 then
                            -- fp64 input is valid when waitCount = 0, output will be valid one clock after waitCount = 11 
                            times_fsm <= get_s2;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                    
                    when get_s2 =>
                        -- mult_dout is valid in this state, holding (time in seconds)^2
                        -- mult_dout is the input to fp64_to_fp32.
                        -- Also set the inputs to fp64 multiplier in this state to find s^3
                        times_fsm <= wait_s3;
                        fsm_waitCount <= (others => '0');
                        
                    when wait_s3 => 
                        if unsigned(fsm_waitCount) = 11 then
                            times_fsm <= get_s3;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                    
                    when get_s3 =>
                        -- mult_dout is valid in this state, holding (time in seconds)^3
                        times_fsm <= wait_s4;
                        fsm_waitCount <= (others => '0');
                        
                    when wait_s4 =>
                        if unsigned(fsm_waitCount) = 11 then
                            times_fsm <= get_s4;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                        
                    when get_s4 =>
                        -- mult_dout is valid in this state, holding (time in seconds)^4
                        -- mult_dout is the input to fp64_to_fp32.
                        -- Also set the inputs to fp64 multiplier in this state to find s^5
                        times_fsm <= wait_s5;
                        fsm_waitCount <= (others => '0');
                        
                    when wait_s5 =>
                        if unsigned(fsm_waitCount) = 11 then
                            times_fsm <= get_s5;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                    
                    when get_s5 =>
                        -- mult_dout is valid in this state, holding (time in seconds)^5
                        times_fsm <= wait_fifo_write;
                        fsm_waitCount <= (others => '0');
                    
                    when wait_fifo_write =>
                        -- Wait for the conversion to single precision and write to the FIFO, since get_next_s overwrites the input signal to the FIFO.
                        if unsigned(fsm_waitCount) = 4 then
                            times_fsm <= get_next_s;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                        
                    when get_next_s =>
                        if (unsigned(time_in_timegroup) = 15) then
                            times_fsm <= done;
                        else
                            --
                            time_in_timegroup <= std_logic_vector(unsigned(time_in_timegroup) + 1);
                            times_fsm <= wait_s2;
                            fsm_waitCount <= (others => '0');
                        end if;
                      
                    when done => 
                        times_fsm <= done;
                        
                    when others =>
                        times_fsm <= done;
                        
                end case;
            end if;
            
            times_fsm_del1 <= times_fsm;
            times_fsm_del2 <= times_fsm_del1;
            times_fsm_del3 <= times_fsm_del2;
            times_fsm_del4 <= times_fsm_del3;
            
            time_in_frame_del1 <= timeGroup & time_in_timegroup(4 downto 0);
            time_in_frame_del2 <= time_in_frame_del1;
            time_in_frame_del3 <= time_in_frame_del2;
            
            if ((times_fsm = done) and (times_fsm_Del4 = done)) then
                o_idle <= '1';
            else
                o_idle <= '0';
            end if;
                        
            case timeGroup is
                -- a "timegroup" is 16 time samples = 16*64*1080 ns = 1105920 ns
                when "000000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(0,64));
                when "000001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920,64));
                when "000010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*2,64));
                when "000011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*3,64));
                when "000100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*4,64));
                when "000101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*5,64));
                when "000110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*6,64));
                when "000111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*7,64));
                
                when "001000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*8,64));
                when "001001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*9,64));
                when "001010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*10,64));
                when "001011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*11,64));
                when "001100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*12,64));
                when "001101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*13,64));
                when "001110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*14,64));
                when "001111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*15,64));

                when "010000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*16,64));
                when "010001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*17,64));
                when "010010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*18,64));
                when "010011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*19,64));
                when "010100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*20,64));
                when "010101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*21,64));
                when "010110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*22,64));
                when "010111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*23,64));
                
                when "011000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*24,64));
                when "011001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*25,64));
                when "011010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*26,64));
                when "011011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*27,64));
                when "011100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*28,64));
                when "011101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*29,64));
                when "011110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*30,64));
                when "011111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*31,64));
                
                when "100000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*32,64));
                when "100001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*33,64));
                when "100010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*34,64));
                when "100011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*35,64));
                when "100100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*36,64));
                when "100101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*37,64));
                when "100110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*38,64));
                when "100111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*39,64));
                
                when "101000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*40,64));
                when "101001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*41,64));
                when "101010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*42,64));
                when "101011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*43,64));
                when "101100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*44,64));
                when "101101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*45,64));
                when "101110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*46,64));
                when "101111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*47,64));
                
                when "110000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*48,64));
                when "110001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*49,64));
                when "110010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*50,64));
                when "110011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*51,64));
                when "110100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*52,64));
                when "110101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*53,64));
                when "110110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*54,64));
                when "110111" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*55,64));
                
                when "111000" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*56,64));
                when "111001" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*57,64));
                when "111010" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*58,64));
                when "111011" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*59,64));
                when "111100" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*60,64));
                when "111101" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*61,64));
                when "111110" => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*62,64));
                when others   => timeGroup_x1105920 <= std_logic_vector(to_unsigned(1105920*63,64));
                
            end case;
            
            if times_fsm = get_offset_epoch_start1 then
                -- At the start of a timegroup, add the starting value for the corner turn frame to the offset for the timeGroup
                -- Note "epoch" in ct_offset_epoch is the polynomial epoch
                ct_offset_epoch_ns <= std_logic_vector(unsigned(time_base_ns) + unsigned(timeGroup_x1105920));
            elsif ((times_fsm = get_s) or (times_fsm = get_next_s)) then
                -- Get the next time sample in a timegroup
                -- 1 time sample = 64 * 1080 = 69120 ns
                ct_offset_epoch_ns <= std_logic_vector(unsigned(ct_offset_epoch_ns) + 69120);
            end if;
            
            -- Inputs to the multiplier
            if times_fsm = convert_to_seconds or times_fsm_del1 = get_s4 then
                -- load multiplier inputs to get time in seconds
                -- loaded when times_fsm_del1 = get_s4 for the update to the next time
                --  * ct_offset_epoch_ns_fp64 should have been stable for many clocks prior to this since 
                --    it is derived directly from ct_offset_epoch_ns, which is set in the state get_s 
                mult_dina <= ct_offset_epoch_ns_fp64;
                mult_dinb <= c_fp64_1eMinus9; -- 1e-9; convert nanoseconds to seconds
            elsif ((times_fsm = get_s) or (times_fsm = get_next_s)) then
                -- multipler output is time in seconds; use for both inputs to calculate t^2
                mult_dina <= mult_dout;
                ct_offset_epoch_seconds_fp64 <= mult_dout;
                mult_dinb <= mult_dout;
            elsif times_fsm = get_s2 or times_fsm = get_s3 or times_fsm = get_s4 then
                -- Load the multiplier inputs to get t^3, t^4, and t^5
                mult_dina <= mult_dout;
                mult_dinb <= ct_offset_epoch_seconds_fp64;
            end if;
            
            -- 3 cycle latency for the fp64 to fp32 conversion, hence time_fsm_del3 
            if ((times_fsm_del3 = get_s) or (times_fsm = get_next_s)) then
                fifo_din(31 downto 0) <= fp32_dout;
            end if;
            if (times_fsm_del3 = get_s2) then
                fifo_din(63 downto 32) <= fp32_dout;
            end if;
            if (times_fsm_del3 = get_s3) then
                fifo_din(95 downto 64) <= fp32_dout;
            end if;
            if (times_fsm_del3 = get_s4) then
                fifo_din(127 downto 96) <= fp32_dout;
            end if;
            if (times_fsm_del3 = get_s5) then
                fifo_din(159 downto 128) <= fp32_dout;
                fifo_din(170 downto 160) <= time_in_frame_del3;
                fifo_wrEn <= '1';
            else
                fifo_wrEn <= '0';
            end if;
            
        end if;
    end process;
    
    -- uint64 to double, 6 cycle latency
    get_fp64nsi : uint64_to_double
    port map (
        aclk => clk,
        s_axis_a_tvalid => '1',                     -- in std_logic;
        s_axis_a_tdata  => ct_offset_epoch_ns,      -- in (63:0);
        m_axis_result_tvalid => open,               -- out std_logic;
        m_axis_result_tdata => ct_offset_epoch_ns_fp64 -- out (63:0)
    );
    
    -- double precision multiplier, 12 cycle latency
    fp64_multi : fp64_mult
    port map (
        aclk => clk, --  IN STD_LOGIC;
        s_axis_a_tvalid => '1', -- : IN STD_LOGIC;
        s_axis_a_tdata   => mult_dina, -- IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid  => '1', -- : IN STD_LOGIC;
        s_axis_b_tdata   => mult_dinb, -- IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid => open, --: OUT STD_LOGIC;
        m_axis_result_tdata => mult_dout  --  OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    );
    
    -- Convert to single precision, 3 cycle latency
    fp64_to_fp32i : fp64_to_fp32
    port map(
        aclk  => clk, -- IN STD_LOGIC;
        s_axis_a_tvalid => '1', --  IN STD_LOGIC;
        s_axis_a_tdata  => mult_dout, --  IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid => open, --  OUT STD_LOGIC;
        m_axis_result_tdata  => fp32_dout --  OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    );
    
    ---------------------------------------------------------------------------------
    -- FIFO for the output time values.
    poly_fifoi : xpm_fifo_sync
    generic map (
        CASCADE_HEIGHT => 0,        -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 1,     -- DECIMAL
        FIFO_WRITE_DEPTH => 64,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 7,   -- DECIMAL
        READ_DATA_WIDTH => 171,     -- DECIMAL
        READ_MODE => "std",         -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "1404", -- String, enable read and write counts and data valid flag.
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 171,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 7    -- DECIMAL
    ) port map (
        almost_empty => open, -- 1-bit output: Almost Empty 
        almost_full => open,  -- 1-bit output: Almost Full
        data_valid => o_t_valid, -- 1-bit output: Read Data Valid
        dbiterr => open,      -- 1-bit output: Double Bit Error
        dout => fifo_dout,    -- READ_DATA_WIDTH-bit output: Read Data
        empty => open,        -- 1-bit output: Empty Flag
        full => open,         -- 1-bit output: Full Flag
        overflow => open,     -- 1-bit output: Overflow
        prog_empty => open,   -- 1-bit output: Programmable Empty
        prog_full => open,    -- 1-bit output: Programmable Full
        rd_data_count => o_fifo_rd_count, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count
        rd_rst_busy => open,  -- 1-bit output: Read Reset Busy
        sbiterr => open,      -- 1-bit output: Single Bit Error
        underflow => open,    -- 1-bit output: Underflow
        wr_ack => open,       -- 1-bit output: Write Acknowledge
        wr_data_count => open, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count
        wr_rst_busy => open,  -- 1-bit output: Write Reset Busy
        din => fifo_din,      -- WRITE_DATA_WIDTH-bit input: Write Data
        injectdbiterr => '0', -- 1-bit input: Double Bit Error Injection
        injectsbiterr => '0', -- 1-bit input: Single Bit Error Injection
        rd_en => i_fifo_rdEn, -- 1-bit input: Read Enable
        rst   => i_fifo_rst,  -- 1-bit input: Reset: Must be synchronous to wr_clk
        sleep => '0',         -- 1-bit input: Dynamic power saving
        wr_clk => clk,        -- 1-bit input: Write clock
        wr_en => fifo_wrEn    -- 1-bit input: Write Enable
    );

    o_t1 <= fifo_dout(31 downto 0);   -- time used in the polynomial in seconds
    o_t2 <= fifo_dout(63 downto 32);  -- time squared
    o_t3 <= fifo_dout(95 downto 64);  -- time cubed
    o_t4 <= fifo_dout(127 downto 96);   -- time to the fourth
    o_t5 <= fifo_dout(159 downto 128);  -- time to the fifth
    o_step <= fifo_dout(169 downto 160); -- time sample within the frame for the current outputs, 0 to 255.
    
    
end Behavioral;
