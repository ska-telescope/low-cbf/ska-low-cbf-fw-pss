----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 31.10.2020 19:55:16
-- Module Name: buffer512x256_wrapper - Behavioral
-- Description: 
--  Buffer memory for ct_atomic_pst_out.vhd
--  The total buffer is 1024 x 256 bits (write) = 512 x 512 bits (read). 
--  The memory is split into 2 pieces, each 256 bits wide to enable different read and write widths.
--  
----------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
Library xpm;
use xpm.vcomponents.all;
USE common_lib.common_pkg.ALL;

entity buffer512x512_wrapper is
    Port(
        i_clk    : in std_logic;
        i_wrEn   : in std_logic;                      -- Write enable
        i_wrAddr : in std_logic_vector(9 downto 0);   -- Write address for 256 bit wide writes
        i_wrData : in std_logic_vector(255 downto 0); -- 256 bit wide write data
        i_rdAddr : in std_logic_vector(8 downto 0);   -- Read address
        o_rdData : out std_logic_vector(511 downto 0) -- read data, 3 cycle latency from the read address.
    );
end buffer512x512_wrapper;

architecture Behavioral of buffer512x512_wrapper is

    signal doutb : t_slv_256_arr(1 downto 0);
    signal wrEn  : t_slv_1_arr(1 downto 0);
    signal wrData : std_logic_vector(255 downto 0);
    signal wrAddr : std_logic_vector(8 downto 0);

begin

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            wrAddr <= i_wrAddr(9 downto 1);
            if i_wrAddr(0) = '0' and i_wrEn = '1' then
                wrEn(0) <= "1";
            else
                wrEn(0) <= "0";
            end if;
            if i_wrAddr(0) = '1' and i_wrEn = '1' then
                wrEn(1) <= "1";
            else
                wrEn(1) <= "0";
            end if;
            wrData <= i_wrData;
            
        end if;
    end process;

   -- xpm_memory_sdpram: Simple Dual Port RAM
   -- Xilinx Parameterized Macro, version 2020.1
    memblocksInst : for i in 0 to 1 generate
        xpm_memory_sdpram_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,               -- DECIMAL
            ADDR_WIDTH_B => 9,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 256,       -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "block",     -- String
            MEMORY_SIZE => 131072,           -- DECIMAL; Total memory size in bits = 512 * 256 
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 256,        -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 1,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 256,       -- DECIMAL
            WRITE_MODE_B => "no_change"      -- String
        ) port map (
            dbiterrb => open,      -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
            doutb => doutb(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,      -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
            addra => wrAddr,       -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => i_rdAddr,     -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_clk,         -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
            clkb => i_clk,         -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is "independent_clock". Unused when parameter CLOCKING_MODE is "common_clock".
            dina => wrData,        -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',            -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
            enb => '1',            -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
            injectdbiterra => '0', -- 1-bit input: Controls double bit error injection on input data when ECC enabled.
            injectsbiterra => '0', -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
            regceb => '1',         -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',           -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
            sleep => '0',          -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => wrEn(i)         -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
    end generate;
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            o_rdData(255 downto 0) <= doutb(0);
            o_rdData(511 downto 256) <= doutb(1);
        end if;
    end process;
    
end Behavioral;
