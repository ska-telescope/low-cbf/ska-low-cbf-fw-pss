----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 19.03.2024
-- Module Name: ct2_poly_eval - Behavioral
-- Description: 
--   * Select which buffer to use for the polynomials in corner turn 2
--      --> Returns o_buf_sel = '0' or '1'
--   * Calculate the value of "t" in the polynomial at the start of the corner turn frame.
--      --> Returns o_poly_time_ns(63:0) = time as an integer in ns that is to be used in the polynomial
--  
-- Calculations :
--   - Which set of polynomials to use ? (compare i_ct_frame with i_poly_buffer0_valid_frame etc)
--   - Determine the time to use for "t" in the polynomial.
--      - To calculate t :
--          - Find corner turn frames from start of validity 
--              ct_offset_frames = i_ct_frame - i_poly_buffer{0,1}_valid_frame
--              ct_offset_nanoseconds = (ct_offset_frames * 53084160);
--              ct_offset_epoch =  ct_offset_nanoseconds + i_poly_buffer{0,1}_offset_ns
--              o_poly_time_ns = ct_offset_epoch (the time since the polynomial epoch in ns)
--
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;


entity ct2_buffer_select is
    port(
        clk : in std_logic;
        --------------------------------------------------------------------
        -- Configuration from the registers
        i_poly_buffer0_valid_frame : in std_logic_vector(47 downto 0);
        i_poly_buffer0_valid_duration : in std_logic_vector(31 downto 0);
        i_poly_buffer0_offset_ns : in std_logic_vector(31 downto 0);
        i_poly_buffer0_valid : in std_logic;
        i_poly_buffer1_valid_frame : in std_logic_vector(47 downto 0);
        i_poly_buffer1_valid_duration : in std_logic_vector(31 downto 0);
        i_poly_buffer1_offset_ns : in std_logic_vector(31 downto 0);
        i_poly_buffer1_valid : in std_logic;
        ---------------------------------------------------------------------
        -- start getting times from (i_ct_frame + i_timeGroup)
        -- Time group within the corner turn frame. Each time group is 32 time samples = 32*192*1080ns = 6635520 ns
        i_start : in std_logic;
        i_ct_frame : in std_logic_vector(36 downto 0);
        --
        o_valid : out std_logic;
        o_buf_sel : out std_logic;   -- Which polynomial buffer to use.
        o_buf_valid : out std_logic; -- Current frame count is within the range of valid frame counts for the polynomial buffer selected.
        o_poly_time_ns : out std_logic_vector(63 downto 0)
    );
end ct2_buffer_select;

architecture Behavioral of ct2_buffer_select is
    
    -- multiply by constant value (x53084160)
    -- 4 clock latency.
    component mult_53084160
    port (
        CLK : in  std_logic;
        A   : in  std_logic_vector(37 downto 0);
        P   : out std_logic_vector(63 downto 0));
    end component;
    
    type bufSel_fsm_type is (choose_buffer, get_offset_frames, get_offset_epoch_start, done);
    signal bufSel_fsm : bufSel_fsm_type := done;
    
    signal poly_buffer0_valid_frame : std_logic_vector(47 downto 0);
    signal poly_buffer0_valid_duration : std_logic_vector(31 downto 0);
    signal poly_buffer0_offset_ns : std_logic_vector(31 downto 0);
    signal poly_buffer0_valid : std_logic;
    signal poly_buffer1_valid_frame :  std_logic_vector(47 downto 0);
    signal poly_buffer1_valid_duration :  std_logic_vector(31 downto 0);
    signal poly_buffer1_offset_ns :  std_logic_vector(31 downto 0);
    signal poly_buffer1_valid :  std_logic;
    
    signal ct_frame_gt_buf0, ct_frame_gt_buf1 : std_logic;
    signal buf0_ct_frame_offset, buf1_ct_frame_offset, ct_frame, ct_frame_offset : std_logic_vector(47 downto 0);
    signal ct_frame_offset_ns : std_logic_vector(63 downto 0);
    signal poly_buffer_offset_ns : std_logic_vector(63 downto 0);
    signal fsm_waitcount : std_logic_vector(5 downto 0) := "000000";
    signal select_buf0, select_buf1 : std_logic := '0';
    signal buf0_ok, buf0_more_recent, buf1_ok : std_logic;
    signal ct_offset_epoch_ns : std_logic_vector(63 downto 0);
    signal ct_offset_ns : std_logic_vector(63 downto 0);
    signal select_valid : std_logic;
    
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            if i_start = '1' then
                -- A pulse on i_start triggers calculation of the module outputs
                -- (enough for a full PST output packet)
                poly_buffer0_valid_frame <= i_poly_buffer0_valid_frame;       -- 48 bit value
                poly_buffer0_valid_duration <= i_poly_buffer0_valid_duration; -- 32 bit value
                poly_buffer0_offset_ns <= i_poly_buffer0_offset_ns;           -- 32 bit value
                poly_buffer0_valid <= i_poly_buffer0_valid;
                poly_buffer1_valid_frame <= i_poly_buffer1_valid_frame;       -- 48 bit value
                poly_buffer1_valid_duration <= i_poly_buffer1_valid_duration; -- 32 bit value
                poly_buffer1_offset_ns <= i_poly_buffer1_offset_ns;           -- 32 bit offset ns
                poly_buffer1_valid <= i_poly_buffer1_valid;
                ct_frame <= "00000000000" & i_ct_frame;     -- 48 bit value
                bufSel_fsm <= choose_buffer;
                fsm_waitCount <= (others => '0');
            else
                -- 
                case bufSel_fsm is
                    when choose_buffer =>
                        -- Compare ct_frame with poly_bufferX_valid_frame, and find the most recent valid buffer.
                        -- 3 clock latency to calculate select_buf0, select_buf1
                        fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        if unsigned(fsm_waitCount) = 3 then
                            bufSel_fsm <= get_offset_frames;
                        end if;
                        
                    when get_offset_frames =>
                        -- Assign ct_frame_offset.
                        -- ct_frame_offset is the number of corner turn frames relative to the frame at which the buffer becomes valid.
                        -- ct_frame_offset is the input signal to the multiplier (multci : mult_53084160) that converts the number of corner turn frames 
                        -- into a time offset in nanoseconds
                        if select_buf1 = '1' then
                            ct_frame_offset <= buf1_ct_frame_offset;
                            poly_buffer_offset_ns(63 downto 0) <= x"00000000" & poly_buffer1_offset_ns;
                        else
                            -- We have to select something, so select buffer 0 whether it is valid or not.
                            ct_frame_offset <= buf0_ct_frame_offset;
                            poly_buffer_offset_ns(63 downto 0) <= x"00000000" & poly_buffer0_offset_ns;
                        end if;
                        o_buf_sel <= select_buf1;
                        o_buf_valid <= select_valid;
                        fsm_waitCount <= (others => '0');
                        bufSel_fsm <= get_offset_epoch_start;
                    
                    when get_offset_epoch_start =>
                        -- Wait for 
                        --  (1) ct_frame_offset    --> ct_frame_offset_ns  (4 clocks) [convert from corner turn frames to nanoseconds]
                        --  (2) ct_offset_ns       --> ct_offset_epoch_ns  (1 clock)  [Add the offset for the nanoseconds from the polynomial epoch to the start of the corner turn frame]
                        if unsigned(fsm_waitCount) = 5 then
                            -- Counts 0 to 4 inclusive in this state, when 5 ct_offset_epoch_ns is being loaded.
                            BufSel_fsm <= done;
                            fsm_waitCount <= (others => '0');
                        else
                            fsm_waitCount <= std_logic_vector(unsigned(fsm_waitCount) + 1);
                        end if;
                        
                    when done => 
                        bufSel_fsm <= done;
                        
                    when others =>
                        bufSel_fsm <= done;
                        
                end case;
            end if;
            
            if bufSel_fsm = done then
                o_valid <= '1';
            else
                o_valid <= '0';
            end if;
            
            if (unsigned(ct_frame) >= unsigned(poly_buffer0_valid_frame)) then
                ct_frame_gt_buf0 <= '1';
            else
                ct_frame_gt_buf0 <= '0';
            end if;
            buf0_ct_frame_offset <= std_logic_vector(unsigned(ct_frame) - unsigned(poly_buffer0_valid_frame));
            if (unsigned(buf0_ct_frame_offset) < unsigned(poly_buffer0_valid_duration)) and (ct_frame_gt_buf0 = '1') and (poly_buffer0_valid = '1') then
                buf0_ok <= '1';
            else
                buf0_ok <= '0';
            end if;
            
            if (unsigned(ct_frame) >= unsigned(poly_buffer1_valid_frame)) then
                ct_frame_gt_buf1 <= '1';
            else
                ct_frame_gt_buf1 <= '0';
            end if;
            buf1_ct_frame_offset <= std_logic_vector(unsigned(ct_frame) - unsigned(poly_buffer1_valid_frame));
            if (unsigned(buf1_ct_frame_offset) < unsigned(poly_buffer1_valid_duration)) and (ct_frame_gt_buf1 = '1') and (poly_buffer1_valid = '1') then
                buf1_ok <= '1';
            else
                buf1_ok <= '0';
            end if;
            
            if (unsigned(poly_buffer0_valid_frame) > unsigned(poly_buffer1_valid_frame)) then
                buf0_more_recent <= '1';
            else
                buf0_more_recent <= '0';
            end if;
            
            if (buf0_ok = '1' and (buf0_more_recent = '1' or buf1_ok = '0')) then
                select_buf0 <= '1';
                select_buf1 <= '0';
                select_valid <= '1';
            elsif buf1_ok = '1' then
                select_buf0 <= '0';
                select_buf1 <= '1';
                select_valid <= '1';
            else
                select_buf0 <= '0';
                select_buf1 <= '0';
                select_valid <= '0';
            end if;
            
            -- Output of the multiplier to convert corner turn frames to nanoseconds.
            ct_offset_ns <= ct_frame_offset_ns;
            if bufSel_fsm = get_offset_epoch_start then
                -- add the register value (poly_buffer_offset_ns)
                -- Note "epoch" in ct_offset_epoch is the polynomial epoch
                ct_offset_epoch_ns <= std_logic_vector(unsigned(ct_offset_ns) + unsigned(poly_buffer_offset_ns));
            end if;
            
        end if;
    end process;
    
    o_poly_time_ns <= ct_offset_epoch_ns;
    
    -- Each corner turn frame is 53.084160 ms
    -- get offset from the SKA epoch in ns = ct_frame_offset * 53084160
    -- 4 clock latency
    multci : mult_53084160
    port map (
        CLK => clk,
        A   => ct_frame_offset(37 downto 0), -- in(37:0);
        P   => ct_frame_offset_ns            -- out(63:0);
    );
    
    
end Behavioral;
