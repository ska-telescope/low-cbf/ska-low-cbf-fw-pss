create_ip -name axi_clock_converter -vendor xilinx.com -library ip -version 2.1 -module_name axi_clock_converter_ct2
set_property -dict [list CONFIG.Component_Name {axi_clock_converter_ct2} CONFIG.ADDR_WIDTH {19} CONFIG.ACLK_ASYNC {1}] [get_ips axi_clock_converter_ct2]
create_ip_run [get_ips axi_clock_converter_ct2]

create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_ct2
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_ct2} CONFIG.MEM_DEPTH {131072} CONFIG.READ_LATENCY {15}] [get_ips axi_bram_ctrl_ct2]
create_ip_run [get_ips axi_bram_ctrl_ct2]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name single_AxB_plusC
set_property -dict [list \
  CONFIG.A_Precision_Type {Single} \
  CONFIG.Add_Sub_Value {Add} \
  CONFIG.C_A_Exponent_Width {8} \
  CONFIG.C_A_Fraction_Width {24} \
  CONFIG.C_Latency {16} \
  CONFIG.C_Mult_Usage {Medium_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {8} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {single_AxB_plusC} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {FMA} \
  CONFIG.Result_Precision_Type {Single} \
] [get_ips single_AxB_plusC]
create_ip_run [get_ips single_AxB_plusC]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name single_A_plus_B
set_property -dict [list \
  CONFIG.Add_Sub_Value {Add} \
  CONFIG.C_Latency {11} \
  CONFIG.C_Mult_Usage {Medium_Usage} \
  CONFIG.Component_Name {single_A_plus_B} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
] [get_ips single_A_plus_B]
create_ip_run [get_ips single_A_plus_B]

create_ip -name mult_gen -vendor xilinx.com -library ip -version 12.0 -module_name mult_53084160
set_property -dict [list \
  CONFIG.CcmImp {Dedicated_Multiplier} \
  CONFIG.Component_Name {mult_53084160} \
  CONFIG.ConstValue {53084160} \
  CONFIG.MultType {Constant_Coefficient_Multiplier} \
  CONFIG.PipeStages {4} \
  CONFIG.PortAType {Unsigned} \
  CONFIG.PortAWidth {38} \
] [get_ips mult_53084160]
create_ip_run [get_ips mult_53084160]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp64_to_fp32
set_property -dict [list \
  CONFIG.A_Precision_Type {Double} \
  CONFIG.C_A_Exponent_Width {11} \
  CONFIG.C_A_Fraction_Width {53} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {3} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {8} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {fp64_to_fp32} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Float_to_float} \
  CONFIG.Result_Precision_Type {Single} \
] [get_ips fp64_to_fp32]
create_ip_run [get_ips fp64_to_fp32]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp32_mult
set_property -dict [list \
  CONFIG.A_Precision_Type {Single} \
  CONFIG.C_A_Exponent_Width {8} \
  CONFIG.C_A_Fraction_Width {24} \
  CONFIG.C_Latency {8} \
  CONFIG.C_Mult_Usage {Medium_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {8} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {fp32_mult} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Multiply} \
  CONFIG.Result_Precision_Type {Single} \
] [get_ips fp32_mult]
create_ip_run [get_ips fp32_mult]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp32_to_fixed
set_property -dict [list \
  CONFIG.A_Precision_Type {Single} \
  CONFIG.C_A_Exponent_Width {8} \
  CONFIG.C_A_Fraction_Width {24} \
  CONFIG.C_Latency {6} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {32} \
  CONFIG.C_Result_Fraction_Width {24} \
  CONFIG.Component_Name {fp32_to_fixed} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Float_to_fixed} \
  CONFIG.Result_Precision_Type {Custom} \
] [get_ips fp32_to_fixed]
create_ip_run [get_ips fp32_to_fixed]

