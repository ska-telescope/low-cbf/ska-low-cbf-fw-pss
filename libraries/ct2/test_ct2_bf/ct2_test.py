# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Standalone code to create configuration and test output of the SKA low Pulsar search beamformer corner turn 2 + beamformer module
---------------------------------------------
Introduction:
Corner turn 2 (CT2) stores data from the filterbank to HBM and then plays them back in the order needed by the beamformer.
This code tests
   * Configuration and calculation of the delay polynomials.
   * Corner turn readout order
   * beamformer configuration
   * beamformer
This code reads in a yaml file and writes out configuration data for the yaml test case.
The configurationd data includes the polynomial coefficients and Jones matrices.
----------------------------------------------
Configuration memory:

"""

import matplotlib.pyplot as plt
import argparse
from pprint import pprint

import numpy as np
import yaml
import typing

# number of distinct random samples to generate per virtual channel. 
RNG_SAMPLES = 8192

def command_line_args():
    parser = argparse.ArgumentParser(description="PSS Corner turn 2 + beamformer configuration generator")
    parser.add_argument(
        "-f",
        "--filePrefix",
        help="prefix for files generated - XXX_poly0.txt, XXX_poly1.txt, XXX_poly2.txt, XXX_poly3.txt, XXX_beam_jones.txt, XXX_station_jones.txt"
    )
    
    parser.add_argument(
        "-b",
        "--bf_data",
        help="prefix for the four beamformer output files, XXX0.txt, XXX1.txt, XXX2.txt, XXX3.txt",
        required=False,
    )
    
    parser.add_argument(
        "-d",
        "--debug",
        help="Debug setting; Plot graphs when analysing data; higher values plot more",
        required=False,
    )
    
    parser.add_argument(
        "-s",
        "--sim_frames",
        help="Number of simulation frames to generate and check output for",
        required=True
    )
    
    parser.add_argument(
        "configuration",
        help="Test Configuration (YAML)",
        type=argparse.FileType(mode="r"),
    )
    return parser.parse_args()

def parse_config(file: typing.IO) -> typing.Dict:
    """
    Reads configuration YAML file, checks if required values are present.

    :param file: The YAML file object
    :return: Settings dict, guaranteed to contain the keys specified in required_keys
    :raises AssertionError: if required key(s) are missing
    """
    config = yaml.safe_load(file)

    required_keys = {"stations_buf0","channels","weights_buf0","weights_buf1","station_jones","beam_jones"}
    assert config.keys() >= required_keys, "Configuration YAML missing required key(s)"
    # print("\n??\tSettings:")
    #for parameter, value in config.items():
    #    if parameter == "stations_buf0" or parameter == "stations_buf1":
    #        print(f"\t:{parameter}")
    #        for n, stn_cfg in config[parameter].items():
    #            print(f"\t  {n}:{stn_cfg}")
    #    else:
    #        print(f"\t{parameter}: {value}")
    return config

def ct2_polynomial_config(poly_data):
    """
    :param poly_data: dictionary indexed by the station ID, with up to 16 polynomials each, one for each beam.
    :return: numpy array of uint32 to be loaded into the polynomial configuration memory, and the total number of 
    stations included.
    """
    # [512 stations] x [16 beams] x [6 coefficients per polynomial] (x 4 bytes per single precision value)
    # In the polynomial configuration memory,
    #  (4-byte word) address = (buffer * 49152) + (beam * 3072) + (station * 6)
    # This function reads for a single buffer (whichever is passed to the function in poly_data)
    config_array = np.zeros((512,16,6), np.float32)
    total_stations = 0
    total_beams = 0
    for station, polynomials in poly_data.items():
        beams = len(polynomials)
        if beams > total_beams :
            total_beams = beams
        if (station+1) > total_stations:
            total_stations = station+1
        for beam in range(beams):
            for coef in range(6):
                config_array[station,beam,coef] = np.float32(polynomials[beam][coef])
                
    return (config_array, total_stations, total_beams)
    
def get_tb_data(tb_file_prefix, total_frames, total_beams, total_coarse):
    # Load PSS packet output data saved by the testbench
    # Saved at the beamformer output, it includes 1 header word then the 
    # File Format : text
    # Each line contains hex data for:
    #   packetCount beam freq_index data0 data1 data2 data3 status
    #   (note freq_index bits (3:0) runs 0 to 8, for 9 blocks of 24 fine channels per coarse channel,
    #         freq_index bits (15:4) counts coarse channels)
    # Each packet consist of 864 lines in the file :
    #   -  lines of beamformed data
    #      Each line is 
    #        (timestamp) (line in packet), (beam), (frequency channel) (scale) (pol1 im) (pol1 re) (pol0 im) (pol0 re) (poly+jones status)
    #      Data order is
    #        data line in file |  Fine  |   Times
    #                     0        0           0
    #                     1        0           1
    #                                  ...
    #                    15        0           15
    #                    16        1           0
    #                                  ...
    #                    31        1           15
    #     etc, so lines 32-47 = fine 2
    #             lines 48-63 = fine 3
    #     Total of (54 fine) * (16 times)= 864 lines of data per packet
    #
    # e.g.
    #  000000032FD1 000 000 000 5 666F 0C4C 6BB7 0AE4 E
    #  000000032FD1 001 000 000 6 51E4 C793 53C6 BE2D E
    #  000000032FD1 002 000 000 6 51B1 C891 5393 BF2B E
    #
    # packet_data is the data as it appears in the packet
    # (time_sample, beam, fine channel, polarisation)
    packet_data = np.zeros((total_frames*768, total_beams, total_coarse * 54, 2), dtype=np.complex128)
    # raw_beamformed is the value after applying the scale factor in the packet
    raw_beamformed = np.zeros((total_frames*768, total_beams, total_coarse * 54, 2), dtype=np.complex128)

    pkt_count = 0  # count of packets being read from the file
    for filecount in range(4):
        fname_full = f"{tb_file_prefix}_packets{filecount}.txt"
        tb_file = open(fname_full)
        for line in tb_file:
            dint = [int(di,16) for di in line.split()]
            for ds in range(5,9):
                if dint[ds] > 32767:
                    dint[ds] = dint[ds] - 65536  # correct for signed values
            line_count = dint[1]
            if pkt_count == 0 and line_count == 0:
                # get the first timestamp, reference all other packets to this timestamp
                first_timestamp = dint[0]
            fine_channel = line_count // 16  # groups of 16 time samples in the packet for each fine channel
            time_sample = (dint[0]-first_timestamp) * 16 + (line_count % 16)   # 
            beam = dint[2]
            freq_base = dint[3] * 54
            pol0 = np.double(dint[8]) + 1j * np.double(dint[7])
            pol1 = np.double(dint[6]) + 1j * np.double(dint[5])
            packet_data[time_sample, beam, freq_base + fine_channel, 0] = pol0
            packet_data[time_sample, beam, freq_base + fine_channel, 1] = pol1
            raw_beamformed[time_sample, beam, freq_base + fine_channel, 0] = pol0 * (2 ** dint[4])
            raw_beamformed[time_sample, beam, freq_base + fine_channel, 1] = pol1 * (2 ** dint[4]) 
            
            if line_count == 863:
                pkt_count += 1
            else:
                line_count += 1
    
    return (packet_data, raw_beamformed)

def get_tb_scaled_data(tb_file_prefix, total_frames, total_beams, total_coarse):
    # Read in the data from the scaled output
    # File Format : text
    # Each line contains hex data for:
    #   packetCount packet_word beam freq_index d0 d1 d2 d3 d4 d5 d6 d7 status 
    #
    # Each packet is 433 lines in the text file.
    # The first line is a header word, which contains a magic value and a 32-bit floating point scale value :
    # e.g. :
    # 000000032FD0 000 000 000 DE AD BE EF 3B 21 C0 29 38
    # D0 to D3 = magic word, D4-D7 = 0x3B21C029 = 0.0024681187 (32-bit floating point)
    # The remaining lines are organised as per the data portion of the PSS output packet :
    # 54 channels "ch", 16 times "t", 2 polarisations "p", complex (I+Q)
    # line1 : (CH0,P0,T0,I),  (CH0,P0,T0,Q),  (CH0,P0,T1,I),  (CH0,P0,T1,Q),  (CH0,P0,T2,I),  (CH0,P0,T2,Q),  (CH0,P0,T3,I),  (CH0,P0,T3,Q)
    # line2 : (CH0,P0,T4,I),  (CH0,P0,T4,Q),  (CH0,P0,T5,I),  (CH0,P0,T5,Q),  (CH0,P0,T6,I),  (CH0,P0,T6,Q),  (CH0,P0,T7,I),  (CH0,P0,T7,Q)
    # line3 : (CH0,P0,T8,I),  (CH0,P0,T8,Q),  (CH0,P0,T9,I),  (CH0,P0,T9,Q),  (CH0,P0,T10,I), (CH0,P0,T10,Q), (CH0,P0,T11,I), (CH0,P0,T11,Q)
    # line4 : (CH0,P0,T12,I), (CH0,P0,T12,Q), (CH0,P0,T13,I), (CH0,P0,T13,Q), (CH0,P0,T14,I), (CH0,P0,T14,Q), (CH0,P0,T15,I), (CH0,P0,T15,Q)
    # line5 - line8 : CH0, P1, T0-15, I+Q
    # line9-16 : CH1
    # Total 54 channels = 54*8 lines = 432 lines
    #
    # packet_data is the data as it appears in the packet, i.e. 8-bit integers (but represented by floats
    # (time_sample, beam, fine channel, polarisation)
    packet_data = np.zeros((total_frames*768, total_beams, total_coarse * 54, 2), dtype=np.complex64)
    # raw_beamformed is the value after applying the scale factor in the packet
    raw_beamformed = np.zeros((total_frames*768, total_beams, total_coarse * 54, 2), dtype=np.complex64)
    # One scale factor for each coarse channel and each 16 time samples
    pkt_scales = np.zeros((total_frames*48, total_beams, total_coarse),dtype = np.double)
    #
    pkt_count = 0  # count of packets being read from the file
    first_timestamp = 0
    for filecount in range(4):
        fname_full = f"{tb_file_prefix}_scaled_packets{filecount}.txt"
        tb_file = open(fname_full)
        for line in tb_file:
            dint = [int(di,16) for di in line.split()]
            line_count = dint[1]
            if line_count == 0:
                # Get the scaling factor
                pkt_scale_uint32 = np.uint32(dint[8] * 256*256*256 + dint[9] * 256*256 + dint[10] * 256 + dint[11])
                pkt_scale = pkt_scale_uint32.view(np.float32)
                
            
            for ds in range(4,12):
                if dint[ds] > 127:
                    dint[ds] = dint[ds] - 256  # correct for signed values
            
            if pkt_count == 0 and line_count == 0:
                # get the first timestamp, reference all other packets to this timestamp
                first_timestamp = dint[0]
            if line_count > 0:
                fine_channel = (line_count-1) // 8  # Groups of 8 lines in the packet for each fine channel
                time_sample_base = (dint[0]-first_timestamp) * 16 + 4*((line_count-1) % 4)   # 
                pol = ((line_count-1) // 4) % 2
                beam = dint[2]
                freq_base = dint[3] * 54
                pkt_scales[(dint[0]-first_timestamp), beam, dint[3]] = pkt_scale
                for t_offset in range(4):  # time offset on this line, 4 time samples per line
                    t0 = np.float32(dint[2*t_offset + 4]) + 1j * np.float32(dint[2*t_offset + 5])
                    packet_data[time_sample_base + t_offset, beam, freq_base + fine_channel, pol] = t0
                    raw_beamformed[time_sample_base + t_offset, beam, freq_base + fine_channel, pol] = t0 / pkt_scale
            
            if line_count == 0:
                pkt_count += 1
            else:
                line_count += 1
    return (packet_data, raw_beamformed, pkt_scales)

def main():
    # Read command-line arguments
    args = command_line_args()
    config = parse_config(args.configuration)
    
    # convert config into a data file to load into the firmware for the polynomials
    for pipeline in range(4):
        # Four parallel pipelines in the firmware with polynomials loaded from 4 different files
        fpoly = open(args.filePrefix + "/" + args.filePrefix + "_poly" + str(pipeline) + ".txt",'wt')
        first_block = True
        for poly_buffer in range(2):
            if poly_buffer == 0:
                cfg_key = "stations_buf0"
                base_addr = 0
            else:
                cfg_key = "stations_buf1"
                base_addr = 196608  # byte address for the start of the buffer
            if cfg_key in config:
                (config_array, total_stations, total_beams) = ct2_polynomial_config(config[cfg_key])
                
                for station in range(total_stations):
                    for beam in range(total_beams):
                        # Does this pipeline evaluate this beam ?
                        if (beam % 4) == pipeline:
                            # Each station is (6 words) * (16 beams) = 96 words = 384 bytes of polynomial data 
                            # write data for 1 polynomial at a time = 24 words; only write up to total_stations and total_beams
                            #  Byte address for even-indexed stations = (buffer * 196608) + (beam * 6144) + ((station/2) * 24)
                            #  Byte address for odd-indexed stations  = (buffer * 196608) + 98304 + (beam * 6144) + (floor(station/2) * 24)                            
                            if (station % 2 == 0): # Even indexed stations go in the first 256 slots
                                block_addr = (beam//4) * 1536 + (station//2) * 6
                            else: # Odd indexed stations go in the second 256 slots
                                block_addr = 24576 + (beam//4) * 1536 + (station//2) * 6
                            if not first_block:
                                fpoly.write("\n")
                            first_block = False
                            fpoly.write(f"[{(base_addr + 4*block_addr):08x}]")   # byte address
                            for n in range(6):
                                coef_fp32 = config_array[station,beam,n]
                                coef_uint32 = np.frombuffer(coef_fp32, dtype=np.uint32)
                                fpoly.write(f"\n{coef_uint32[0]:08x}")
        # Write the sky frequencies for each virtual channel
        # Note : Same virtual channels for all 4 pipelines
        # These go to a block of memory starting at byte address 393216 in the polynomial memory.
        sky_freq = np.zeros(2052,dtype = np.uint32) # 2048 words in the sky frequency memory, but make this a multiple of 6 so all writes can be the same size.
        for coarse in range(config["g_COARSE_CHANNELS"]):
            for station in range(config["g_STATIONS"]):
                sky_fp32 = np.float32(config["channels"][coarse])
                sky_freq[2*(coarse * config["g_STATIONS"] + station)] =  np.frombuffer(sky_fp32, dtype=np.uint32)
        virtual_channels = config["g_COARSE_CHANNELS"] * config["g_STATIONS"]
        base_addr = 393216   # base address of the sky frequencies memory
        for vc_div3 in range((virtual_channels+3) // 3):
            vc = vc_div3 * 3
            if first_block:
                fpoly.write(f"[{(base_addr + 8 * vc):08x}]") # 8 bytes per virtual channel
                first_block = False
            else:
                fpoly.write(f"\n[{(base_addr + 8 * vc):08x}]")
            for n in range(6):
                fpoly.write(f"\n{sky_freq[vc*2 + n]:08x}") # 2 entries in sky_freq per virtual channel (one unused)
    
    #####################################################################################   
    ## Create Station Jones matrices and write to a file
    # There is only one set of Station Jones matrices for all beams.
    # (Two buffers) x (1024 virtual channels) x (2 rows) x (2 columns) x (2 [real + imaginary])
    jones = np.zeros((2,1024,2,2,2),dtype = np.int16)
    if config["station_jones"] == 0:
        print('Setting station_Jones matrices to 0 (beamformer output will be zeros)')
    if config["station_jones"] == 1:
        # Identity matrices everywhere
        print("setting station Jones matrices to identity matrices")
        for buf in range(2):
            for vc in range(1024):
                jones[buf,vc,0,0,0] = 16384  # real part = 1, for top left of the Jones matrix
                jones[buf,vc,1,1,0] = 16384  # real part = 1 for bottomright of the Jones matrix
    elif config["station_jones"] == 2:
        # pseudo-random values everywhere
        print("Setting Jones station matrices to pseudo random numbers")
        rng = np.random.default_rng(1234)
        jones = np.int16(np.round(32766 * (rng.random((2,1024,2,2,2))-0.5)))
    else:
        print("! Unrecognised station Jones matrix configuration")

    # Write weights to the Jones matrices config file
    # (2 buffers) x (1024 virtual channels)
    weights = np.zeros((2,1024),dtype = np.uint16)
    for coarse in range(config["g_COARSE_CHANNELS"]):
        for station in range(config["g_STATIONS"]):
            weights[0, coarse * config["g_STATIONS"] + station] = np.uint16(config['weights_buf0'][station])
            weights[1, coarse * config["g_STATIONS"] + station] = np.uint16(config['weights_buf1'][station])
    
    # Write Jones matrices to the config file
    # Writes are in blocks of 4 virtual channels = 16 x (32 bit words)
    # Flatten Jones array and reinterpret as uint32
    # There are (2 buffers) * (1024 virtual channels) * (2 rows) * (2 columns) = 8192 uint32 in "jones_uint32"
    jones_flattened = np.reshape(jones,2*1024*2*2*2)
    jones_uint32 = np.frombuffer(jones_flattened, dtype = np.uint32)
    print("Writing Station Jones matrices")
    fjones = open(args.filePrefix + "/" + args.filePrefix + "_station_jones.txt", 'wt')
    for buffer in range(2):
        # Write Jones
        base_addr = buffer * 16384
        for data_block in range((virtual_channels+3) // 4):
            fjones.write(f"[{(base_addr + 64 * data_block):08x}]\n")  # byte address; 16 x 4 byte words per write.
            for data_word in range(16):
                # Write uint32 values to file.
                fjones.write(f"{jones_uint32[buffer * 4096 + data_block*16 + data_word]:08x}\n")
        # Also write weights
        weights_base_addr = 32768 + buffer * 2048
        for data_block in range((virtual_channels+31) // 32):  # 2 virtual channels per 32-bit word in the file, 16 words per write.
            fjones.write(f"[{(weights_base_addr + 64 * data_block):08x}]\n")  # byte address; 16 x 4 byte words per write.
            for data_word in range(16):
                # Write uint32 values to file.
                w0 = weights[buffer, data_block * 32 + data_word * 2]
                w1 = weights[buffer, data_block * 32 + data_word * 2 + 1]
                fjones.write(f"{(w1*65536 + w0):08x}\n")

    #########################################################################################
    ## Create beam Jones matrices and write to a file
    # There is a jones matrix for every coarse channel and every beam
    # (Two buffers) x (1024 coarse channels) x (beams) x (2 rows) x (2 columns) x (2 [real + imaginary])
    total_coarse = config["g_COARSE_CHANNELS"]
    beam_jones = np.zeros((2, total_coarse, total_beams, 2, 2, 2), dtype=np.int16)
    if config["beam_jones"] == 0:
        print('Setting Beam Jones matrices to 0 (beamformer output will be zeros)')
    if config["beam_jones"] == 1:
        # Identity matrices everywhere
        print("setting Beam Jones matrices to identity matrices")
        for buf in range(2):
            for coarse in range(total_coarse):
                for beam in range(total_beams):
                    beam_jones[buf, coarse, beam, 0, 0, 0] = 32767  # real part = 1, for top left of the Jones matrix
                    beam_jones[buf, coarse, beam, 1, 1, 0] = 32767  # real part = 1 for bottomright of the Jones matrix
    elif config["beam_jones"] == 2:
        # pseudo-random values everywhere
        print("Setting Beam Jones matrices to pseudo random numbers")
        rng = np.random.default_rng(1234)
        beam_jones = np.int16(np.round(32766 * (rng.random((2, total_coarse, total_beams, 2, 2, 2)) - 0.5)))
    else:
        print("! Unrecognised Beam Jones matrix configuration")

    # Write the beam Jones matrices to the config file
    # Writes are in blocks of 4 virtual channels = 16 x (32 bit words)
    # Flatten Jones array and reinterpret as uint32
    # There are (2 buffers) * (1024 virtual channels) * (2 rows) * (2 columns) = 8192 uint32 in "jones_uint32"
    
    if (total_coarse * total_beams <= 2048):
        print(f"Writing {total_coarse * total_beams} Beam Jones matrices (per buffer)")
    else:
        print(f"Configuration asks for {total_coarse*total_beams} jones matrices, but maximum supported is 2048")
        exit(-1)
    f_beam_jones = open(args.filePrefix + "/" + args.filePrefix + "_beam_jones.txt", 'wt')
    for buffer in range(2):
        beam_jones_flattened = np.reshape(beam_jones[buffer,:,:,:,:,:], total_coarse * total_beams * 2 * 2 * 2)
        jones_uint32 = np.frombuffer(beam_jones_flattened, dtype=np.uint32)
        # Write Jones
        # The total number of matrices supported is 2048 per buffer
        # Each buffer is 32 kBytes (2048 matrices x 16 bytes/matrix)
        base_addr = buffer * 32768
        total_matrices = total_coarse * total_beams
        for data_block in range((total_matrices + 3) // 4):
            f_beam_jones.write(f"[{(base_addr + 64 * data_block):08x}]\n")  # byte address; 16 x 4 byte words per write.
            for data_word in range(16):
                # Write uint32 values to file.
                if (data_block*16 + data_word) < (total_matrices*4):
                    f_beam_jones.write(f"{jones_uint32[data_block * 16 + data_word]:08x}\n")
                else:
                    # Write in blocks of 4 matrices, so the last few words can be unused
                    f_beam_jones.write(f"{0:08x}\n")
    
    ##############################################################################################
    # find the expected output.
    # output packets consist of 16 time samples and 54 frequency samples.
    if int(args.sim_frames) > 0:
        
        total_frames = args.sim_frames
        apply_station_jones = True
        # 16 counters used to generate sample data in the testbench, initial values for the counters
        counter_start = [0, 0x100, 0x200, 0x300, 0x4000, 0x4100, 0x4200, 0x4300, 0x8000, 0x8100, 0x8200, 0x8300, 0xC000, 0xC100, 0xC200, 0xC300]    
        print(f"computing expected beamformer result for {total_frames} corner turn frames")
        total_stations = config["g_STATIONS"]
        raw_bf_samples = np.zeros((total_frames,total_beams,total_coarse,256,216),dtype = np.complex64)
        # 54 fine channels per coarse channel, 768 time samples per corner turn, 2 polarisations
        # [time, beam, fine_channel, polarisation]
        raw_beamformed = np.zeros((total_frames*768,total_beams,total_coarse*54,2), dtype = np.complex128)
        # Packet weights are 54 values (= 1 per fine channel) at the start of each output PSS packet
        # Weights are the same for all beams.
        # Weights depend on flagging, which is different for different sets of time samples.
        # There are 48 packets (of 16 time samples each) in each frame (total 48*16 = 768 time samples per corner turn frame)
        packet_weights_unflagged = np.zeros((total_frames*48, total_coarse, 54), dtype = np.double)
        packet_weights_total = np.zeros((total_frames*48, total_coarse, 54), dtype = np.double)
        for frame in range(total_frames):
            # Get the Polynomials to use
            this_frame = config["g_FRAME_COUNT_START"] + frame
            if (this_frame >= config["g_POLY_BUF0_FRAME_START"]) and (this_frame < (config["g_POLY_BUF0_FRAME_START"] + config["g_POLY_BUF0_VALID_DURATION"])):
                poly_buf0_valid = True
            else:
                poly_buf0_valid = False
            if (this_frame >= config["g_POLY_BUF1_FRAME_START"]) and (this_frame < (config["g_POLY_BUF1_FRAME_START"] + config["g_POLY_BUF1_VALID_DURATION"])):
                poly_buf1_valid = True
            else:
                poly_buf1_valid = False
            if config["g_POLY_BUF0_FRAME_START"] > config["g_POLY_BUF1_FRAME_START"]:
                poly_buf0_more_recent = True
            else:
                poly_buf0_more_recent = False
            if poly_buf0_valid and (poly_buf0_more_recent or (not poly_buf1_valid)):
                (poly_config, total_stations, total_beams) = ct2_polynomial_config(config["stations_buf0"])
                # Time to use in the polynomial at the start of the corner turn frame
                t_poly_ns = (this_frame - config["g_POLY_BUF0_FRAME_START"]) * 24 * 2048 * 1080.0 + config["g_POLY_BUF0_OFFSET_NS"]
            else:
                (poly_config, total_stations, total_beams) = ct2_polynomial_config(config["stations_buf1"])
                t_poly_ns = (this_frame - config["g_POLY_BUF1_FRAME_START"]) * 24 * 2048 * 1080.0 + config["g_POLY_BUF1_OFFSET_NS"]
            
            # Get the Jones matrices to use
            if (this_frame >= config["g_JONES_BUF0_FRAME_START"]) and (this_frame < (config["g_JONES_BUF0_FRAME_START"] + config["g_JONES_BUF0_VALID_DURATION"])): 
                jones_buf0_valid = True
            else:
                jones_buf0_valid = False
            if (this_frame >= config["g_JONES_BUF1_FRAME_START"]) and (this_frame < (config["g_JONES_BUF1_FRAME_START"] + config["g_JONES_BUF1_VALID_DURATION"])): 
                jones_buf1_valid = True
            else:
                jones_buf1_valid = False
            if config["g_JONES_BUF0_FRAME_START"] > config["g_JONES_BUF1_FRAME_START"] :
                jones_buf0_more_recent = True
            else:
                jones_buf0_more_recent = False
            if jones_buf0_valid and (jones_buf0_more_recent or (not jones_buf1_valid)):
                jones_select = 0
            else:
                jones_select = 1
            
            for beam in range(total_beams):
                print(f"Calculating frame {frame}, beam {beam}")
                for coarse in range(total_coarse):
                    for time_sample in range(768):  # 768 time samples per corner turn frame
                        abs_sample = (config["g_FRAME_COUNT_START"] + frame) * 768 + time_sample
                        # Evaluate the polynomials for all stations for this time sample
                        # Find the time to use in the polynomials
                        t_poly_s = 1e-9 * (t_poly_ns + time_sample * 64 * 1080.0)
                        beam_poly = np.zeros(total_stations)
                        beam_phase = np.zeros(total_stations)
                        beam_phase_step = np.zeros(total_stations)
                        for station in range(total_stations):
                            beam_poly[station] = (poly_config[station, beam, 0] + poly_config[station, beam, 1] * t_poly_s + 
                                poly_config[station, beam, 2] * (t_poly_s**2) + poly_config[station, beam, 3] * (t_poly_s**3) +
                                poly_config[station, beam, 4] * (t_poly_s**4) + poly_config[station, beam, 5] * (t_poly_s**5))
                            # phase to apply (as a number of rotations)
                            beam_phase[station] = beam_poly[station] * config["channels"][coarse]
                            # 14.4e-6 = Frequency step between fine channels in GHz
                            beam_phase_step[station] = beam_poly[station] * 14.46759259259259e-6 
                        
                        if time_sample < 0:
                            print(f"for time = {time_sample}, coarse = {coarse}, beam = {beam}, frame = {frame} : ")
                            print(f"phase = {beam_phase}")
                            print(f"phase_step = {beam_phase_step}")
                        
                        for fine in range(54):  # 54 fine channels per coarse channel 
                            # Compute the beam output (weighted sum of all the stations)
                            pol0_sum = 0
                            pol1_sum = 0
                            for station in range(total_stations):
                                # Get the sample generated in the testbench
                                # count in the testbench 
                                count_offset = (station // 4) * 768 * 54 + time_sample*54 + fine
                                if config["g_DATATYPE"] == 0:
                                    pol0_re_full = np.uint16((counter_start[4 * (station % 4) + 0] + count_offset) % 65536)
                                    pol0_im_full = np.uint16((counter_start[4 * (station % 4) + 1] + count_offset) % 65536)
                                    pol1_re_full = np.uint16((counter_start[4 * (station % 4) + 2] + count_offset) % 65536)
                                    pol1_im_full = np.uint16((counter_start[4 * (station % 4) + 3] + count_offset) % 65536)
                                    
                                    pol0_re = np.frombuffer(pol0_re_full, dtype=np.int16)
                                    pol0_im = np.frombuffer(pol0_im_full, dtype=np.int16)
                                    pol1_re = np.frombuffer(pol1_re_full, dtype=np.int16)
                                    pol1_im = np.frombuffer(pol1_im_full, dtype=np.int16)
                                elif config["g_DATATYPE"] == 1:
                                    # FD_data(0)(15 downto 0) <= 
                                    #   std_logic_vector(to_unsigned(128 * pkt_packet_count,16)) when pkt_packet_count < 256 and pkt_fine_channel = 40 else
                                    #   std_logic_vector(to_unsigned(32767 - 128 * (pkt_packet_count-256),16)) when pkt_fine_channel = 40 else
                                    if (station % 4) == 0 and (fine == 40):
                                        if time_sample < 256:
                                            pol0_re = 128 * time_sample 
                                        else:
                                            pol0_re = 32767 - 128 * (time_sample-256)
                                    else:
                                        pol0_re = 0
                                    pol0_im = 0
                                    pol1_re = 0
                                    pol1_im = 0
                                elif config["g_DATATYPE"] == 2:
                                    #FD_data(0)(15 downto 0) <= 
                                    #    std_logic_vector(to_unsigned(42 * pkt_packet_count,16)) when pkt_packet_count < 192 and pkt_fine_channel = 0 else
                                    #    std_logic_vector(to_unsigned(8064 - 42 * (pkt_packet_count-192),16)) when pkt_packet_count < 576 and pkt_fine_channel = 0 else
                                    #    std_logic_vector(to_unsigned(-8022 + 42 * (pkt_packet_count-576),16)) when pkt_fine_channel = 0 else (others => '0');
                                    #FD_data(0)(31 downto 16) <=  -- imaginary part, runs -8022 to +8022 to -8022
                                    #    std_logic_vector(to_unsigned(-8022 + 42 * (pkt_packet_count),16)) when pkt_packet_count < 384 and pkt_fine_channel = 0 else
                                    #    std_logic_vector(to_unsigned(8064 - 42 * (pkt_packet_count-384),16)) when pkt_fine_channel = 0 else (others => '0'); 
                                    if  (fine == 0):
                                        if time_sample < 192:
                                            pol0_re = 42 * time_sample
                                        elif time_sample < 576:
                                            pol0_re = 8064 - 42 * (time_sample - 192)
                                        else:
                                            pol0_re = -8022 + 42 * (time_sample - 576)
                                        if time_sample < 384:
                                            pol0_im = -8022 + 42 * time_sample
                                        else:
                                            pol0_im = 8064 - 42 * (time_sample - 384)
                                    else:
                                        pol0_re = 0
                                        pol0_im = 0
                                    pol1_re = 0
                                    pol1_im = 0
                                    
                                elif config["g_DATATYPE"] == 3:
                                    #
                                    if fine == 16:
                                        pol0_amplitude = 0
                                        pol1_amplitude = 4
                                    elif fine == 20:
                                        pol0_amplitude = 0
                                        pol1_amplitude = 3
                                    elif fine == 24:
                                        pol0_amplitude = 0
                                        pol1_amplitude = 2
                                    elif fine == 28:
                                        pol0_amplitude = 0
                                        pol1_amplitude = 1
                                    elif fine == 40:
                                        pol0_amplitude = 2
                                        pol1_amplitude = 0
                                    elif fine == 44:
                                        pol0_amplitude = 3
                                        pol1_amplitude = 0
                                    elif fine == 48:
                                        pol0_amplitude = 4
                                        pol1_amplitude = 0
                                    elif fine == 52:
                                        pol0_amplitude = 5
                                        pol1_amplitude = 0
                                    else:
                                        pol0_amplitude = 0
                                        pol1_amplitude = 0
                                    
                                    if time_sample < 192:
                                        pol0_re = time_sample * pol0_amplitude
                                        pol1_re = time_sample * pol1_amplitude
                                    elif time_sample < 576:
                                        pol0_re = pol0_amplitude * (192 - (time_sample - 192))
                                        pol1_re = pol1_amplitude * (192 - (time_sample - 192))
                                    else:
                                        pol0_re = pol0_amplitude * (-192 + (time_sample - 576))
                                        pol1_re = pol1_amplitude * (-192 + (time_sample - 576))
                                    
                                    if time_sample < 384:
                                        pol0_im = pol0_amplitude * (-192 + time_sample)
                                        pol1_im = pol1_amplitude * (-192 + time_sample)
                                    else:
                                        pol0_im = pol0_amplitude * (192 - (time_sample - 384))
                                        pol1_im = pol1_amplitude * (192 - (time_sample - 384))
                                    
                                else:
                                    print("!!!! config g_DATATYPE is not recognised - Using zero data !!!! ")
                                    pol0_re = 0
                                    pol0_im = 0
                                    pol1_re = 0
                                    pol1_im = 0
                                
                                pol0 = np.double(pol0_re) + 1j * np.double(pol0_im)
                                pol1 = np.double(pol1_re) + 1j * np.double(pol1_im)
                                
                                # Multiply by the Jones Matrix
                                # (beams) x (Two buffers) x (1024 virtual channels) x (2 rows) x (2 columns) x (2 [real + imaginary])
                                vc = station + coarse * total_stations
                                if apply_station_jones:
                                    jones00 = np.double(jones[jones_select,vc,0,0,0]) + 1j * np.double(jones[jones_select,vc,0,0,1])
                                    jones01 = np.double(jones[jones_select,vc,0,1,0]) + 1j * np.double(jones[jones_select,vc,0,1,1])
                                    jones10 = np.double(jones[jones_select,vc,1,0,0]) + 1j * np.double(jones[jones_select,vc,1,0,1])
                                    jones11 = np.double(jones[jones_select,vc,1,1,0]) + 1j * np.double(jones[jones_select,vc,1,1,1])
                                    # jones are 16-bit values scaled so that unity is 16384
                                    pol0_jones = np.round((jones00 * pol0 + jones01 * pol1)/16384)
                                    pol1_jones = np.round((jones10 * pol0 + jones11 * pol1)/16384)
                                    
                                    # Apply flagging; Most negative value (0x8000) indicates the result should be flagged 
                                    # OR if the outcome of the jones matrix multiplication is outside the valid range for a 16-bit signed value.
                                    # Flagged samples are replaced with zeros for the beamformer.
                                    if ((np.real(pol0) == -32768) or (np.imag(pol0) == -32768) or (np.real(pol1) == -32768) or (np.imag(pol1) == -32768) or
                                        (np.real(pol0_jones) > 32767) or (np.real(pol0_jones) < -32768) or (np.imag(pol0_jones) > 32767) or (np.imag(pol0_jones) < -32768) or
                                        (np.real(pol1_jones) > 32767) or (np.real(pol1_jones) < -32768) or (np.imag(pol1_jones) > 32767) or (np.imag(pol1_jones) < -32768)):
                                        pol0_jones = 0
                                        pol1_jones = 0
                                    else:
                                        # Not flagged, so include this weight
                                        if beam == 0: # same for all beams, only do this once
                                            packet_weights_unflagged[frame*48 + time_sample//16, coarse, fine] += weights[jones_select,vc]
                                    if beam == 0: # same for all beams, only do the calculation once
                                        packet_weights_total[frame*48 + time_sample//16, coarse, fine] += np.double(weights[jones_select,vc])
                                        
                                    #if (beam == 0) and (time_sample == 0) and (fine < 4):
                                    #    print(f"-- Station = {station}, fine = {fine}, pol0 = {pol0}, pol1 = {pol1}, pol0_jones = {pol0_jones}, pol1_jones = {pol1_jones}")
                                    
                                else:
                                    pol0_jones = pol0
                                    pol1_jones = pol1
                                
                                # Apply the phase correction
                                phase_offset = beam_phase[station] + beam_phase_step[station] * fine
                                phase_correction = np.exp(1j * 2 * np.pi * phase_offset)
                                pol0_phase = pol0_jones * phase_correction * 32  # x32 to match the firmware scaling
                                pol1_phase = pol1_jones * phase_correction * 32
                                # accumulate across the stations
                                pol0_sum += pol0_phase
                                pol1_sum += pol1_phase
                                
                            # total_frames,total_beams,total_coarse*216,256,2
                            # Apply beam Jones
                            # (Two buffers) x (1024 coarse channels) x (beams) x (2 rows) x (2 columns) x (2 [real + imaginary])
                            #  beam_jones = np.zeros((2, total_coarse, total_beams, 2, 2, 2), dtype=np.int16)
                            jones00 = np.double(beam_jones[jones_select,coarse,beam,0,0,0]) + 1j * np.double(beam_jones[jones_select,coarse,beam,0,0,1])
                            jones01 = np.double(beam_jones[jones_select,coarse,beam,0,1,0]) + 1j * np.double(beam_jones[jones_select,coarse,beam,0,1,1])
                            jones10 = np.double(beam_jones[jones_select,coarse,beam,1,0,0]) + 1j * np.double(beam_jones[jones_select,coarse,beam,1,0,1])
                            jones11 = np.double(beam_jones[jones_select,coarse,beam,1,1,0]) + 1j * np.double(beam_jones[jones_select,coarse,beam,1,1,1])
                            # jones are 16-bit values scaled so that unity is 16384
                            pol0_sum_jones = np.round((jones00 * pol0_sum + jones01 * pol1_sum)/16384)
                            pol1_sum_jones = np.round((jones10 * pol0_sum + jones11 * pol1_sum)/16384)
                            
                            raw_beamformed[frame*768+time_sample, beam, coarse*54 + fine, 0] = pol0_sum_jones
                            raw_beamformed[frame*768+time_sample, beam, coarse*54 + fine, 1] = pol1_sum_jones
                            
        #######################################################
        # Get the output of the simulation
        if args.bf_data:
            print("Loading the simulation output")
            # Arrays returned are : (time_sample, beam, fine channel, polarisation)
            (tb_packet_data, tb_raw_beamformed) = get_tb_data(args.bf_data, total_frames, total_beams, total_coarse)
            (tb_packet_scaled_data, tb_raw_scaled_beamformed, pkt_scales) = get_tb_scaled_data(args.bf_data, total_frames, total_beams, total_coarse)
            
            if int(args.debug) > 0:
                plt.figure()
                plt.subplot(8,1,1)
                plt.plot(np.real(tb_packet_scaled_data[:,0,16,1]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,16,1]),'g.-')
                plt.title('Channel 16, pol 1')
                plt.subplot(8,1,2)
                plt.plot(np.real(tb_packet_scaled_data[:,0,20,1]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,20,1]),'g.-')
                plt.title('Channel 20, pol 1')
                plt.subplot(8,1,3)
                plt.plot(np.real(tb_packet_scaled_data[:,0,24,1]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,24,1]),'g.-')
                plt.title('Channel 24, pol 1')
                plt.subplot(8,1,4)
                plt.plot(np.real(tb_packet_scaled_data[:,0,28,1]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,28,1]),'g.-')
                plt.title('Channel 28, pol 1')
                plt.subplot(8,1,5)
                plt.plot(np.real(tb_packet_scaled_data[:,0,40,0]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,40,0]),'g.-')
                plt.title('Channel 40, pol 0')
                plt.subplot(8, 1, 6)
                plt.plot(np.real(tb_packet_scaled_data[:,0,44,0]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,44,0]),'g.-')
                plt.title('Channel 44, pol 0')
                plt.subplot(8, 1, 7)
                plt.plot(np.real(tb_packet_scaled_data[:,0,48,0]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,48,0]),'g.-')
                plt.title('Channel 48, pol 0')
                plt.subplot(8,1,8)
                plt.plot(np.real(tb_packet_scaled_data[:,0,52,0]),'r.-')
                plt.plot(np.imag(tb_packet_scaled_data[:,0,52,0]),'g.-')
                plt.title('Channel 52, pol 0')
                
                plt.figure()
                plt.subplot(8,1,1)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,16,1]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,16,1]),'g.-')
                plt.title('de-scaled Channel 16, pol 1')
                plt.subplot(8,1,2)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,20,1]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,20,1]),'g.-')
                plt.title('de-scaled Channel 20, pol 1')
                plt.subplot(8,1,3)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,24,1]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,24,1]),'g.-')
                plt.title('de-scaled Channel 24, pol 1')
                plt.subplot(8,1,4)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,28,1]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,28,1]),'g.-')
                plt.title('de-scaled Channel 28, pol 1')
                plt.subplot(8,1,5)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,40,0]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,40,0]),'g.-')
                plt.title('de-scaled Channel 40, pol 0')
                plt.subplot(8, 1, 6)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,44,0]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,44,0]),'g.-')
                plt.title('de-scaled Channel 44, pol 0')
                plt.subplot(8, 1, 7)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,48,0]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,48,0]),'g.-')
                plt.title('de-scaled Channel 48, pol 0')
                plt.subplot(8,1,8)
                plt.plot(np.real(tb_raw_scaled_beamformed[:,0,52,0]),'r.-')
                plt.plot(np.imag(tb_raw_scaled_beamformed[:,0,52,0]),'g.-')
                plt.title('de-scaled Channel 52, pol 0')        
                
                plt.show()
            
            #############################################################
            # Compare data on a packet basis between python and firmware 
            print("Comparing python and simulation output")
            max_max_diff = 0
            max_max_scaled_diff = 0
            max_max_scaled_diff_fw8bit = 0
            scaled_max = 0
            scale_ratio_max_error = 0   # biggest error in the scale factor
            for frame in range(total_frames):
                for beam in range(total_beams):
                    for freq in range(total_coarse):
                        for times in range(48):
                            # The packet as produced at the output of the beamformer, before scaling to 8-bit
                            tb_pkt = tb_raw_beamformed[(frame*768 + times*16):(frame*768 + times*16+16), beam, (freq*54): (freq*54+54), :]
                            # The equivalent simulated value calculated in the python above
                            python_pkt = raw_beamformed[(frame*768 + times*16):(frame*768 + times*16+16), beam, (freq*54): (freq*54+54), :]
                            # the 8-bit data after scaling and reordering, at the output of the firmware scaling module
                            tb_8bit_pkt = tb_packet_scaled_data[(frame*768 + times*16):(frame*768 + times*16+16), beam, (freq*54): (freq*54+54), :]
                            # The scale factor used in the firmware to get to 8-bit data
                            tb_scale_factor = pkt_scales[frame*48 + times, beam, freq]
                            
                            tb_diff = tb_pkt - python_pkt
                            max_diff = np.max(np.abs(tb_diff))
                            # rms within the packet
                            tb_pkt_rms = np.sqrt(np.sum(tb_pkt * np.conj(tb_pkt))/(16*54*4))
                            python_pkt_rms = np.sqrt(np.sum(python_pkt * np.conj(python_pkt))/(16*54*4))
                            # Scale for 8 bit data
                            #print(f"pkt_rms = {tb_pkt_rms}")
                            scale = 10.5/tb_pkt_rms
                            # compare scale and tb_scale_factor, they should match closely
                            scale_ratio = scale / tb_scale_factor 
                            #print(f"Python scale = {scale}, firmware scale = {tb_scale_factor}, ratio = {scale_ratio}")
                            # Compare the output of the beamformer
                            tb_pkt_scaled = tb_pkt * scale
                            tb_pkt_scaled_max_real = np.max(np.real(tb_pkt_scaled))
                            tb_pkt_scaled_max_imag = np.max(np.imag(tb_pkt_scaled))
                            if tb_pkt_scaled_max_real > scaled_max:
                                scaled_max = tb_pkt_scaled_max_real
                            if tb_pkt_scaled_max_imag > scaled_max:
                                scaled_max = tb_pkt_scaled_max_imag
                            python_pkt_scaled = python_pkt * scale
                            python_pkt_scaled_with_fw_scale = python_pkt * tb_scale_factor
                            # saturate to -128 to 127 range
                            for t1 in range(16):
                                for f1 in range(54):
                                    for p1 in range(2):
                                        r1 = np.real(python_pkt_scaled_with_fw_scale[t1, f1, p1])
                                        i1 = np.imag(python_pkt_scaled_with_fw_scale[t1, f1, p1])
                                        if r1 > 127:
                                            r1 = 127
                                        elif r1 < -128:
                                            r1 = -128
                                        if i1 > 127:
                                            i1 = 127
                                        elif i1 < -128:
                                            i1 = -128
                                        python_pkt_scaled_with_fw_scale[t1, f1, p1] = r1 + 1j * i1
                            
                            scaled_diff = tb_pkt_scaled - python_pkt_scaled
                            scaled_diff_max = np.max(np.abs(scaled_diff))
                            
                            # Compare with the scaled data produced by the simulation
                            scaled_diff_fw8bit = tb_8bit_pkt - python_pkt_scaled_with_fw_scale
                            scaled_diff_fw8bit_max = np.max(np.abs(scaled_diff_fw8bit))
                            
                            if np.abs(scale_ratio-1) > np.abs(scale_ratio_max_error):
                                # Difference in the scale factor from the firmware and the value calculated in the python
                                scale_ratio_max_error = (scale_ratio - 1)
                            if scaled_diff_fw8bit_max > max_max_scaled_diff_fw8bit:
                                max_max_scaled_diff_fw8bit = scaled_diff_fw8bit_max
                                max_scaled_diff_loc_fw8bit = (frame, beam, freq, times)
                            
                            if scaled_diff_max > max_max_scaled_diff:
                                max_max_scaled_diff = scaled_diff_max
                                max_scaled_diff_loc = (frame, beam, freq, times)
                            if max_diff > max_max_diff :   
                                # Difference in the 16-bit output of the beamformer
                                max_max_diff = max_diff
                                max_diff_loc = (frame, beam, freq, times)
                                print(f"  -- frame {frame}, beam {beam}, freq {freq}, time {times}, max diff = {max_diff}")
                                print(f"      scaled for 8bit max diff = {scaled_diff_max}")
                                print(f"     - tb pkt rms = {tb_pkt_rms}, python pkt rms = {python_pkt_rms}")
                                # print(f"     - tb_pkt size = {tb_pkt.shape}")
                                if int(args.debug) > 0:
                                    plt.figure()
                                    plt.subplot(4,1,1)
                                    plt.plot(np.real(tb_pkt[:,0,0]),'r.-')
                                    plt.plot(np.real(python_pkt[:,0,0]),'g.-')
                                    plt.title('freq 0, pol 0, real, tb red, python green')
                                    plt.subplot(4,1,2)
                                    plt.plot(np.real(tb_pkt[:,40,0]),'r.-')
                                    plt.plot(np.real(python_pkt[:,40,0]),'g.-')
                                    plt.title('freq 40, pol 0, real, tb red, python green')
                                    plt.subplot(4,1,3)
                                    plt.plot(np.real(python_pkt_scaled_with_fw_scale[:,0,0]),'g.-')
                                    plt.plot(np.real(tb_8bit_pkt[:,0,0]),'r.-')
                                    plt.title("fine channel 0, real, pol 0, tb red, python green")
                                    plt.subplot(4,1,4)
                                    plt.plot(np.imag(python_pkt_scaled_with_fw_scale[:,0,0]),'g.-')
                                    plt.plot(np.imag(tb_8bit_pkt[:,0,0]),'r.-')
                                    plt.title("fine channel 0, imag, pol 0, tb red, python green")
                                    plt.show()
                                
            print(f"max diff python vs firmware = {max_max_diff}, 8-bit packet diff = {max_max_scaled_diff}, at frame {max_diff_loc[0]}, beam {max_diff_loc[1]}, freq {max_diff_loc[2]}, time {max_diff_loc[3]}")
            print(f"max diff python vs firmware 8-bit at beamformer output = {max_max_scaled_diff}, at frame {max_scaled_diff_loc[0]}, beam {max_scaled_diff_loc[1]}, freq {max_scaled_diff_loc[2]}, time {max_scaled_diff_loc[3]}")
            print(f"max diff python vs firmware 8-bit using fw scaling module = {max_max_scaled_diff_fw8bit}, at frame {max_scaled_diff_loc_fw8bit[0]}, beam {max_scaled_diff_loc_fw8bit[1]}, freq {max_scaled_diff_loc_fw8bit[2]}, time {max_scaled_diff_loc_fw8bit[3]}")
            print(f"Max fractional diff in the scaling factor = {scale_ratio_max_error}")
            print(f"Max packet value after scaling for 8-bit = {scaled_max}")
            
            if (max_max_scaled_diff > 0.5) or (max_max_scaled_diff_fw8bit > 1) or (scale_ratio_max_error > 0.001):
                print(f"!!! :( Test FAIL  ): !!!")
            else:
                print(f"(: Test PASS :)")
            
            frame = max_diff_loc[0]
            beam = max_diff_loc[1]
            freq = max_diff_loc[2]
            times = max_diff_loc[3]
            
            if int(args.debug) > 0:
                print(f"Plot for frame {frame}, beam {beam}, coarse channel {freq}, time {times}")
                tb_pkt = tb_raw_beamformed[(frame*768 + times*16):(frame*768 + times*16+16), beam, (freq*54): (freq*54+54), :]
                python_pkt = raw_beamformed[(frame*768 + times*16):(frame*768 + times*16+16), beam, (freq*54): (freq*54+54), :]
                fig, axs = plt.subplots(4,4)
                for time in range(16):
                    # 16 time samples in a packet, plot all 54 frequency channels for each time sample
                    axs[time//4, time%4].plot(np.real(python_pkt[time,:,0]),'ro')
                    axs[time//4, time%4].plot(np.imag(python_pkt[time,:,0]),'go')
                    axs[time//4, time%4].plot(np.real(tb_pkt[time,:,0]),'r.-')
                    axs[time//4, time%4].plot(np.imag(tb_pkt[time,:,0]),'g.-')
                    axs[time//4, time%4].set_title(f"time={time},pol=0, r,g circle python, lines fw")
                plt.show()
        

if __name__ == "__main__":
    main()