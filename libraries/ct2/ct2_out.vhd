----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 30.10.2020 22:21:03
-- Module Name: ct_atomic_pst_out - Behavioral
-- Description: 
--    Corner turn between the filterbanks and the beamformer for PST processing. 
-- 
-- Data coming in from the filterbanks :
--   4 dual-pol channels, with burst of 54 fine channels at a time.
--   Total number of bytes per clock coming in is  (4 channels)*(2 pol)*(2+2 complex) = 32 bytes.
--   with roughly 54 out of every 66 clocks active.
--   Total data rate in is thus roughly (32 bytes * 8 bits)*54/66 * 300 MHz = 62 Gb/sec (this is the average data rate while data is flowing)
--   Actual total data rate in is (long term average)
--    (54/64 fine channels used) * (1/1080ns sampling period) * (2 pol) * (16+16 bits/sample) * (1024 channels) = 51.2 Gb/sec 
--
-- Storing to HBM
--   We get 24*2048*1080ns = 53.08416 ms of data for 4 channels, then 53.08416ms for the next 4 channels, up to 1024 channels.
--   The beamformers need this to be rearranged so the data for all stations can be processed before moving to the next time.
--   The number of PSS filterbank outputs per frame is related to the number of SPS packets per frame by
--     (SPS packets * (2048/64)); so 24 LFAA packets per frame = 768 filterbank frames
--   Groups of 4 virtual channels are stored as a contiguous block of memory, so the address in the memory is calculated as:
--     Given F = Fine Channel  (0 to 53)
--           V = virtual channel (0 to up to 1023)
--           T = Time within the block (0 to 767, for corner turn with 24 SPS packets)
--     Byte Address = T*(1024/4)*2048 + floor(V/4)*2048 + F*32 + 8*mod(V,4)
--   Note :
--     * 8 bytes per fine channel = (2 pol) * (2+2 complex)
--     * 4 virtual channels at a time = 4*8 = 32 bytes
--     * 54 fine channels. 54*32 = 1728 bytes, so the first 1728 bytes are used in each 2048 byte block
--     * 4 virtual channels come in at a time, so a 1728 byte write is done for each packet that comes from the filterbank
--     * Then writes skip ahead by (1024/4)*2048 = 512 Kbytes to the next time sample
--     * On the read side, blocks of 1728 bytes are read (all fine channels for 4 virtual channels) for 
--       all stations used in the beamforming.
--   
--   There is a buffer in this module to hold data from the 4 virtual channels while it is being written to the HBM
--   The buffer is 512 deep x 512 bits wide.
--      - Split into 16 buffers, each 32 deep (27 used, 5 spare);
--        Each of the 16 buffers holds data for a particular set of 4 virtual channels, for 54 fine channels.
--        - Each of the  buffers is 27 deep x 512 bits wide
--          - 32 bytes = 256 bits is received each clock cycle from the filterbank, 
--            so every 2nd clock, 512 bits (=64 bytes) is written to the buffer. 
--   Writes to the HBM for each packet consist of 1 write of 27 words, for 27 x 512 bit words total.  
--
-- Data out to the beamformer
--   Use a (slightly greater than) 400 MHz clock, 128 bits/clock, for a (peak) data rate of 51.2 Gbits/sec. 
--
--   128 bits = 2 * 64 bits = 2 dual-pol (16+16 bit complex) samples
--   
--   Data is processed by the beamformer in groups of 54 fine channels
--   The order of data going to the beamformer :
--
--    For timeGroup = 0:48                         -- 53 ms corner turn, 48*16=768 total time samples
--       For Coarse = 0:(i_coarse-1)               -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 16 + (0:16)     -- 16 times needed for an output packet
--             For Station_block = 0:(i_stations/4)
--                For fine_offset = 0:54              -- Total of 54 fine channels per SPS channel
--                   For station = 0:4
--                      Send a 128 bit word (i.e. 1 fine channels x 2 stations x (2 pol) x (16+16 bit complex data)).
--
----------------------------------------------------------------------------------

library IEEE, ct_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--library DSP_top_lib
--use DSP_top_lib.DSP_top_pkg.all;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct2_out is
    generic (
        g_USE_META : boolean := FALSE;  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
        -- The number of clocks to output a packet is 864 (=(54 fine channels)*(16 times))
        -- g_PACKET_GAP is the gap from one packet output to the next, so 960 allows for 960-864 = 96 clocks idle time between packets
        g_PACKET_GAP        : integer;
        g_PACKETS_PER_FRAME  : std_logic_vector(9 downto 0) -- Packets per frame; Should be 768 (for 24 SPS packets per frame)
    );
    port(
        -- Parameters, in the i_axi_clk domain.
        i_stations : in std_logic_vector(10 downto 0); -- up to 1024 stations
        i_coarse   : in std_logic_vector(9 downto 0);  -- Number of coarse channels.
        i_virtualChannels : in std_logic_vector(10 downto 0); -- total virtual channels (= i_stations * i_coarse)
        o_HBMBuf0PacketCount : out std_logic_vector(36 downto 0);
        o_HBMBuf1PacketCount : out std_logic_vector(36 downto 0);
        i_beams_enabled : in std_logic_vector(9 downto 0);
        i_bad_polynomials : in std_logic;
        
        -- Indicate start of readout of a corner turn frame
        -- Outputs on i_axi_clk
        o_readout_start : out std_logic;
        o_readout_frame : out std_logic_vector(36 downto 0); -- Corner turn frame that we are about to start reading out, relative to the SKA epoch
        -- Polynomial FIFO is empty, on i_BF_clk 
        i_poly_fifo_empty : in std_logic;
        
        --i_BFJonesSelect : in std_logic;
        --i_BFPhaseSelect : in std_logic;
        --i_JonesSwitch   : in std_logic_vector(31 downto 0); -- Packet count to switch to the new value of BFJonesSelect
        --i_phaseSwitch   : in std_logic_vector(31 downto 0); -- Packet count to switch to the new value of BFPhaseSelect
        i_rst : in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof            : in std_logic; -- pulse high at the start of every frame. (1 frame is ~ 53ms of data).
        i_FB_clk         : in std_logic; -- filterbank clock, expected to be 450 MHz
        i_frameCount     : in std_logic_vector(36 downto 0); -- Corner turn frames since SKA epoch, same for all simultaneous output streams.
        i_virtualChannel : in t_slv_16_arr(3 downto 0);      -- 4 virtual channels, one for each of the PSS data streams.
        i_HeaderValid    : in std_logic_vector(3 downto 0);
        i_data           : in t_slv_64_arr(3 downto 0); -- (2 pol)x(16+16 bit complex) = 64 bits, for each of 4 virtual channels.
        i_dataValid      : in std_logic;
        
        ----------------------------------------------------------------------------
        -- Data out to the beamformer
        i_BF_clk  : in std_logic; -- beamformer clock, expected to be 400 MHz
        -- o_data and o_flagged have a 4 cycle latency relative to the other control signals.
        o_data    : out std_logic_vector(127 downto 0); -- 2 consecutive stations delivered every clock.
        o_flagged : out std_logic_vector(1 downto 0);   -- o_flagged is aligned with o_data.
        --
        o_fine    : out std_logic_vector(7 downto 0);  -- fine channel, 0 to 53
        o_coarse  : out std_logic_vector(9 downto 0);  -- Coarse channel, indexed from 0 to however many distinct coarse channels there are.
        o_stations01 : out std_logic;  -- Output ordering : Four stations delivered over 2 clocks for each fine channel; this is the first of the two clock cycles.
        o_firstStation : out std_logic;
        o_lastStation  : out std_logic;
        o_timeStep     : out std_logic_vector(3 downto 0);  -- Time within the packet, 0 to 15 (16 time samples per PSS output packet)
        o_station      : out std_logic_vector(9 downto 0);  -- Station for the first of the 4 stations being output in a burst of 2 clocks
        o_virtualChannel : out std_logic_vector(9 downto 0); -- Note this takes steps of 4 virtual channels (4 stations delivered every 2 clocks in the output data stream)
        o_packetCount  : out std_logic_vector(47 downto 0); -- PSS output packet count, units of 16*64*1080ns = 1.10592 ms since the SKA epoch.
        o_outputPktOdd : out std_logic; -- Alternates between 0 and 1 for each burst of data that corresponds to a set of 9 output packets.
        o_good_polynomials : out std_logic; -- good station polynomials.
        o_valid   : out std_logic; -- aligns with the control signals, not the data.
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        i_axi_clk : in std_logic;
        -- aw bus = write address
        m02_axi_awvalid  : out std_logic;
        m02_axi_awready  : in std_logic;
        m02_axi_awaddr   : out std_logic_vector(29 downto 0);
        m02_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m02_axi_wvalid    : out std_logic;
        m02_axi_wready    : in std_logic;
        m02_axi_wdata     : out std_logic_vector(511 downto 0);
        m02_axi_wlast     : out std_logic;
        -- b bus - write response
        m02_axi_bvalid    : in std_logic;
        m02_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m02_axi_arvalid   : out std_logic;
        m02_axi_arready   : in std_logic;
        m02_axi_araddr    : out std_logic_vector(29 downto 0);
        m02_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid    : in std_logic;
        m02_axi_rready    : out std_logic;
        m02_axi_rdata     : in std_logic_vector(511 downto 0);
        m02_axi_rlast     : in std_logic;
        m02_axi_rresp     : in std_logic_vector(1 downto 0);
        
        ---------------------------------------------------
        -- Monitoring and Error signals on the i_axi_clk domain.
        o_bufferOverflowError : out std_logic; -- buffer between data input from the filterbanks and the HBM has overflowed; this should never happen.
        o_readoutError : out std_logic;  -- readout to the beamformer didn't finish before the next readout started. This should never happen.
        o_readoutClocks : out std_logic_vector(31 downto 0); -- Number of beamformer (400MHz) clocks required to read the most recent frame out from HBM
        o_readInClocks : out std_logic_vector(31 downto 0);   -- Number of axi (300MHz) clocks required to write the most recent frame into HBM
        o_readInAllClocks : out std_logic_vector(31 downto 0); -- Number of axi (300MHz) clocks between the start of one frame and the start of the next.
        o_dataMismatch : out std_logic;
        --
        o_sof_count         : out std_logic_vector(31 downto 0);
        o_fb_packet_count   : out std_logic_vector(31 downto 0);
        o_cur_frameCount    : out std_logic_vector(31 downto 0);
        o_recent_vcs        : out std_logic_vector(31 downto 0);
        o_dbg_packetInFrame : out std_logic_vector(31 downto 0);
        -- on the 400MHz clock.
        o_dataMismatchBFclk : out std_logic
    );
end ct2_out;

architecture Behavioral of ct2_out is

    signal data0, data1, data2, data3 : std_logic_vector(63 downto 0);
    signal dataValid : std_logic := '0';
    signal dataCount : std_logic_vector(7 downto 0) := "00000000";  -- Counts through the 216 fine channels in an incoming packet.
    signal frameCount : std_logic_vector(36 downto 0);
    signal virtualChannel : t_slv_16_arr(3 downto 0);
    signal packetInFrame, nextPacketInFrame : std_logic_vector(9 downto 0);
    
    signal cdc_src_send : std_logic := '0';
    signal cdc_src_rcv : std_logic;
    signal cdc_src_in : std_logic_vector(62 downto 0);
    signal cdc_dest_out : std_logic_vector(62 downto 0);
    signal cdc_dest_req : std_logic;
    
    type aw_fsm_type is (start, set_aw, wait_fifo, done);
    signal aw_fsm : aw_fsm_type := done;
    signal AWchan : std_logic_vector(1 downto 0);
    signal AWvirtualChannel : std_logic_vector(9 downto 0);
    signal AWpacketInFrame : std_logic_vector(9 downto 0);
    signal wdataFIFO_wrDataCount : std_logic_vector(9 downto 0);
    signal firstInFrame : std_logic := '0';
    
    signal wdataFIFO_wrEn : std_logic;
    signal wdataFIFO_empty : std_logic;
    signal wdataFIFO_rdEn : std_logic;
    signal wdataFIFO_dout : std_logic_vector(512 downto 0); -- 512 data bits, plus the axi_wlast bit.
    signal packetBuf0Used, packetBuf1Used, packetBuf2Used, packetBuf3Used : std_logic := '0';
    signal wdataFIFO_din : std_logic_vector(512 downto 0);
    signal AWFIFO_dout, awFIFO_din : std_logic_vector(29 downto 0);
    signal awFIFO_empty : std_logic;
    signal awFIFO_rdEn : std_logic := '0';
    signal awFIFO_wrEn : std_logic := '0';
    signal awFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal fifoIn_m02_axi_awaddr : std_logic_vector(29 downto 0);
    signal HBMBuf0PacketCount, HBMBuf1PacketCount : std_logic_vector(36 downto 0);
    -- HBMBuf2PacketCount, HBMBuf3PacketCount : std_logic_vector(31 downto 0);
    signal AWFrameCount : std_logic_vector(36 downto 0);
    signal AWFirstInFrame : std_logic := '0';
    signal AWLastInFrame : std_logic := '0';
    signal maxVirtualChannel : std_logic_vector(10 downto 0);
    signal HBMRdBuffer : std_logic := '0';
    signal HBMBufferEnable : std_logic_vector(3 downto 0);
    signal rstDel1 : std_logic := '0';
    signal HBMWrBuffer : std_logic := '0';
    signal rdFrameCount, frameCount37 : std_logic_vector(36 downto 0);
    signal stationsMinusOne : std_logic_vector(10 downto 0);
    signal coarseMinusOne : std_logic_vector(9 downto 0);
    
    signal rdStation : std_logic_vector(9 downto 0);
    signal rdTime : std_logic_vector(9 downto 0);
    signal rdCoarse : std_logic_vector(8 downto 0);
    signal totalStations : std_logic_vector(10 downto 0);
    signal coarse_x_stations : unsigned(19 downto 0);
    signal outputCoarse_x_stations : unsigned(20 downto 0);
    signal ARvirtualChannel : std_logic_vector(9 downto 0);
    signal ARaddr : std_logic_vector(29 downto 0);
    signal packetsPerFrameMinusOne : std_logic_vector(9 downto 0);
    
    type rd_fsm_type is (start, wait_readout_start, set_start, get_addr, send_addr, wait_ready, wait_calc0, wait_calc1, update_stationTimeCoarse, check_space, done);
    signal rd_fsm : rd_fsm_type := done;
    signal outstandingRequests : std_logic_vector(5 downto 0) := "000000";
    signal outstandingRequests_x32, outstandingRequests_x27 : std_logic_vector(10 downto 0);
    signal outstandingRequests_x4 : std_logic_vector(10 downto 0);
    signal outstandingRequests_x1 : std_logic_vector(10 downto 0);
    signal fifoSpaceUsed : std_logic_vector(10 downto 0);
    signal rdataFIFO_wrDataCount : std_logic_vector(9 downto 0);
    signal newRequest, requestDone : std_logic;
    
    signal axi2bf_src_rcv, axi2bf_src_send : std_logic := '0';
    signal axi2BF_dataIn : std_logic_vector(68 downto 0);
    signal axi2bf_dest_out : std_logic_vector(68 downto 0);
    signal BFpacketsPerFrameMinusOne : std_logic_vector(9 downto 0);
    signal BFstations, BFstationsMinusOne : std_logic_vector(10 downto 0);
    signal BFcoarse, BFcoarseMinusOne : std_logic_vector(9 downto 0);
    signal axi2bf_dest_req : std_logic;
    type readout_fsm_type is (wait_start_fine, wait_poly_fifo, start_fine, wait_fifo, p0, p1, p2, p3, done);
    signal readout_fsm, readout_fsm_del1, readout_fsm_del2, readout_fsm_del3, readout_fsm_del4 : readout_fsm_type := done;
    signal rdataFIFO_rdEn : std_logic;
    signal rdataFIFO_dout : std_logic_vector(511 downto 0);
    signal rdataFIFO_empty : std_logic;
    signal rdataFIFO_rdDataCount : std_logic_vector(9 downto 0);
    signal outputBuffer : std_logic_vector(639 downto 0);
    
    signal outputFine, outputFineDel1, outputFineDel2, outputFineDel3, outputFineDel4 : std_logic_vector(7 downto 0) := "00000000";
    signal outputStation, outputStationDel1, outputStationDel2, outputStationDel3, outputStationDel4 : std_logic_vector(9 downto 0) := "0000000000";
    signal outputCoarse, outputCoarseDel1, outputCoarseDel2, outputCoarseDel3, outputCoarseDel4 : std_logic_vector(9 downto 0) := "0000000000";
    signal BFFrameCount : std_logic_vector(36 downto 0);
    signal outputTime : std_logic_vector(9 downto 0);
    signal packetCountDel1, packetCountDel2 : std_logic_vector(5 downto 0);
    signal lastStation, lastCoarse, lastTimeIn16, lastTime : std_logic := '0';
    signal BFlastStation, BFlastTimeIn16, BFlastCoarse, BFlastTime : std_logic := '0';
    signal rstDel2, rstDel3 : std_logic;
    signal FBbufferOverflowError : std_logic;
    signal rdataFIFO_rst, awFIFO_rst, wdataFIFO_rst : std_logic := '0';
    signal AXIbufferOverflowError : std_logic;
    signal FB_virtualChannels : std_logic_vector(10 downto 0) := "00000000000";
    
    signal BFlastStationDel2, BFlastStationDel3, BFlastStationDel4 : std_logic;
    signal BFfirstStationDel1, BFfirstStationDel2, BFfirstStationDel3, BFfirstStationDel4 : std_logic;
    signal outputTimeDel1, outputTimeDel2, outputTimeDel3, outputTimeDel4 : std_logic_vector(9 downto 0);
    signal outputVirtualChannelDel2, outputVirtualChannelDel3, outputVirtualChannelDel4 : std_logic_vector(9 downto 0);
    signal outputCoarseChannelDel1, outputCoarseChannelDel2, outputCoarseChannelDel3, outputCoarseChannelDel4 : std_logic_vector(9 downto 0);
    
    signal AWChannelValid : std_logic_vector(3 downto 0) := "0000";
    signal axi2bf_dest_reqDel1 : std_logic;
    signal outputBufferDel1, outputBufferDel2, outputBufferDel3, outputBufferDel4 : std_logic_vector(127 downto 0);
    
    signal m02_arlen_read6 : std_logic;
    signal m02_axi_araddrInt : std_logic_vector(29 downto 0);
    signal gapCount : std_logic_vector(23 downto 0);
    signal gapCount_eq0 : std_logic := '0';
    signal minGapDataIn0, minGapDataIn : std_logic_vector(23 downto 0);
    signal readoutClocks, clocksPerCornerTurnOut : std_logic_vector(31 downto 0);
        
    signal BF2axi_dataIn : std_logic_vector(31 downto 0);
    signal BF2axi_transferCount : std_logic_vector(3 downto 0) := "0000";
    signal BF2axi_src_send : std_logic := '0';
    signal bf2axi_src_rcv : std_logic;
    signal BF2axi_dest_out : std_logic_vector(31 downto 0);
    signal BF2axi_dest_req : std_logic;
    signal readInCount, readInAllClocks : std_logic_vector(31 downto 0);
    signal cdc_dest_req_del1 : std_logic;
    signal fineCount : std_logic_vector(7 downto 0);
    signal outputFineDel4x2 : std_logic_vector(7 downto 0);
    signal metaRecon : std_logic_vector(31 downto 0);
    signal dataMisMatchInt : std_logic := '0';
    signal sample_flagged : std_logic_vector(7 downto 0);
    
    signal beams_enabled    : std_logic_vector(9 downto 0);
    signal valid_int : std_logic := '0';
    signal sof_del : std_logic;
    signal sof_count, fb_packet_count, cur_frameCount, recent_vcs, dbg_packetInFrame : std_logic_vector(31 downto 0) := (others => '0');
    signal max_packetInFrame, recent_packetInFrame : std_logic_vector(9 downto 0) := "0000000000";
    signal fb_dbg_reg, axi_dbg_reg : std_logic_vector(159 downto 0);
    signal readout_start_int : std_logic;
    signal wait_poly_fifo_count : std_logic_vector(3 downto 0) := "1111";
    signal outputPktOdd : std_logic;
    signal outputPktOddDel1, outputPktOddDel2, outputPktOddDel3, outputPktOddDel4 : std_logic;
    signal bad_polynomials : std_logic;
    signal bad_polynomials_BF_clk : std_logic;
    
    signal beams_enabled_div4 : std_logic_vector(7 downto 0);
    signal beams_enabled_ceil_div4 : std_logic_vector(7 downto 0);
    signal holdoff_count : std_logic_vector(7 downto 0) := x"00";
    signal BFFrameCount_x16, BFFrameCount_x32, packetCountDel3, packetCountDel4 : std_logic_vector(43 downto 0);
    
begin
    
    rdataFIFO_rst <= rstDel2; -- i_axi_clk domain
    awFIFO_rst <= rstDel3;  -- i_axi_clk domain
    
    -- Copy reset across to the wdata FIFO
    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    ) port map (
        dest_pulse => wdataFIFO_rst, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_FB_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_axi_clk,       -- 1-bit input: Source clock.
        src_pulse => rstDel3,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );
    
    -- Copy buffer overflow error signal to the i_axi_clk domain
    xpm_cdc_BFoverflow_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => AXIbufferOverflowError, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_FB_clk,       -- 1-bit input: Source clock.
        src_pulse => FBbufferOverflowError,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );    
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            
            -- Hold bufferoverflow error
            if rstDel3 = '1' then
                o_bufferOverflowError <= '0';
            elsif AXIbufferOverflowError = '1' then
                o_bufferOverflowError <= '1';
            end if;
        end if;
    end process;
    
    
    -- Get the number of virtual channels into the i_FB_clk domain
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 11          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => FB_virtualChannels, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_FB_clk, -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_axi_clk,   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => i_virtualChannels  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );
    
    xpm_cdc_array_single_dbginst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 160         -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => axi_dbg_reg, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_axi_clk,   -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_FB_clk,     -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => fb_dbg_reg     -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );
    
    fb_dbg_reg(31 downto 0) <= sof_count;
    fb_dbg_reg(63 downto 32) <= fb_packet_count;
    fb_dbg_reg(95 downto 64) <= cur_frameCount;
    fb_dbg_reg(127 downto 96) <= recent_vcs;
    fb_dbg_reg(159 downto 128) <= dbg_packetInFrame;
    
    o_sof_count  <= axi_dbg_reg(31 downto 0);       -- out std_logic_vector(31 downto 0);
    o_fb_packet_count <= axi_dbg_reg(63 downto 32); -- out std_logic_vector(31 downto 0);
    o_cur_frameCount <= axi_dbg_reg(95 downto 64);  -- out std_logic_vector(31 downto 0);
    o_recent_vcs <= axi_dbg_reg(127 downto 96);     -- out std_logic_vector(31 downto 0);
    o_dbg_packetInFrame <= axi_dbg_reg(159 downto 128);
    
    process(i_FB_clk)
    begin
        if rising_edge(i_FB_clk) then
            
            -- get input data into 64 bit registers.
            -- Delay data0, data1 and data2 by different amounts so that the part of the buffer they are being written to doesn't clash
            if (g_USE_META) then
                -- Each 32 bits has:
                --   (7:0) = fine channel, runs 0 to 53.
                --   (19:8) = time sample, runs from 0 to 767  (total 768 time samples per 53 ms corner turn frame)
                --   (31:20) = virtual channel.
                
                if unsigned(packetInFrame) >= (unsigned(g_PACKETS_PER_FRAME) - 1) then  
                    -- (24 SPS packets) * (2048 samples/SPS packet) / (64 samples/filterbank packet) 
                    --  = 768 packets per frame; packetInFrame counts 0 to 767
                    nextPacketInFrame <= (others => '0');
                else
                    nextPacketInFrame <= std_logic_vector(unsigned(packetInFrame) + 1);
                end if;
                
                if i_dataValid = '1' and dataValid = '0' then
                    fineCount <= x"01";
                    data0(7 downto 0) <= x"00";
                    data1(7 downto 0) <= x"00";
                    data2(7 downto 0) <= x"00";
                    data3(7 downto 0) <= x"00";
                    data0(19 downto 8) <= "00" & nextPacketInFrame;
                    data1(19 downto 8) <= "00" & nextPacketInFrame;
                    data2(19 downto 8) <= "00" & nextPacketInFrame;
                    data3(19 downto 8) <= "00" & nextPacketInFrame;
                elsif i_dataValid = '1' then
                    fineCount <= std_logic_vector(unsigned(fineCount) + 1);
                    data0(7 downto 0) <= fineCount;
                    data1(7 downto 0) <= fineCount;
                    data2(7 downto 0) <= fineCount;
                    data3(7 downto 0) <= fineCount;
                    data0(19 downto 8) <= "00" & packetInFrame;
                    data1(19 downto 8) <= "00" & packetInFrame;
                    data2(19 downto 8) <= "00" & packetInFrame;
                    data3(19 downto 8) <= "00" & packetInFrame;
                end if;
                data0(31 downto 20) <= i_virtualChannel(0)(11 downto 0);
                data1(31 downto 20) <= i_virtualChannel(1)(11 downto 0);
                data2(31 downto 20) <= i_virtualChannel(2)(11 downto 0);
                data3(31 downto 20) <= i_virtualChannel(3)(11 downto 0);
                data0(63 downto 32) <= (others => '0');
                data1(63 downto 32) <= (others => '0');
                data2(63 downto 32) <= (others => '0');
                data3(63 downto 32) <= (others => '0');
            else
                data0 <= i_data(0);
                data1 <= i_data(1);
                data2 <= i_data(2);
                data3 <= i_data(3);
            end if;
            
            dataValid <= i_dataValid;

            if dataValid = '1' and dataCount(0) = '1' then
                -- Write 512 bits every second clock.
                wdataFIFO_wrEn <= '1';
                wdataFIFO_din(511 downto 256) <= data3 & data2 & data1 & data0;
                if unsigned(dataCount) = 53 then
                    wdataFIFO_din(512) <= '1'; -- final word in the packet from the filterbank
                else
                    wdataFIFO_din(512) <= '0';
                end if;
            else
                wdataFIFO_wrEn <= '0';
                wdataFIFO_din(255 downto 0) <= data3 & data2 & data1 & data0;
            end if;
            
            if i_headerValid(0) = '1' or i_headerValid(1) = '1' or i_headerValid(2) = '1' or i_headerValid(3) = '1' then
                frameCount <= i_frameCount; -- 37 bit corner turn frames since SKA epoch; frame count is the same for all simultaneous streams from the filterbanks.
                virtualChannel <= i_virtualChannel;
            end if;
            
            if i_sof = '1' then  -- start of a 53 ms frame
                -- Count of the packet within the frame. For 53ms frames, this will count from 0 to 767 for every group of 4 virtual channels in the frame.
                packetInFrame <= (others => '1');
            elsif i_dataValid = '1' and dataValid = '0' then
                dataCount <= "00000000";
                if unsigned(packetInFrame) >= (unsigned(g_PACKETS_PER_FRAME) - 1) then  
                    -- packetInFrame counts through 768 time samples per frame (0 to 767)
                    packetInFrame <= (others => '0');
                else
                    packetInFrame <= std_logic_vector(unsigned(packetInFrame) + 1);
                end if;
            elsif dataValid = '1' then
                -- word in the packet from the filterbank, counts 0 to 53
                dataCount <= std_logic_vector(unsigned(dataCount) + 1);  -- The value in dataCount aligns with the sample in data0, data1, data2, data3.
            end if;
            
            ----------------------------------------------------------------------------
            -- Gather some info on the incoming data stream
            sof_del <= i_sof;
            if i_sof = '1' and sof_del = '0' then
                sof_count <= std_logic_vector(unsigned(sof_count) + 1);
            end if;
            
            if i_dataValid = '0' and dataValid = '1' then
                fb_packet_count <= std_logic_vector(unsigned(fb_packet_count) + 1);
            end if;
            
            if packetInFrame(9) = '0' and unsigned(packetInFrame) > unsigned(max_packetInFrame) then
                max_packetInFrame <= packetInFrame;
            end if;
            
            if i_dataValid = '0' and dataValid = '1' then
                recent_packetInFrame <= packetInFrame;
            end if;
            
            dbg_packetInFrame(9 downto 0) <= max_packetInFrame;
            dbg_packetInFrame(15 downto 10) <= "000000";
            dbg_packetInFrame(25 downto 16) <= recent_packetInFrame;
            dbg_packetInFrame(31 downto 26) <= "000000";
            cur_frameCount <= i_frameCount(31 downto 0);
            recent_vcs(7 downto 0) <= virtualChannel(0)(7 downto 0);
            recent_vcs(15 downto 8) <= virtualChannel(1)(7 downto 0);
            recent_vcs(23 downto 16) <= virtualChannel(2)(7 downto 0);
            recent_vcs(31 downto 24) <= virtualChannel(3)(7 downto 0);
            
            ----------------------------------------------------------------------------
            -- At the end of every packet, send a message to the axi clock domain with the virtual channels and timestamps.
            -- This is used to generate the addresses on the aw bus for the packet just received.
            if i_sof = '1' then
                firstInFrame <= '1';
            elsif i_dataValid = '0' and dataValid = '1' then
                firstInFrame <= '0';
            end if;
            
            maxVirtualChannel <= std_logic_vector(unsigned(FB_virtualChannels) - 1);
            
            if i_dataValid = '0' and dataValid = '1' then
                cdc_src_send <= '1';
                cdc_src_in(9 downto 0) <= virtualChannel(0)(9 downto 0); -- 4 virtual channels at a time, consecutive numbering starting from virtualChannel(0)
                cdc_src_in(46 downto 10) <= frameCount;      -- 37 bit corner turn frame (units of ~53ms) since SKA epoch
                cdc_src_in(56 downto 47) <= packetInFrame;   -- 10 bit count of the packet in the frame, 0 to 767
                cdc_src_in(57) <= firstInFrame;
                -- set last in frame, if this is the final packet.
                if ((unsigned(packetInFrame) = (unsigned(g_PACKETS_PER_FRAME) - 1)) and 
                    ((virtualChannel(0)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(1)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(2)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(3)(9 downto 0) = maxVirtualChannel(9 downto 0)))) then
                    cdc_src_in(58) <= '1';  -- last packet in the frame. 
                else
                    cdc_src_in(58) <= '0';
                end if;
                if (unsigned(virtualChannel(0)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(59) <= '1';
                else
                    cdc_src_in(59) <= '0';
                end if;
                if (unsigned(virtualChannel(1)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(60) <= '1';
                else
                    cdc_src_in(60) <= '0';
                end if;
                if (unsigned(virtualChannel(2)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(61) <= '1';
                else
                    cdc_src_in(61) <= '0';
                end if;
                if (unsigned(virtualChannel(3)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(62) <= '1';
                else
                    cdc_src_in(62) <= '0';
                end if;
            elsif cdc_src_rcv = '1' then
                cdc_src_send <= '0';
            end if;

        end if;
    end process;
    
    
    -- FWFT FIFO to cross the clock domain to the AXI clock, and to interface to the ready/valid on the m02_axi_wdata bus
    xpm_fifo_async_inst : xpm_fifo_async
    generic map (
        CDC_SYNC_STAGES => 2,       -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 512,    -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 10,  -- DECIMAL
        READ_DATA_WIDTH => 513,     -- DECIMAL; 512 data bits, plus one extra bit for last in a transaction.
        READ_MODE => "fwft",        -- String
        RELATED_CLOCKS => 0,        -- DECIMAL
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 513,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 10   -- DECIMAL
    ) port map (
        almost_empty => open,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,    -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,     -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,        -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => wdataFIFO_dout,   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => wdataFIFO_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.
        full => open,             -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full. 
        overflow => open,         -- 1-bit output: Overflow: 
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full: 
        rd_data_count => open,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => wdataFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => wdataFIFO_din,     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_clk => i_axi_clk,      -- 1-bit input: Read clock: Used for read operation. rd_clk must be a free running clock.
        rd_en => wdataFIFO_rdEn,  -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => wdataFIFO_rst,     -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',             -- 1-bit input: Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_FB_clk,       -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => wdataFIFO_wrEn   -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO. 
    );
    
    m02_axi_wvalid <= not wdataFIFO_empty;
    
    wdataFIFO_rdEn <= (not wdataFIFO_empty) and m02_axi_wready;
    m02_axi_wdata <= wdataFIFO_dout(511 downto 0);
    m02_axi_wlast <= wdataFIFO_dout(512);
    
    -- Notify the axi bus clock domain that a new packet is available.
    -- We need the virtual channels, and the packet count for the packet.
    xpm_cdc_handshake_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 4,    -- DECIMAL; range: 2-10
        WIDTH => 63          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => cdc_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => cdc_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => cdc_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been received.
        dest_ack => '1',          -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_axi_clk,    -- 1-bit input: Destination clock.
        src_clk => i_FB_clk,      -- 1-bit input: Source clock.
        src_in => cdc_src_in,     -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => cdc_src_send  -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );
    
    o_readInAllClocks <= readInAllClocks;
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            rstDel1 <= i_rst;
            rstDel2 <= rstDel1;
            rstDel3 <= rstDel2;
            
            cdc_dest_req_del1 <= cdc_dest_req;
            if cdc_dest_req_del1 = '1' and AWFirstInFrame = '1' then
                readInCount <= (others => '0');
                readInAllClocks <= readInCount;
            else
                readInCount <= std_logic_vector(unsigned(readInCount) + 1);
            end if;
            if cdc_dest_req_del1 = '1' and AWLastInFrame = '1' then
                o_readInClocks <= readInCount;
            end if;
            
            if rstDel1 = '1' then
                HBMWrBuffer <= '0';
                awFIFO_wrEn <= '0';
            elsif cdc_dest_req = '1' then
                -- This occurs at the end of each packet from the filterbanks,
                -- where a packet is a burst of 54 fine channels for 4 consecutive simultaneous virtual channels.
                -- Each packet needs a HBM aw request, for a 27x512bit = 1728 byte write to HBM.
                AWvirtualChannel(9 downto 0) <= cdc_dest_out(9 downto 0);    -- "V" in the address equation
                AWframeCount <= cdc_dest_out(46 downto 10);
                AWpacketInFrame <= cdc_dest_out(56 downto 47);   -- "T" in the equation below for the address
                AWFirstInFrame <= cdc_dest_out(57);
                AWLastInFrame <= cdc_dest_out(58);
                AWchannelValid <= cdc_dest_out(62 downto 59);

                if cdc_dest_out(57) = '1' then -- this is the first packet in a new frame (i.e. 53.08416 ms)
                    frameCount37 <= cdc_dest_out(46 downto 10); -- frame count used in the meta data at the output to the beamformer.
                    -- Update the HBM buffer to use.
                    HBMWrBuffer <= not HBMWrBuffer;
                end if;
                aw_fsm <= start;
            else
                case aw_fsm is
                    when start =>
                        -- Generate write requests; Single burst of 27 beats
                        --    27 beats = all 54 fine channels, 4 virtual channels
                        -- The write address is 
                        --    Byte Address = T*(1024/4)*2048 + floor(V/4)*2048 + F*32 + 8*mod(V,4)
                        -- Fine channel (F) = 0, so 
                        --    Address(18 downto 11) = virtual channel/4 (8 bit value, 0 to 255)
                        --    Address(28 downto 19) = Time sample (10 bit value, 0 to 767)
                        --    Address(29) = buffer select (each buffer is 0.5GBytes, total 1 GByte)
                        aw_fsm <= set_aw;
                        awFIFO_wrEn <= '0';
                        AWchan <= "00";
                        fifoIn_m02_axi_awaddr(29) <= HBMWrBuffer;
                        if (AWFirstInFrame = '1' and HBMWrBuffer = '0') then
                            HBMBuf0PacketCount <= AWFrameCount(36 downto 0);
                        end if;
                        if (AWFirstInFrame = '1' and HBMWrBuffer = '1') then
                            HBMBuf1PacketCount <= AWFrameCount(36 downto 0);
                        end if;
                        
                    when set_aw => --
                        fifoIn_m02_axi_awaddr(28 downto 19) <= AWpacketInFrame(9 downto 0);
                        fifoIn_m02_axi_awaddr(18 downto 11) <= AWvirtualChannel(9 downto 2);  -- 
                        awFIFO_wrEn <= '1';
                        aw_fsm <= wait_fifo;
                        
                    when wait_fifo =>
                        awFIFO_wrEn <= '0';
                        if (unsigned(awFIFO_wrDataCount) < 24) then
                            -- There is space for 512/27 ~= 19 packets in the wdata FIFO,
                            -- so it will fill before the awFIFO fills. So no need to worry about what 
                            -- happens if there is overflow in the awFIFO.
                            aw_fsm <= done;
                        end if;
                        
                    when done =>
                        awFIFO_wrEn <= '0';
                        aw_fsm <= done;
                    
                    when others =>
                        awFIFO_wrEn <= '0';
                        aw_fsm <= done;
                end case;
            end if;
            
        end if;
    end process;
    
    o_HBMBuf0PacketCount <= HBMBuf0PacketCount;
    o_HBMBuf1PacketCount <= HBMBuf1PacketCount;
    
    awFIFO_din(29 downto 11) <= fifoIn_m02_axi_awaddr(29 downto 11);
    awFIFO_din(10 downto 0) <= "00000000000";  -- The write address is always aligned to 2K.
    
    -- aw transactions go into a FIFO
    xpm_awfifo_sync_inst : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 30,      -- DECIMAL
        READ_MODE => "fwft",        -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 30,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    ) port map (
        almost_empty => open,
        almost_full => open,
        data_valid => open,      -- 1-bit output: Read Data Valid
        dbiterr => open,         -- 1-bit output: Double Bit Error: 
        dout => awFIFO_dout,     -- READ_DATA_WIDTH-bit output: Read Data.
        empty => awFIFO_empty,   -- 1-bit output: Empty Flag:
        full => open,            -- 1-bit output: Full Flag: 
        overflow => open,        -- 1-bit output: Overflow: 
        prog_empty => open,      -- 1-bit output: Programmable Empty.
        prog_full => open,       -- 1-bit output: Programmable Full.
        rd_data_count => open,   -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count.
        rd_rst_busy => open,     -- 1-bit output: Read Reset Busy.
        sbiterr => open,         -- 1-bit output: Single Bit Error.
        underflow => open,       -- 1-bit output: Underflow.
        wr_ack => open,          -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => awFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,     -- 1-bit output: Write Reset Busy.
        din => awFIFO_din,       -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',    -- 1-bit input: Double Bit Error Injection: Injects a double bit error if ecc is enabled.
        injectsbiterr => '0',    -- 1-bit input: Single Bit Error Injection.
        rd_en => awFIFO_rdEn,    -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO. 
        rst => awFIFO_rst,       -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',            -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_axi_clk,     -- 1-bit input: Write clock: Used for write operation.
        wr_en => awFIFO_wrEn     -- 1-bit input: Write Enable
    );
    
    m02_axi_awvalid  <= not awFIFO_empty;
    awFIFO_rdEn <= (not awFIFO_empty) and m02_axi_awready;
    m02_axi_awaddr <= awFIFO_dout(29 downto 0);
    m02_axi_awlen <= "00011010";  -- All writes are 27 beats.
    
    ---------------------------------------------------------------------------------------------------
    -- Read side
    -- Read data in the correct order and send on 128 bit words to the beamformer
    -- 
    -- Tasks :
    --   - Generate the m02_axi_ar* bus (m02_axi_arvalid, m02_axi_arready, m02_axi_araddr, m02_axi_arlen)
    --   - Buffer data as it comes back from the HBM
    --   - put it out to the beamformer on a 128 bit bus.
    --
    --  Data coming back from the HBM goes to a 512 deep by 512 bit wide FIFO.
    --
    -- Read Order
    -- ----------
    -- Reminder : 
    --    - 54 fine channels, 4 virtual channels per packet -> 27 x 512 bit words in the HBM, in consecutive memory locations.
    --    - HBM byte Address = T*(1024/4)*2048 + floor(V/4)*2048 + F*32 + 8*mod(V,4)
    --                   where F = Fine Channel  (0 to 215)
    --                         V = virtual channel (0 to up to 1023)
    --                         T = Time within the block (0 to 255, assuming the first stage corner turn is 24 LFAA blocks) 
    --
    -- Processing for the beamformer needs data for all stations for each virtual channel.
    -- Reads are in blocks of 1728 bytes, corresponding to a full coarse channel for 4 stations.
    --
    -- The data output order is:
    --
    --    For timeGroup = 0:48                         -- 53 ms corner turn, 48*16=768 total time samples
    --       For Coarse = 0:(i_coarse-1)               -- For 512 stations, 2 coarse, this is 0:1
    --          For Time = timeGroup * 16 + (0:16)     -- 32 times needed for an output packet
    --             For Station_block = 0:(i_stations/4)
    --                *Read 1728 bytes
    --                For fine_offset = 0:54              -- Total of 54 fine channels per SPS channel
    --                   For station = 0:4
    --                      Send a 128 bit word (i.e. 1 fine channel x 2 stations x (2 pol) x (16+16 bit complex data)).    
    --
    --  So first few reads for the 512 stations, two channels case is : 
    --    TimeGroup 0, Time = 0, station 0:3, coarse 0, 27 HBM words (=54 fine channels)
    --    TimeGroup 0, Time = 0, station 4:7, coarse 0, " 
    --    ...
    --    TimeGroup 0, Time = 0, station 508:511, coarse 0, "
    --    TimeGroup 0, Time = 1, station 0:3, coarse 0,
    --    ...
    --    TimeGroup 0, Time = 1, station 508:511, coarse 0, words 0-5
    --    ...
    ---------------------------------------------------------------------------------------------------
    
    ARvirtualChannel <= std_logic_vector(unsigned(rdStation) + unsigned(coarse_x_stations(9 downto 0)));
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
        
            stationsMinusOne <= std_logic_vector(unsigned(i_stations) - 1);
            coarseMinusOne <= std_logic_vector(unsigned(i_coarse) - 1);
            packetsPerFrameMinusOne <= std_logic_vector(unsigned(g_PACKETS_PER_FRAME) - 1);
            totalStations <= i_stations;
            coarse_x_stations <= unsigned(rdCoarse) * unsigned(totalStations);  -- 9 bit value x 11 bit value = 20 bit result.
            
            -- some tests for the loops registered here to ensure easy timing:
            if (rdStation(9 downto 2) = stationsMinusOne(9 downto 2)) then
                -- rdStation is the last group of 4 stations
                lastStation <= '1';
            else
                lastStation <= '0';
            end if;
            if rdCoarse = coarseMinusOne(8 downto 0) then
                lastCoarse <= '1';
            else
                lastCoarse <= '0';
            end if;
            if rdTime(3 downto 0) = "1111" then
                lastTimeIn16 <= '1';
            else
                lastTimeIn16 <= '0';
            end if;
            if (rdTime = packetsPerFrameMinusOne(9 downto 0)) then
                lastTime <= '1';
            else
                lastTime <= '0';
            end if;
            
            if cdc_dest_req = '1' and cdc_dest_out(58) = '1' then -- this is the last packet in the frame
                bad_polynomials <= i_bad_polynomials;
            end if;
            
            if cdc_dest_req = '1' and cdc_dest_out(58) = '1' then -- this is the last packet in the frame
                HBMRdBuffer <= HBMWrBuffer;   -- HBMRdBuffer is the top bit of the HBM address (bit 29)
                rdFrameCount <= frameCount37;
                rd_fsm <= start;
                
                rdTime <= (others => '0');     --outermost loop, run from 0 to (i_packetsPerFrame-1)
                rdCoarse <= (others => '0');   --    next level loop, runs from 0 to (i_coarse-1)
                rdStation <= (others => '0');  --            innermost loop, runs from 0 to (i_stations-1)
            else
                case rd_fsm is
                    when start => 
                        m02_axi_arvalid <= '0';
                        rd_fsm <= set_start;
                    
                    when set_start =>
                        -- tell the next level up that we are about to start reading out a new corner turn frame
                        m02_axi_arvalid <= '0';
                        rd_fsm <= wait_readout_start;
                        holdoff_count <= "11111111";
                    
                    when wait_readout_start =>
                        -- Delay a while so the polynomials are ready
                        if unsigned(holdoff_count) > 0 then
                            holdoff_count <= std_logic_vector(unsigned(holdoff_count) - 1);
                        else
                            rd_fsm <= get_addr;
                        end if;
                    
                    when get_addr =>
                        -- Byte Address = T*(1024/4)*2048 + floor(V/4)*2048 + F*32 + 8*mod(V,4)
                        -- Fine channel (F) = 0, so 
                        --    Address(18 downto 11) = virtual channel/4 (8 bit value, 0 to 255)
                        --    Address(28 downto 19) = Time sample (10 bit value, 0 to 767)
                        --    Address(29) = buffer select (each buffer is 0.5GBytes, total 1 GByte)
                        m02_axi_ARaddrInt(29) <= HBMRdBuffer;
                        m02_axi_ARaddrInt(28 downto 19) <= rdTime(9 downto 0); 
                        m02_axi_ARaddrInt(18 downto 11) <= ARvirtualChannel(9 downto 2); -- virtual channel = rdStation + i_stations*rdCoarse;
                        m02_axi_ARaddrInt(10 downto 0) <= "00000000000";  -- Reads always start on a 2kByte boundary.
                        rd_fsm <= send_addr;
                    
                    when send_addr =>
                        rd_fsm <= wait_ready;
                        m02_axi_arvalid <= '1';
                    
                    when wait_ready => 
                        if m02_axi_arready = '1' then
                            rd_fsm <= wait_calc0;
                            m02_axi_arvalid <= '0';
                        end if;
                    
                    when wait_calc0 => -- few clocks to wait for the fifoSpacedUsed calculation to complete before it is used in "check_space"
                        rd_fsm <= wait_calc1;
                        m02_axi_arvalid <= '0';
                        
                    when wait_calc1 => 
                        rd_fsm <= update_stationTimeCoarse;
                        m02_axi_arvalid <= '0';
                        
                    when update_stationTimeCoarse =>
                        -- For timeGroup = 0:48                         -- 53 ms corner turn, 48*16=768 total time samples
                        --    For Coarse = 0:(i_coarse-1)               -- For 512 stations, 2 coarse, this is 0:1
                        --       For Time = timeGroup * 16 + (0:16)     -- 32 times needed for an output packet
                        --          For Station_block = 0:(i_stations/4)
                        --             *Read 1728 bytes
                        m02_axi_arvalid <= '0';
                        if lastStation = '1' then
                            rdStation <= (others => '0');
                            if lastTimeIn16 = '1' then
                                if lastCoarse = '1' then
                                    rdCoarse <= "000000000";
                                    if lastTime = '1' then
                                        rdTime <= "0000000000";
                                        rd_fsm <= done;
                                    else
                                        rdTime <= std_logic_vector(unsigned(rdTime) + 1);
                                        rd_fsm <= check_space;
                                    end if;
                                else
                                    rdCoarse <= std_logic_vector(unsigned(rdCoarse) + 1);
                                    rdTime(3 downto 0) <= "0000";
                                    rd_fsm <= check_space;
                                end if;
                            else
                                rdTime <= std_logic_vector(unsigned(rdTime) + 1);
                                rd_fsm <= check_space;
                            end if;
                        else
                            rdStation <= std_logic_vector(unsigned(rdStation) + 4);
                            rd_fsm <= check_space;
                        end if;
                    
                    when check_space => 
                        m02_axi_arvalid <= '0';
                        -- Make sure we have space in the output buffer for any packets that come back.
                        if ((unsigned(fifoSpaceUsed) < 480) and (unsigned(outstandingRequests) < 60)) then
                            rd_fsm <= get_addr;  -- Note: 480 and 60 are arbitrarily chosen to be safe values; should still work with 506 and 63.
                        end if;
                        
                    when done =>
                        m02_axi_arvalid <= '0';
                        rd_fsm <= done;
                    
                    when others =>
                        rd_fsm <= done;
                end case;
            end if;
            
            -- 27 beats per request
            outstandingRequests_x27 <= std_logic_vector(unsigned(outstandingRequests_x32) - unsigned(outstandingRequests_x4) - unsigned(outstandingRequests_x1));
            fifoSpaceUsed <= std_logic_vector(unsigned(outstandingRequests_x27) + unsigned(rdataFIFO_wrDataCount));
            
            if rd_fsm = start then
                outstandingRequests <= (others => '0');
            elsif newRequest = '1' and requestDone = '0' then
                outstandingRequests <= std_logic_vector(unsigned(outstandingRequests) + 1);
            elsif newRequest = '0' and requestDone = '1' then
                outstandingRequests <= std_logic_vector(unsigned(outstandingRequests) - 1);
            end if;
            
            if rd_fsm = send_addr then
                newRequest <= '1';
            else
                newRequest <= '0';
            end if;
            if m02_axi_rvalid = '1' and m02_axi_rlast = '1' then
                requestDone <= '1';
            else
                requestDone <= '0';
            end if;
            
            if (cdc_dest_req = '1' and cdc_dest_out(58) = '1' and (rd_fsm /= done or unsigned(outstandingRequests) /= 0)) then
                o_readoutError <= '1';
            else
                o_readoutError <= '0';
            end if;
            
            if rd_fsm = set_start then
                readout_start_int <= '1';
                o_readout_frame <= rdFrameCount;
            else
                readout_start_int <= '0';
            end if;
            o_readout_start <= readout_start_int;
            
        end if;
    end process;
    
    m02_axi_ARlen <= "00011010"; -- always read 27 beats = 1728 bytes
    m02_axi_ARaddr <= m02_axi_ARaddrInt;
    
    outstandingRequests_x32 <= outstandingRequests & "00000";
    outstandingRequests_x4 <= "000" & outstandingRequests &  "00";
    outstandingRequests_x1 <= "00000" & outstandingRequests;
    
    xpm_fifo_rdata_inst : xpm_fifo_async
    generic map (
        CDC_SYNC_STAGES => 2,       -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 2,     -- DECIMAL
        FIFO_WRITE_DEPTH => 512,    -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 10,  -- DECIMAL
        READ_DATA_WIDTH => 512,     -- DECIMAL  -- 512 data bits.
        READ_MODE => "std",         -- String
        RELATED_CLOCKS => 0,        -- DECIMAL
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 512,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 10   -- DECIMAL
    ) port map (
        almost_empty => open,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,    -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,     -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,        -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => rdataFIFO_dout,   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => rdataFIFO_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.
        full => open,             -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full. 
        overflow => open,         -- 1-bit output: Overflow: 
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full: 
        rd_data_count => rdataFIFO_rdDataCount,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => rdataFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => m02_axi_rdata,     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_clk => i_BF_clk,      -- 1-bit input: Read clock: Used for read operation. rd_clk must be a free running clock.
        rd_en => rdataFIFO_rdEn,  -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => rdataFIFO_rst,     -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',             -- 1-bit input: Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_axi_clk,       -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => m02_axi_rvalid   -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO. 
    );
    
    m02_axi_rready <= '1'; -- we never make more requests than we can fit in the FIFO.
    
    --

    -- Get i_packetsPerFrame(9:0), i_stations(10:0) and i_coarse(9:0) into the i_BF_clk domain
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            if (rd_fsm = start) then
                axi2bf_src_send <= '1';
            elsif axi2bf_src_rcv = '1' then
                axi2bf_src_send <= '0';
            end if;
            axi2BF_dataIn(9 downto 0) <= (others => '0');
            axi2BF_dataIn(20 downto 10) <= i_stations;
            axi2BF_dataIn(30 downto 21) <= i_coarse;
            axi2BF_dataIn(31) <= '0';
            axi2BF_dataIn(68 downto 32) <= rdFrameCount;  -- 37 bit count of corner turn frames since the SKA epoch
            
            if BF2axi_dest_req = '1' then
                o_readoutClocks <= BF2axi_dest_out;
            end if;
            
        end if;
    end process;
    
    xpm_cdc_handshake_psc_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 69          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => axi2bf_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => axi2bf_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => axi2bf_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1', -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_BF_clk, -- 1-bit input: Destination clock.
        src_clk => i_axi_clk,   -- 1-bit input: Source clock.
        src_in => axi2BF_dataIn,  -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => axi2BF_src_send   -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );

    -- Get stats from the Beamform clock domain back into the axi clock domain for viewing in the register interface
    xpm_cdc_handshake_BF2axi_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 32          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => bf2axi_dest_out,  -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => bf2axi_dest_req,  -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => bf2axi_src_rcv,    -- 1-bit output: Acknowledgement from destination logic that src_in has been
                                      -- received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1',              -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_axi_clk,        -- 1-bit input: Destination clock.
        src_clk => i_BF_clk,          -- 1-bit input: Source clock.
        src_in => BF2axi_dataIn,       -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => BF2axi_src_send   -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );

    BF2axi_dataIn <= readoutClocks;
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
        
            BF2axi_transferCount <= std_logic_vector(unsigned(BF2axi_transferCount) + 1);
        
            if BF2axi_transferCount = "1111" then
                BF2axi_src_send <= '1';
            elsif bf2axi_src_rcv = '1' then
                BF2axi_src_send <= '0';
            end if;
            
            -- Get the number of stations, number of coarse channels and number of packets per frame from the clock domain crossing.
            -- Also, the clock crossing occurs once at the start of each corner turn frame (i.e. every 53ms),
            -- so is used to control the readout for a frame.
            if axi2bf_dest_req = '1' then
                BFstations <= axi2bf_dest_out(20 downto 10);
                BFcoarse <= axi2bf_dest_out(30 downto 21);
                BFFrameCount <= axi2bf_dest_out(68 downto 32);  -- Corner turn frame count since the SKA epoch
            end if;
            
            axi2bf_dest_reqDel1 <= axi2bf_dest_req;
            
            BFstationsMinusOne <= std_logic_vector(unsigned(BFstations) - 1);
            BFCoarseMinusOne <= std_logic_vector(unsigned(BFCoarse) - 1);
            BFpacketsPerFrameMinusOne <= std_logic_vector(unsigned(g_PACKETS_PER_FRAME) - 1);
            
            -- Get data out of the FIFO and pass it on.
            if axi2bf_dest_req = '1' then
                readout_fsm <= wait_poly_fifo;
                wait_poly_fifo_count <= "1111";
                outputFine <= "00000000";  -- This value is the current fine channel we are outputting divided by 3, so it runs 0-71
                outputStation <= "0000000000";
                outputCoarse <= "0000000000";
                outputTime <= "0000000000";  -- counts through packets in time; maximum is 256, assuming a 53 ms corner turn (24 SPS frames).
                outputPktOdd <= '0'; -- alternates between 0 and 1 as data goes to different output packets. Used downstream in the beamformers.
            else
                case readout_fsm is
                    when wait_start_fine =>
                        if gapCount_eq0 = '1' then
                            readout_fsm <= start_fine;
                        end if;
                    
                    when wait_poly_fifo =>
                        -- wait until there is polynomial data available in the FIFO.
                        if i_poly_fifo_empty = '0' and wait_poly_fifo_count = "0000" then
                            readout_fsm <= start_fine;
                        end if;
                        if wait_poly_fifo_count /= "0000" then
                            wait_poly_fifo_count <= std_logic_vector(unsigned(wait_poly_fifo_count) - 1);
                        end if;
                    
                    when start_fine =>  -- start of the read of a group of 216 fine channels (=1 coarse channel).
                        readout_fsm <= wait_fifo;
                    
                    when wait_fifo =>
                        if (rdataFIFO_empty = '0') then
                            readout_fsm <= p0;
                        end if;
                    
                    -- The 4 states (p0, p1, p2, p3) read 1 x 512 bit word from the FIFO and output 4 x 128 bit words to the beamformers.
                    -- That is data for 2 fine channels and 4 stations.
                    when p0 => -- 2 stations for even indexed fine channel
                        readout_fsm <= p1;
                    
                    when p1 => -- second 2 stations for even indexed fine channel
                        readout_fsm <= p2;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p2 => -- 2 stations for odd indexed fine channel
                        readout_fsm <= p3;
                    
                    when p3 => -- second 2 stations for odd indexed fine channel
                        if ((unsigned(outputFine) = 53) and (BFlastStation = '1') and 
                            (BFlastCoarse = '1') and (BFlastTime = '1')) then
                            readout_fsm <= done;
                        elsif ((unsigned(outputFine) = 53) and
                               (BFlastStation = '1') and
                               (BFlastTimeIn16 = '1')) then
                            readout_fsm <= wait_start_fine;
                        elsif (rdataFIFO_empty = '0') then
                            readout_fsm <= p0;
                        else
                            readout_fsm <= wait_fifo;
                        end if;
                        
                        -- counters keeping track of the loops are:
                        --   outputFine
                        --   outputStation
                        --   outputTime
                        --   outputCoarse
                        --
                        -- The data output order is:
                        --
                        --    For timeGroup = 0:48                         -- 53 ms corner turn, 48*16=768 total time samples
                        --       For Coarse = 0:(i_coarse-1)               -- For 512 stations, 2 coarse, this is 0:1
                        --          For Time = timeGroup * 16 + (0:16)     -- 32 times needed for an output packet
                        --             For Station_block = 0:(i_stations/4)
                        --                For fine_offset = 0:54              -- Total of 54 fine channels per SPS channel
                        --                   For station = 0:4
                        --                      Send a 128 bit word (i.e. 1 fine channels x 2 stations x (2 pol) x (16+16 bit complex data)).
                        --
                        if unsigned(outputFine) = 53 then
                            if BFlastStation = '1' then
                                outputStation <= "0000000000";
                                if BFlastTimeIn16 = '1' then
                                    outputFine <= "00000000";
                                    outputPktOdd <= not outputPktOdd;
                                    if BFlastCoarse = '1' then
                                        outputCoarse <= (others => '0');
                                        if BFlastTime = '1' then
                                            outputTime <= "0000000000";
                                        else
                                            outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                        end if;
                                    else
                                        outputCoarse <= std_logic_vector(unsigned(outputCoarse) + 1);
                                        outputTime <= std_logic_vector(unsigned(outputTime) - 15);
                                    end if;
                                else
                                    outputFine <= "00000000"; -- back to the first fine channel for the next time sample.
                                    outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                end if;
                            else
                                outputFine <= "00000000"; -- back to the first fine channel for the same time sample, next group of 4 stations.
                                outputStation <= std_logic_Vector(unsigned(outputStation) + 4);
                            end if;
                        else
                            outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                        end if;
                        
                    when done =>
                        readout_fsm <= done;
                        
                    when others =>
                        readout_fsm <= done;
                end case;
            end if;
            
            if readout_fsm_del1 = start_fine then  -- need to use del1 because in the first run through minGapDataIn is not valid until one clock after we are in the state start_fine.
                gapCount <= minGapDataIn; -- std_logic_vector(to_unsigned(minGap,20));
            elsif unsigned(gapCount) /= 0 then
                gapCount <= std_logic_vector(unsigned(gapCount) - 1);
            end if;
            
            if axi2bf_dest_req = '1' then
                clocksPerCornerTurnOut <= (others => '0');
            elsif readout_fsm /= done then
                clocksPerCornerTurnOut <= std_logic_vector(unsigned(clocksPerCornerTurnOut) + 1);
            end if;
            
            if readout_fsm = done and readout_fsm_del1 /= done then
                readoutClocks <= clocksPerCornerTurnOut;
            end if;
            
            if (unsigned(gapCount) = 0) then
                gapCount_eq0 <= '1';
            else
                gapCount_eq0 <= '0';
            end if;
            
            if outputStation(9 downto 2) = BFstationsMinusOne(9 downto 2) then
                BFlastStation <= '1';  -- last group of 4 stations
            else
                BFlastStation <= '0';
            end if;
            
            if unsigned(outputStation) = 0 then
                BFfirstStationDel1 <= '1';
            else
                BFfirstStationDel1 <= '0';
            end if; 
            
            if outputTime(3 downto 0) = "1111" then
                BFlastTimeIn16 <= '1';
            else
                BFlastTimeIn16 <= '0';
            end if;
            
            if (outputCoarse = BFcoarseMinusOne) then
                BFlastCoarse <= '1';
            else
                BFlastCoarse <= '0';
            end if;
            
            if (outputTime = BFpacketsPerFrameMinusOne) then
                BFlastTime <= '1';
            else
                BFlastTime <= '0';
            end if;
            
            
            readout_fsm_del1 <= readout_fsm;
            readout_fsm_del2 <= readout_fsm_del1;
            readout_fsm_del3 <= readout_fsm_del2;
            readout_fsm_del4 <= readout_fsm_del3;
            
            outputFineDel1 <= outputFine;
            outputFineDel2 <= outputFineDel1;
            outputFineDel3 <= outputFineDel2;
            outputFineDel4 <= outputFineDel3;
            
            outputStationDel1 <= outputStation;
            outputStationDel2 <= outputStationDel1;
            outputStationDel3 <= outputStationDel2;
            outputStationDel4 <= outputStationDel3;
            
            BFlastStationDel2 <= BFlastStation;  -- BFLastStation is already one clock behind outputStation
            BFlastStationDel3 <= BFlastStationDel2;
            BFlastStationDel4 <= BFlastStationDel3;
            
            BFfirstStationDel2 <= BFfirstStationDel1;
            BFfirstStationDel3 <= BFfirstStationDel2;
            BFfirstStationDel4 <= BFfirstStationDel3;
            
            outputCoarseDel1 <= outputCoarse;
            outputCoarseDel2 <= outputCoarseDel1;
            outputCoarseDel3 <= outputCoarseDel2;
            outputCoarseDel4 <= outputCoarseDel3;
            
            outputPktOddDel1 <= outputPktOdd;
            outputPktOddDel2 <= outputPktOddDel1;
            outputPktOddDel3 <= outputPktOddDel2;
            outputPktOddDel4 <= outputPktOddDel3;
            
            -- The output packet count is a count of the PSS packets since the SKA epoch.
            -- There are 16 time samples in each output PST packet, 
            -- and 768 time samples in each corner turn frame.
            -- 768/16 = 48 PSS frames per corner turn frame.
            -- So output packet count = 48 * BFFrameCount + outputTime(9 downto 4)
            packetCountDel1 <= outputTime(9 downto 4);
            packetCountDel2 <= packetCountDel1;
            packetCountDel3 <= "000000" & x"00000000" & packetCountDel2;
            BFframeCount_x16 <= "000" & BFFrameCount & "0000";
            BFframeCount_x32 <= "00" & BFFrameCount & "00000";
            packetCountDel4 <= std_logic_vector(unsigned(BFFrameCount_x16) + unsigned(BFFrameCount_x32) + unsigned(packetCountDel3)); -- 44 bit result
            
            outputTimeDel1 <= outputTime(9 downto 0);
            outputTimeDel2 <= outputTimeDel1;
            outputTimeDel3 <= outputTimeDel2;
            outputTimeDel4 <= outputTimeDel3;
            
            if readout_fsm = p0 then
                rdataFIFO_rdEn <= '1';
            else
                rdataFIFO_rdEn <= '0';
            end if;
            
            -- output register; converts 512 bit words from the FIFO to 128 bit words on the output bus
            if readout_fsm_del3 = p0 then  -- del3 to account for the fifo read latency.
                outputBuffer(511 downto 0) <= rdataFIFO_dout;
            elsif readout_fsm_del3 = p1 or readout_fsm_del3 = p2 or readout_fsm_del3 = p3 then
                outputBuffer(383 downto 0) <= outputBuffer(511 downto 128);
            end if;
            
            outputCoarse_x_stations <= unsigned(outputCoarse) * unsigned(BFstations);  -- 9 bit value x 11 bit value = 20 bit result.
            outputVirtualChannelDel2 <= std_logic_vector(unsigned(outputStationDel1) + unsigned(outputCoarse_x_stations(9 downto 0)));
            outputVirtualChannelDel3 <= outputVirtualChannelDel2;
            outputVirtualChannelDel4 <= outputVirtualChannelDel3;
            
            -- Actual outputs
            outputBufferDel1 <= outputBuffer(127 downto 0);
            outputBufferDel2 <= outputBufferDel1;
            outputBufferDel3 <= outputBufferDel2;
            outputBufferDel4 <= outputBufferDel3;
            --outputBufferDel5 <= outputBufferDel4;
            --outputBufferDel6 <= outputBufferDel5;
            --outputBufferDel7 <= outputBufferDel6;
            --outputBufferDel8 <= outputBufferDel7;
            o_data <= outputBufferDel4;  -- o_data has a 4 clock latency relative to the other beamformer outputs (o_valid, o_fine etc.). This is required by the beamformer. 
            
            for i in 0 to 7 loop
                if outputBufferDel3(i*16+15 downto i*16) = "1000000000000000" then
                    sample_flagged(i) <= '1';
                else
                    sample_flagged(i) <= '0';
                end if;
            end loop;
            
            if sample_flagged(3 downto 0) = "0000" then
                o_flagged(0) <= '0';  -- "o_flagged" aligns with o_data
            else
                o_flagged(0) <= '1';
            end if;
            if sample_flagged(7 downto 4) = "0000" then
                o_flagged(1) <= '0';
            else
                o_flagged(1) <= '1';
            end if;
            
            if (readout_fsm_del4 = p0 or readout_fsm_del4 = p1 or readout_fsm_del4 = p2 or readout_fsm_del4 = p3) then
                valid_int <= '1';
            else
                valid_int <= '0';
            end if;
            
            if readout_fsm_del4 = p0 or readout_fsm_del4 = p2 then
                o_stations01 <= '1';
            else
                o_stations01 <= '0';
            end if;
            
            o_fine <= outputFineDel4;        -- 8 bits
            o_coarse <= outputCoarseDel4;
            o_firstStation <= BFfirstStationDel4;
            o_lastStation  <= BFlastStationDel4;
            o_timeStep <= outputTimeDel4(3 downto 0);
            o_station <= outputStationDel4;  -- 10 bits
            o_packetCount <= x"0" & packetCountDel4;
            o_outputPktOdd <= outputPktOddDel4;
            o_virtualChannel <= outputVirtualChannelDel4;
            
            beams_enabled <= i_beams_enabled;
            if i_beams_enabled(1 downto 0) = "00" then
                beams_enabled_ceil_div4 <= beams_enabled_div4;
            else
                beams_enabled_ceil_div4 <= std_logic_vector(unsigned(beams_enabled_div4) + 1);
            end if;
            -- Each beamformer instance requires c_minGapData clocks to output a packet. 
            -- There are 4 parallel pipelines, so the worst case number of packets to be output
            --  = ceil(beams_enabled/4)
            minGapDataIn0 <= std_logic_vector(to_unsigned(g_PACKET_GAP,16) * unsigned(beams_enabled_ceil_div4));
            minGapDataIn <= std_logic_vector(unsigned(minGapDataIn0) + 128);  -- add a bit extra to ensure no clashes due to pipeline delays in the beamforming.
            
            -- Reconstruct the data we expect to see if in the test mode, where the data was replaced with meta data.
            outputFineDel4x2 <= outputFineDel3(6 downto 0) & '0';
            
            metaRecon(7 downto 0) <= std_logic_vector(unsigned(outputFineDel4) + unsigned(outputFineDel4x2));  -- outputFineDel4 should align with the data in outputBuffer(31:0)
            metaRecon(19 downto 8) <= "00" & outputTimeDel4;
            metaRecon(31 downto 20) <= "00" & outputVirtualChannelDel4;
            
            if ((metaRecon /= outputBufferDel1(31 downto 0)) and valid_int = '1') then
                dataMismatchInt <= '1';
            else
                dataMisMatchInt <= '0';
            end if;
            o_dataMismatchBFclk <= dataMisMatchInt;
        end if;
    end process;
    
    o_valid <= valid_int;
    
    beams_enabled_div4 <= beams_enabled(9 downto 2);
    
    -- convert dataMisMatch to the i_axi_clk clock domain.
    xpm_cdc_pulsedm_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 0,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    ) port map (
        dest_pulse => o_dataMisMatch, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,  -- 1-bit input: Destination clock.
        dest_rst => '0',        -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_BF_clk,    -- 1-bit input: Source clock.
        src_pulse => dataMismatchInt, -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'          -- 1-bit input: optional; required when RST_USED = 1
    );
    
    
    xpm_cdc_single_bp_i : xpm_cdc_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    ) port map (
        dest_out => bad_polynomials_BF_clk, -- 1-bit output: src_in synchronized to the destination clock domain.
        dest_clk => i_BF_clk,          -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => i_axi_clk,         -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => bad_polynomials    -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );
    
    o_good_polynomials <= not bad_polynomials_BF_clk;
    
end Behavioral;
